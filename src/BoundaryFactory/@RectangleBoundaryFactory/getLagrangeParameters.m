 %=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
%   Boundary sections:
%             
%   _____3_____ 
%  |           |
%  |           |         
%  4           2  
%  |           | 
%  |_____1_____|
% 
% Get Lagrange parameter u  (range: [-1,1]) at all 4 boundary edges

function [u,currentSection,TotalNumberOfBoundarySections] =getLagrangeParameters(obj,GlobalCoordinate)


             TotalNumberOfBoundarySections=4;
             
             %compute Lagrange parameters and current section:
             PositionVector=(GlobalCoordinate-obj.origin)'; %the position vector with respect to the origin of the rectangle, NOT the global position vector
             
             VectorComponentMatrix=[[obj.lengths(1) 0 0]',[0 obj.lengths(2) 0]']; 
             
             VectorComponentWeight=VectorComponentMatrix\PositionVector; % the weight of the vectors in both directions , for the current position (range: [0,1])
             
             if abs(VectorComponentWeight(2))<1E-12  % GlobalCoordinate lies in section 1
               u= -1+2*VectorComponentWeight(1); % local Lagrange parameter u
               currentSection=1;
             elseif abs(VectorComponentWeight(1)-1.0)<1E-12 % GlobalCoordinate lies in section 2
               u= -1+2*VectorComponentWeight(2);
               currentSection=2;
             elseif abs(VectorComponentWeight(2)-1.0)<1E-12% GlobalCoordinate lies in section 3
               u= -1+2*VectorComponentWeight(1);
               currentSection=3;
             elseif abs(VectorComponentWeight(1))<1E-12 % GlobalCoordinate lies in section 4
               u= -1+2*VectorComponentWeight(2);
               currentSection=4;
             else
               u=[];
               currentSection=[];    
             end
             
      
end