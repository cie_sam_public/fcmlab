%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Parallelepiped Boundary Factory
%(defines the AREA of all 6 FACES of a parallelepiped spanned by three vectors Xi, Eta and Zeta)
%
%         Eta =[X2-X0 Y2-Y0 Z2-Z0]   
%                  ^
%                  |
%                  |      
%                  |    ^ Zeta =[X3-X0 Y3-Y0 Z3-Z0] 
%                  |   /
%                  |  /
%                  | /
%                  |/
% origin [X0 Y0 Z0] - - - - - - - >Xi =[X1-X0 Y1-Y0 Z1-Z0] 
% Boundary parameters:
%      ____________
%     /| beta     /|
%    / | /  ^    / |
%   /  |/__/    /  |
%  /___|_______/  ^|   Lagrange parameters u and v here depend on 3 angles alpha, beta and gamma !
%  |   |_______|| |gamma
%  |   / alpha ||/ /
%  |  / |  ^   |  /
%  | /  |__|   | /
%  |/__________|/


classdef ParallelepipedBoundaryFactoryWithAngleParameter < ParallelepipedBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = ParallelepipedBoundaryFactoryWithAngleParameter(origin, vectorXi,vectorEta, vectorZeta,numberOfSegmentsXi,numberOfSegmentsEta,numberOfSegmentsZeta )
            obj = obj@ParallelepipedBoundaryFactory(origin, vectorXi,vectorEta, vectorZeta,numberOfSegmentsXi,numberOfSegmentsEta,numberOfSegmentsZeta);
        end
 %% overloaded Lagrange parameter functions:
 
        function  [u,currentSection,TotalNumberOfBoundarySections] =getLagrangeParameters(obj,GlobalCoordinate)
             
             %only one section for the boundary:
             TotalNumberOfBoundarySections=1;
             currentSection=1;
             
             %% compute Lagrange parameters:
             center=0.5*obj.vectorXi+0.5*obj.vectorEta+0.5*obj.vectorZeta;
             PositionVector=(GlobalCoordinate-center-obj.origin)'; %the position vector with respect to the center of the parallelepiped
             
             % Get the projection onto the planes Xi-Eta, Xi-Zeta and Eta-Zeta
             % of the vector which points form the origin of the
             % parallelepiped to the CENTER of the parallelepiped:
             centerXiEta=(obj.vectorXi*center')/(obj.vectorXi*obj.vectorXi')*obj.vectorXi+(obj.vectorEta*center')/(obj.vectorEta*obj.vectorEta')*obj.vectorEta;
             centerXiZeta=(obj.vectorXi*center')/(obj.vectorXi*obj.vectorXi')*obj.vectorXi+(obj.vectorZeta*center')/(obj.vectorZeta*obj.vectorZeta')*obj.vectorZeta;
             centerEtaZeta=(obj.vectorEta*center')/(obj.vectorEta*obj.vectorEta')*obj.vectorEta+(obj.vectorZeta*center')/(obj.vectorZeta*obj.vectorZeta')*obj.vectorZeta;

             % Get the projection onto the planes Xi-Eta, Xi-Zeta and Eta-Zeta
             % of the POSITION vector:
             projectionXiEta=(obj.vectorXi*PositionVector)/(obj.vectorXi*obj.vectorXi')*obj.vectorXi+(obj.vectorEta*PositionVector)/(obj.vectorEta*obj.vectorEta')*obj.vectorEta;
             projectionXiZeta=(obj.vectorXi*PositionVector)/(obj.vectorXi*obj.vectorXi')*obj.vectorXi+(obj.vectorZeta*PositionVector)/(obj.vectorZeta*obj.vectorZeta')*obj.vectorZeta;
             projectionEtaZeta=(obj.vectorEta*PositionVector)/(obj.vectorEta*obj.vectorEta')*obj.vectorEta+(obj.vectorZeta*PositionVector)/(obj.vectorZeta*obj.vectorZeta')*obj.vectorZeta;

             % Compute the first angle:
              if (cross(-centerXiEta,projectionXiEta)*cross(obj.vectorXi,obj.vectorEta)')>=0
                 alpha=acos(-centerXiEta*projectionXiEta'/(norm(centerXiEta)*norm(projectionXiEta)));
             else
                 alpha=2*pi-acos(-centerXiEta*projectionXiEta'/(norm(centerXiEta)*norm(projectionXiEta)));

              end
              
              % Compute the second angle:
             if (cross(-centerXiZeta,projectionXiZeta)*cross(obj.vectorXi,obj.vectorZeta)')>=0
                 beta=acos(-centerXiZeta*projectionXiZeta'/(norm(centerXiZeta)*norm(projectionXiZeta)));
             else
                 beta=2*pi-acos(-centerXiZeta*projectionXiZeta'/(norm(centerXiZeta)*norm(projectionXiZeta)));

             end
             
               % Compute the third angle 
             if (cross(-centerEtaZeta,projectionEtaZeta)*cross(obj.vectorEta,obj.vectorZeta)')>=0
                 gamma=acos(-centerEtaZeta*projectionEtaZeta'/(norm(centerEtaZeta)*norm(projectionEtaZeta)));
             else
                 gamma=2*pi-acos(-centerEtaZeta*projectionEtaZeta'/(norm(centerEtaZeta)*norm(projectionEtaZeta)));
             end
             
              % Compute u and v out of the angles (do not use angles corresponding to smallest radius):
              
              [minimum,I]=min([norm(projectionXiEta),norm(projectionXiZeta),norm(projectionEtaZeta)]);
             
              switch I
                  case 1
                      u(1)=-1.0+2.0*gamma/(2*pi);
                      u(2)=-1.0+2.0*beta/(2*pi);     
                  case 2
                      u(1)=-1.0+2.0*alpha/(2*pi);
                      u(2)=-1.0+2.0*gamma/(2*pi);        
                  case 3                      
                      u(1)=-1.0+2.0*alpha/(2*pi);
                      u(2)=-1.0+2.0*beta/(2*pi);
               end
        end
  
       
    end
    
    properties (Access = private)
    end
    
end