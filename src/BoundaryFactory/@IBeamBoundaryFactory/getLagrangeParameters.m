 %=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
%    -----------    
%   |    sec3   |
%    - - - - - - 
%       |    | 
%       |sec2|   
%    -----------    
%   |    sec1   |
%    - - - - - - 
%
%  ^z
%  | 
%  |
%   ---->x
% Get Lagrange parameters u and v (range: [-1,1]) for every section 

function [u,currentSection,TotalNumberOfBoundarySections] =getLagrangeParameters(obj,GlobalCoordinate)
            
             TotalNumberOfBoundarySections=3;
             
             %compute Lagrange parameters and current section:
             PositionVector=(GlobalCoordinate)'; 
             
             if abs(PositionVector(3))<=obj.Thick1  % section 1
               u(1)= -1+2*PositionVector(1)/obj.Width; % local Lagrange parameter u
               u(2)= -1+2*PositionVector(3)/obj.Thick1; % local Lagrange parameter v
               currentSection=1;
             elseif abs(PositionVector(3))>obj.Thick1 && abs(PositionVector(3))<obj.Height-obj.Thick1 % section 2
               u(1)= -1+2*(PositionVector(1)-(obj.Width-obj.Thick2)/2)/(obj.Thick2); % local Lagrange parameter u
               u(2)= -1+2*(PositionVector(3)-obj.Thick1)/(obj.Height-2*obj.Thick1); % local Lagrange parameter v
               currentSection=2;
             elseif abs(PositionVector(3))>=obj.Height-obj.Thick1 % section 3
               u(1)= -1+2*PositionVector(1)/obj.Width; % local Lagrange parameter u
               u(2)= -1+2*(PositionVector(3)+obj.Thick1-obj.Height)/(obj.Thick1); % local Lagrange parameter v
               currentSection=3;
             else
               u=[];
               currentSection=[];
             end
             
end