%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Straight Line Boundary Factory
% boundary segments are not crossing element-borders


classdef StraightLineBoundaryFactoryNonCrossing < StraightLineBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = StraightLineBoundaryFactoryNonCrossing( startPoint, endPoint, numberOfSegments, MeshFactory )
            obj = obj@StraightLineBoundaryFactory(startPoint, endPoint, numberOfSegments);
            obj.IntersectionCoefficients = obj.calcIntersections(MeshFactory);
        end
        
        %%
        function Lines = getBoundary(obj)
            
            % outer points
            for i=1:length(obj.IntersectionCoefficients)
                    Vertices(i) = Vertex(obj.startPoint +  obj.IntersectionCoefficients(i) * (obj.endPoint-obj.startPoint)); % insert an additional vertex for the boundary segmentation at the intersection point
            end
            
            % construct Lines
            Lines = [];
            for i = 1 : length(Vertices)-1
                Lines =[Lines Line(Vertices(i),Vertices(i+1))];
            end
        end
    end
    
     %%
    methods (Access = private)
        function IntersectionCoefficients = calcIntersections(obj,MeshFactory)% calculate intersection points of mesh edges with the boundary
            Tolerance=1E-5;                                                   
            MeshNodes = MeshFactory.createNodes;
            MeshEdges= MeshFactory.createEdges(MeshNodes);
            SegmentVector=obj.endPoint-obj.startPoint;
            if ~isa(MeshEdges(1).getgeometricSupport,'Line') 
                Logger.ThrowException(['Mesh Edges are not Lines']);
            end
            k=1;
            IntersectionCoefficients=[];                                             
            for i=1:length(MeshEdges)
                EdgeNodes=MeshEdges(i).getNodes; %get nodes of the edge
                EdgeVector=EdgeNodes(2).getCoords-EdgeNodes(1).getCoords;
                LinearDependencyMatrix=[SegmentVector;EdgeNodes(1).getCoords-obj.startPoint;EdgeNodes(2).getCoords-obj.startPoint];
                
                if rank(LinearDependencyMatrix,Tolerance)==2 && abs(abs(SegmentVector*EdgeVector'/(norm(EdgeVector)*norm(SegmentVector)))-1)>Tolerance % exclude edges which are not coplanar OR parallel
                    A=[-SegmentVector' EdgeVector'];
                    b=[obj.startPoint-EdgeNodes(1).getCoords]';
                    [R,jb]=rref(A'); A=A(jb,:); b=b(jb); % strike row jb out of the linear system of equations (linear dependent row)
                    x=A\b; %compute the intersection point with the edge
                    if x(2)<1+Tolerance && x(2)>-Tolerance && x(1)<1-Tolerance && x(1)>Tolerance && mod(x(1),1/obj.numberOfSegments)>Tolerance % continue if the intersection point lies within    
                        IntersectionCoefficients(k)=x(1);                                                                                      % the line but NOT at point which is already a border of two boundary segments
                        k=k+1;  
                    end
                end
            end
            IntersectionCoefficients=[IntersectionCoefficients (0:obj.numberOfSegments)/obj.numberOfSegments];     
            IntersectionCoefficients=unique(IntersectionCoefficients);     
            IntersectionCoefficients=sort(IntersectionCoefficients);          

         end
    end
    
    properties (Access = private)
        IntersectionCoefficients
    end
    
end