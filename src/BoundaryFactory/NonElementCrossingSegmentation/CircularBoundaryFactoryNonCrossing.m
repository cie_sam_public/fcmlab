%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% CircularBoundaryFactoryNonCrossing

classdef CircularBoundaryFactoryNonCrossing < CircularBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = CircularBoundaryFactoryNonCrossing(Center,Radius,NumberOfDivisions,Outer,MeshFactory)
            obj = obj@CircularBoundaryFactory(Center,Radius,NumberOfDivisions,Outer);
            obj.IntersectionCoefficients = obj.calcIntersections(MeshFactory);
        end
        
        %%
        function Arcs = getBoundary(obj)
            
           % if outer == true then theta->counterclockwise else clockwise
             theta = (2*obj.Outer-1)*obj.IntersectionCoefficients*(2*pi);
                
            % outer points
            for i = 1:length(theta)
                Vertices(i) = Vertex([obj.Center(1)+obj.Radius*cos(theta(i)) ...
                    obj.Center(2)+obj.Radius*sin(theta(i)) obj.Center(3)]);
            end
            VertexCenter=Vertex(obj.Center);
            % construct Lines
            Arcs = [];
            for i = 1 : length(theta)-1
                Arcs =[Arcs Arc(Vertices(i),Vertices(i+1),VertexCenter)];
            end
            Arcs = [Arcs Arc(Vertices(obj.NumberOfDivisions),Vertices(1),VertexCenter)];
        end
    end
    %%
    methods (Access = private)
        function IntersectionCoefficients = calcIntersections(obj,MeshFactory)% calculate intersection points of mesh edges with the boundary
            Tolerance=1E-5;                                                   
            MeshNodes = MeshFactory.createNodes;
            MeshEdges= MeshFactory.createEdges(MeshNodes);
            if ~isa(MeshEdges(1).getgeometricSupport,'Line') 
                Logger.ThrowException(['Mesh Edges are not Lines']);
            end
            k=1;
            IntersectionCoefficients=[];                                             
            for i=1:length(MeshEdges)
                EdgeNodes=MeshEdges(i).getNodes; %get nodes of the edge
                EdgeVector=EdgeNodes(2).getCoords-EdgeNodes(1).getCoords;

                EdgeVectorNorm=EdgeVector/norm(EdgeVector);
                
                if EdgeNodes(1).getZ==obj.Center(3) && EdgeNodes(2).getZ==obj.Center(3) ...
                        && norm(cross(EdgeVectorNorm,(EdgeNodes(1).getCoords-obj.Center)))<obj.Radius % continue if the mesh edge is coplanar AND their distance to the center is smaller than the radius
                   ax=EdgeVector(1); ay=EdgeVector(2);qx=EdgeNodes(1).getX;qy=EdgeNodes(1).getY;xc=obj.Center(1);yc=obj.Center(2);r=obj.Radius;
                   A=ax^2+ay^2;B=2*ax*qx-2*ax*xc+2*ay*qy-2*ay*yc;C=qx^2-2*qx*xc+xc^2+qy^2-2*qy*yc+yc^2-r^2;

                   l(1)=(-B-sqrt(B^2-4*A*C))/(2*A); l(2)=(-B+sqrt(B^2-4*A*C))/(2*A); %Compute the two solutions (l1 and l2) for the itersection of circle and edge
                   for j=1:2
                    if isreal(l(j)) && l(j)<1+Tolerance && l(j)>-Tolerance % if the solution is real and within the edge length compute intersection point 
                        Intersection=l(j)*EdgeVector+EdgeNodes(1).getCoords;
                        theta=acos((Intersection(1)-obj.Center(1))/obj.Radius); %compute the angle of the intersection point
                        if  asin((Intersection(2)-obj.Center(2))/obj.Radius)<0
                            theta=2*pi-theta;
                        end
                        if mod(theta/(2*pi),1/obj.NumberOfDivisions)>Tolerance 
                            IntersectionCoefficients(k)=theta/(2*pi);                                                   
                            k=k+1;
                        end
                    end
                   end
                end
            end
            IntersectionCoefficients=[IntersectionCoefficients (0:obj.NumberOfDivisions)/obj.NumberOfDivisions];     
            IntersectionCoefficients=unique(IntersectionCoefficients);     
            IntersectionCoefficients=sort(IntersectionCoefficients);    
         end
    end
        
    
    
    properties (Access = private)
        IntersectionCoefficients
    end
    
end