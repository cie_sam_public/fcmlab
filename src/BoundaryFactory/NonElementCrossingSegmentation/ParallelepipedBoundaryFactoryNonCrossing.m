%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
% Parallelepiped Boundary Factory Non Crossing
%(defines the AREA of all 6 FACES of a parallelepiped spanned by three vectors Xi, Eta and Zeta)
% boundary segments are not crossing element-borders
%   Boundary sections:
%      ____________
%     /|          /|     ^Eta
%    / |   5     / |     |    ^Zeta
%   /  |    <3> /  |     |   /        
%  /___|_______/   |     |  /   
%  |   |_______|_2_|     | /  
%  |<4>/       |   /      - - - - - > Xi
%  |  /   1 <6>|  /
%  | /         | /
%  |/__________|/
% Origin
%
% Get all quads at the 6 faces

classdef ParallelepipedBoundaryFactoryNonCrossing < ParallelepipedBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = ParallelepipedBoundaryFactoryNonCrossing(origin, vectorXi,vectorEta, vectorZeta,numberOfSegmentsXi,numberOfSegmentsEta,numberOfSegmentsZeta,MeshFactory )
            obj=obj@ParallelepipedBoundaryFactory(origin, vectorXi,vectorEta, vectorZeta,numberOfSegmentsXi,numberOfSegmentsEta,numberOfSegmentsZeta);
            obj.meshFactory=MeshFactory;
        end
        
        %%
       function Quads = getBoundary(obj)
           Quads = [];
           
           factory=PlaneAreaBoundaryFactoryNonCrossing (obj.origin, obj.vectorXi,obj.vectorEta, obj.numberOfSegmentsXi,obj.numberOfSegmentsEta,obj.meshFactory); %all Quads at face 1
           Quads = [ Quads, factory.getBoundary() ];
           
           factory=PlaneAreaBoundaryFactoryNonCrossing ((obj.origin+obj.vectorXi), obj.vectorZeta,obj.vectorEta, obj.numberOfSegmentsZeta,obj.numberOfSegmentsEta,obj.meshFactory); %all Quads at face 2
           Quads = [ Quads, factory.getBoundary() ];    
           
           factory=PlaneAreaBoundaryFactoryNonCrossing ((obj.origin+obj.vectorZeta), obj.vectorXi,obj.vectorEta, obj.numberOfSegmentsXi,obj.numberOfSegmentsEta,obj.meshFactory); %all Quads at face 3
           Quads = [ Quads, factory.getBoundary() ];
           
           factory=PlaneAreaBoundaryFactoryNonCrossing (obj.origin, obj.vectorZeta,obj.vectorEta, obj.numberOfSegmentsZeta,obj.numberOfSegmentsEta,obj.meshFactory); %all Quads at face 4
           Quads = [ Quads, factory.getBoundary() ];
           
           factory=PlaneAreaBoundaryFactoryNonCrossing ((obj.origin+obj.vectorEta), obj.vectorXi,obj.vectorZeta, obj.numberOfSegmentsXi,obj.numberOfSegmentsZeta,obj.meshFactory); %all Quads at face 5
           Quads = [ Quads, factory.getBoundary() ];
           
           factory=PlaneAreaBoundaryFactoryNonCrossing (obj.origin, obj.vectorXi,obj.vectorZeta, obj.numberOfSegmentsXi,obj.numberOfSegmentsZeta,obj.meshFactory); %all Quads at face 6
           Quads = [ Quads, factory.getBoundary() ];
       end
    end
     %%
    properties (Access = private)
        meshFactory;
    end
    
end