%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
   
% Plane Area BoundaryFactory
%(defines the AREA of a parallelogram spanned by two vectors Xi and Eta) 
% boundary segments are not crossing element-borders 
% ---> prerequisit for non element-crossing segments: edges of mesh and
%      boundary segmentation must be parallel !!!!
%
%                      Eta =[X2-X0 Y2-Y0 Z2-Z0]   
%                      ^ - - - - - - -   
%                     /              /
%                    /              /
%                   /              /
%                  /              /
% origin [X0 Y0 Z0] - - - - - - - >Xi =[X1-X0 Y1-Y0 Z1-Z0] 


classdef PlaneAreaBoundaryFactoryNonCrossing < PlaneAreaBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = PlaneAreaBoundaryFactoryNonCrossing(  origin, vectorXi,vectorEta, numberOfSegmentsXi,numberOfSegmentsEta, MeshFactory )
            obj=obj@PlaneAreaBoundaryFactory(origin, vectorXi,vectorEta, numberOfSegmentsXi,numberOfSegmentsEta);
            [obj.IntersectionCoefficientsXi,obj.IntersectionCoefficientsEta]= obj.calcIntersections(MeshFactory);
        end
        
        %%
        function Quads = getBoundary(obj)
            
            Quads = [];

            for i=1:length(obj.IntersectionCoefficientsEta)-1
                for j=1:length(obj.IntersectionCoefficientsXi)-1
                                        
                    Vertex0 = Vertex( obj.origin + obj.IntersectionCoefficientsXi(j)*obj.vectorXi+ obj.IntersectionCoefficientsEta(i)*obj.vectorEta);
                    Vertex1 = Vertex( obj.origin + obj.IntersectionCoefficientsXi(j+1)*obj.vectorXi+ obj.IntersectionCoefficientsEta(i)*obj.vectorEta);
                    Vertex2 = Vertex( obj.origin + obj.IntersectionCoefficientsXi(j)*obj.vectorXi+ obj.IntersectionCoefficientsEta(i+1)*obj.vectorEta);
                    Vertex3 = Vertex( obj.origin + obj.IntersectionCoefficientsXi(j+1)*obj.vectorXi+ obj.IntersectionCoefficientsEta(i+1)*obj.vectorEta);
                    
                 
                    Line0= Line(Vertex0,Vertex1);
                    Line1= Line(Vertex1,Vertex3);
                    Line2= Line(Vertex3,Vertex2);
                    Line3= Line(Vertex2,Vertex0);
                         
                    Quads=[Quads, Quad([Vertex0,Vertex1,Vertex2,Vertex3],[Line0,Line1,Line2,Line3])];
                end
             end
            
        end
        
    end
    
       %%
    methods (Access = private)
        function [IntersectionCoefficientsXi,IntersectionCoefficientsEta] = calcIntersections(obj,MeshFactory)% calculate intersection points of mesh faces
            Tolerance=1E-5;                                                                                    % with the boundary 
            MeshNodes = MeshFactory.createNodes;
            MeshEdges= MeshFactory.createEdges(MeshNodes);
            MeshFaces= MeshFactory.createFaces(MeshNodes,MeshEdges);
            if ~isa(MeshFaces(1).getgeometricSupport,'Rectangle') 
                Logger.ThrowException(['Mesh Faces are not Rectangles']);
            end
            
            k=1;
            l=1;
            IntersectionCoefficientsXi=[]; 
            IntersectionCoefficientsEta=[];                                             

            for i=1:length(MeshFaces)
                FaceNodes=MeshFaces(i).getNodes; %get nodes of the face
                NormalVectorFace=cross(FaceNodes(2).getCoords-FaceNodes(1).getCoords,FaceNodes(4).getCoords-FaceNodes(1).getCoords); NormalVectorFace=NormalVectorFace/norm(NormalVectorFace);
                ReferencePointFace=FaceNodes(1).getCoords-obj.origin;
                dFace=NormalVectorFace*(ReferencePointFace)'; %get scalar value of the Hesse normal form of the plane (mesh face)
               
                NormalVectorBoundary=cross(obj.vectorXi,obj.vectorEta); NormalVectorBoundary=NormalVectorBoundary/norm(NormalVectorBoundary);
                dBoundary=NormalVectorBoundary*[obj.origin-obj.origin]';%get scalar value of the Hesse normal form of the plane (boundary)

                IntersectionVector=cross(NormalVectorFace,NormalVectorBoundary); %direction of the intersection of the two planes
                
                ReferencePointOfIntersection=1/((NormalVectorFace*NormalVectorFace')*(NormalVectorBoundary*NormalVectorBoundary')-(NormalVectorFace*NormalVectorBoundary')^2)...
                    *(dFace*(NormalVectorBoundary*NormalVectorBoundary')-dBoundary*(NormalVectorFace*NormalVectorBoundary')*NormalVectorFace+...
                    dBoundary*(NormalVectorFace*NormalVectorFace')-dFace*(NormalVectorFace*NormalVectorBoundary')*NormalVectorBoundary); % one reference point of the intersection

                LinearDependencyMatrix=[obj.vectorXi;obj.vectorEta;FaceNodes(1).getCoords-obj.origin;FaceNodes(2).getCoords-obj.origin;FaceNodes(3).getCoords-obj.origin];

                if rank(LinearDependencyMatrix,Tolerance)==3 && abs(abs(NormalVectorFace*NormalVectorBoundary')-1)>Tolerance% exclude faces which are coplanar OR parallel
                    
                    if abs(abs(IntersectionVector*obj.vectorXi')/norm(obj.vectorXi)-1)<Tolerance            
                        
                    A=[-obj.vectorEta' IntersectionVector'];
                    b=[-ReferencePointOfIntersection]';
                    [R,jb]=rref(A'); A=A(jb,:); b=b(jb); % strike row jb out of the linear system of equations (linear dependent row)
                    x=A\b; %compute the intersection point of the MESH-BOUNDARY intersection vector with the boundary vector Eta
                    if x(1)<1-Tolerance && x(1)>Tolerance && mod(x(1),1/obj.numberOfSegmentsEta)>Tolerance % continue if the intersection point lies within the line but NOT at point which is already 
                    IntersectionCoefficientsEta(k)=x(1);                                                   % a border of two boundary segments
                    k=k+1;
                    end
                    
                    elseif abs(abs(IntersectionVector*obj.vectorEta')/norm(obj.vectorEta)-1)<Tolerance
                        
                    A=[-obj.vectorXi' IntersectionVector'];
                    b=[-ReferencePointOfIntersection]';
                    [R,jb]=rref(A'); A=A(jb,:); b=b(jb); % strike row jb out of the linear system of equations (linear dependent row)
                    x=A\b; %compute the intersection point of the MESH-BOUNDARY intersection vector with the boundary vector Xi
                    if x(1)<1-Tolerance && x(1)>Tolerance && mod(x(1),1/obj.numberOfSegmentsXi)>Tolerance % continue if the intersection point lies within the line but NOT at point which is already 
                    IntersectionCoefficientsXi(l)=x(1);                                                   % a border of two boundary segments
                    l=l+1;
                    end
                    
                    else
                        
                        Logger.ThrowException(['Mesh edges are not parallel to boundary edges']);
                        
                    end

                end
            end
            IntersectionCoefficientsXi=[IntersectionCoefficientsXi (0:obj.numberOfSegmentsXi)/obj.numberOfSegmentsXi];     
            IntersectionCoefficientsXi=unique(IntersectionCoefficientsXi);
            IntersectionCoefficientsXi=sort(IntersectionCoefficientsXi); 
            
            IntersectionCoefficientsEta=[IntersectionCoefficientsEta (0:obj.numberOfSegmentsEta)/obj.numberOfSegmentsEta];     
            IntersectionCoefficientsEta=unique(IntersectionCoefficientsEta);
            IntersectionCoefficientsEta=sort(IntersectionCoefficientsEta);  

         end
    end
    
    properties (Access = private)
        IntersectionCoefficientsXi
        IntersectionCoefficientsEta
    end
    
    
end