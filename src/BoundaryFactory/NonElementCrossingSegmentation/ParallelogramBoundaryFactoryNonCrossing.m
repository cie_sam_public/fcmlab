%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
     
% Parallelogram BoundaryFactory
%(defines the 4 EDGES of a parallelogram spanned by two vectors Xi and Eta) 
% boundary segments are not crossing element-borders
%
%                      Eta =[X2-X0 Y2-Y0 Z2-Z0]   
%                      ^ - - - - - - -   
%                     /              /
%                    /              /
%                   /              /
%                  /              /
% origin [X0 Y0 Z0] - - - - - - - >Xi =[X1-X0 Y1-Y0 Z1-Z0] 

classdef ParallelogramBoundaryFactoryNonCrossing < ParallelogramBoundaryFactory
    
    methods (Access = public)
        %% constructor
        function obj = ParallelogramBoundaryFactoryNonCrossing(origin, vectorXi,vectorEta,numberOfSegmentsXi,numberOfSegmentsEta,MeshFactory)
           obj=obj@ParallelogramBoundaryFactory(origin, vectorXi,vectorEta,numberOfSegmentsXi,numberOfSegmentsEta );
           obj.meshFactory = MeshFactory;
        end
        
        %%
        function Lines = getBoundary(obj)
            Lines = [];
            
            factory = StraightLineBoundaryFactoryNonCrossing( obj.origin, obj.origin+obj.vectorXi, obj.numberOfSegmentsXi, obj.meshFactory );
            Lines = [ Lines, factory.getBoundary( )];
    
            factory = StraightLineBoundaryFactoryNonCrossing( obj.origin+obj.vectorXi, obj.origin+obj.vectorXi+obj.vectorEta, obj.numberOfSegmentsEta,obj.meshFactory );
            Lines = [ Lines, factory.getBoundary( )];

            factory = StraightLineBoundaryFactoryNonCrossing( obj.origin+obj.vectorXi+obj.vectorEta, obj.origin+obj.vectorEta, obj.numberOfSegmentsXi,obj.meshFactory );
            Lines = [ Lines, factory.getBoundary( )];
    
            factory = StraightLineBoundaryFactoryNonCrossing( obj.origin+obj.vectorEta, obj.origin, obj.numberOfSegmentsEta, obj.meshFactory);
            Lines = [ Lines, factory.getBoundary( )];
        end
    end
    
    properties (Access = private)
      meshFactory
    end
    
end