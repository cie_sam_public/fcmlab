%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%


function subPoint = lineMeshIntersection(obj, segment, meshLine )

subPoint = [];

segVert = segment.getVertices();
segStart = segVert(1).getCoords();
segEnd = segVert(2).getCoords();
segVec = segEnd-segStart;

meshVert = meshLine.getVertices();
meshStart = meshVert(1).getCoords();
meshEnd = meshVert(2).getCoords();
meshVec = meshEnd-meshStart;

alpha = acosd(dot(segVec, meshVec)/(norm(segVec)*norm(meshVec)));

if alpha < 10e-4 || alpha > 179.999 %comparing against the angle because its better
    return 
end
 
D = [segVec(1) -meshVec(1); segVec(2) -meshVec(2)];
P = [(meshStart(1)-segStart(1));(meshStart(2)-segStart(2))];
t = D\P;


if t(1) < 1 && t(1) > 0.0 %means that the point lies on the segment 
    subPoint = segStart+t(1)*segVec;
end

end

