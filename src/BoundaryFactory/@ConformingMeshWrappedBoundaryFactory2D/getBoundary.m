%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function newBoundary = getBoundary( obj )



hx = obj.meshLengthX/obj.numberOfXDivisions;
hy = obj.meshLengthY/obj.numberOfYDivisions;
meshLines = [];
newBoundary = [];
subPoints = [];

for i=1:obj.numberOfXDivisions+1
    xPoint(1,:) = obj.meshOrigin + [1 0 0]*(i-1)*hx;
    xPoint(2,:) = (obj.meshOrigin + [0 1 0]*obj.meshLengthY) + [1 0 0]*(i-1)*hx;
    temp = Line(Vertex(xPoint(1,:)), Vertex(xPoint(2,:)));
    meshLines = [meshLines; temp];
    %plot3(xPoint(:,1),xPoint(:,2),xPoint(:,3),'k-');
  
end

for i=1:obj.numberOfYDivisions+1
    yPoint(1,:) = obj.meshOrigin + [0 1 0]*(i-1)*hy;
    yPoint(2,:)= (obj.meshOrigin + [1 0 0]*obj.meshLengthX) + [0 1 0]*(i-1)*hy;
    temp = Line(Vertex(yPoint(1,:)), Vertex(yPoint(2,:)));
    meshLines = [meshLines; temp];
    %plot3(yPoint(:,1),yPoint(:,2),yPoint(:,3),'k-');
end


for i=1:length(obj.boundary)
    currentSegment = obj.boundary(i); 
    segVert = currentSegment.getVertices();
    segStart = segVert(1).getCoords();
    segEnd = segVert(2).getCoords();  
    for j=1:length(meshLines)
        temp = obj.lineMeshIntersection(currentSegment,meshLines(j));
        subPoints = [subPoints; temp];
    end
    subPoints = [segStart; subPoints; segEnd];
    temp = obj.createLines(subPoints);
    newBoundary = [newBoundary, temp];
    subPoints = [];
    newPoints = [];
end



end

