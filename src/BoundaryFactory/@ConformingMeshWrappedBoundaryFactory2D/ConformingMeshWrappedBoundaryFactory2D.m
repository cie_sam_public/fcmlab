%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef ConformingMeshWrappedBoundaryFactory2D
    
    
    properties
        boundary
        meshOrigin
        meshLengthX
        meshLengthY
        numberOfXDivisions
        numberOfYDivisions
    end
    
    methods
        
        function obj = ConformingMeshWrappedBoundaryFactory2D(Boundary, MeshOrigin, MeshLengthX, MeshLengthY, NumberOfXDivisions, NumberOfYDivisions)
            obj.boundary = Boundary;
            obj.meshOrigin = MeshOrigin;
            obj.meshLengthX = MeshLengthX;
            obj.meshLengthY = MeshLengthY;
            obj.numberOfXDivisions = NumberOfXDivisions;
            obj.numberOfYDivisions = NumberOfYDivisions;
        end
        
        subPoint = lineMeshIntersection(obj, segment, meshLine )
        newBoundary = getBoundary(obj)
       
        
        function subBoundary = createLines(obj,coordVector)
            subBoundary = [];
            DSubSeg(1,1) = 0; 
            DSubSeg(1,2) = 1;
            for i=2:size(coordVector,1)
               DSubSeg(i,1) = norm(coordVector(i,:)-coordVector(1,:));
               DSubSeg(i,2) = i;
            end
            [~,ai,~] = unique(DSubSeg(:,1));
            DSubSeg = DSubSeg(ai,:); 
            for i=1:size(DSubSeg)-1
               Vertex1 = Vertex(coordVector(DSubSeg(i,2),:));
               Vertex2 = Vertex(coordVector(DSubSeg(i+1,2),:));
               temp = Line(Vertex1,Vertex2);
               subBoundary = [subBoundary, temp];
            end
          
         
        end
        
    end
    
end

