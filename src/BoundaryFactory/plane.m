classdef plane < handle

    
    properties
      normalVector
      pointOnPlane %one point if the plane
      distance
      u %orthogonal vectors
      v
    end
    
    methods
        function obj = plane(pointOnPlane, n)
            if ~any(n);
                Logger.ThrowException('Normalvector can not be [0 0 0]!');
            end
            obj.normalVector = n'/norm(n);
            obj.pointOnPlane = pointOnPlane';
            arbitraryVector = [1.2; 1.3; 1.4]; %use this vector for finding an orthogonal vector to n;
            if eq(arbitraryVector,obj.normalVector)% if thay are equal just choose an other one;
                arbitraryVector = [0 1 0];
            end
            obj.u = cross(arbitraryVector, obj.normalVector);
            obj.v = cross(obj.u, obj.normalVector);
            obj.distance = abs(dot(pointOnPlane, n));
        end
        %----
        function normalVector = getNormalVector(obj)
             normalVector = obj.normalVector;
        end
        %----
        function pointOnPlane = getPointOnPlane(obj)
                  pointOnPlane   = obj.pointOnPlane;
        end
        %----
        function distance = getDistance(obj)
                  distance  = obj.distance;
        end
        %----
        function [u,v] = getPlaneVectors(obj)
            u = obj.u;
            v = obj.v;
        end
        
        function plotPlane(obj, xLim, zLim)
           
            pointA = obj.pointOnPlane -xLim/2*obj.u -zLim/2*obj.v;
            pointB = pointA + xLim*obj.u; 
            pointC = pointB + zLim*obj.v;
            pointD = pointA + zLim*obj.v;
            verts = [pointA'; pointB'; pointC'; pointD'];
            faces = [1 2 3 4];
            
             patch('faces',faces,'vertices',verts);            
             hold on;
            grid on;
            alpha(0.3);
        end
    end
    
    
end

