 %=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
%   Boundary sections:
%      ____________
%     /|          /|     ^Eta
%    / |   5     / |     |    ^Zeta
%   /  |    <3> /  |     |   /        
%  /___|_______/   |     |  /   
%  |   |_______|_2_|     | /  
%  |<4>/       |   /      - - - - - > Xi
%  |  /   1 <6>|  /
%  | /         | /
%  |/__________|/
% Origin
%
% Get Lagrange parameters u and v (range: [-1,1]) at all 6 boundary faces

function [u,currentSection,TotalNumberOfBoundarySections] =getLagrangeParameters(obj,GlobalCoordinate)
          
             TotalNumberOfBoundarySections=6;
             
             %compute Lagrange parameters and current section:
             PositionVector=(GlobalCoordinate-obj.origin)'; %the position vector with respect to the origin of the parallelepiped
             
             VectorComponentMatrix=[obj.vectorXi',obj.vectorEta',obj.vectorZeta']; 
             
             VectorComponentWeight=VectorComponentMatrix\PositionVector; % the weight of the three vectors Xi, Eta and Zeta, for the current position (range: [0,1])
             
             if abs(VectorComponentWeight(3))<1E-12  % section 1
               u(1)= -1+2*VectorComponentWeight(1); % local Lagrange parameter u
               u(2)= -1+2*VectorComponentWeight(2); % local Lagrange parameter v
               currentSection=1;
             elseif abs(VectorComponentWeight(1)-1.0)<1E-12 % section 2
               u(1)= -1+2*VectorComponentWeight(2); 
               u(2)= -1+2*VectorComponentWeight(3); 
               currentSection=2;
             elseif abs(VectorComponentWeight(3)-1.0)<1E-12 % section 3 
               u(1)= -1+2*VectorComponentWeight(1); 
               u(2)= -1+2*VectorComponentWeight(2);
               currentSection=3;
             elseif abs(VectorComponentWeight(1))<1E-12 % section 4
               u(1)= -1+2*VectorComponentWeight(2); 
               u(2)= -1+2*VectorComponentWeight(3); 
               currentSection=4;
             elseif abs(VectorComponentWeight(2)-1.0)<1E-12 % section 5  
               u(1)= -1+2*VectorComponentWeight(1); 
               u(2)= -1+2*VectorComponentWeight(3); 
               currentSection=5;
             elseif abs(VectorComponentWeight(2))<1E-12  % section 6
               u(1)= -1+2*VectorComponentWeight(1); 
               u(2)= -1+2*VectorComponentWeight(3); 
               currentSection=6;
             else
               u=[];
               currentSection=[];
             end
          
             
end