classdef STLwithConformingMeshBoundaryFactory  < STLBoundaryFactory
    
    properties
        tolerance
        planes
        processedBoundary
        initialBoundary
        readyToProcessFlag
    end
    
    
    methods
        function obj = STLwithConformingMeshBoundaryFactory (BinarySTLFile,tolerance)
            obj  = obj@STLBoundaryFactory(BinarySTLFile);
            obj.BinarySTLFile = BinarySTLFile;
            obj.tolerance = tolerance;
            obj.initialBoundary = obj.getBoundary();
            obj.processedBoundary = Triangle.empty();
            obj.planes = plane.empty(); 
            obj.readyToProcessFlag = false;
        end
        %%
        plotBoundary(obj,str);
        processBoundary(obj)
        plotNew(obj, Boundary);
        planes = extractPlanesFromMeshData(obj, Origin, NumOfXDivisions, NumOfYDivisions, NumOfZDivisions, Lx, Ly, Lz );
        
        %% geter
        function processedBoundary = getProcessedBoundary(obj)
            processedBoundary = obj.processedBoundary;
        end
    end
    
    methods (Access = private)
        bool = needsPartitioning(obj, plane, triangle)        
        newTriangles = cutTriangleWithPlane(obj, triangle, plane)
        intersectionPoint =  linePlaneIntersection(obj, vertex1, vertex2, plane) 
    end
           
        
    
end
    



