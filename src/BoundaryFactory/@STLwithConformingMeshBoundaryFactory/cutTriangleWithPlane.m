function  newTriangles = cutTriangleWithPlane(obj, triangle, plane)

vertexMatrix = [];

planeNormal = plane.getNormalVector();
planePoint = plane.getPointOnPlane();
trianglePoints  = triangle.getVerticesCoordinates();
triangleVertices = triangle.getVertices();
triangleVertices(4) = [];
dotProduct=zeros(1,3);
for i=1:3
    dotProduct(i) = dot(planeNormal,planePoint-trianglePoints(i,:)');
    if dotProduct(i) < obj.tolerance && dotProduct(i) > 0.0
        dotProduct(i) = 0.0;
    end
end


whichConstelation = prod(dotProduct); %determines the Side of the single Vertex


if whichConstelation == 0.0              %Single Vertex is on Plane! %!!!!!
    
    intersectionPoint(1) = triangleVertices(find(dotProduct==0));
    vertexLeft = triangleVertices(find(dotProduct<0));
    vertexRight= triangleVertices(find(dotProduct>0));
%     
%     assertEqual('Triangle Vectors are not assempled in the rigth way\n', size(vertexleft),[1 1]);
%     assertEqual('Triangle Vectors are not assempled in the rigth way\n', size(vertexright),[1 1]);
    
    intersectionPoint(2) = obj.linePlaneIntersection(vertexLeft,vertexRight,plane);
    
    vertexMatrix = [intersectionPoint(1) intersectionPoint(2) vertexLeft; ...
                    intersectionPoint(1) intersectionPoint(2) vertexRight];
    
end



if whichConstelation < 0.0              %the single Vertex is beneath the plane!
    vertexBeneath = triangleVertices(find(dotProduct<0)); %1x1
    vertexAbove = triangleVertices(find(dotProduct>0)); %1x2
%     assertEqual('Triangle Vectors are not assempled in the rigth way!\n', size(vertexBeneath),[1 1]);
%     assertEqual('Triangle Vectors are not assempled in the rigth way!\n', size(vertexAbove),[1 2]);
    
    intersectionPoint(1) = obj.linePlaneIntersection(vertexBeneath, vertexAbove(1), plane);
    intersectionPoint(2) = obj.linePlaneIntersection(vertexBeneath, vertexAbove(2), plane);
     
    vertexMatrix = [intersectionPoint(1) intersectionPoint(2) vertexBeneath;...
                    intersectionPoint(1) intersectionPoint(2) vertexAbove(1);...
                    intersectionPoint(2) vertexAbove(1) vertexAbove(2)];
    
end
    
    
if whichConstelation > 0.0               %the single Vertex is above the plane!
    vertexBeneath = triangleVertices(find(dotProduct<0)); %1x2
    vertexAbove = triangleVertices(find(dotProduct>0)); %1x1
%     assertEqual('Triangle Vectors are not assempled in the rigth way\n', size(vertexBeneath),[1 2]);
%     assertEqual('Triangle Vectors are not assempled in the rigth way\n', size(vertexAbove),[1 1]);
    
    intersectionPoint(1) = obj.linePlaneIntersection(vertexAbove, vertexBeneath(1),plane);
    intersectionPoint(2) = obj.linePlaneIntersection(vertexAbove, vertexBeneath(2),plane);
    
    vertexMatrix = [intersectionPoint(1) intersectionPoint(2) vertexAbove;...
                    intersectionPoint(1) intersectionPoint(2) vertexBeneath(1);...
                    intersectionPoint(2) vertexBeneath(1) vertexBeneath(2)];

end

if isempty(vertexMatrix)
  Logger.ThrowException('vertexMatrix musst not be empty!');
end 

for i=1:size(vertexMatrix,1)
    Line1   = Line(vertexMatrix(i,1), vertexMatrix(i,2));
    Line2   = Line(vertexMatrix(i,2), vertexMatrix(i,3));
    Line3   = Line(vertexMatrix(i,3), vertexMatrix(i,1));

    Vertices = vertexMatrix(i,:);
    Lines = [Line1 Line2 Line3];
    newTriangles(i) = Triangle(Vertices, Lines);
end

end

