function  bool = needsPartitioning(obj, plane, triangle)

planeNormal = plane.getNormalVector();
planePoint = plane.getPointOnPlane();
triangleChoords  = triangle.getVerticesCoordinates();


dotProduct=zeros(1,3);
for i=1:3
    dotProduct(i) = dot(planeNormal,planePoint-triangleChoords(i,:)');
    if abs(dotProduct(i)) < obj.tolerance && abs(dotProduct(i)) > 0.0
        dotProduct(i) = 0.0;
    end
end

if all(dotProduct<=0) || all(dotProduct>=0)
    bool = false; 
else
    bool = true;
end


end

