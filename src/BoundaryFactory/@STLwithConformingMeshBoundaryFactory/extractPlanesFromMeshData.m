function [ planes ] = extractPlanesFromMeshData( obj, Origin, NumOfXDivisions, NumOfYDivisions, NumOfZDivisions, Lx, Ly, Lz ) %vlt. noch anderes nennen;


normalX =  [1 0 0];
normalY =  [0 1 0];
normalZ =  [0 0 1];

hx = [Lx/NumOfXDivisions 0 0];
hy = [0 Ly/NumOfYDivisions 0];
hz = [0 0 Lz/NumOfZDivisions];


obj.planes(end+1) = plane(Origin,normalX); 
obj.planes(end+1) = plane(Origin,normalY);
obj.planes(end+1) = plane(Origin,normalZ);



for i=1:NumOfXDivisions-1
    obj.planes(end+1) = plane(Origin+hx*i, normalX);
end

for i=1:NumOfYDivisions-1
    obj.planes(end+1) = plane(Origin+hy*i, normalY);
   
end

for i=1:NumOfZDivisions-1
    obj.planes(end+1) = plane(Origin+hz*i, normalZ);
 
end

% for i=1:length(obj.planes)
%     obj.planes(i).plotPlane(2,2);
% end

planes = obj.planes;
obj.readyToProcessFlag = true;
end

