function  processBoundary(obj)

if obj.readyToProcessFlag == false
    Logger.ThrowException('You forgot to call extractPlanesFromMeshData(...)-Function!');
end


STLTriangles = obj.initialBoundary;

currentTriangles = Triangle.empty();
fragmentedTriangles = Triangle.empty();
newTriangles = Triangle.empty();

deletePositionVector = [];
Logger.TaskStart('Processing the boundary...','release');
for i=1:length(STLTriangles)
    currentTriangles = STLTriangles(i);
    %fprintf('%d/%d \n',i,length(STLTriangles));
    while ~isempty(currentTriangles)
        for j=1:length(currentTriangles)
            planeCount = 1;
            while ~obj.needsPartitioning(obj.planes(planeCount),currentTriangles(j))
                if planeCount == length(obj.planes)
                    fragmentedTriangles = [fragmentedTriangles currentTriangles(j)];
                    deletePositionVector(end+1) = j;
                    break
                end
                planeCount = planeCount +1;
            end
        end
        currentTriangles(deletePositionVector) = [];
        deletePositionVector = [];
        while ~isempty(currentTriangles)
            %fprintf('   %d CT\n', length(currentTriangles));
            for m=1:length(obj.planes)
                if obj.needsPartitioning(obj.planes(m),currentTriangles(1))
                    tempTriangles = obj.cutTriangleWithPlane(currentTriangles(1),obj.planes(m));
                    currentTriangles(1) = [];
                    newTriangles = [newTriangles tempTriangles];
                end
                if isempty(currentTriangles)
                    break
                end
            end
        end
        if ~isempty(newTriangles)
            currentTriangles = newTriangles;
            newTriangles = [];
        end
    end
    
    
end


Logger.Log(['input: ',num2str(length(STLTriangles)),' --> output: ',num2str(length(fragmentedTriangles)),' Triangles'],'release');
obj.processedBoundary = fragmentedTriangles;
Logger.TaskFinish('Processing the boundary...','release');

end




