function intersectionPoint =  linePlaneIntersection(obj, vertex1, vertex2, plane)
    
    Coord1 = vertex1.Coords';
    Coord2 = vertex2.Coords';
    n = plane.getNormalVector();
    PointOnPlane = plane.getPointOnPlane();
    
    lineVector = (Coord2-Coord1);
    t = dot((PointOnPlane -Coord1),n)/dot(lineVector,n);
    intersectionCoords = Coord1 + t*(lineVector);
    temp = Coord1 + 1*(lineVector); %conect the two vertices, which equals t = 1. 
    
    if ~eq(temp,Coord2)
        Logger.ThrowException('Error in linePlaneIntersection: lineVector probably points in the worng direction');
    end
    if abs(t)> 1.1
        Logger.ThrowException('Error linePlaneIntersection: running index t exeedes line length!');
    end 
    
   intersectionPoint = Vertex(intersectionCoords');


end
