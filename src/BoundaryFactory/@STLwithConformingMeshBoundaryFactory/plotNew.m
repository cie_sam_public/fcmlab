function  plotNew( obj, Boundary)
hold all; 

for i=1:length(Boundary)
   
    Vertices = Boundary(i).getVertices();
    
    t = 0:0.1:1;
    xLine1 = (1-t)*Vertices(1).Coords(1)+t*Vertices(2).Coords(1);
    yLine1=  (1-t)*Vertices(1).Coords(2)+t*Vertices(2).Coords(2);
    zLine1 = (1-t)*Vertices(1).Coords(3)+t*Vertices(2).Coords(3);
    
    xLine2 = (1-t)*Vertices(2).Coords(1)+t*Vertices(3).Coords(1);
    yLine2 = (1-t)*Vertices(2).Coords(2)+t*Vertices(3).Coords(2);
    zLine2 = (1-t)*Vertices(2).Coords(3)+t*Vertices(3).Coords(3);
    
    xLine3 = (1-t)*Vertices(3).Coords(1)+t*Vertices(4).Coords(1);
    yLine3 = (1-t)*Vertices(3).Coords(2)+t*Vertices(4).Coords(2);
    zLine3 = (1-t)*Vertices(3).Coords(3)+t*Vertices(4).Coords(3);
    
    xLine4 = (1-t)*Vertices(4).Coords(1)+t*Vertices(1).Coords(1);
    yLine4=  (1-t)*Vertices(4).Coords(2)+t*Vertices(1).Coords(2);
    zLine4 = (1-t)*Vertices(4).Coords(3)+t*Vertices(1).Coords(3);
    
    color = 'k';
    plot3(xLine1,yLine1,zLine1, color);
    plot3(xLine2,yLine2,zLine2, color);
    plot3(xLine3,yLine3,zLine3, color);
    plot3(xLine4,yLine4,zLine4, color);
    xlabel('x-Axis')
    ylabel('y-Axis')
    zlabel('z-Axis')
end



end

