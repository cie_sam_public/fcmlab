%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 %defines Quad segments on a plane area
function Quads = getBoundary(obj)
    Quads = [];
    
 
   dXi=obj.vectorXi/obj.numberOfSegmentsXi;
   dEta=obj.vectorEta/obj.numberOfSegmentsEta;

        
   for i=1:obj.numberOfSegmentsEta
      for j=1:obj.numberOfSegmentsXi
         
          Vertex0 = Vertex( obj.origin + (j-1) * dXi+(i-1)*dEta);
          Vertex1 = Vertex( obj.origin + (j) * dXi+(i-1)*dEta);
          Vertex2 = Vertex( obj.origin + (j-1) * dXi+(i)*dEta);
          Vertex3 = Vertex( obj.origin + (j) * dXi+(i)*dEta);
          
          Line0= Line(Vertex0,Vertex1);
          Line1= Line(Vertex1,Vertex3);
          Line2= Line(Vertex3,Vertex2);
          Line3= Line(Vertex2,Vertex0);

         Quads=[Quads, Quad([Vertex0,Vertex1,Vertex2,Vertex3],[Line0,Line1,Line2,Line3])];
          
      end
       
   end


end