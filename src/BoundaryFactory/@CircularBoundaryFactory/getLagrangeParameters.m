 %=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%
function  [u,currentSection,TotalNumberOfBoundarySections] =getLagrangeParameters(obj,GlobalCoordinate)
             
             %only one section for the boundary:
             TotalNumberOfBoundarySections=1;
             currentSection=1;
             
             %% compute Lagrange parameters:
             Xi=[1 0 0];
             Eta=[0 1 0];
             
             PositionVector=(GlobalCoordinate-obj.Center)'; %the position vector with respect to the origin of the cube, NOT the global position vector

          
             % Compute the first angle (for parameter u):
              if (cross(Xi,PositionVector)*cross(Xi,Eta)')>=0
                 alpha=acos(Xi*PositionVector/(norm(Xi)*norm(PositionVector)));
             else
                 alpha=2*pi-acos(Xi*PositionVector/(norm(Xi)*norm(PositionVector)));

              end
            

              u=-1.0+2.0*alpha/(2*pi);
              
              
        end