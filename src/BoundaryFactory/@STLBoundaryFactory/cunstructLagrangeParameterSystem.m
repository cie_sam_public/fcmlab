%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
% This function defines certain properties for a parameter space which can
% be used for a B-Spline or Legendre-polynomial discretization along the
% STL-boundary. This can be used in order to apply the Lagrange Method for
% the imposition of boundary conditions along the STL- boundary. 

function  cunstructLagrangeParameterSystem(obj)
            
            [x,y,z]=obj.stlread(obj.BinarySTLFile);
            
            obj.origin=[min(min(x(:,:))) min(min(y(:,:))) min(min(z(:,:)))]; % define the minimal coordinate values as origin of the STL-boundary
            
            obj.vectorXi=[(max(max(x(:,:)))-min(min(x(:,:)))) 0 0]; % define a parallelepiped with vectors Xi,Eta,Zeta which envelops the STL-boundary
            obj.vectorEta=[0 (max(max(y(:,:)))-min(min(y(:,:)))) 0];
            obj.vectorZeta=[0 0 (max(max(z(:,:)))-min(min(z(:,:))))];
            
            center=[mean(mean(x(:,:))) mean(mean(y(:,:))) mean(mean(z(:,:)))]-obj.origin; % get the center as the mean value of all STL-points
            normDist=max([norm(obj.vectorXi),norm(obj.vectorEta),norm(obj.vectorZeta)]);
            
            obj.vectorXi(1)=max([obj.vectorXi(1),0.1*normDist]); % if one of the vectors Xi,Eta, Zeta is close to zero, replace ist with a non-zero vector
            obj.vectorEta(2)=max([obj.vectorEta(2),0.1*normDist]);
            obj.vectorZeta(3)=max([obj.vectorZeta(3),0.1*normDist]);
            distance=0; % the minimal distance of the center of the structure to the STL-boundary triangles

            while distance<0.02*normDist
                distance=normDist;
                
                for i = 1:size(x,2);
                    distance1 = norm(center-[x(1,i) y(1,i) z(1,i)]); 
                    distance2 = norm(center-[x(2,i) y(2,i) z(2,i)]);
                    distance3 = norm(center-[x(3,i) y(3,i) z(3,i)]);
                
                    distance=min([distance,distance1,distance2,distance3]); 
                    
                    if distance<0.02*normDist % if the center lies closely on the STL-surface move it randomly, 
                                              % in order to allow calculation of Lagrange Parameters by angles (non-zero radius required)
                        randomVariable1=rand;
                        randomVariable2=rand;
                        center=center+[randomVariable1*0.05*normDist (1-randomVariable1)*0.05*normDist  randomVariable2*0.05*normDist];
                        break;
                    end
                end
            end
            
            obj.center=center;

end