%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% assumes that the rectangle's origin equals the mesh's origin
function Lines = getBoundary(obj)

    Lines = [ ];
    
    cornerPoints = [ obj.origin; ...
                     obj.origin( 1 ) + obj.lengths( 1 ), obj.origin( 2 ), 0; ...
                     obj.origin( 1 ) + obj.lengths( 1 ), obj.origin( 2 ) + obj.lengths( 2 ), 0; ... 
                     obj.origin( 1 ), obj.origin( 2 ) + obj.lengths( 2 ), 0 ];

    % all lines for 1. edge
    regularPoint1 = cornerPoints(2,:)-[obj.ProtrusionX 0 0];
    factory = StraightLineBoundaryFactory( cornerPoints(1,:), regularPoint1, floor(obj.lengths(1)/obj.hX)  );
    Lines = [ Lines, factory.getBoundary( ) ];
    factory = StraightLineBoundaryFactory( regularPoint1, cornerPoints(2,:), 1);
    Lines = [Lines, factory.getBoundary() ];
    
    % all lines for 2. edge
    regularPoint2 = cornerPoints(3,:)-[0 obj.ProtrusionY 0];
    factory = StraightLineBoundaryFactory( cornerPoints(2,:),regularPoint2, floor(obj.lengths(2)/obj.hY)  );
    Lines = [ Lines, factory.getBoundary( ) ];
    factory = StraightLineBoundaryFactory( regularPoint2, cornerPoints(3,:), 1);
    Lines = [Lines, factory.getBoundary() ];
    
    % all lines for 3. edge
    regularPoint3 = cornerPoints(3,:)-[obj.ProtrusionX 0 0];
    factory = StraightLineBoundaryFactory( cornerPoints(3,:), regularPoint3, 1 );
    Lines = [ Lines, factory.getBoundary( ) ];
    factory = StraightLineBoundaryFactory( regularPoint3, cornerPoints(4,:), floor(obj.lengths(1)/obj.hX));
    Lines = [Lines, factory.getBoundary() ];
    
    % all lines for 4. edge
    regualarPoint4 = cornerPoints(4,:)-[0 obj.ProtrusionY  0];
     factory = StraightLineBoundaryFactory( cornerPoints(4,:), regualarPoint4, 1 );
    Lines = [ Lines, factory.getBoundary( ) ];
    factory = StraightLineBoundaryFactory( regualarPoint4, obj.origin, floor(obj.lengths(2)/obj.hY) );
    Lines = [Lines, factory.getBoundary() ];
    
    coords = [];
    for i=1:length(Lines)
        vertices = Lines(i).getVertices();
        for j=1:length(vertices)
            temp = vertices(j).getCoords();
            coords = [coords; temp];
        end
    end
    hold on;
    grid on;
    plot3(coords(:,1),coords(:,2),coords(:,3),'g-')
    
    

end