classdef STLwithIndividualPlanesBoundaryFactory  < STLBoundaryFactory
    
    properties
        tolerance
        planes
        processedBoundary
        initialBoundary
        
    end
    
    
    methods
        function obj = STLwithIndividualPlanesBoundaryFactory (BinarySTLFile, Planes, tolerance)
            obj  = obj@STLBoundaryFactory(BinarySTLFile);
            obj.BinarySTLFile = BinarySTLFile;
            obj.planes = Planes;
            obj.tolerance = tolerance;
            obj.initialBoundary = obj.getBoundary();
            obj.processedBoundary = Triangle.empty();
        end
        %%
        plotBoundary(obj,str);
        processBoundary(obj)
        plotNew(obj, Boundary);
        function processedBoundary = getProcessedBoundary(obj)
            processedBoundary = obj.processedBoundary;
        end
    end
    
    methods (Access = private)
        bool = needsPartitioning(obj, plane, triangle)        
        newTriangles = cutTriangleWithPlane(obj, triangle, plane)
        intersectionPoint =  linePlaneIntersection(obj, vertex1, vertex2, plane) 
    end
           
        
    
end
    



