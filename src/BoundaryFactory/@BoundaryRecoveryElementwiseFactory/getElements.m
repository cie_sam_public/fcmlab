%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function QuadArray = getElements(obj)

Mesh = obj.Analysis.getMesh();

Elements = Mesh.getElements();

%Bounding Box as Polygon
xmin=obj.BoundingBoxX(1);
xmax=obj.BoundingBoxX(2);
ymin=obj.BoundingBoxY(1);
ymax=obj.BoundingBoxY(2);
%input for polyxpoly
xb=[xmin xmax xmax xmin xmin];
yb=[ymin ymin ymax ymax ymin];
% input for rectint [X,Y,WIDTH,HEIGHT]
BB=[xmin, ymin, (xmax-xmin), (ymax-ymin)];
% Elements container
ElInsideBB=[];

for i=1:length(Elements)
    
    Nodes = Elements(i).getNodes();
    
    x=[];
    y=[];
    
    for k=1:length(Nodes)
        x=[x Nodes(k).getX()];
        y=[y Nodes(k).getY()];
    end
    
    %Close Polygon % input for polyxpoly
    x=[x x(1)];
    y=[y y(1)];
    
    % input for rectint, expecting that elements start with left lower
    % corner and are ordered couner clockwise
    % maybe check with ispolycw
    % Position Vector [x,y,w,h]
    Ei=[x(1), y(1), (x(2)-x(1)), (y(3)-y(1))];
    
    area=rectint(BB,Ei);
    arearef=rectint(Ei,Ei);
    % element outside BB
    if area==0
        %disp('Element outsided BoundingBox!')
        % element completely inside BB
    elseif area==arearef
        %disp('Element completely inside')
        ElInsideBB=[ElInsideBB Ei];
        
    elseif area==rectint(BB,BB)% Bounding Box completely inside one Element
        ElInsideBB=[ElInsideBB BB];
        %disp('BoundingBox completely in one Element')
       
    elseif area>0&&area<arearef % element partly inside BB
        
        [xint, yint]=intersections(xb,yb,x,y);
        in= inpolygon(x,y,xb,yb);
        
        
        if sum(in)==0% element surrounds BB with two or three edges
            %disp('element surrounds BB with three edges')
            
            if length(xint)==4% case1: two edges around
                %build position vector
                xi=min(xint);
                yi=min(yint);
                width=(max(xint)-min(xint));
                height=(max(yint)-min(yint));
                El=[xi,yi,width,height];
                ElInsideBB=[ElInsideBB El];
                %disp('two edges case')
               
            else %three edges surrounding
                %disp('three edges case')
                    
                 if abs(xint(1)-xint(2))<0.001 % cut vertically
                    
                    %disp('cut vertically')
                    if x(1)<xmin% element on the left
                        xi=xmin;
                        yi=ymin;
                        height=ymax-ymin;
                        width=area/height;
                        disp('left')
                        El=[xi,yi,width,height];
                        ElInsideBB=[ElInsideBB El];
                        
                    elseif x(1)==xint(1)% element on the right
                        xi=xint(1);
                        yi=ymin;
                        height=ymax-ymin;
                        width=area/height;
                        disp('right')
                        El=[xi,yi,width,height];
                        ElInsideBB=[ElInsideBB El];
                    end
                   
                else  % cut horizontally
                    %disp('cut horizontally')
                    
                    if y(1)<ymin% lower
                        xi=xmin;
                        yi=ymin;
                        width=xmax-xmin;
                        height=area/width;
                        %disp('lower')
                        El=[xi,yi,width,height];
                        ElInsideBB=[ElInsideBB El];
                        
                        
                    elseif y(1)==yint(1)% upper
                        xi=xmin;
                        yi=yint(1);
                        width=xmax-xmin;
                        height=area/width;
                        %disp('upper')
                        El=[xi,yi,width,height];
                        ElInsideBB=[ElInsideBB El];
                    end
                end
            end
        end
        
        
        
        
        % element cut by corner of BB
        if xint(1)~=xint(2)&&yint(1)~=yint(2)
            %disp('corner')
            %build position vector
            xi=min(xint);
            yi=min(yint);
            width=(max(xint)-min(xint));
            height=(max(yint)-min(yint));
            El=[xi,yi,width,height];
            ElInsideBB=[ElInsideBB El];
            
            % element cut by edge of BB
        else
            % cut horizontally
            if yint(1)==yint(2)
                % cut by lower edge
                if yint(1)==ymin
                    xi=min(xint);
                    width=max(xint)-min(xint);
                    height=area/width;
                    yi=ymin;
                    %disp('lower')
                    El=[xi,yi,width,height];
                    ElInsideBB=[ElInsideBB El];
                    % cut by upper edge
                elseif yint(1)==ymax
                    xi=min(xint);
                    width=max(xint)-min(xint);
                    height=area/width;
                    yi=ymax-height;
                    %disp('upper')
                    El=[xi,yi,width,height];
                    ElInsideBB=[ElInsideBB El];
                end
                
                % cut vertically
            elseif xint(1)==xint(2)
                % cut by left edge
                if xint(1)==xmin
                    xi=xmin;
                    yi=min(yint);
                    height=max(yint)-min(yint);
                    width=area/height;
                    %disp('left')
                    El=[xi,yi,width,height];
                    ElInsideBB=[ElInsideBB El];
                    % cut by right edge of BB
                elseif xint(1)==xmax
                    yi=min(yint);
                    height=max(yint)-min(yint);
                    width=area/height;
                    xi=xmax-width;
                    %disp('right')
                    El=[xi,yi,width,height];
                    ElInsideBB=[ElInsideBB El];
                end
            end
            
        end
        
    end
    
    
end
PosVect=ElInsideBB;


%Preparation of Quads for QuadTree
retQuadArray=[];
for i=1:4:length(ElInsideBB)
    
    xi=ElInsideBB(i);
    yi=ElInsideBB(i+1);
    width=ElInsideBB(i+2);
    height=ElInsideBB(i+3);
    % Optional Visualization
    %rectangle('Position',[xi, yi, width,height])
    
    vertex1 = Vertex([xi yi 0.0]);
    vertex2 = Vertex([(xi+width) yi 0.0]);
    vertex3 = Vertex([(xi+width) (yi+height) 0.0]);
    vertex4 = Vertex([xi (yi+height) 0.0]);
    
    line1 = Line(vertex1,vertex2);
    line2 = Line(vertex2,vertex3);
    line3 = Line(vertex3,vertex4);
    line4 = Line(vertex4,vertex1);
    
    retQuadArray=[retQuadArray, Quad([vertex1,vertex2,vertex3,vertex4],[line1,line2,line3,line4])];
    
end
QuadArray=retQuadArray;
end