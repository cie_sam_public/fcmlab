classdef GradientJumpPenalisationVisualizer < handle
    
    properties
        feMesh
        domainIndex
        seedpoints
        stabilizedFaces
        gradientJumpPenalisation
        resolution
    end
    
    methods
        function obj=GradientJumpPenalisationVisualizer(FeMesh, GradientJumpPenalisationClass)
            obj.gradientJumpPenalisation = GradientJumpPenalisationClass;
            obj.feMesh = FeMesh;
            obj.resolution = GradientJumpPenalisationClass.getResolution();
        end
        
        
        function visualizeMySetup(obj,varargin)
            figure
            if nargin == 1
                obj.visualizeMesh();
                obj.visualizeSeedpoints(obj.resolution);
                obj.visualizeFaces();
            else
                for i=1:length(varargin)
                    if strcmp(varargin{i},'Mesh')
                        obj.visualizeMesh();
                    elseif strcmp(varargin{i},'Seedpoints')
                        obj.visualizeSeedpoints(obj.resolution)
                    elseif strcmp(varargin{i},'Faces')
                        obj.visualizeFaces();
                    end
                end
            end
        end
                    
            

        
        function visualizeMesh(obj)
            
            edges = obj.feMesh.getEdges();
            
            lightGrayValue = 0.9;
            darkGrayValue = 0.0;
            
            
            hold on;
            for i = 1:length( edges )
                nodes = edges(i).getNodes();
                
                x = [ nodes(1).getX(), nodes(2).getX() ];
                y = [ nodes(1).getY(), nodes(2).getY() ];
                z = [ nodes(1).getZ(), nodes(2).getZ() ];
                
                line( x, y, z, ...
                    'Color',[ darkGrayValue darkGrayValue darkGrayValue ] , ...
                    'Marker', 'none',...
                    'MarkerFaceColor', [ darkGrayValue darkGrayValue darkGrayValue ], ...
                    'MarkerEdgeColor', 'none' );
            end
            
            axis tight;
            grid off;
            
        end
        
        function visualizeSeedpoints(obj, resolution)
            elements = obj.feMesh.getElements();
            domainSeedpoints = [];
            for i=1:length(elements)
                temp = elements(i).getGlobalSeedpoints(resolution);
                for j=1:length(temp)
                    if elements(i).getDomainIndex(temp(j,:)) == 2;% only plot the seedpoints which are in the domain
                        domainSeedpoints = [domainSeedpoints; temp(j,:)];
                    end
                end
            end
            
            plot3(domainSeedpoints(:,1),domainSeedpoints(:,2),domainSeedpoints(:,3),'go');
            
        end
        
        function visualizeFaces(obj)
            faces = obj.gradientJumpPenalisation.getStabilizedFaces();
            for i=1:length(faces)
                
                nodes = faces{i}.getNodes();
                coords = zeros(length(nodes),3);
                
                for j=1:length(nodes)
                    coords(j,:) = nodes(j).getCoords();
                end
                hold all
                if size(coords,1) == 2
                    handle = line(coords(:,1),coords(:,2),coords(:,3));
                    set(handle,'color','red')
                elseif size(coords,1) == 4
                handle = patch(coords(:,1),coords(:,2),coords(:,3),'r');
                alpha(handle,0.6);
                else
                    Logger.ThrowException(['Something wrong with the amount of points given']);
                end 
            end
            
        end
        
        
    end
    
end

