classdef FeMesh2DForVTK < AbsPostProcessingMeshFactory
    
    methods( Access = public )
        
        function obj = FeMesh2DForVTK( gridSizes , fileName)
            obj.GridSizes = gridSizes;
            obj.FileName = fileName;
        end
        
        function fileName=getFileName(obj)
            fileName=obj.FileName;
        end
    
        function postProcessingMesh = createPostProcessingMesh( obj, FeMesh )
            [globalCoordinates, triangles] = obj.getSurfaceMesh( FeMesh );
            numberOfPoints = size( globalCoordinates, 1 );
            resultPoints = ResultPoint.empty(numberOfPoints,0);
            for i = 1:numberOfPoints
                resultPoints(i) = obj.convertToResultPoint( globalCoordinates(i,:), FeMesh );
            end
            postProcessingMesh = PostProcessingMesh( resultPoints, triangles );
        end
    end
    
    methods( Access = private )
    
        function [vertices, tris] = getSurfaceMesh( obj, FeMesh  )
            origin = FeMesh.getMeshOrigin();
            spacings = [ FeMesh.getLX(), FeMesh.getLY() ];
            endPoint = origin + spacings;
            sections = ceil( endPoint - origin)./obj.GridSizes;
            x1D = linspace( origin(1), endPoint(1), sections(1) );
            y1D = linspace( origin(2), endPoint(2), sections(2) );
            [vertices, tris] = obj.getAllVertices(FeMesh, x1D, y1D, 0.0 );
  
        end
        
        function [vertices, tris] = getAllVertices( obj, FeMesh, x1D, y1D, zD1  )
            vertices=[];             
            [x,y,z] = meshgrid( x1D, y1D, zD1 );
            xyz = [x(:),y(:),z(:)];
            vertices=[vertices;xyz];
            tris=[];
       
        end
    end
    
    properties ( Access = private )
        GridSizes;
        FileName;
    end
end