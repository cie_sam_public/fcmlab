%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef FeMeshCurve1D < AbsPostProcessingMeshFactory
        
    methods( Access = public )
        
        function obj = FeMeshCurve1D( GridSize )
            obj.GridSize = GridSize;
        end
        
    end
    
        
    methods( Access = public )
        
        function postProcessingMesh = createPostProcessingMesh( obj, FEMesh )
             xiPoints = linspace(-1,1, max(ceil(2/obj.GridSize),3));
            numberOfPoints = length(xiPoints);
            
            resultPoints = ResultPoint.empty(numberOfPoints,0);
            
              
            meshElements = FEMesh.getElements();
            for iElement = 1:length(meshElements)
                element = meshElements( iElement );
                
                for iLocalPoint = 1:numberOfPoints
                    iGlobalPoint = numberOfPoints * (iElement-1) + iLocalPoint;
                    localCoordinates = xiPoints( iLocalPoint );
                    globalCoodinates = element.mapLocalToGlobal( localCoordinates );

                    resultPoints( iGlobalPoint ) = ResultPoint( element, localCoordinates, globalCoodinates );    
                end
            
            faces = [];
            postProcessingMesh = PostProcessingMesh( resultPoints, faces );
            
        end
        
        end
    end
    
    properties( Access = private )
        GridSize;
    end
    
end

