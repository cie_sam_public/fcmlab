classdef FeMesh3DforVTK < AbsPostProcessingMeshFactory
    
    methods( Access = public )
        
        function obj = FeMesh3DforVTK( gridSizes , fileName)
            obj.GridSizes = gridSizes;
            obj.FileName = fileName;
        end
        
        function fileName=getFileName(obj)
            fileName=obj.FileName;
        end
    
        function postProcessingMesh = createPostProcessingMesh( obj, FeMesh )
            [globalCoordinates, triangles] = obj.getSurfaceMesh( FeMesh );
            numberOfPoints = size( globalCoordinates, 1 );
            resultPoints = ResultPoint.empty(numberOfPoints,0);
            for i = 1:numberOfPoints
                resultPoints(i) = obj.convertToResultPoint( globalCoordinates(i,:), FeMesh );
            end
            postProcessingMesh = PostProcessingMesh( resultPoints, triangles );
        end
    end
    
    methods( Access = private )
    
        function [vertices, tris] = getSurfaceMesh( obj, FeMesh  )
            origin = FeMesh.getMeshOrigin();
            spacings = [ FeMesh.getLX(), FeMesh.getLY(), FeMesh.getLZ() ];
            endPoint = origin + spacings;
            sections = ceil( endPoint - origin)./obj.GridSizes;
            x1D = linspace( origin(1), endPoint(1), sections(1) );
            y1D = linspace( origin(2), endPoint(2), sections(2) );
            z1D = linspace( origin(3), endPoint(3), sections(3) );
            [vertices, tris] = obj.getAllVertices(FeMesh, x1D, y1D, z1D );
  
        end
        
        function [vertices, tris] = getAllVertices( obj, FeMesh, x1D, y1D, z1D  )
            vertices=[];            
            for i=1:length(z1D)  
            [x,y,z] = meshgrid( x1D, y1D, z1D(i) );
            xyz = [x(:),y(:),z(:)];
            vertices=[vertices;xyz];
            end
            tris=[];
       
        end
    end
    
    properties ( Access = private )
        GridSizes;
        FileName;
    end
end