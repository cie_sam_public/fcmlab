%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef FeMeshFreeSurface2D < AbsPostProcessingMeshFactory
    
    methods( Access = public )
        
        function obj = FeMeshFreeSurface2D( gridSizes )
            obj.GridSizes = gridSizes;
        end
        
        %%
        function postProcessingMesh = createPostProcessingMesh( obj, FeMesh )
            
            [globalCoordinates, triangles, localCoordinates, elements] = obj.getSurfaceMesh( FeMesh );
                        
            numberOfPoints = size( globalCoordinates, 1 );
                        
            resultPoints = ResultPoint.empty(numberOfPoints,0);
            for i = 1:numberOfPoints
                resultPoints(i) = ResultPoint( elements{i}, localCoordinates(i,:), globalCoordinates(i,:) ); 
            end
            
            postProcessingMesh = PostProcessingMesh( resultPoints, triangles );
            
        end
        
    end
    
    methods( Access = private )
        
        %%
        function [vertices, tris, localCoordinates, elements] = getSurfaceMesh( obj, FeMesh  )
            
            xiPoints =  linspace(-1,1, max( ceil(2/obj.GridSizes(1)),3) );
            etaPoints = linspace(-1,1, max( ceil(2/obj.GridSizes(2)),3));
            
            meshElements = FeMesh.getElements();
            
            totalNumberOfPoints = length(meshElements)*length(xiPoints)*length(etaPoints);

            vertices = zeros(totalNumberOfPoints, 3);
            localCoordinates = zeros(totalNumberOfPoints, 3);
            tris = zeros(totalNumberOfPoints, 3);
            elements = cell(totalNumberOfPoints, 1);
            
            
            iTriangle = 1;
            iGlobalPoint=0;
            for iElement = 1:length(meshElements)
                element = meshElements(iElement);
                geometricSupport = element.getGeometricSupport();
                                
                X=zeros(1,length(etaPoints)*length(xiPoints));
                Y=zeros(1,length(etaPoints)*length(xiPoints));
                Z=zeros(1,length(etaPoints)*length(xiPoints));

                numberOfVertices = iGlobalPoint;

                    for iEta=1:length(etaPoints)
                        for iXi=1:length(xiPoints)
                            
                            iLocalPoint = (iEta-1)*length(xiPoints) + iXi;
                            iGlobalPoint = (iElement-1)*length(xiPoints)*length(etaPoints) + iLocalPoint;

                            coords = geometricSupport.mapLocalToGlobal([xiPoints(iXi) etaPoints(iEta) 0]);

                            vertices(iGlobalPoint, 1:3) = coords;
                            localCoordinates(iGlobalPoint, 1:3) = [xiPoints(iXi) etaPoints(iEta) 0];
                            elements{iGlobalPoint} = element;

                            X( iLocalPoint ) = coords(1);
                            Y( iLocalPoint ) = coords(2);
                            Z( iLocalPoint ) = coords(3);
                        end
                    end
                    
                    %tris2 =  delaunay( X, Y )  ;
                    aphShape = alphaShape( X', Y' );
                    aphShape.Alpha = 0.95;
                    tris2 = alphaTriangulation(aphShape);
                    tris(iTriangle:iTriangle+size(tris2,1)-1, :) = tris2+numberOfVertices;
                     
                    iTriangle = iTriangle+size(tris2,1);
            end
            
             
        end
        
        function [vertices, tris]  = mergeMeshes( obj, vertices1, surface1, vertices2, surface2 )
            
            vertices = [ vertices1; vertices2];
            
            numberOfVertices = size(vertices1, 1);
            
            triangles2 = surface2 + numberOfVertices;
            tris = [ surface1; triangles2 ];
        end
        
         
        
    end
    
    properties( Access = private )
       
        GridSizes;
    end
    
end

