%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef IgaMeshVisualizer < AbsMeshVisualizer
    
    methods( Access = public )
        
        function obj = IgaMeshVisualizer( igaMesh, realVisualizer, bezierControlPointsFilter, controlPointFilter )
            obj = obj@AbsMeshVisualizer( realVisualizer );
            obj.igaMesh = igaMesh;
            
            if nargin<=2
                bezierControlPointsFilter=cell(0);
            end
            if nargin<=3
                controlPointFilter=cell(0);
            end
            
            obj.bezierControlPointsFilter = bezierControlPointsFilter;
            obj.controlPointFilter = controlPointFilter;
        end
    end
    
    %%
    methods( Access = protected )
        
        function handles =  visualizeMeshes( obj, handles )
            
            for i = 1:length(handles)
                %handles{i} = obj.plotBezierPhysicalMesh( handles{i} );
                %handles{i} = obj.plotControlMesh( handles{i} );
                handles{i} = obj.plotMultiLevelControlMesh( handles{i} );
                
            end
        end
        
    end
    
    %%
    methods( Access = private )
        
        function handle = plotBezierPhysicalMesh( obj, handle )
            
            edges = obj.igaMesh.getEdges();
            set( 0,'CurrentFigure', handle );
            
            fillColor = [0 0 0];
            edgeColor = [0.1 0 0];
            
            
            Xi = -1:0.1:1;
            
            globalCoordinates = zeros(length(Xi), 3);
            hold on;
            for i = 1:length( edges )
                support = edges{i}.getgeometricSupport();
                
                for iXi = 1:length(Xi)
                    globalCoordinates(iXi,:) = support.mapLocalToGlobal([Xi(iXi) 0 0]);
                end
                
                % draw bezier element edges
                line( globalCoordinates(:,1), globalCoordinates(:,2), globalCoordinates(:,3), ...
                    'Color',fillColor );
                
                % draw bezier element local control points
                %circleRadius = 0.03;
                %circleRadius = 0.00001;
                circleRadius = 0.1;
                edgeControlPoints  = support.getControlPoints;
                for iControlPoint = 1:size(edgeControlPoints, 1)
                    
                    controlPoint = edgeControlPoints(iControlPoint, :);
                    pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
                    
%                     rectangle('Position',pos, ...
%                         'Curvature',[1 1], ...
%                         'FaceColor',fillColor , ...
%                         'EdgeColor',edgeColor ...
%                         );
                    
                    filledCircle(controlPoint(1:2),circleRadius/2,10,fillColor);
%                                      viscircles(controlPoint(1:2), circleRadius, ...
%                                              'Color',fillColor , ...
%                                              'EdgeColor',edgeColor ...
%                                           );
                end
                
            end
            
            % draw edges of weak neumann conditions
            
            fillColor = [ 0.5 0.5 0.5 ];
            for iFilter = 1:length( obj.bezierControlPointsFilter )
                
                filter = obj.bezierControlPointsFilter{iFilter};
                for edge = obj.igaMesh.findEdges(filter)
                    support = edge.getgeometricSupport();
                    for iXi = 1:length(Xi)
                        globalCoordinates(iXi,:) = support.mapLocalToGlobal([Xi(iXi) 0 0]);
                    end
                    
                    
                    line( globalCoordinates(:,1), globalCoordinates(:,2), globalCoordinates(:,3), ...
                        'Color',fillColor , ...
                        'LineWidth',3 );
                end
                
            end
            
            axis tight;
            grid off;
        end
        
        
        function handle = plotControlMesh( obj, handle )
            
            controlPoints = obj.igaMesh.getGeometry().getControlPoints();
            numberOfControlPointsInXi = obj.igaMesh.getGeometry().numberOfControlPointsInXi();
            numberOfControlPointsInEta = obj.igaMesh.getGeometry().numberOfControlPointsInEta();
            
            
            
            set( 0,'CurrentFigure', handle );
            
            dimensions = [ ...
                max(controlPoints(:,1))-min(controlPoints(:,1)) ...
                max(controlPoints(:,2))-min(controlPoints(:,2)) ...
                max(controlPoints(:,3))-min(controlPoints(:,3))];
            
            dimensions(dimensions==0) = 1e8;
            
            fillColor = [1 0 0];
            edgeColor = [0.4 0 0];
            %circleRadius = min( dimensions )/30;
            circleRadius = 44;
            
            
            % Draw connecting lines
            
            for iEta=1:numberOfControlPointsInEta
                ix = (iEta-1)*numberOfControlPointsInXi + (1:numberOfControlPointsInXi);
                line( controlPoints( ix,1), controlPoints(ix,2), ...
                    'Color', edgeColor , ...
                    'Marker', 'none',...
                    'LineWidth', 0.2, ...
                    'LineStyle','--');
            end
            
            
            for iXi=1:numberOfControlPointsInXi
                ix = iXi+1 : numberOfControlPointsInXi : numberOfControlPointsInXi*numberOfControlPointsInEta;
                
                line( controlPoints( ix,1), controlPoints(ix,2), ...
                    'Color', edgeColor , ...
                    'Marker', 'none',...
                    'LineWidth', 0.2, ...
                    'LineStyle','--');
            end
            
            % Draw control points
            
            for iControlPoint = 1:size(controlPoints, 1)
                controlPoint = controlPoints(iControlPoint, :);
                pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
                rectangle('Position',pos, ...
                    'Curvature',[1 1], ...
                    'FaceColor',fillColor , ...
                    'EdgeColor',edgeColor ...
                    );
            end
            
            
            % Draw strong Dirichlet control points
            
            dirichletCPfillColor = [ 1 1 0 ];
            for iFilter = 1:length( obj.controlPointFilter )
                filter = obj.controlPointFilter{iFilter};
                
                fcps = obj.igaMesh.getDegreesOfFreedom(filter,1);
                for iControlPoint =  fcps';
                    controlPoint = controlPoints(iControlPoint, :);
                    pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
                    rectangle('Position',pos, ...
                        'Curvature',[1 1], ...
                        'FaceColor',dirichletCPfillColor , ...
                        'EdgeColor',dirichletCPfillColor ...
                        );
                end
                
            end
            
            axis tight;
            grid off;
        end
        
        function handle = plotMultiLevelControlMesh( obj, handle )
            
            multiLevelgeometry = obj.igaMesh.getGeometry();
            levelGeometries = multiLevelgeometry.getGeometries();
            
            hold on;
            for iLevel = 1:length(levelGeometries)
                
                geometry = levelGeometries{iLevel};
                [Aa, ~, ~ ,~] = multiLevelgeometry.getRefinementDataStructures;
                Aa = Aa{iLevel};
                Aa = logical(Aa(:));
                
                controlPoints = geometry.getControlPoints();
                numberOfControlPointsInXi = geometry.numberOfControlPointsInXi();
                numberOfControlPointsInEta = geometry.numberOfControlPointsInEta();
                
                
                
                set( 0,'CurrentFigure', handle );
                
                dimensions = [ ...
                    max(controlPoints(:,1))-min(controlPoints(:,1)) ...
                    max(controlPoints(:,2))-min(controlPoints(:,2)) ...
                    max(controlPoints(:,3))-min(controlPoints(:,3))];
                
                dimensions(dimensions==0) = 1e8;
                
                %%fillColor = [iLevel/length(levelGeometries) 0 0];
                %fillColor = [1-(iLevel-1)/2 0 0];
                fillColor = [1-(1-1)/2 0 0];
                edgeColor = [0.4 0 0];
                %circleRadius = min( dimensions )/30;
                
                %circleRadius = 0.15;
                %circleRadius = 0.07;
                circleRadius = 0.04;
                
                
                % Draw connecting lines
                
                for iEta=1:numberOfControlPointsInEta
                    ix = (iEta-1)*numberOfControlPointsInXi + (1:numberOfControlPointsInXi);
                    
                    ix = ix(Aa(ix));
                    
                    line( controlPoints( ix,1), controlPoints(ix,2), ...
                        'Color', edgeColor , ...
                        'Marker', 'none',...
                        'LineWidth', 0.2, ...
                        'LineStyle','--');
                end
                
                
                for iXi=1:numberOfControlPointsInXi
                    ix = iXi+1 : numberOfControlPointsInXi : numberOfControlPointsInXi*numberOfControlPointsInEta;
                    
                    ix = ix(Aa(ix));
                    line( controlPoints( ix,1), controlPoints(ix,2), ...
                        'Color', edgeColor , ...
                        'Marker', 'none',...
                        'LineWidth', 0.2, ...
                        'LineStyle','--');
                end
                
                % Draw control points
                
                for iControlPoint = 1:size(controlPoints, 1)
                    if Aa(iControlPoint)
                        controlPoint = controlPoints(iControlPoint, :);
                        pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
                        
                        if iLevel==1
%                             rectangle('Position',pos, ...
%                                 'Curvature',[1 1], ...
%                                 'FaceColor',fillColor , ...
%                                 'EdgeColor',edgeColor ...
%                                 );
                            
                            filledCircle(controlPoint(1:2),circleRadius/2,10,fillColor);
                        elseif iLevel==2
                                %rectangle('Position',pos, ...
                                %'FaceColor',fillColor , ...
                                %'EdgeColor',edgeColor ...
                                %);
                                a=circleRadius*0.95;
                                h=sqrt(2)/2*a;
                                fill( [controlPoint(1) controlPoint(1)+h controlPoint(1) controlPoint(1)-h  ], ...
                                 [controlPoint(2)-h controlPoint(2) controlPoint(2)+h controlPoint(2)], ...
                                 fillColor ...
                                 );
                        else
                            a=circleRadius*0.95;
                            h=sqrt(3)/2*2*a;
                                 fill( [controlPoint(1)-a/2 controlPoint(1)+a/2 controlPoint(1)  ], ...
                                 [controlPoint(2)-h/3 controlPoint(2)-h/3 controlPoint(2)+h/3], ...
                                 fillColor ...
                                 );
                        end
                        %filledCircle(controlPoint(1:2),circleRadius,10,fillColor);
                    end
                end
                
                
                % Draw strong Dirichlet control points
                
                dirichletCPfillColor = [ 1 1 0 ];
                for iFilter = 1:length( obj.controlPointFilter )
                    filter = obj.controlPointFilter{iFilter};
                    
                    fcps = obj.igaMesh.getDegreesOfFreedom(filter,1);
                    for iControlPoint =  fcps';
                        controlPoint = controlPoints(iControlPoint, :);
                        pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
                        rectangle('Position',pos, ...
                            'Curvature',[1 1], ...
                            'FaceColor',dirichletCPfillColor , ...
                            'EdgeColor',dirichletCPfillColor ...
                            );
                        %filledCircle(controlPoint(1:2),circleRadius,10,dirichletCPfillColor);
                    end
                    
                end
                
            end
            axis tight;
            grid off;
            hold off;
        end
        
        
    end
    
    properties( Access = private )
        igaMesh
        bezierControlPointsFilter
        controlPointFilter
    end
    
end

