classdef SolutionAsVTK2DVisualizer < AbsVisualizer
    
    methods( Access = public )
        
        %%
        function obj = SolutionAsVTK2DVisualizer( mesh, timeStepToVisualize, gridSizes,filename )
            obj.gridSizes=gridSizes;
            obj.FeMesh=mesh;
            obj.fileName=filename;
            obj.timeStepToVisualize = timeStepToVisualize;
        end
        
        %%
        function handles = visualizeResults( obj, postProcessingMesh, results, resultLabel )
            [ xyz, tris ] = postProcessingMesh.getPostProcessingSurface();
            
            Lx = obj.FeMesh.getLX;
            Ly = obj.FeMesh.getLY;
            origin = obj.FeMesh.getMeshOrigin;
            DomainMeasurement = [ Lx, Ly ];
            endPoint = origin + DomainMeasurement;
            numberOfPointsInXYZ = floor(ceil( endPoint - origin)./obj.gridSizes);
            
            s=size(xyz,1);
            dimensionOfResults=size(results,1);
            coordsAndResults=zeros(s,dimensionOfResults+3);

            for i=1:s
                coordsAndResults(i,1:dimensionOfResults)=results(1:dimensionOfResults,i);
                coordsAndResults(i,dimensionOfResults+1:dimensionOfResults+3)=xyz(i,:);
            end   
            
            coordsAndResults=unique(coordsAndResults,'rows');
            
            A=sortrows(coordsAndResults,linspace(dimensionOfResults+3,dimensionOfResults+1,3));
            
            fileNameTimeStep = strcat(obj.fileName, '_', int2str(obj.timeStepToVisualize.getTimeStep-1), '.vtk');
            
            h = fopen(fileNameTimeStep,'wt');
            fprintf(h, '# vtk DataFile Version 3.0\n');
            fprintf(h, 'TravellingHeatSource2DIgA\n');
            fprintf(h, 'ASCII\n');
            fprintf(h, 'DATASET STRUCTURED_POINTS\n');
            fprintf(h, 'DIMENSIONS %d %d %d\n',numberOfPointsInXYZ(1),numberOfPointsInXYZ(2),1);
            fprintf(h, 'ORIGIN %d %d %d\n',origin(1),origin(2), 0.0) ;
            
            fprintf(h, 'SPACING %d %d %d\n',obj.gridSizes(1),obj.gridSizes(2), 0.0);
            fprintf(h, 'POINT_DATA %d\n', numberOfPointsInXYZ(1) * numberOfPointsInXYZ(2) );
            labelstr = char(strcat('SCALARS',{' '}, resultLabel,{' '},'double','\n'));
            fprintf(h, labelstr ,dimensionOfResults);
            fprintf(h, 'LOOKUP_TABLE default\n');
            for i=1:size(A,1)
                fprintf(h, '%d ',A(i,1:dimensionOfResults) );
                fprintf(h,'\n');
            end
            fclose(h);
            
            handles={};
        end
        
        
    end
    
    properties( Access = private )
        FeMesh
        gridSizes
        fileName
        timeStepToVisualize
    end
    
end

