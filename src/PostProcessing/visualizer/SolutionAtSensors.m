classdef SolutionAtSensors < AbsVisualizer
    
    methods( Access = public )
        
        function sensorData=getSensorData(obj)
            sensorData=obj.sensorData;
        end
        
        %%
        function obj = SolutionAtSensors( mesh, gridSizes,sensors )
            obj.gridSizes=gridSizes;
            obj.FeMesh=mesh;
            obj.sensors=sensors;
            obj.sensorData=zeros( size(sensors,1) , 1 );
        end
        
        %%
        function handles = visualizeResults( obj, postProcessingMesh, results, resultLabel )
            [ xyz, tris ] = postProcessingMesh.getPostProcessingSurface();
            if size(results,1)==3
                for i=1:size(obj.sensors,1)
                    obj.sensorData(i,1) = results( obj.sensors(i,4) , i);
                end
            elseif size(results,1)==2
                for i=1:size(obj.sensors,1)
                    obj.sensorData(i,1) = results( obj.sensors(i,4) , i);
                end       
            elseif size(results,1)==1
                for i=1:size(obj.sensors,1)
                    obj.sensorData(i,1) = results( 1 , i);
                end    
            end
            handles=[];
        end
        
        
    end
    
    properties( Access = private )
        FeMesh
        gridSizes
        sensors
        sensorData
    end
    
end

