%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef EnergyNormError < AbsDerivativeBasedProcessor
    
    
    methods( Access = public )
        
        function obj = EnergyNormError( solutionNumber, analyticalStrain, domainIndex, scale )
            obj = obj@AbsDerivativeBasedProcessor( solutionNumber, 'Energy Norm Error', domainIndex );
            obj.analyticalStrain = analyticalStrain;
            obj.scale = scale;
        end
        
    end
    
    methods( Access = protected )
        
        function setup( obj, resultPoint )
            
                    element = resultPoint.getElement();
                    localCoords = resultPoint.getLocalCoordinates();
                    obj.globalCoords = resultPoint.getGlobalCoordinates();
                    obj.B = element.getB( localCoords );            
                    material = element.getMaterial;
                    obj.C = material(obj.domainIndex).getMaterialMatrix(obj.globalCoords);
                    obj.problemDimensionality = element.getDofDimension();               
        end
        function result = evaluateProcessor( obj, dofs )
            
            strainError = obj.analyticalStrain(obj.globalCoords) - obj.B * dofs;
            result = obj.scale * 0.5 *  strainError' * obj.C * strainError;
            
        end
        
    end
    
    
    properties
        globalCoords
        scale
        analyticalStrain
    end
    
end

