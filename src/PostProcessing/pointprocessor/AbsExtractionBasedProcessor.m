%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef AbsExtractionBasedProcessor < AbsResultPointProcessor
    
    methods( Access = public )
        
        function obj = AbsExtractionBasedProcessor( solutionNumbers, label ,domainIndex )
            obj = obj@AbsResultPointProcessor( solutionNumbers, label );
            obj.domainIndex = domainIndex;
        end
        
        function result = evaluate( obj, resultPoint )
            
            obj.resultPoint = resultPoint;
            
            obj.element = resultPoint.getElement();
            
            obj.setup( resultPoint );
            
            numberOfModes = length( obj.solutionNumbers );
            
            for i = 1:numberOfModes
                result( :, i ) = obj.evaluateProcessor(i);
            end
            
        end
        
    end
    
    methods( Access = protected )
        
        function setup( obj, resultPoint )
            
            localCoords = resultPoint.getLocalCoordinates();
            
            obj.N = obj.element.getShapeFunctionsMatrix( localCoords );
            
            obj.resultContainer = obj.element.getPostResults;
            
            obj.postProcessContainer = obj.element.getPostProcessContainer;
            
            if ~isKey(obj.postProcessContainer,0)
                % "0" is the key of the M-Matrix
                M = obj.element.doDomainIntegration(@obj.getIntegrandM);
                obj.postProcessContainer(0) = M;
                obj.element.setPostProcessContainer(obj.postProcessContainer);
            end
            
            obj.setResultID;
        end
        
        function result = evaluateProcessor( obj, solutionNumber)
            
            obj.loadCase = solutionNumber;
            
            M = obj.postProcessContainer(0);
            
            % Check if F-Matrix has been calculated before for this load
            % case and result:
            if ~isKey(obj.postProcessContainer,solutionNumber)
                loadCasePostProcessContainer = containers.Map('KeyType','int32','ValueType','any');
            else
                loadCasePostProcessContainer = obj.postProcessContainer(solutionNumber);
            end
            
            if ~isKey(loadCasePostProcessContainer,obj.ID_Result)
                F = obj.element.doDomainIntegration(@obj.getIntegrandF);
                loadCasePostProcessContainer(obj.ID_Result) = F;
                obj.postProcessContainer(solutionNumber) = loadCasePostProcessContainer;
                obj.element.setPostProcessContainer(obj.postProcessContainer);                
            else
                F = loadCasePostProcessContainer(obj.ID_Result);
            end
            
            % M * c = F
            c = M\F; % calculate coefficient of shape functions for projection
            
            % calculate result at resultPoint
            resultAtResultPoint = obj.N * c;
            
            result = obj.calcResult(resultAtResultPoint);
        end
        
        function integrandM = getIntegrandM(obj,Coord)
            % Coord is GP Coordinates
            shapeFuMatrix = obj.element.getShapeFunctionsMatrix( Coord );
            integrandM = shapeFuMatrix' * shapeFuMatrix;
        end
        
        function integrandF = getIntegrandF(obj,Coord)
            % Coord is GP Coordinates
            shapeFuMatrix = obj.element.getShapeFunctionsMatrix( Coord );
            result = obj.getResultAtGP(Coord);
            integrandF = shapeFuMatrix'*result;
        end
        
        function result = getResultAtGP(obj,Coord)
            GP_ID = obj.element.getGPID(Coord);
            GPResults = obj.resultContainer(GP_ID);
            loadCaseResults = GPResults(obj.loadCase);
            noOfIncrements = length(loadCaseResults); % get number of last increment
            incrementResults = loadCaseResults(noOfIncrements); % get results for last increment
            result = incrementResults(obj.ID_Result);
        end
    end
    
    methods(Abstract, Access = public )
       setResultID(obj)
       calcResult(obj, resultAtResultPoint)
    end
    
    properties( Access = protected )
        domainIndex
        resultContainer
        N
        postProcessContainer
        loadCase  
        ID_Result
    end
end

