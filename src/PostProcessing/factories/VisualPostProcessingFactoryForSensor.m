classdef VisualPostProcessingFactoryForSensor < AbsVisualPostProcessingFactory
    
 methods( Access = public )
        
        function obj = VisualPostProcessingFactoryForSensor( FeMesh, gridSizes ,sensors)
            obj.FeMesh = FeMesh;           
            obj.gridSizes = gridSizes;
            obj.sensors = sensors;
        end
    end
    
    methods
        
        function visualizer = createVisualizer( obj )
           
                      
            visualizer = SolutionAtSensors( obj.FeMesh, obj.gridSizes,obj.sensors );
            
        end
        
        function postProcessingMeshFactory = createPostProcessingMeshFactory( obj )
            
            postProcessingMeshFactory = OnlySomePoints( obj.sensors );
           
         end
        
    end
    
    properties( Access = private )
        FeMesh;
        gridSizes;
        sensors
        BoundaryFactories;
    end
    
    
end

