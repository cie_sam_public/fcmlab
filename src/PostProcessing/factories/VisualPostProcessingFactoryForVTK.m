classdef VisualPostProcessingFactoryForVTK < AbsVisualPostProcessingFactory
    
 methods( Access = public )
        
        function obj = VisualPostProcessingFactoryForVTK( FeMesh, gridSizes, fileName )
            obj.FeMesh = FeMesh;           
            obj.gridSizes = gridSizes;
            if nargin < 3
                obj.fileName = 'default.vtk';
            else
                obj.fileName = fileName;
            end
        end
        
         function filename=getFilename(obj)
            filename=obj.fileName;
        end
    end
    
    methods
        
        function visualizer = createVisualizer( obj )
           
            surfaceVisualizer = SolutionAsVTKVisualizer( obj.FeMesh, obj.gridSizes,obj.fileName );            
            visualizer = FeMeshVisualizer( obj.FeMesh, surfaceVisualizer );
            
        end
        
        function postProcessingMeshFactory = createPostProcessingMeshFactory( obj )
            
            postProcessingMeshFactory =  FeMesh3DforVTK( obj.gridSizes, obj.fileName );
           
         end
        
    end
    
    properties( Access = private )
        FeMesh;
        gridSizes;
        fileName
        BoundaryFactories;
    end
    
    
end

