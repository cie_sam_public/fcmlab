classdef VisualPostProcessingFactoryIGA2DForVTK < AbsVisualPostProcessingFactory
    
 methods( Access = public )
        
        function obj = VisualPostProcessingFactoryIGA2DForVTK( FeMesh, gridSizes,...
                solutionNumberToProcess, fileName, bezierControlPointsFilter, controlPointFilter )
            obj.FeMesh = FeMesh;           
            obj.gridSizes = gridSizes;
            obj.solutionNumberToProcess = solutionNumberToProcess;
            
            if nargin < 4
                fileName = 'default.vtk';
            end
            if nargin < 5
                bezierControlPointsFilter= cell(0);
            end
            if nargin < 6 
                controlPointFilter=cell(0);
            end
            obj.fileName = fileName;
            obj.bezierControlPointsFilter = bezierControlPointsFilter;
            obj.controlPointFilter = controlPointFilter;
        end
        
         function filename=getFilename(obj)
            filename=obj.fileName;
         end
        
         function mesh = getFeMesh(obj)
             mesh = obj.FeMesh;
         end
         
         function obj = setFeMesh(obj, mesh)
             obj.FeMesh = mesh;
         end
    end
    
    methods
        
        function visualizer = createVisualizer( obj )
           
            surfaceVisualizer = SolutionAsVTK2DVisualizer( obj.FeMesh,...
                obj.solutionNumberToProcess, obj.gridSizes, obj.fileName );            
            visualizer = IgaMeshVisualizer( obj.FeMesh, surfaceVisualizer,...
                obj.bezierControlPointsFilter, obj.controlPointFilter );
            
        end
        
        function postProcessingMeshFactory = createPostProcessingMeshFactory( obj )
            
            postProcessingMeshFactory =  FeMesh2DForVTK( obj.gridSizes, obj.fileName );
           
         end
        
    end
    
    properties( Access = private )
        FeMesh;
        gridSizes;
        solutionNumberToProcess;
        fileName;
        bezierControlPointsFilter;
        controlPointFilter;
    end
    
    
end

