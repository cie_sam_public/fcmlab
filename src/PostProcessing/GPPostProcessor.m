%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function GPPostProcessor(feMesh, elementID, GPID)
% This function plots the results of a Gauss Point over the load history in
% a nonlinear analysis

% Get element and information from it
try % check if user input is valid
    element = feMesh.getElement(elementID);
catch
    Logger.ThrowException('Element not existing in this Mesh!');
end

PostResults = element.getPostResults;
stressID = element.getStressID;
epsilonElastID = element.getEpsilonElastID;
epsilonPlastID = element.getEpsilonPlastID;

try % check if user input is valid
    GPResults = PostResults(GPID);
catch
    Logger.ThrowException('Gauss Point not existing in this Element!');
end

NoLoadSteps = length(GPResults);
NoLoadIncr = length(GPResults(1));

ResultMatrix = zeros(NoLoadSteps*NoLoadIncr,4);

% Loop to extract results from element
index = 0;
for lc = 1:NoLoadSteps
    LoadCaseResults = GPResults(lc);
    for linc = 1:NoLoadIncr;
        Results = LoadCaseResults(linc);
        index = index + 1;
        ResultMatrix(index,1) = index;
        ResultMatrix(index,4) = Results(epsilonPlastID);
        
        eps = Results(epsilonElastID);
        str = Results(stressID);
        
        if length(str) == 2; % 2D
            misesStr = sqrt(str(1,1)^2 + str(2,2)^2 - str(1,1)*str(2,2) + 3*str(1,2)^2);
            misesEps = sqrt(eps(1,1)^2 + eps(2,2)^2 - eps(1,1)*eps(2,2) + 3*eps(1,2)^2);
            ResultMatrix(index,2) = misesEps + ResultMatrix(index,4); % Ee + Ep
            ResultMatrix(index,3) = misesStr; % S
            
        elseif length(str) == 3; % 3D
            J2_str = ((str(1,1)-str(2,2))^2 + (str(2,2)-str(3,3))^2 + (str(3,3)-str(1,1))^2)/6 + str(1,2)^2 + str(3,2)^2 + str(1,3)^2;
            J2_eps = ((eps(1,1)-eps(2,2))^2 + (eps(2,2)-eps(3,3))^2 + (eps(3,3)-eps(1,1))^2)/6 + eps(1,2)^2 + eps(3,2)^2 + eps(1,3)^2;
            
            misesStr = sqrt(3*J2_str);
            misesEps = sqrt(3*J2_eps);
            ResultMatrix(index,2) = misesEps + ResultMatrix(index,4); % Ee + Ep
            ResultMatrix(index,3) = misesStr; % S
            
        else %1D
            ResultMatrix(index,2) = eps + ResultMatrix(index,4); % Ee + Ep
            ResultMatrix(index,3) = str; % S
        end
    end
end

% extract columns of ResultMatrix to plot them
indices = ResultMatrix(:,1);
strain = ResultMatrix(:,2);
stress = ResultMatrix(:,3);
plasticStrain = ResultMatrix(:,4);

% Plot stress-strain
figure('Name','Gauss Point Result','NumberTitle','off')
plot(strain,stress,'k-x','linewidth', 1.5);
title(['GP Result, Element: ', num2str(elementID),', Gauss Point: ',num2str(GPID)]);
xlabel('Strain'); ylabel('Stress')
grid on
grid minor

% Plot development of accumulated plastic strain
figure('Name','Gauss Point Result','NumberTitle','off')
plot(indices,plasticStrain,'b-x','linewidth', 1.5);
title(['GP Result, Element: ', num2str(elementID),', Gauss Point: ',num2str(GPID)]);
xlabel('Load Increment')
ylabel('Accumulated Plastic Strain')
grid on
grid minor

end