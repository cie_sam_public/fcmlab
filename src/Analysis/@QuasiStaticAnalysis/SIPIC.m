%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm for SIPIC preconditioner for SPD matrices      %
% Eindhoven University of Technology                       %
% 21-4-2016                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function S = SIPIC(A,gmma,epsilon)

if (nargin < 3) || isempty(epsilon)
  epsilon = 100*eps; %elimination tolerance
end
if (nargin < 2) || isempty(gmma)
  gmma = 0.9;        %orthonormalization tolerance
end

n = length(A);                       %matrix size
p = true(n,1);                       %(un)eliminated functions
S = spdiags(1./sqrt(diag(A)),0,n,n); %initial (scaling) preconditioner
Sigma = {};                          %set of grouped dependencies
[I,J] = find(abs(tril(S*A*S',-1))>gmma);  %dependence indices

iteration = 0;
while ~isempty(I) && iteration < 10  %if there are dependencies

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % group dependencies                                       %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % The dependencies loop over all groups in Sigma. If the 
    % dependency intersects a group, the group is added to the
    % dependency to form a new group. After the loop over all
    % groups, the dependency (new group) is added to Sigma and
    % the old group(s) is(are) removed.

    for dependency_index = 1:length(I)
        dependency = [I(dependency_index),J(dependency_index)];
        group_removals = []; %groups to be removed
        for group_index = 1:length(Sigma)
            group = Sigma{group_index};
            if intersect(group,dependency)
                dependency = union(group,dependency); %new group
                group_removals(end+1) = group_index;  %remove old group
            end
        end
        Sigma(group_removals) = []; %remove old group(s)
        Sigma{end+1} = dependency;  %add new group
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % orthonormalize dependencies                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % First, the groups are sorted to minimize additional fill-in.
    % For every group Gram-Schmidt orthonormalization is applied.
    % Functions that are smaller than epsilon after
    % orthogonalization are not rescaled but eliminated.

    for group_index = 1:length(Sigma)
        group = Sigma{group_index};
        [~,order] = sort(sum(A(group,p)~=0,2)); %define order
        group = group(order);                   %set order
        A_group = A(group,group);               %local matrix A
        S_group = diag(1./sqrt(diag(A_group))); %local Preconditioner S
        for i = 1 : length(group)
            for j = 1 : i-1                     %orthogonalise to all j<i
                S_group(i,:) = S_group(i,:) ...
                    - (S_group(i,:)*A_group*S_group(j,:)')*S_group(j,:);         
            end
            if (S_group(i,:)*A_group*S_group(i,:)') <= epsilon %check norm
              S_group(i,:) = 0;    %set value to 0 for further GM process
              p(group(i)) = false; %eliminate function
            else
              S_group(i,:) = ...   %rescale function
                  S_group(i,:)/sqrt(S_group(i,:)*A_group*S_group(i,:)');
            end
        end
        S(group,group) = S_group;  %assemble
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % check if new dependencies have emerged                   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [I,J] = find(abs(tril(S*A*S',-1))>gmma); %dependence indices
    iteration = iteration + 1;
        
end

S = S(p,:); %return output
