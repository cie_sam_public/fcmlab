function visualizeSensorDataOverTime(obj)
    dt=obj.endTime/obj.numberOfTimeSteps;
    for t=1:obj.numberOfTimeSteps+1    
       time(t)=(t-1)*dt; 
       if (~isempty(obj.postProcessor))
            obj.scatterSolution(obj.solutionVectors(:,t));
            obj.postProcessor.visualizeResults( obj.meshes );
            sensorData(:,t)=obj.postProcessor.getVisualizer.getSensorData;
       end    
    end
       
    figure
    hold on
    for s=1:size(sensorData,1)
        subplot(size(sensorData,1),1,s); 
        plot( time , sensorData(s,:) );
        title(['Sensor Nr. ',num2str(s)]);
        xlabel('time');
        ylabel('Displacement');
    end
    hold off
end
