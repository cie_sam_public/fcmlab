classdef NewmarkWaveEquation < AbsAnalysis
   %%
    methods (Access = public)
        % constructor
        function obj = NewmarkWaveEquation(meshFactories,numberOfTimeSteps, endTime,initialCondition, timeState)
            obj = obj@AbsAnalysis(meshFactories);
            obj.numberOfTimeSteps=numberOfTimeSteps;
            obj.endTime=endTime;
            obj.initialCondition=initialCondition;
            obj.currentTimeState=timeState;
            
        end
        
        % Add Prescribed Loads
        function addLoadCases(obj,loadCases) 
            obj.loadCases = [obj.loadCases loadCases];
            Logger.Log([num2str(length(loadCases)) ' Load Cases Added......'],'release');
        end
        
        function initialCondition = getInitialCondition(obj)
            initialCondition = obj.initialCondition;
        end
        
        function endTime = getEndTime(obj)
            endTime = obj.endTime;
        end
        
        function timeState=getTimeState(obj)
            timeState=obj.currentTimeState;
        end
        
        function numberOfTimeSteps = getNumberOfTimeSteps(obj)
            numberOfTimeSteps = obj.numberOfTimeSteps;
        end
        
        function solution = getSolutionVectors(obj)
            solution = obj.solutionVectors;
        end
        
        function movie = getMovie(obj)
            movie = obj.movie;
        end
                
        function registerPostProcessor( obj, postProcessor )
            obj.postProcessor = postProcessor;
        end
        
        function strainEnergy = getStrainEnergy(obj)
            strainEnergy = obj.strainEnergy;
        end
        
        function kineticEnergy = getKineticEnergy(obj)
            kineticEnergy = obj.kineticEnergy;
        end
        
        solutionVectors = solve(obj);
        
    end
   
  %%
    properties (Access = private)
        numberOfTimeSteps
        currentTimeState
        endTime
        initialCondition
        loadVector
        solutionVectors
        postProcessor
        movie
        
        strainEnergy
        kineticEnergy
    end 
    
end

