function solutionVectors = solve(obj)

            if(size(obj.loadCases) ~= 1)
                ME = MException('Invalid number of Load cases');
                throw(ME);
            end
            
            %Newmark Parameters
            beta = 1/4;
            gamma = 1/2;
             
            totalNumberOfDofs = obj.getTotalNumberOfDofs();
            for k=1:totalNumberOfDofs
            obj.solutionVectors(k,1) = obj.initialCondition(1);
            oldVelocity(k,1) = obj.initialCondition(2);
            oldAcceleration(k,1) = obj.initialCondition(3);
            end
            
            endTime = obj.getEndTime;
            startTime = obj.getTimeState.getTime;
            numberOfTimeSteps = obj.numberOfTimeSteps;
            dt=(endTime-startTime)/numberOfTimeSteps;
            
            %coefficients for later use
            a1=1/(beta*dt*dt);
            a2=1/(beta*dt);
            a3=(1-2*beta)/(2*beta);
            a4=gamma*dt;
            a5=(1-gamma)*dt;
            
            Logger.TaskStart('Computing multi-mesh mass matrix M','release');
            massMatrix = obj.computeMassMatrix(totalNumberOfDofs);
            Logger.TaskFinish('Computing multi-mesh mass matrix M','release');
            
            Logger.TaskStart('Computing multi-mesh stiffness matrix K','release');
            stiffnessMatrix = obj.computeStiffnessMatrix(totalNumberOfDofs);
            Logger.TaskFinish('Computing multi-mesh stiffness matrix K','release');
            
            stiffnessMatrixUncoupled = stiffnessMatrix;
            massMatrixUncoupled = massMatrix;
            
            Logger.TaskStart('Multi-mesh coupling','release');
            stiffnessMatrix = obj.applyInterfaceCoupling(stiffnessMatrix);
            Logger.TaskFinish('Multi-mesh coupling','release');
            
            K_s = stiffnessMatrix;
            K_m = massMatrix;
            K_eff = a1*K_m + K_s;
            
            obj.strainEnergy(1) = 1/2 * obj.solutionVectors(:,1)' * stiffnessMatrixUncoupled * obj.solutionVectors(:,1);
            obj.kineticEnergy(1) = 1/2 * oldVelocity' * massMatrixUncoupled * oldVelocity;
            
            Logger.TaskStart('Solving','release');
            for t=2:numberOfTimeSteps+1
                obj.currentTimeState.updateTime(dt);
                loadVector = obj.computeLoadVectors(totalNumberOfDofs);
                F_eff = loadVector+(a1*K_m*obj.solutionVectors(:,t-1) ) + (a2*K_m*oldVelocity) + (a3*K_m*oldAcceleration);
                
                [K_effWithDRB ,F_effWithDRB] = obj.applyDirichletBoundaryConditions(K_eff,F_eff);
                
                obj.solutionVectors(:,t) = K_effWithDRB\F_effWithDRB;
                newAcceleration = a1*(obj.solutionVectors(:,t) - obj.solutionVectors(:,t-1) ) - a2*oldVelocity - a3*oldAcceleration;
                newVelocity = oldVelocity + a4*newAcceleration + a5*oldAcceleration;
                                 
              
                oldAcceleration=newAcceleration;
                oldVelocity=newVelocity; 
                
                obj.strainEnergy(t) = 1/2 * obj.solutionVectors(:,t)' * stiffnessMatrixUncoupled * obj.solutionVectors(:,t);
                obj.kineticEnergy(t) = 1/2 * oldVelocity' * massMatrixUncoupled * oldVelocity;
            end
            Logger.TaskFinish('Solving','release');
            
            solutionVectors = obj.solutionVectors;
                        
        end  
