function save3DSolutionVectorsToVTK(obj, fileName)
   for t=1:obj.numberOfTimeSteps+1    
        if (~isempty(obj.postProcessor))
            obj.scatterSolution(obj.solutionVectors(:,t));
            obj.postProcessor.visualizeResults( obj.meshes );
        end
        a=[fileName num2str(t) '.vtk'];
        movefile(obj.postProcessor.getPostProcessingMeshFactory.getFileName,a);
   end
end
