function createMovie2D(obj,name)

    for t=1:obj.numberOfTimeSteps+1    
        if (~isempty(obj.postProcessor))
            obj.scatterSolution(obj.solutionVectors(:,t));
            obj.postProcessor.visualizeResults( obj.meshes );
        end
        
        F(t) = getframe;
        close all;
                
    end
    obj.movie=F;
    movie(F,3);

   % movie2avi(F, name, 'compression', 'FFDS');
end     