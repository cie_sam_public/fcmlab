%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function convergencePlot(~, k, convArray, iterArray, incr, convCriteria, lc, noLoadCases, iter)
% This function plots the convergence graph

    figure(1);
    %Plot number of iterations per load increment
    subplot(2,1,1); % load increments
    z = 1/incr:1/incr:1;
    bar(z(1:iter),iterArray,0.4,'b');
    title(['Convergence Plot for Loadcase:  ', num2str(lc),' / ',num2str(noLoadCases)]);
    xlabel('load increment lambda');  ylabel('number of iterations');
    xlim([0 1.05]);
    
    % Plot Convergence of error
    subplot(2,1,2); 
    semilogy(convArray(:,1),convArray(:,2),'bx-','linewidth', 2,'MarkerEdgeColor','k');
    lineConvCrit = line([1 k],[convCriteria convCriteria],'Color','r','Linewidth',1.5); % Convergence criteria
    xlabel('number of iteration');  ylabel('error');
    xlim([1 inf]);  ylim([convCriteria/10 inf]);
    legend(lineConvCrit,'Convergence Criteria')
    drawnow; % forces imidiate plotting

end

