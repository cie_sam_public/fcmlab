%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% Solver
function [solutionVectors, strainEnergy] = solve(obj,NoIncrements)

totalNumberOfDofs = obj.getTotalNumberOfDofs();

loadVectors = obj.computeLoadVectors(totalNumberOfDofs);

% Initialize zero solution vector
[d1,noLoadCases] = size(loadVectors);
obj.solutionVectors = zeros(d1,1);
obj.scatterSolution(obj.solutionVectors);

incr = NoIncrements;
maxIterations = 10;
convCriteria = 1E-8;

for lc = 1:noLoadCases % LOOP LOADCASES
    fprintf('\n\n');
    fprintf(['******* loadcase = ',num2str(lc),' ******************************\n']);
    for lambda = 1/incr:1/incr:1 % LOOP LOAD INCREMENTS; 0...1
        fprintf('\n');
        fprintf(['===== lambda = ',num2str(lambda),' =====\n']);
        error = 1; % needed to start iterations
        k=0; % iteration counter
        while error > convCriteria && k < maxIterations % LOOP ITERATIONS
            k = k + 1; % Update iteration counter
            
            fprintf('\n');
            fprintf(['--- iteration = ',num2str(k),' ---\n']);
            % Assemble Stiffness Matrix
            stiffnessMatrix = obj.computeStiffnessMatrix(totalNumberOfDofs);
            
            % constrain stiffness matrix and load vector
            [stiffnessMatrix, loadVectors] = obj.applyDirichletBoundaryConditions(stiffnessMatrix,loadVectors);
            stiffnessMatrix = obj.applyInterfaceCoupling(stiffnessMatrix);
            
            % compute and constrain internal force vector
            internalForce = obj.computeInternalForce;
            
            [~, internalForce] = obj.applyDirichletBoundaryConditions(stiffnessMatrix,internalForce);
            
            
            % gradually apply the load taking the previous load as start
            if lc == 1
                externalForce = lambda*loadVectors(:,lc);
            else
                deltaFext = loadVectors(:,lc)-loadVectors(:,lc-1);
                externalForce = loadVectors(:,lc-1) + lambda*deltaFext;
            end
            
            % compute residual
            residual = internalForce - externalForce;
            
            % compute displacement update
            deltaU = - stiffnessMatrix \ residual;
            
            % update displacements
            obj.solutionVectors(:,lc) = obj.solutionVectors(:,lc) + deltaU;
            obj.scatterSolution(obj.solutionVectors);
            
            % compute error
            error = norm(deltaU,'fro');
            disp(['error = ',num2str(error)])
            
            %% Convergence Plot
            % variables for plotting:
            convArray(k,1) = k;
            convArray(k,2) = error;
            loadIncr = int32(incr*lambda); % number of load increment
            iterArray(loadIncr) = k;
            
            obj.convergencePlot(k, convArray, iterArray, incr, convCriteria, lc, noLoadCases, loadIncr);
            
        end % LOOP ITERATIONS
        obj.updateIncrementVariables(lc, loadIncr)
        clear convArray;
        
    end % LOOP LOAD INCREMENTS
    
    if lc < noLoadCases % copy solution of converged loadcase to initial solution of next load iteration
        obj.solutionVectors(:,lc+1) = obj.solutionVectors(:,lc);
        obj.scatterSolution(obj.solutionVectors);
    end
    
    clear iterArray;
    
end % LOOP LOADCASES

solutionVectors = obj.solutionVectors;
strainEnergy = obj.strainEnergy;

end