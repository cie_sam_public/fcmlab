%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Abstract Analysis class

classdef AbsAnalysis < handle
    %%
    methods (Access = public)
        % Constructor
        function obj = AbsAnalysis(meshFactories)
            
            AbsNumberingScheme.internalCounter(0);
            obj.meshes = {};
            
            Logger.TaskStart('Creating the FEM system','release');
            if length(meshFactories) == 1
                obj.meshes{1} = meshFactories.createMesh();
            else
                for i = 1:length(meshFactories)
                    obj.meshes{end+1} = meshFactories{i}.createMesh();
                end
            end
            Logger.TaskFinish('Creating the FEM system','release');
            
            obj.dirichletBoundaryConditions = {};
            obj.dirichletBoundaryConditionMeshIds = {};
            obj.interfaceCoupling = {};
            obj.interfaceCouplingMeshIds = {};
            obj.gradientJumpPenalisation = {};
        end
        
        % Add Prescribed Dirichlet Boundary Conditions
        function addDirichletBoundaryCondition(obj, dirichletBC, MeshId)
            if nargin < 3
                MeshId = 1;
            end
            
            obj.dirichletBoundaryConditions{end + 1} = dirichletBC;
            obj.dirichletBoundaryConditionMeshIds{end+1} = MeshId;
            Logger.Log('Dirichlet Boundary Condition added.','release');
        end
        
        function addInterfaceCoupling(obj, interfaceCoupling, FirstMeshId, SecondMeshId)
            obj.interfaceCoupling{end + 1} = interfaceCoupling;
            obj.interfaceCouplingMeshIds{end + 1, 1} = FirstMeshId;
            obj.interfaceCouplingMeshIds{end, 2} = SecondMeshId;
        end
        
        function addGradientJumpPenalisation(obj, gradientJumpPenalisation)
            obj.gradientJumpPenalisation{end + 1} = gradientJumpPenalisation;
        end
        
        % Get Meshes
        function meshes = getMesh(obj)
            if length(obj.meshes) == 1
                meshes = obj.meshes{1};
            else
                meshes = obj.meshes;
            end
        end
        
        function totalNumberOfDofs = getTotalNumberOfDofs(obj)
            totalNumberOfDofs = 0;
            for meshId = 1:length(obj.meshes)
                totalNumberOfDofs = totalNumberOfDofs + obj.meshes{meshId}.getNumberOfDofs;
            end
        end
        
        function stiffnessMatrix = computeStiffnessMatrix(obj, totalNumberOfDofs)
            stiffnessMatrix = sparse(totalNumberOfDofs, totalNumberOfDofs);
            for MeshId = 1:length(obj.meshes)
                stiffnessMatrix = obj.meshes{MeshId}.assembleStiffnessMatrix(stiffnessMatrix);
            end
        end
        
        function massMatrix = computeMassMatrix(obj, totalNumberOfDofs)
            massMatrix = sparse(totalNumberOfDofs, totalNumberOfDofs);
            for MeshId = 1:length(obj.meshes)
                massMatrix = obj.meshes{MeshId}.assembleMassMatrix(massMatrix);
            end
        end
        
        function loadVectors = computeLoadVectors(obj, totalNumberOfDofs)
            lengthLoadCases = length(obj.loadCases);
            for i = 1:lengthLoadCases
                loadVectors(:,i) = zeros(totalNumberOfDofs,1);
            end
            for MeshId = 1:length(obj.meshes)
                for i = 1:lengthLoadCases
                    loadVectors(:,i) = obj.meshes{MeshId}.assembleLoadVector(obj.loadCases(i),loadVectors(:,i),MeshId);
                end
            end
        end
        
        function [stiffnessMatrix, loadVectors] = applyDirichletBoundaryConditions(obj, stiffnessMatrix, loadVectors)
            lengthBC = length(obj.dirichletBoundaryConditions);
            for MeshId = 1:length(obj.meshes)
                currentMesh = obj.meshes{MeshId};
                for i = 1:lengthBC
                    if obj.dirichletBoundaryConditionMeshIds{i} == MeshId
                        obj.dirichletBoundaryConditions{i}.setNumberOfBC(lengthBC);
                        [stiffnessMatrix, loadVectors] = ...
                            obj.dirichletBoundaryConditions{i}.modifyLinearSystem(currentMesh,stiffnessMatrix,loadVectors);
                    end
                end
            end
        end
        
        function stiffnessMatrix = applyInterfaceCoupling(obj, stiffnessMatrix)
            if ~isempty(obj.interfaceCoupling)
                for i = 1:length(obj.interfaceCoupling)
                    FirstMesh = obj.meshes{obj.interfaceCouplingMeshIds{i,1}};
                    SecondMesh = obj.meshes{obj.interfaceCouplingMeshIds{i,2}};
                    stiffnessMatrix = obj.interfaceCoupling{i}.modifyLinearSystem(FirstMesh, SecondMesh, stiffnessMatrix);
                end
            end
        end
        
        function [stiffnessMatrix,F] = applyGradientJumpPenalisation(obj, stiffnessMatrix ,loadVectors)
            if ~isempty(obj.gradientJumpPenalisation)
                for MeshId = 1:length(obj.meshes)
                    currentMesh = obj.meshes{MeshId};
                    [stiffnessMatrix, F] = obj.gradientJumpPenalisation{1}.modifyLinearSystem(currentMesh,stiffnessMatrix,loadVectors); % 1Fehler
                end
            end
        end
        
        function scatterSolution(obj, solutionVectors)
            for MeshId = 1:length(obj.meshes)
                obj.meshes{MeshId}.scatterSolution(solutionVectors);
            end
        end
        
        function internalForce = computeInternalForce(obj)
            for MeshId = 1:length(obj.meshes)
                internalForce = obj.meshes{MeshId}.assembleInternalForceVector;
            end
        end
        
        function updateIncrementVariables(obj, lc, loadIncr)
            for MeshId = 1:length(obj.meshes)
                obj.meshes{MeshId}.UpdateIncrementVariables(lc, loadIncr);
            end
        end
        
        function obj = generateRefinedMesh(obj, meshFactory, meshID)
                obj.meshes{meshID} = meshFactory.createMesh();            
        end
        
    end
    %%
    methods (Abstract, Access = public)
        addLoadCases(obj,loadCases)
        solve(obj)
    end
    %%
    properties (Access = protected)
        meshes
        loadCases
        dirichletBoundaryConditions
        dirichletBoundaryConditionMeshIds
        interfaceCoupling
        interfaceCouplingMeshIds
        gradientJumpPenalisation
    end
    
end
