%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
    %% Solver
        function [solutionVectors, VoxelDomain] = solve(obj)

       totalNumberOfDofs = obj.getTotalNumberOfDofs();

       MySensors = obj.VoxelDomain.getAllVoxelPoints();
       MyVoxelPostProcessingMeshFactory = OnlySomePoints(MySensors);
       PostProcessingMesh = MyVoxelPostProcessingMeshFactory.createPostProcessingMesh(obj.meshes);
       PostProcessor = GenericPostProcessor(PostProcessingMesh);
       CurrentAlphaMatrix = obj.VoxelDomain.getCTData();
       for j = 1 : obj.NumberOfIterationsteps

            stiffnessMatrix = obj.computeStiffnessMatrix(totalNumberOfDofs);

            loadVectors = obj.computeLoadVectors(totalNumberOfDofs);
   
            [stiffnessMatrix, loadVectors] = obj.applyDirichletBoundaryConditions(stiffnessMatrix,loadVectors);

            for l = 1:length(obj.loadCases)
                Logger.TaskStart('Solving the Linear Equation System','release');
                obj.solutionVectors(:,l) = stiffnessMatrix \ loadVectors(:,l);
                Logger.TaskFinish('Solving the Linear Equation System','release');            
            end

            obj.scatterSolution(obj.solutionVectors);
            
            if j == obj.NumberOfIterationsteps
                break
            end
            
            indexOfPhysicalDomain = 2;
            loadCaseToVisualize = 1; 
            vonMises = VonMisesStress( loadCaseToVisualize, indexOfPhysicalDomain );
            PostProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );
            TempResultsCell = PostProcessor.computeResults(PostProcessingMesh);

            TempStressResults = cell2mat(TempResultsCell);
                % Materialumlagerungsschleife
                for l = 1:1: size(MySensors,1)
                    Alphatemp = (TempStressResults(l)/obj.SigmaObj)^obj.Rho;
                    obj.VoxelDomain.setValueAtPoint(MySensors(l,:),Alphatemp,obj.AlphaMin, obj.SigmaObj,TempStressResults(l),obj.SF);
                end
%               M�gliche Abbruchkriterium
%               if abs(sum(sum(sum(CurrentAlphaMatrix)))-sum(sum(sum(obj.VoxelDomain.getCTData()))))<obj.TOL*(size(CurrentAlphaMatrix,1)*size(CurrentAlphaMatrix,2)*size(CurrentAlphaMatrix,3))
%                     break
%               end
                
            CurrentAlphaMatrix = obj.VoxelDomain.getCTData();
            CurrentAlphaMatrix = rot90(CurrentAlphaMatrix);
            I = mat2gray(CurrentAlphaMatrix);
            imshow(I);
            figure,imshow(I);
            obj.VoxelDomain.CTDataToVTKWriter(j,MySensors);
            clear TempStressResults stiffnessMatrix

       end                   
    %% Ende der Schleife
        solutionVectors = obj.solutionVectors;
        VoxelDomain = obj.VoxelDomain;    
        end  
