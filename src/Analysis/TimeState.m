classdef TimeState < handle
    
    properties 
    currentTime
    timeStep
    end
    
    methods
    % constructor
         function obj = TimeState(startTime)
            obj.currentTime=startTime;
            obj.timeStep = 1; %start
         end
         
         function obj = updateTime(obj,timeIncrement)
            obj.currentTime=obj.currentTime+timeIncrement;
            obj.timeStep = obj.timeStep+1;
            Logger.Log(['Time updated to t = ',num2str(obj.currentTime)],'release');
         end
         
         function time = getTime(obj)
             time=obj.currentTime;
         end
         
         function timeStep = getTimeStep(obj)
             timeStep=obj.timeStep;
         end
         
    end     
    
    
   
end

