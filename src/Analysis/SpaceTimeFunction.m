classdef SpaceTimeFunction < handle
    
    properties
        timeState;
        spaceTimeFunctionHandle;
    end
    
    methods
        function obj = SpaceTimeFunction(timeState, spaceTimeFunctionHandle)
            obj.timeState=timeState;
            obj.spaceTimeFunctionHandle = spaceTimeFunctionHandle;
        end
           
        function res = subsref(obj, S)
            
            switch S(1).type
                case '()'       
                        x = S.subs{1};
                        y = S.subs{2};
                        z = S.subs{3};
                   
                    res = obj.spaceTimeFunctionHandle( x,y,z, obj.timeState.getTime());                     
            end   
        end 
                
    end
 end

