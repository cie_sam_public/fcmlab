function massMatrix = computeMassMatrix(obj, totalNumberOfDofs, lastConvergedSolution)
%COMPUTEMASSMATRIX

massMatrix = sparse(totalNumberOfDofs, totalNumberOfDofs);
for MeshId = 1:length(obj.meshes)
    massMatrix = obj.meshes{MeshId}.assembleMassMatrix(massMatrix);
end


end

