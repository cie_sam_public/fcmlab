%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% Solver
function solve(obj)

%% Create refine Mesh from geometry
refinedMeshFactory = obj.refinementStrategy.generateRefinedMeshFactory(obj.material,...
    obj.splineGeometry, obj.dimensionality, obj.numberOfGaussPoints, obj.origin, obj.Lx, obj.Ly);
obj.generateRefinedMesh(refinedMeshFactory, 1);
totalNumberOfDofs = obj.getTotalNumberOfDofs();

% Initialize zero solution vector
obj.solutionVectors = zeros(totalNumberOfDofs,obj.numberOfTimeSteps + 1 );
obj.scatterSolution(obj.solutionVectors);

lastConvergentSolution = zeros(totalNumberOfDofs,1 );

maxIterations = 10;
convCriteria = 1E-06;
timeStepSize =  obj.endTime/obj.numberOfTimeSteps;


for timeStep = 1:obj.numberOfTimeSteps+1 % LOOP TIME STEPS
    fprintf('\n');
    fprintf(['===== Time Step = ',num2str(timeStep),' =====\n']);
    
    error = 1; % needed to start iterations
    k=0; % iteration counter
    
    % update mesh in post-processor and register
    obj.postProcessingFactory.setFeMesh(obj.getMesh);
    obj.registerPostProcessor;
    
    loadVector = obj.computeLoadVectors(totalNumberOfDofs);
    
    %% Start Newton-Raphson
    while error > convCriteria && k < maxIterations % LOOP ITERATIONS
        k = k + 1; % Update iteration counter
        
        fprintf('\n');
        fprintf(['--- iteration = ',num2str(k),' ---\n']);
        % Assemble Conductivity and Capacity Matrix
        conductivityMatrix = obj.computeStiffnessMatrix(totalNumberOfDofs);
        capacityMatrix = obj.computeMassMatrix(totalNumberOfDofs);
        
        % constrain conductivity matrix and load vector
        [conductivityMatrix, loadVector] = obj.applyDirichletBoundaryConditions(conductivityMatrix,loadVector);
        
        %residuum
        r = loadVector * timeStepSize - conductivityMatrix * obj.solutionVectors(:,timeStep) * timeStepSize +...
            capacityMatrix * (lastConvergentSolution) - capacityMatrix * obj.solutionVectors(:,timeStep);
        
        %jacobian of the residuum
        J = conductivityMatrix* timeStepSize + capacityMatrix;
        
        % compute temperature update
        deltaU = J \ r;
        
        % update displacements
        obj.solutionVectors(:,timeStep) = obj.solutionVectors(:,timeStep) + deltaU;
        obj.scatterSolution(obj.solutionVectors);
        
        % compute error
        error = norm(deltaU,'fro');
        disp(['error = ',num2str(error)])
        
        
    end % LOOP ITERATIONS
    
    %     obj.updateIncrementVariables(timeStep, timeStepSize)
    
    clear convArray;
    
    lastConvergentSolution = obj.solutionVectors(:,timeStep);
    obj.solutionVectors(:,timeStep + 1) = obj.solutionVectors(:,timeStep);
    
    if timeStep < obj.numberOfTimeSteps % copy solution of converged loadcase to initial solution of next load iteration
        obj.scatterSolution(obj.solutionVectors);
    end
    
    obj.currentTimeState.updateTime(timeStepSize);
    % Register Temperature
    obj.postProcessor.registerPointProcessor( @SolutionComponent, { timeStep , obj.indexOfPhysicalDomain } );
    %     % Register Heat Flux in x direction
    %     obj.postProcessor.registerPointProcessor( @StressComponent, { timeStep, obj.indexOfPhysicalDomain, 1 } );
    %     % Register Heat Flux in y direction
    %     obj.postProcessor.registerPointProcessor( @StressComponent, { timeStep, obj.indexOfPhysicalDomain, 2 } );
    
    obj.postProcessor.visualizeResults(obj.getMesh());
    
end % LOOP TIME STEPS

% obj.postProcessor.visualizeResults(obj.getMesh());
end