%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef BackwardEulerAnalysis < AbsAnalysis
    %Solve nonlinear transient problem using Backward Euler time 
    %integration and Newton-Raphson solver.
    %
    methods (Access = public)
        % constructor
        function obj = BackwardEulerAnalysis(initialMeshFactories, material, refinementStrategy, refinementDepth, splineGeometry, ...
                postProcessingFactory, numberOfTimeSteps, endTime, initialCondition, timeVariable, indexOfPhysicalDomain,...
                dofDimension, numberOfGaussPoints, origin, Lx, Ly)
            obj = obj@AbsAnalysis(initialMeshFactories);
            obj.material = material;
            obj.refinementStrategy = refinementStrategy;
            obj.refinementDepth = refinementDepth;
            obj.splineGeometry = splineGeometry;
            obj.postProcessingFactory = postProcessingFactory;
            obj.numberOfTimeSteps = numberOfTimeSteps;
            obj.endTime = endTime;
            obj.initialCondition = initialCondition;
            obj.currentTimeState = timeVariable;
            obj.indexOfPhysicalDomain = indexOfPhysicalDomain;
            obj.dimensionality = dofDimension;
            obj.numberOfGaussPoints = numberOfGaussPoints;
            obj.origin = origin;
            obj.Lx = Lx;
            obj.Ly = Ly;
            
        end
        
        % Add Prescribed Loads
        function addLoadCases(obj,loadCases)
            obj.loadCases = [obj.loadCases loadCases];
            Logger.Log([num2str(length(loadCases)) ' Load Cases Added......'],'release');
        end
        
        function loadCase = getLoadCases(obj,loadCasesID)
            loadCase = obj.loadCases(loadCasesID);
        end
        
        function addTimeVariable(obj,timeVariable)
            obj.currentTimeState = timeVariable;
        end
        
        function initialCondition = getInitialCondition(obj)
            initialCondition = obj.initialCondition;
        end
        
        function endTime = getEndTime(obj)
            endTime = obj.endTime;
        end
        
        function timeState = getTimeState(obj)
            timeState=obj.currentTimeState;
        end
        
        function numberOfTimeSteps = getNumberOfTimeSteps(obj)
            numberOfTimeSteps = obj.numberOfTimeSteps;
        end
        
        function solution = getSolutionVectors(obj)
            solution = obj.solutionVectors;
        end
        
        function movie = getMovie(obj)
            movie = obj.movie;
        end
        
        function registerPostProcessor( obj )
            obj.postProcessor = obj.postProcessingFactory.creatVisualPostProcessor();
        end
        
        function strainEnergy = getStrainEnergy(obj)
            strainEnergy = obj.strainEnergy;
        end
        
        function kineticEnergy = getKineticEnergy(obj)
            kineticEnergy = obj.kineticEnergy;
        end
        
        solutionVectors = solve(obj);
        
    end
    
    %%
    properties (Access = private)
        postProcessor
        postProcessingFactory
        material
        refinementStrategy
        refinementDepth
        splineGeometry
        numberOfTimeSteps
        currentTimeState
        endTime
        initialCondition
        loadVector
        solutionVectors
        movie
        convergencePlot
        strainEnergy
        kineticEnergy
        indexOfPhysicalDomain
        dimensionality
        numberOfGaussPoints
        origin
        Lx
        Ly
    end
    
end