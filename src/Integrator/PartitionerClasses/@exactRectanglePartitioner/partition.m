%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function subDomains = partition(obj, domain )

%indexGeometry must be a quad, otherwise it will crash in postprocessing
%when it wants to concatinate a rectangles and quads

vertices(1) = Vertex([-1 -1 0]);
vertices(2) = Vertex([ 1 -1 0]);
vertices(3) = Vertex([ 1  1 0]);
vertices(4) = Vertex([-1  1 0]);

lines(1) = Line(vertices(1),vertices(2));
lines(2) = Line(vertices(2),vertices(3));
lines(3) = Line(vertices(3),vertices(4));
lines(4) = Line(vertices(4),vertices(1));

indexGeometry = Quad(vertices,lines);

points = domain.getVerticesCoordinates();


for i=1:size(points,1)
  domainIndexVector(i) = obj.embeddedDomain.getDomainIndex([points(i,1),points(i,2)]);
end

if all(domainIndexVector==2) || all(domainIndexVector==1)
    subDomains = indexGeometry;
else
   subDomains = obj.getSubdomains(domainIndexVector,domain);
end

end

