%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
% possible cases to handle
% case1:
% 2---1
% |   |
% 2---1
% case2:
% 1---1
% |   |
% 2---2
% case3:
% 1---1
% |   |
% 2---1


function subDomains = getSubdomains( obj, domainIndexVector, domain )

case1 = [2 1 1 2];
case2 = [2 2 1 1];
case3 = [2 1 1 1];

domainCoords = domain.getVerticesCoordinates();
realLengthX = abs(domainCoords(1,1)-domainCoords(2,1));
realLengthY = abs(domainCoords(2,2)-domainCoords(3,2));

scaledProtrusionX = obj.ProtrusionX/realLengthX*2;
scaledProtrusionY = obj.ProtrusionY/realLengthY*2;

vertices(1) = Vertex([-1 -1 0]);
vertices(2) = Vertex([ 1 -1 0]);
vertices(3) = Vertex([ 1  1 0]);
vertices(4) = Vertex([-1  1 0]);

vertices(5) = Vertex([-1 -1 0]+[scaledProtrusionX  0 0]); %% X
vertices(6) = Vertex([-1  1 0]+[scaledProtrusionX  0 0]);
vertices(7) = Vertex([-1 -1 0]+[0 scaledProtrusionY 0]); %% Y
vertices(8) = Vertex([ 1 -1 0]+[0 scaledProtrusionY 0]);
vertices(9) = Vertex([-1 -1 0]+[scaledProtrusionX  scaledProtrusionY 0]); %% mid point



if eq(domainIndexVector,case1)
    
    lines(1) = Line(vertices(1),vertices(5));
    lines(2) = Line(vertices(5),vertices(6));
    lines(3) = Line(vertices(6),vertices(4));
    lines(4) = Line(vertices(4),vertices(1));
        subDomains(1) = Quad(vertices([1 5 6 4]),lines);
    
  
    lines(1) = Line(vertices(5),vertices(2));
    lines(2) = Line(vertices(2),vertices(3));
    lines(3) = Line(vertices(3),vertices(6));
    lines(4) = Line(vertices(6),vertices(5));
        subDomains(2) = Quad(vertices([5 2 3 6]),lines);


elseif eq(domainIndexVector,case2)
    
    lines(1) = Line(vertices(1),vertices(2));
    lines(2) = Line(vertices(2),vertices(8));
    lines(3) = Line(vertices(8),vertices(7));
    lines(4) = Line(vertices(7),vertices(1));
        subDomains(1) = Quad(vertices([1 2 8 7]),lines);
    
    lines(1) = Line(vertices(7),vertices(8));
    lines(2) = Line(vertices(8),vertices(3));
    lines(3) = Line(vertices(3),vertices(4));
    lines(4) = Line(vertices(4),vertices(7));
        subDomains(2) = Quad(vertices([7 8 3 4]),lines);
    

elseif eq(domainIndexVector,case3)
    
    lines(1) = Line(vertices(1),vertices(5));
    lines(2) = Line(vertices(5),vertices(9));
    lines(3) = Line(vertices(9),vertices(7));
    lines(4) = Line(vertices(7),vertices(1));
        subDomains(1) = Quad(vertices([1 5 9 7]),lines);
    
    
    lines(1) = Line(vertices(5),vertices(2));
    lines(2) = Line(vertices(2),vertices(8));
    lines(3) = Line(vertices(8),vertices(9));
    lines(4) = Line(vertices(9),vertices(5));
        subDomains(2) = Quad(vertices([5 2 8 9]),lines);
    
    lines(1) = Line(vertices(9),vertices(8));
    lines(2) = Line(vertices(8),vertices(3));
    lines(3) = Line(vertices(3),vertices(6));
    lines(4) = Line(vertices(6),vertices(9));
        subDomains(3) = Quad(vertices([9 8 3 6]),lines);
    
    lines(1) = Line(vertices(7),vertices(9));
    lines(2) = Line(vertices(9),vertices(6));
    lines(3) = Line(vertices(6),vertices(4));
    lines(4) = Line(vertices(4),vertices(7));
        subDomains(4) = Quad(vertices([7 9 6 4]),lines);
else
    
    Logger.ThrowException('Unknown domainIndexVector..!');
end
    
    
 figure
for i=1:length(subDomains)
    coords = subDomains(i).getVerticesCoordinates();
    hold on;
    grid on;
    patch(coords(:,1),coords(:,2),coords(:,3))
end
close gcf;
end

