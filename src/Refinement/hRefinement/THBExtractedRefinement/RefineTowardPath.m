classdef RefineTowardPath < AbsHRefinementScheme
    %REFINETOWARDPATH refine mesh towrds a user-defined path
    

    methods (Access = public)
        %% Constructor
        function obj = RefineTowardPath(initialMeshFactory, path, timeState, depth)
            obj = obj@AbsHRefinementScheme(initialMeshFactory, depth);
            obj.path = path;
            obj.timeState = timeState;
        end
        
        function check = shallThisElementBeRefined(obj, XiID, EtaID, levelSize)
            % check if the path coordinates at a given time steps are
            % inside or outside a given knot-span
            coordinates = obj.path(obj.timeState.getTimeStep,:);
            element = obj.initialMesh.findElementByPoint(coordinates);
            elementID = element.getId;
            [iEta, iXi] = ind2sub(levelSize, elementID);

            check = false;
            
            %if inside the element
            if iXi == XiID && iEta == EtaID
                check = true;
            end
               
            return
        end
    end
    
    properties
        path
        timeState
    end
    
end

