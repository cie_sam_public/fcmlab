classdef AbsHRefinementScheme
    %ABSHREFINEMENTSCHEME 
    
    methods (Access = public)
        %% Constructor
        function obj = AbsHRefinementScheme(initialMeshFactory, depth)
            obj.initialMesh = initialMeshFactory.createMesh();
            obj.depth = depth;
        end
    end
    
    methods (Abstract)
        shallThisElementBeRefined(obj)
    end
    
    properties
        initialMesh
        depth
    end
    
end

