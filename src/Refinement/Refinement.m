%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef Refinement
        
    methods (Access = public)
        %% empty constructor
        function obj = Refinement( varargin )
            
            if nargin == 0
                % No refinements
                obj.hRefinement = [];
                obj.pRefinement = [];
            elseif nargin == 1
                % Only hRef
                obj.hRefinement = varargin{1};
                obj.pRefinement = [];
            else
                obj.hRefinement = varargin{1};
                obj.pRefinement = varargin{2};
            end
            
        end
        
        %% generate a refined mesh factory
        function meshFactory =  generateRefinedMeshFactory(obj, material, splineGeometry, dimensionality, numberOfGaussPoints, origin, Lx, Ly)
            
            % Instanciate the refinementTree data structure matrix
            depth = obj.hRefinement.depth;
            elementRefinements = 2^depth;
            levels = ones(splineGeometry.numberOfKnotSpansInXi*elementRefinements,...
                splineGeometry.numberOfKnotSpansInEta*elementRefinements);
            
            % Bisect initial geometry
            numberOfLevels = depth+1;
            multiLevelGeometries{1} = splineGeometry;
            
            for iLevel = 2:numberOfLevels
                multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
            end
            
            polDegreeXi = splineGeometry.getPolynomialDegreeXi;
            polDegreeEta = splineGeometry.getPolynomialDegreeEta;
            
            % get knots on the base level
            knotsXi = splineGeometry.getKnotsXi;
            knotsXi = knotsXi(polDegreeXi+1:end-polDegreeXi);
            for iKnotsXi = 1:length(knotsXi)-1
                    knotsEta = splineGeometry.getKnotsEta;
                    knotsEta = knotsEta(polDegreeEta+1:end-polDegreeEta);
                    for iKnotsEta = 1:length(knotsEta)-1
                        check = obj.hRefinement.shallThisElementBeRefined(iKnotsXi,iKnotsEta,...
                            [splineGeometry.numberOfKnotSpansInXi, splineGeometry.numberOfKnotSpansInEta]);
                        if check == true
                            %modify refinementTree
                            levels((iKnotsEta-1)*elementRefinements+1:(iKnotsEta-1)*elementRefinements+elementRefinements,...
                                (iKnotsXi-1)*elementRefinements+1:(iKnotsXi-1)*elementRefinements+elementRefinements) = ...
                                ones(elementRefinements,elementRefinements)*numberOfLevels;
                            break;
                        end
                    end % end loop over Eta knot spans
            end % end loop over Xi knot spans

                            
            % Creation of the FEM system
            DofDimension = dimensionality;           
            multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, levels, levels);
            
            [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTreeLocal( multiLevelGeometry );
            
            geometryFactory = ExtractedTHBSurfaceFactory( multiLevelGeometry, MultiLevelBezierExtractionOperator );
            elementFactory = ElementFactoryPhaseChangeExtractedTHBSurface(material, numberOfGaussPoints, multiLevelGeometry,...
                MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator );
            meshFactory = MeshFactoryAdaptiveIGA2D(multiLevelGeometry, DofDimension, elementFactory, geometryFactory, origin, Lx, Ly);
            
        end
    end
    
    
    properties
        hRefinement
        pRefinement
    end
    
end

