%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function checkRegime(obj, Element, Coord)
% This function checks whether the current state is in the elastic or the 
% plastic regime and sets the respectiv modulus (elastic or plastic)
% Computational Methods for Plasticity, Box 7.3

    % Get temporary Container at GP 
    tempCont = Element.getTempContAtGP(Coord);
    permanentCont = Element.getPermContAtGP(Coord);
    
    % Get the indices of where the variables are stored to avoid confusion
    [ID_E_El, ID_E_Pl, ID_S] = Element.getIndicesOfTempCont;
    
    % Extract Variables from permanent container & make them the trial variables
    epsilonElastOld = permanentCont(ID_E_El);
    epsilonPlastTrial = permanentCont(ID_E_Pl);

    % Calculate EpsilonTrial (elastic)   
    epsilonElastTrial = obj.calcEpsilonElastTrial(Element, Coord, epsilonElastOld);

    [epsilonElastDevTrial, epsilonElastHydTrial] = obj.calcDevHydVariables(epsilonElastTrial);
    
    % calculate trial stresses:
    stressHydTrial = 3 * obj.bulkModulus * epsilonElastHydTrial; % hydrostatic trial stress
    stressDevTrial = 2 * obj.shearModulus * epsilonElastDevTrial; % deviatoric trial stress

    qTrial = obj.calcVMStress(stressDevTrial); % v.Mises trial stress
    
    % check if yielding occurs or not
    currentYieldStress = obj.calcYieldStress(epsilonPlastTrial);
    
    phiTrial = qTrial - currentYieldStress; % (7.96)
    
    if phiTrial <= 1E-10 % elastic regime
        % trial variables are set as new variables
        epsilonElast = epsilonElastTrial;
        epsilonPlast = epsilonPlastTrial;
        stress = stressDevTrial + stressHydTrial*eye(3);
        obj.MaterialMatrix = obj.C_el; % set elastic modulus
        
    else % plastic regime
        % do return mapping:
        [dGamma, epsilonElast, epsilonPlast, stress] = obj.returnMapping(phiTrial, qTrial, stressDevTrial, stressHydTrial, epsilonElastHydTrial, epsilonPlastTrial);

        obj.MaterialMatrix = obj.calcPlastModulus(qTrial, dGamma, epsilonElastDevTrial);% set elastic modulus
    end

    % Write back variables into container
    tempCont(ID_E_El) = epsilonElast;
    tempCont(ID_E_Pl) = epsilonPlast;
    tempCont(ID_S) = stress;
    
    % Write the container back into the element
    Element.setTempContAtGP(Coord, tempCont);
end