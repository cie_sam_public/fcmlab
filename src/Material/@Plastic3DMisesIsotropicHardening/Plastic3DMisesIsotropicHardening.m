%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Linear isotropic von Mises hardening material model
% follows the book: Computational Methods for Plasticity by de Souza Neto,
% Peric & Owen
% Especially chapter 7.3.1 & 7.4.2

classdef Plastic3DMisesIsotropicHardening < AbsPlasticMaterial
    
    methods (Access = public)
        %% constructor
        function obj = Plastic3DMisesIsotropicHardening(E,Poisson,H,YieldStress,Density,ScalingFactor)
            obj.C_el = zeros(6);
            obj.C_el(1:3,1:3) = Poisson;
            obj.C_el(1,1) = 1-Poisson;
            obj.C_el(2,2) = 1-Poisson;
            obj.C_el(3,3) = 1-Poisson;
            obj.C_el(4,4) = (1-2*Poisson)/2;
            obj.C_el(5,5) = (1-2*Poisson)/2;
            obj.C_el(6,6) = (1-2*Poisson)/2;
            obj.C_el = (E/(1+Poisson)/(1-2*Poisson)) * obj.C_el;
            
            obj.MaterialMatrix = obj.C_el;
            obj.shearModulus = E/(2*(1+Poisson));
            obj.bulkModulus = E/(3*(1-2*Poisson));
            
            obj.plasticModulus = H;
            obj.YieldStress = YieldStress;
            obj.ScalingFactor = ScalingFactor;
            
            % claculation of fourth-order deviatoric projection tensor I_d
            % Needed to calculate the plastic modulus:
            IxI = zeros(6); % tensor product of second-order identity tensors
            IxI(1:3,1:3) = 1;
            I_s = eye(6);   % fourth-order symmetric identity tensor (Voigt notation)
            for i=4:6
                I_s(i,i) = 0.5;
            end
            obj.I_d = I_s - IxI/3;
            
            Logger.Log(['3D linear plastic material with Youngs modulus = ',num2str(E),...
                ', Hardening Modulus = ',num2str(H),...
                ', density = ',num2str(Density),', scaling factor = ', num2str(ScalingFactor),...
                ' and Yieldstress = ',num2str(obj.YieldStress)],'release');
        end
        
        PlastModulus = calcPlastModulus(obj, qTrial, dGamma, epsilonElastDevTrial);
        checkRegime(obj, Element, Coord);
        [dGamma, epsilonElast, epsilonPlast, stress] = returnMapping(obj, phiTrial, qTrial, stressDevTrial, stressHydTrial, epsilonElastHydTrial, epsilonPlastTrial);
        epsilonElastTrial = calcEpsilonElastTrial(~, Element, Coord, epsilonElastOld);
    end
       
    %% Properties
    properties (Access = private)
        C_el    % Elasticity Tensor
        shearModulus       
        bulkModulus      
        I_d     % Needed to calculate the plastic modulus
    end
end