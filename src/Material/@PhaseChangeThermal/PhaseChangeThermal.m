classdef PhaseChangeThermal < AbsThermalMaterial
    %PHASECHANGETHERMAL Material description following enthalpy-based
    %phase-change model from "D. Celentano, E. Oñate, and S. Oller. A
    %temperature-based formulation for finite element analysis of 
    %generalized phase-change problems. Int. J. Numer. Meth. Engng, 
    %(37):3441–3465, 1994"
    
    methods (Access = public)
        %% constructor
        function obj = PhaseChangeThermal(density, capacity, ...
                enthalpyJump, Tmelting, timeState, dimension, scalingFactor)

            obj.rho = density;
            obj.capacity = capacity;
            obj.enthalpyJump = enthalpyJump;
            obj.Tmelting = Tmelting;
            obj.dimension = dimension;
            obj.ScalingFactor = scalingFactor;
            obj.TimeState = timeState;
            
            for iDimension = 1:dimension
                obj.MaterialMatrix(iDimension,iDimension) = obj.ScalingFactor;
            end
            
            Logger.Log(['2D nonlinear thermal material with ',...
                ', specific heat capacity = ',num2str(capacity),', density = ',num2str(density),...
                ', enthalpy jump = ',num2str(enthalpyJump),', scaling factor = ', num2str(scalingFactor),...
                ' and T melting = ',num2str(obj.Tmelting)],'release');
        end
        
        evaluateCapacity(obj, Element, Coord)
        evaluateConductivity(obj, Element, Coord);
        
    end

    %% Properties
    properties (Access = public)
        rho     % density of the material 
        capacity
        enthalpyJump
        Tmelting
        TlastConvergent
        dimension
        TimeState
    end
end

