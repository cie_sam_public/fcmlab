function [ capacityPhaseChange ] = evaluateCapacity( obj, element, coords )
%EVALUATECAPACITY for Ti-6Al-4V from K. C. Mills,"Recommended Values of
%Thermophysic Properties for selected commercial alloys", 2002.

% evaluate temperature at coords
loadcase = obj.TimeState.getTimeStep();
dofVector = element.getDofVector(loadcase);
if loadcase~=1
    lastConvDofVector = element.getDofVector(loadcase-1);
else
    lastConvDofVector = element.getDofVector(loadcase);
end
shapeFunctions = element.getShapeFunctionsMatrix(coords);
T = shapeFunctions(1,:) * dofVector;
TlastConvergent = shapeFunctions(1,:) * lastConvDofVector;

capacityPhaseChange = obj.rho * obj.capacity + obj.rho * obj.enthalpyJump * ...
    phaseChangeFuncDer(T, TlastConvergent, obj.Tmelting) * (tanh( (T - obj.Tmelting) / obj.Tmelting) + 1);

capacityPhaseChange = obj.rho * obj.capacity;

for iDimension=1:obj.dimension
    obj.Density(iDimension,iDimension) = capacityPhaseChange;
end

end

function f_pcDer = phaseChangeFuncDer(T, TlastConvergent, Tmel)

if T == TlastConvergent
    f_pcDer = 0.0;
else
    f_pcDer = (phaseChangeFunc(T, Tmel) - phaseChangeFunc(TlastConvergent, Tmel))...
        / (T - TlastConvergent);
end

end

function f_pc = phaseChangeFunc(T, Tmel)

if T <= Tmel
    f_pc = 0.0;
else
    f_pc = 1.0;
end

end

