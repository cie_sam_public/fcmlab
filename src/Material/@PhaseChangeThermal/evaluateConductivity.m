function [k]  = evaluateConductivity( obj, element, coords )
%EVALUATECONDUCTIVITY for Ti-6Al-4V from K. C. Mills,"Recommended Values of
%Thermophysic Properties for selected commercial alloys", 2002.

% evaluate temperature at coords
loadCase = obj.TimeState.getTimeStep();
dofVector = element.getDofVector(loadCase);
shapeFunctions = element.getShapeFunctionsMatrix(coords);
T = shapeFunctions(1,:) * dofVector;

if T < obj.Tmelting
    k =  34.0;
else
    k = 26.7;
end

for iDimension=1:obj.dimension
    obj.MaterialMatrix(iDimension,iDimension) = k;
end

end

