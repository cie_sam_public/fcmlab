%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Material class to provide material data for the 2D plane stress case

classdef VoxelMaterial2DHookePlaneStress < AbsMaterial
    
    methods (Access = public)
        function obj = VoxelMaterial2DHookePlaneStress(E,Poisson,Density,ScalingFactor,MyVoxelDomain)
            obj.VoxelDomain = MyVoxelDomain;
            obj.Density = Density;
            obj.YoungsModulus = E;
            obj.PoissonRatio = Poisson;
            obj.ScalingFactor = ScalingFactor;
                Logger.Log(['2D linear elastic plane stress material with Young''s modulus = '...
                ,num2str(E), ', Poissons ratio = ',num2str(Poisson),' and scaling factor = ', num2str(ScalingFactor)],'release');
        end
  
    
         function MaterialMatrix = getMaterialMatrix(obj,GlobalCoord)
             
            Alpha = obj.VoxelDomain.getValueAtPoint(GlobalCoord);            
                  
            E= Alpha*obj.getYoungsModulus;
            Poisson = obj.getPoissonRatio;  
            MaterialMatrix = zeros(3,3);
            MaterialMatrix(1,1) = 1;
            MaterialMatrix(2,2) = 1;
            MaterialMatrix(3,3) = (1-Poisson)/2;
            MaterialMatrix(1,2) = Poisson;
            MaterialMatrix(2,1) = Poisson;
            MaterialMatrix = (E/(1-Poisson^2))* MaterialMatrix;

            MaterialMatrix = obj.getScalingFactor * MaterialMatrix;
         end
          
        function PoissonRatio = getPoissonRatio(obj)
            PoissonRatio = obj.PoissonRatio;
        end
    end
    
    properties (Access = private)
            VoxelDomain;
            PoissonRatio;
    end
    
end



