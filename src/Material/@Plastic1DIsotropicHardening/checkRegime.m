%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function checkRegime(obj, Element, Coord)
% This function checks whether the current state is in the elastic or the 
% plastic regime and sets the respectiv modulus (elastic or plastic)
% Computational Inelasticity, Box 1.4

    % Get temporary Container at GP 
    tempCont = Element.getTempContAtGP(Coord);
    permanentCont = Element.getPermContAtGP(Coord);
    
    % Get the indices of where the variables are stored to avoid confusion
    [ID_E_El, ID_E_Pl, ID_S, ID_HV] = Element.getIndicesOfTempCont;
    
    % Extract Variables from container & make them the trial variables
    epsilonElastOld = permanentCont(ID_E_El);
    epsilonPlastTrial = permanentCont(ID_E_Pl);
    hardeningVar = permanentCont(ID_HV);
    
    % Calculate EpsilonTrial (elastic)
    epsilonElastTrial = obj.calcEpsilonElastTrial(Element, Coord, epsilonElastOld);
    
    % calculate trial stress:
    stressTrial = obj.YoungsModulus * epsilonElastTrial; % trial stress

    % check if yielding occurs or not
    currentYieldStress = obj.calcYieldStress(hardeningVar);
    
    phiTrial = abs(stressTrial) - currentYieldStress; 
    
    if phiTrial <= 1E-10 % elastic regime
        % trial variables are set as new variables
        epsilonElast = epsilonElastTrial;
        epsilonPlast = epsilonPlastTrial;
        stress = stressTrial;
        
        obj.MaterialMatrix = obj.YoungsModulus * obj.Area; % set Young's modulus
        
    else % plastic regime
        [epsilonElast, epsilonPlast, stress, hardeningVar] = obj.returnMapping(phiTrial, stressTrial, epsilonPlastTrial, hardeningVar);
               
        obj.MaterialMatrix = obj.tangentModulus * obj.Area; % set tangent modulus
    end

    % Write back variables into container
    tempCont(ID_E_El) = epsilonElast;
    tempCont(ID_E_Pl) = epsilonPlast;
    tempCont(ID_S) = stress;
    tempCont(ID_HV) = hardeningVar;
    
    % Write the container back into the element
    Element.setTempContAtGP(Coord, tempCont);
end

