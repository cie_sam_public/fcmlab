%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Linear isotropic hardening material model
% follows the book: Computational Inelasticity by Simo&Hughes
% Especially Box 1.4

classdef Plastic1DIsotropicHardening < AbsPlasticMaterial
    
    methods (Access = public)
        %% constructor
        function obj = Plastic1DIsotropicHardening(A,E,E_t,YieldStress,Density,ScalingFactor)
            obj.YoungsModulus = E; % Young's Modulus
            obj.tangentModulus = E_t; % Elastoplastic Tangent Modulus
            obj.plasticModulus = (E*E_t)/(E-E_t); % Plastic Modulus
            
            obj.Area = A;
            obj.YieldStress = YieldStress*A;
            obj.Density = Density*A;
            obj.ScalingFactor = ScalingFactor;
            Logger.Log(['1D linear plastic material with Young''s modulus = ',num2str(E),...
                ', Elastoplastic Tangent Modulus = ',num2str(E_t),', area = ',num2str(A),...
                ', density = ',num2str(Density),', scaling factor = ', num2str(ScalingFactor),...
                ' and Yieldstress = ',num2str(obj.YieldStress)],'release');
        end
        
        checkRegime(obj, Element, Coord);
        [epsilonElast, epsilonPlast, stress, hardeningVar] = returnMapping(obj, phiTrial, stressTrial, epsilonPlastTrial, hardeningVar);
        epsilonElastTrial = calcEpsilonElastTrial(~, Element, Coord, epsilonElastOld);
    end

    %% Properties
    properties (Access = private)
        tangentModulus % Elastoplastic Tangent Modulus
        Area
    end
end