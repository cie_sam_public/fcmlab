classdef VoxelMaterialHooke3D < AbsMaterial    
    methods (Access = public)
        
        function obj = VoxelMaterialHooke3D(E,Poisson,Density,ScalingFactor,MyVoxelDomain)
                obj.VoxelDomain = MyVoxelDomain;
                obj.EModulus = E;
                obj.Poisson = Poisson;
                obj.Density = Density;
                obj.ScalingFactor = ScalingFactor;
        end

        
        function MaterialMatrix = getMaterialMatrix(obj,GlobalCoord)
            %get value from Voxel File by lookup in VoxelDomain
             Alpha = obj.VoxelDomain.getValueAtPoint(GlobalCoord);
             %In funktion auslagern?
             E= Alpha*obj.EModulus;
             ScalingFactor = obj.getScalingFactor;
            MaterialMatrix = zeros(6,6);
            MaterialMatrix(1:3,1:3) = obj.Poisson;
            MaterialMatrix(1,1) = 1-obj.Poisson;
            MaterialMatrix(2,2) = 1-obj.Poisson;
            MaterialMatrix(3,3) = 1-obj.Poisson;
            MaterialMatrix(4,4) = (1-2*obj.Poisson)/2;
            MaterialMatrix(5,5) = (1-2*obj.Poisson)/2;
            MaterialMatrix(6,6) = (1-2*obj.Poisson)/2;
            MaterialMatrix = (E/(1+obj.Poisson)/(1-2*obj.Poisson)) * MaterialMatrix;
%           Logger.Log(['3D linear elastic material with Young''s modulus = '...
%                ,num2str(E), ', Poissons ratio = ',num2str(Poisson),' and scaling factor = ', num2str(ScalingFactor)],'release');
            MaterialMatrix = ScalingFactor * MaterialMatrix;
        end
        
        
        % returns the density multiplied with the scaling factor
        function Density = getDensity(obj)
             %VoxelValue = getValueAtPoint(obj.VoxelDomain,GlobalCoord);
            Density = obj.ScalingFactor * obj.Density;
        end
        
        % returns the scaling factor
        function ScalingFactor = getScalingFactor(obj)
            ScalingFactor = obj.ScalingFactor;
        end
        
         % returns the Young Modulus
        function YoungModulus = getYoungModulus(obj)
            YoungModulus = obj.YoungModulus;
        end
        
         % returns the Poisson Ratio
        function PoissonRatio = getPoissonRatio(obj)
            PoissonRatio = obj.PoissonRatio;
        end
    end

    properties (SetAccess = private)
        VoxelDomain;
        EModulus;
        Poisson;
    end
end

