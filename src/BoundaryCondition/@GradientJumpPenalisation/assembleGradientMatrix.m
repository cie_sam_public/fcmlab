function gradientMatrix = assembleGradientMatrix(obj, geometricSupportFace , element, localCoordinates )

normal = geometricSupportFace.calcNormalVector([0 0 0]); %dummy coords
%normal = abs(normal);

geometricSupportElement = element.getGeometricSupport();
Jacobi = geometricSupportElement.calcJacobian(localCoordinates);

if obj.spaceDim == 2
    Jacobi = Jacobi(1:2,1:2);
    invJacobi = inv(Jacobi);
else
    invJacobi = inv(Jacobi);
end
DofPerDirection = element.getNumberOfDofsPerDirection();
SpaceDim = element.getSpaceDimension();
DofDim = element.getDofDimension();

Dofs = DofPerDirection^SpaceDim;

 
NDeriv = zeros(DofDim, Dofs);
Deriv = element.evalDerivOfShapeFunct(localCoordinates); 

part = length(Deriv)/DofDim;
for i=1:DofDim
    NDeriv(i,:) = Deriv(1:part);
    Deriv(1:part) = [];
end

NDeriv = invJacobi * NDeriv;


NDeriv = normal * NDeriv;



if DofDim == 1
    GradientMatrix = NDeriv;
elseif DofDim == 2
   gradientMatrix =  [NDeriv, NDeriv];
elseif DofDim == 3
    gradientMatrix =  [NDeriv, NDeriv,NDeriv];
else
    Logger.ThrowException('wrong Dof Dimension');
end

end

