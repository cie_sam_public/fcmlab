%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2016					%
%                                                                       %
%=======================================================================%
classdef GradientJumpPenalisation < handle
    %
    %This class finds all cells, which are cut by an Element up to an
    %defined ratio.
    
    properties
        somethingToStabilize
        resolution
        ratio
        integrationScheme
        penaltyValue
        stabilizedFaces
        domainIndex
        spaceDim
        mesh
    end
    
    methods (Access = public)
        function obj = GradientJumpPenalisation(integrationScheme,spaceDim,domainIndex,resolution,ratio, penaltyValue)
            obj.resolution = resolution;
            obj.ratio = ratio;
            obj.integrationScheme = integrationScheme;
            obj.penaltyValue = penaltyValue;
            obj.somethingToStabilize = true;
            obj.spaceDim = spaceDim;
            obj.domainIndex = domainIndex;
        end
        
        bool = facesEqual(obj, face1, face2 );
        [stiffnessMatrix, loadVectors] =  modifyLinearSystem(obj,mesh, stiffnessMatrix, loadVectors);
        GradientMatrix = assembleGradientMatrix(obj,geometricSupport, element, localCoordinate);
        [ badFaces, directionTable] = getUnstableFaces(obj);
        [ badFace, directionTable] = searchThroughElement(obj, element, idConditionTable, directionTable)
        
        
        
        function stabilizedFaces =  getStabilizedFaces(obj)
            stabilizedFaces = obj.stabilizedFaces;
        end
        
        function Resolution =  getResolution(obj)
            Resolution = obj.resolution;
        end
        
    end
    
    
end




