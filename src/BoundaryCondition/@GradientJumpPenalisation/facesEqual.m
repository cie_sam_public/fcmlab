function bool = facesEqual(obj, face1, face2 )

nodes1 = face1.getNodes();
nodes2 = face2.getNodes();

for i=1:length(nodes1)
    coords1(i,:) = nodes1(i).getCoords();
    coords2(i,:) = nodes2(i).getCoords();
end



count = 1; 
while true % checks if the coords are equal. Be aware that the coords seldomly are in the right order in the matrix!
    row = all(bsxfun(@eq,coords1(count,:),coords2),2);
    if any(row)
        coords2(row==1,:)=[]; 
        if count == size(coords1,1)
            bool = true;
            break
        end
        count = count +1;
    else
        
        bool = false;
        break
    end
    
end

