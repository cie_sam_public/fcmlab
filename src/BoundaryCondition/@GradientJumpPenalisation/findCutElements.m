%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2016					%
%                                                                       %
%=======================================================================%

% This function returns all badly cut Elements as well as the porper ones from the mesh, up to a
% defined ratio. If no Elements are cut badly only goodElements are
% returned.
% The resolution indicates how many seedpoints will be used to test the
% condititon of the element.


function [badElements, goodElements, redundantElements] = findCutElements(obj, Mesh, resolution, ratio )


elements = Mesh.getElements();
badElements ={};
goodElements = {};
redundantElements = {}; 

numberOfSeedpoints = prod(resolution);


for i=1:length(elements)
    amountOfPointsInsideDomain = 0;
    currentSeedpoints = elements(i).getGlobalSeedpoints(resolution);
    for j=1:numberOfSeedpoints
        if elements(i).getDomainIndex(currentSeedpoints(j,:)) == obj.domainIndex %DomainIndex noch mit übergeben!!!!
            amountOfPointsInsideDomain = amountOfPointsInsideDomain +1;
        end
    end
    currentRatio = amountOfPointsInsideDomain/numberOfSeedpoints;
    
    if currentRatio > ratio
        goodElements{end+1} = elements(i);
    
    elseif currentRatio <= ratio && currentRatio > 0.0
        badElements{end+1} = elements(i);
    
    elseif currentRatio == 0
        redundantElements{end+1} = elements(i);
        %Logger.Log(['Element with ID:', num2str(elements(i).getId()),' contains no physical Domain.\n'],'release');
    else
        Logger.ThrowException('This warning should never appear');
    end
end

if length(badElements)+length(goodElements)+length(redundantElements) ~= length(elements)
    Logger.ThrowException('Some Elements got lost during the process, this must not happen!');
end



end
