%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2016					%
%                                                                       %
%=======================================================================%

function [ badFaces, connectionTable]= getUnstableFaces(obj)

elements = obj.mesh.getElements();
idConditionTable = [];
numberOfSeedpoints = prod(obj.resolution);
badElements = {};
badFaces = {};

for i=1:length(elements)
    amountOfPointsInsideDomain = 0;
    currentSeedpoints = elements(i).getGlobalSeedpoints(obj.resolution);
    for j=1:numberOfSeedpoints
        if elements(i).getDomainIndex(currentSeedpoints(j,:)) == obj.domainIndex 
            amountOfPointsInsideDomain = amountOfPointsInsideDomain +1;
        end
    end
    currentRatio = amountOfPointsInsideDomain/numberOfSeedpoints;
    
    if currentRatio > obj.ratio
        idConditionTable(i,1) = elements(i).getId(); % enough seedpoints in domain
        idConditionTable(i,2) = 1;
        
    elseif currentRatio <= obj.ratio && currentRatio > 0.0 % badly cut
        badElements{end+1} = elements(i);
        idConditionTable(end+1,1) = elements(i).getId();
        idConditionTable(end,2) = 2;
        
    elseif currentRatio == 0.0
        idConditionTable(end+1,1) = elements(i).getId(); % not cut at all
        idConditionTable(end,2) = 3;
    else
        Logger.ThrowException('This warning should never appear');
    end
end

if size(idConditionTable) ~= length(elements)
    Logger.ThrowException('Some Elements got lost during the process, this must not happen!');
end

connectionTable = []; %because no directions are known per default
for i=1:length(badElements)
    [tempFaces, connectionTable] = obj.searchThroughElement(badElements{i},idConditionTable, connectionTable);
    if ~isempty(tempFaces)
        for j=1:length(tempFaces)
            badFaces{end+1} = tempFaces{j};
        end
    end
end

if isempty(badFaces)
    Logger.Log('There is no unstable Face Found','release'); 
    obj.somethingToStabilize = false;
end

end










