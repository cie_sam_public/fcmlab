%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2016					%
%                                                                       %
%=======================================================================%
function [stiffnessMatrix,F]  =  modifyLinearSystem(obj,mesh,stiffnessMatrix,F)

Logger.TaskStart('Stabilize Domain','release')

obj.mesh = mesh;
[ badFaces, connectionTable] = obj.getUnstableFaces();

if obj.somethingToStabilize
    for j=1:length(badFaces)
        if obj.spaceDim == 2
            geometricSupport = badFaces{j}.getLine();

        elseif  obj.spaceDim == 3
            geometricSupport = badFaces{j}.getQuad();
        else
            Logger.ThrowException('Spacedimention is strange...');
        end
        
        firstElement = mesh.getElement(connectionTable(j,1));
        secondElement = mesh.getElement(connectionTable(j,2));
        
        integrationPoints = obj.integrationScheme.getCoordinates(geometricSupport);
        integrationWeights = obj.integrationScheme.getWeights(geometricSupport);
        
        for i = 1:size(integrationPoints,1)
            detJ = geometricSupport.calcDetJacobian(integrationPoints(i,:));
            GlobalCoordinate = geometricSupport.mapLocalToGlobal(integrationPoints(i,:));
   
            locationMatrix = [firstElement.getLocationMatrix secondElement.getLocationMatrix];
            
            localCoordinateFirstElement = firstElement.mapGlobalToLocal(GlobalCoordinate);
            localCoordinateSecondElement = secondElement.mapGlobalToLocal(GlobalCoordinate);
            
           gradientFirstElement = obj.assembleGradientMatrix(geometricSupport, firstElement, localCoordinateFirstElement);
            gradientSecondElement= obj.assembleGradientMatrix(geometricSupport, secondElement, localCoordinateSecondElement);
            
            Ks11 =   obj.penaltyValue* (gradientFirstElement' *gradientFirstElement)  * integrationWeights(i) * detJ;
            Ks12 = - obj.penaltyValue* (gradientFirstElement' *gradientSecondElement) * integrationWeights(i) * detJ;
            Ks21 = - obj.penaltyValue* (gradientSecondElement'*gradientFirstElement)  * integrationWeights(i) * detJ;
            Ks22 =   obj.penaltyValue* (gradientSecondElement'*gradientSecondElement) * integrationWeights(i) * detJ;
            
            Ks = [Ks11 Ks12; Ks21 Ks22];
            
            stiffnessMatrix(locationMatrix,locationMatrix) = stiffnessMatrix(locationMatrix,locationMatrix) + Ks;
        end
        
    end
    
end
obj.stabilizedFaces = badFaces;
Logger.Log(['Faces stabilized: ', num2str(length(badFaces))], 'release');
Logger.TaskFinish('Stabilize Domain','release')

end

