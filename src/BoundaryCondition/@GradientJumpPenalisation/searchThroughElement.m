%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2016					%
%                                                                       %
%=======================================================================%
function  [ badFace, directionTable] = searchThroughElement(obj, element, idConditionTable, directionTable)

badFace = {};

myId = element.getId();
idsToCheckAgainst =  [];
if obj.spaceDim == 2
    myPointOfView = [ 0 -1 0; 1 0 0; 0 1 0; -1 0 0;]; % the position of them equals the index of the faces gotten by the element
    myFaces = element.getEdges();
elseif obj.spaceDim == 3
    myPointOfView = [0 0 -1; 0 -1 0; 1 0 0; 0 1 0; -1 0 0; 0 0 1]; %the position of them equals the index of the faces gotten by the element
    myFaces = element.getFaces();
else
    Logger.ThrowException('Wrong space dimention!');
end

origin = obj.mesh.getMeshOrigin();

Lx = obj.mesh.getLX;
Ly = obj.mesh.getLY;
Lz = obj.mesh.getLZ;

endPoint = origin + [Lx Ly Lz];

NumOX = obj.mesh.getNumberOfXDivisions;
NumOY = obj.mesh.getNumberOfYDivisions;
NumOZ = obj.mesh.getNumberOfZDivisions;

if obj.spaceDim == 2
    distanceMatrix = [Lx/NumOX      0 0; ...
                      0      Ly/NumOY 0; ...
                      0      0        0];
else
    distanceMatrix = [Lx/NumOX 0 0; ...
                      0 Ly/NumOY 0; ...
                      0 0 Lz/NumOZ];
end

geoSupport = element.getGeometricSupport();
supCoords = geoSupport.getVerticesCoordinates();
centroid = mean(supCoords);


for i=1:length(myPointOfView)
    pointToGetNextElement(i,:) = centroid'+ distanceMatrix*myPointOfView(i,:)';
end

for i=1:length(pointToGetNextElement)
    if all(pointToGetNextElement(i,:) >= origin) && all(pointToGetNextElement(i,:) <= endPoint)
        
        neighborElement = obj.mesh.findElementByPoint(pointToGetNextElement(i,:));
        neighborsId = neighborElement.getId();
        
        value = idConditionTable(find(idConditionTable(:,1) == neighborsId),2);
        if ~isempty(directionTable)
            idsToCheckAgainst = directionTable(find(directionTable(:,2) == myId),1);
        end
        
        if value ~= 3 && ~any(eq(idsToCheckAgainst,neighborsId))
            directionTable(end+1,1) = myId;
            directionTable(end,2) = neighborsId;
            badFace{end+1} = myFaces(i);
        end
    end
end
        
 
end


