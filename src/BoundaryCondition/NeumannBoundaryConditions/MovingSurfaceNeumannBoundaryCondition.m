classdef MovingSurfaceNeumannBoundaryCondition < WeakTopologyNeumannBoundaryCondition
    %MOVINGSURFACENEUMANNBOUNDARYCONDITION This class creates an instance
    %of WeakTopologyNeumannBoundaryCondition with a Neumann condition defined by a
    %load function on a moving 2D bounding box. It is an observer of
    %AbsTimeState. At each time state the BBox moves to the next points on
    %path.
    
    methods (Access = public)
        %% constructor
        function obj = MovingSurfaceNeumannBoundaryCondition(integrationScheme,...
                path, loadFunction, width, length, timeState)
            obj = obj@WeakTopologyNeumannBoundaryCondition(loadFunction,integrationScheme);
            
            obj.Path = path;
            obj.BBoxWidth = width;
            obj.BBoxLength = length;
            obj.TimeState = timeState;
            
        end
        
        function movingSurface = extractMovingSurface(obj)
%             Construct a BBox around the moving heat source such that:
%
%                 NW++++++++Front++++++++NE
%                 +                       +
%               Left     Heat Source    Right
%                 +                       +
%                 SW+++++++++Rear++++++++SE
            
            
            center = obj.Path(obj.TimeState.getTimeStep(), :);
            
            vertexNE = Vertex([center(1)+obj.BBoxWidth/2, center(2)+obj.BBoxLength/2, center(3)]);
            vertexSE = Vertex([center(1)+obj.BBoxWidth/2, center(2)-obj.BBoxLength/2, center(3)]);
            vertexNW = Vertex([center(1)-obj.BBoxWidth/2, center(2)+obj.BBoxLength/2, center(3)]);
            vertexSW = Vertex([center(1)-obj.BBoxWidth/2, center(2)-obj.BBoxLength/2, center(3)]);
            vertices = [vertexSE, vertexNE, vertexNW, vertexSW];
            edgeFront = Line(vertexNE, vertexNW);
            edgeLeft = Line(vertexNW, vertexSW);
            edgeRight = Line(vertexSE, vertexNE);
            edgeRear = Line(vertexSW, vertexSE);
            edges = [edgeRight, edgeFront, edgeLeft, edgeRear];
            
            movingSurface = Quad(vertices, edges);
        end
        
        function F = augmentLoadVector(obj,Mesh, F)
            F =zeros(size(F));
            
            movingSurface = obj.extractMovingSurface;
            center = movingSurface.calcCentroid();
            
            MyWeakNeumannBoundaryCondition = WeakMovingNeumannBoundaryCondition(obj.LoadFunction,...
                obj.IntegrationScheme, movingSurface, obj.TimeState, center);
            F = MyWeakNeumannBoundaryCondition.augmentLoadVector(Mesh,F);
        end
        
        
    end
    
    properties
        Path
        BBoxWidth
        BBoxLength
        TimeState
    end
    
end

