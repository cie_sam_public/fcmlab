%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Weak Lagrange Dirichlet Algorithm with all nonzero INTERNAL shape 
% functions used for the boundary discretization

classdef LagrangePointCollocationAlgorithm < AbsLagrangeAlgorithm
    %%
    methods (Access = public)
         % constructor 
        function obj = LagrangePointCollocationAlgorithm()

        end
        
           
        function [modifiedK, modifiedF] = modifyLinearSystem(obj,BoundaryGeometryAllSegments,IntegrationScheme,PrescribedFunction,Direction,modifiedK,modifiedF,Mesh)
               A=[];
               B=[];
               Dofs=Mesh.getNumberOfDofs();

          for seg=1:length(BoundaryGeometryAllSegments)
               
            BoundaryGeometry=BoundaryGeometryAllSegments(seg);
           
            
            IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
          
            for i = 1:size(IntegrationPoints,1)

               
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                       
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                SpaceDimension = Element.getSpaceDimension;
                DofsPerDir = (Element.getNumberOfDofsPerDirection)^SpaceDimension;
                LocationMatrix = Element.getLocationMatrix;
                LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
             
               
                N = Element.getShapeFunctionsMatrix(LocalCoordinate);

                
                PrescribedValue = PrescribedFunction(GlobalCoordinate(1),GlobalCoordinate(2),GlobalCoordinate(3));
              

                for j = 1:length(Direction)
                    if (Direction(j) ~= 0)
                        
                        OneDirectionLocationMatrix = LocationMatrix(1+(j-1)*DofsPerDir:j*DofsPerDir);
                        ascattered=zeros(1,Dofs);
                        OneDirectionN = N(j,1+(j-1)*DofsPerDir:j*DofsPerDir);
                        a=OneDirectionN;
                        ascattered = obj.scatterLagrangeMatrixIntoGlobal(a,OneDirectionLocationMatrix,1,ascattered);

                        b=PrescribedValue;
                      A = [A;ascattered];
                      B = [B;b];
                    end
                end
                

             
            end
              
          end
          K=modifiedK(1:Dofs,1:Dofs);
          F=modifiedF(1:Dofs);
          Aold=modifiedK(Dofs+1:size(modifiedK,1),1:Dofs);
          Bold=modifiedF(Dofs+1:length(modifiedF));

          A=[Aold;A];
          B=[Bold;B];

          

           %expand the linear system
           modifiedK=[K,A';A,zeros(size(A,1),size(A,1))];
           modifiedF=[F;B];

                 
    end
    
    end
    %%
    properties (Access = protected)

    end
end

