%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Weak Lagrange Dirichlet Algorithm using B-Spline shape 
% functions for the discretization of the Lagrange multiplier field at
% the boundary

classdef LagrangeBSplineAlgorithm < AbsLagrangeAlgorithm
    %%
    methods (Access = public)
         % constructor 
        function obj = LagrangeBSplineAlgorithm() %use the constructor of the 2D or 3D subclass, not this one

        end
        
           
        function [modifiedK, modifiedF] = modifyLinearSystem(obj,BoundaryGeometryAllSegments,IntegrationScheme,PrescribedFunction,Direction,modifiedK,modifiedF,Mesh)
            % Get the number of boundary sections (example: 6 faces for a cube) 
            % and the space dimenension of the boundary (example: 2D for
            % the faces of a cube):
            [~,~,TotalNumberOfBoundarySections]=obj.BoundaryFactory.getLagrangeParameters([0 0 0]);
            BoundarySpaceDimension=BoundaryGeometryAllSegments.getBoundarySpaceDimension();
            Dofs=Mesh.getNumberOfDofs();
            
            % Initialize matrix A containing the products of B-Spline 
            % boundary functions and domain shape functions, and vector B 
            % containing the product of B-Spline boundary functions 
            % and the displacement value:       
            A=zeros(prod(obj.n)*(TotalNumberOfBoundarySections)^BoundarySpaceDimension* (obj.BoundarySubDivisionsPerDirection)^BoundarySpaceDimension,Dofs);
            B=zeros(prod(obj.n)*(TotalNumberOfBoundarySections)^BoundarySpaceDimension* (obj.BoundarySubDivisionsPerDirection)^BoundarySpaceDimension,1);
            
          for seg=1:length(BoundaryGeometryAllSegments) %loop over all boundary segments
               
            BoundaryGeometry=BoundaryGeometryAllSegments(seg);
           
            IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
            IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);

            for i = 1:size(IntegrationPoints,1)

               
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                       
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                LocationMatrix = Element.getLocationMatrix;
                LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
                
                % Get the current boundary section and the parameter u 
                % for the B-Spline functions within the range [-1,1]
                % for 1D boundaries u is a scalar, for 2D a vector:   
                [u,CurrentBoundarySection,TotalNumberOfBoundarySections] =obj.BoundaryFactory.getLagrangeParameters(GlobalCoordinate);
                
                % Get new parameter u if the boundary is divided into 
                % subdivisions with an own range [-1,1]
                [u,currentSubDivision]=obj.ApplyBoundarySubdivision(u);              

                % Compute B-Spline function values 
                % for 1D and 2D boundaries. Arrange value always in a column vector:
                NL=obj.computeBSplineShapeFunc(u)';
                
                % Combine the current boundary section with the current subdivision 
                %(example: for a cube 2 subdivisions in every direction yield
                % 4 subdivisions per face; 6 faces times 4 yields 24 subdivisions for a cube): 
                CurrentSubDivision=(CurrentBoundarySection-1)* obj.BoundarySubDivisionsPerDirection+currentSubDivision;

                %get the shape functions of the element:
                N = Element.getShapeFunctionsMatrix(LocalCoordinate);
                SpaceDimension = Element.getSpaceDimension;
                DofsPerDir = (Element.getNumberOfDofsPerDirection)^SpaceDimension;

                
                detJ = BoundaryGeometry.calcDetJacobian(IntegrationPoints(i,:));
                PrescribedValue = PrescribedFunction(GlobalCoordinate(1),GlobalCoordinate(2),GlobalCoordinate(3));
                

                for j = 1:length(Direction)
                    if (Direction(j) ~= 0)
                              
                        OneDirectionN = N(j,1+(j-1)*DofsPerDir:j*DofsPerDir);

                        OneDirectionLocationMatrix = LocationMatrix(1+(j-1)*DofsPerDir:j*DofsPerDir);
                        % compute matrix a
                        a=detJ* IntegrationWeights(i)  * (NL)' * (OneDirectionN);
                        %scatter into positions:
                        A = obj.scatterLagrangeMatrixIntoGlobal(a,OneDirectionLocationMatrix,(CurrentSubDivision-1)*size(NL,2)+1:(CurrentSubDivision)*size(NL,2),A);
                                                
                        % compute vector b
                        b= detJ*IntegrationWeights(i) * PrescribedValue*(NL)';
                        %scatter into positions:
                        B = obj.scatterLagrangeVectorIntoGlobal(b,(CurrentSubDivision-1)*size(NL,2)+1:(CurrentSubDivision)*size(NL,2),B);
                       

                    end
                end
                

             
            end
              
          end
        % Expand linear system of equations:
         A=[modifiedK(Dofs+1:size(modifiedK,1),1:Dofs); A];
         B=[modifiedF(Dofs+1:size(modifiedK,1),1); B];
         
         modifiedK=[modifiedK(1:Dofs,1:Dofs),A';A,zeros(size(A,1),size(A,1))];
         modifiedF=[modifiedF(1:Dofs,1);B];
    end
    
    end
    %%
    properties (Access = protected)
        U  % for 1D a vector, for 2D matrix
        pmax %for 1D a scalar, else array
        BoundaryFactory
        m   %for 1D scalar, else array
        n   %for 1D scalar, else array
        BoundarySubDivisionsPerDirection % Every subdivision has an own Lagrange parameter range [-1,1]
    end
end

