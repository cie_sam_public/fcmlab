%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Get Lagrange Legendre shape functions for 1D and 2D boundaries

function NL=computeLegendreShapeFunc(obj,u)
if length(u)==1
    
    for i=1:obj.pmax-1

%       Legendre Polynomial:
%           Phi(1,i) = getLegendrePolynomial(u,i+1);  

%       Integrated Legendre Polynomial:
        factor = 1/sqrt(4*i-2);
        Phi(1,i) = factor *(getLegendrePolynomial(u,i+1)-getLegendrePolynomial(u,i-1)); 

    end
    N1 = 0.5*(1-u);
    N2 = 0.5*(1+u);
    
    EdgeModes = Phi(1,:);
                   
    NL = [N1 N2  EdgeModes];               

elseif length(u)==2
    
    for i=1:obj.pmax-1
%        Legendre Polynomial:        
%           PhiR(1,i) = getLegendrePolynomial(u(1),i+1);
%           PhiS(1,i) = getLegendrePolynomial(u(2),i+1);

%       Integrated Legendre Polynomial:
        factor = 1/sqrt(4*i-2);
        PhiR(1,i) = factor *(getLegendrePolynomial(u(1),i+1)-getLegendrePolynomial(u(1),i-1));
        PhiS(1,i) = factor *(getLegendrePolynomial(u(2),i+1)-getLegendrePolynomial(u(2),i-1));

     end
    
    
    OneMinusR = 1 - u(1);
    OnePlusR = 1 + u(1);
    OneMinusS = 1 - u(2);
    OnePlusS = 1 + u(2);
    
    N1 = 0.25 * OneMinusR * OneMinusS;
    N2 = 0.25 * OnePlusR * OneMinusS;
    N3 = 0.25 * OnePlusR * OnePlusS;
    N4 = 0.25 * OneMinusR * OnePlusS;
    NodalModes=[N1 N2 N3 N4];
    
    DofsPerEdge=obj.pmax-1;
    
    E1 = 0.5*OneMinusS*PhiR(1,:);% edge 1
    E2=  0.5*OnePlusR*PhiS(1,:); % edge 2
    E3 = 0.5*OnePlusS*PhiR(1,:); % edge 3
    E4 = 0.5*OneMinusR*PhiS(1,:);% edge 4
    EdgeModes=[E1 E2 E3 E4];
    
    k = 1; % k just helps to have them in the correct position
     for i = 1:DofsPerEdge
        for j = 1:DofsPerEdge
            % the formula states NintI,j(r) = phi(i,r) * phi(j,s);
            FaceModes(1,k) = PhiR(1,i) * PhiS(1,j);
            k = k+1;
        end
     end
     
     NL = [NodalModes EdgeModes FaceModes];
     
end
end