%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Get the Lagrange paramter u if the paramter space on a certain boundary
% is diveded to subdivisions
% Example: For a 1D boundary with 2 subdivisions u= 0.75 yields uSub=0.5


function [uSubdivision,currentSubDivision]=ApplyBoundarySubdivision(obj,u)
                
                numberOfDivisions=obj.BoundarySubDivisionsPerDirection;
                
            
                if length(u)==1
                
                currentSubDivision=fix((u+1-eps)/2*numberOfDivisions)+1; % find current subdivision

                uSubdivision=2*(((u+1)/2-(currentSubDivision-1)/numberOfDivisions)*numberOfDivisions)-1;
                
                elseif length(u)==2
                
                currentSubDivisionDirection1=fix((u(1)+1-eps)/2*numberOfDivisions)+1; % find current subdivision in direction 1
                currentSubDivisionDirection2=fix((u(2)+1-eps)/2*numberOfDivisions)+1; % find current subdivision in direction 2
                
                currentSubDivision=currentSubDivisionDirection1+(currentSubDivisionDirection2-1)*numberOfDivisions; % find current subdivision

                uSubdivision(1)=2*(((u(1)+1)/2-(currentSubDivisionDirection1-1)/numberOfDivisions)*numberOfDivisions)-1;
                uSubdivision(2)=2*(((u(2)+1)/2-(currentSubDivisionDirection2-1)/numberOfDivisions)*numberOfDivisions)-1;
                
                end
end