%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
%
% Weak Lagrange Dirichlet Algorithm using the SAME shape 
% functions for the discretization of the Lagrange multiplier field at
% the boundary, as for the displacement field 


classdef LagrangeSameFunctionAlgorithm < AbsLagrangeAlgorithm
    %%
    methods (Access = public)
         % constructor 
        function obj = LagrangeSameFunctionAlgorithm() 
        end
        
           
        function [modifiedK, modifiedF] = modifyLinearSystem(obj,BoundaryGeometryAllSegments,IntegrationScheme,PrescribedFunction,Direction,modifiedK,modifiedF,Mesh)
           
         
            
            % Initialize matrix A containing the products of nonzero
            % shape functions as Lagrange boundary functions and 
            % domain shape functions, and vector B 
            % containing the product of the boundary functions 
            % and the displacement value:       

           A=zeros(size(modifiedK,1),size(modifiedK,1));
           B=zeros(size(modifiedK,1),1);
            
          for seg=1:length(BoundaryGeometryAllSegments) %loop over all boundary segments
               
            BoundaryGeometry=BoundaryGeometryAllSegments(seg);
           
            IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
            IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);

            for i = 1:size(IntegrationPoints,1)

               
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                       
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                LocationMatrix = Element.getLocationMatrix;
                LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);

                %get the shape functions of the element:
                N = Element.getShapeFunctionsMatrix(LocalCoordinate);
                SpaceDimension = Element.getSpaceDimension;
                DofsPerDir = (Element.getNumberOfDofsPerDirection)^SpaceDimension;

                detJ = BoundaryGeometry.calcDetJacobian(IntegrationPoints(i,:));
                PrescribedValue = PrescribedFunction(GlobalCoordinate(1),GlobalCoordinate(2),GlobalCoordinate(3));
                
                % construct a location matrix excluding all columns which
                % have only zero entries in the shape function matrix:
                NonZeroLocationMatrix=LocationMatrix(:,any(N)); 
                % get indices of nonzero columns:         
                [~,NonzeroColumns]=find(N);
                NonzeroColumns=unique(NonzeroColumns);
                
                for j = 1:length(Direction)
                    if (Direction(j) ~= 0)
                              
                        OneDirectionN = N(j,1+(j-1)*DofsPerDir:j*DofsPerDir);

                        OneDirectionLocationMatrix = LocationMatrix(1+(j-1)*DofsPerDir:j*DofsPerDir);
                        
                        % get all nonzero columns in this direction: 
                        OneDirectionNonzeroColumns=intersect(NonzeroColumns,1+(j-1)*DofsPerDir:j*DofsPerDir);
                        NL=N(j,OneDirectionNonzeroColumns);
                        OneDirectionNonZeroLocationMatrix=intersect(NonZeroLocationMatrix,OneDirectionLocationMatrix);

                        % compute matrix a
                        a=detJ* IntegrationWeights(i)  * (NL)' * (OneDirectionN);
                        %scatter into positions:
                        A = obj.scatterLagrangeMatrixIntoGlobal(a,OneDirectionLocationMatrix,OneDirectionNonZeroLocationMatrix,A);
                                                
                        % compute vector b
                        b=detJ*IntegrationWeights(i) * PrescribedValue*(NL)';
                        %scatter into positions:
                        B = obj.scatterLagrangeVectorIntoGlobal(b,OneDirectionNonZeroLocationMatrix,B);
                       

                    end
                end
                

             
            end
              
          end
          
          % Remove rows correspondinng to non intersected elements
          B_temp=B;
          B(~(any(A,2)+any(B_temp,2)))=[];    
          A(~(any(A,2)+any(B_temp,2)),:)=[];  %if a row is zero in the matrix AND the vector
                                              %we do not need a Lagrange multiplier

         
           %expand the linear system
           modifiedK=[modifiedK,A';A,zeros(size(A,1),size(A,1))];
           modifiedF=[modifiedF;B];
    end
    
    end
    %%
    properties (Access = protected)
    end
end

