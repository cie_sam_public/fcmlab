%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%          
         
classdef SegmentwiseApproachUDHuerta < AbsNonSeparatedPenaltyValueApproach
    methods (Access = public)
        %% constructor
        function obj = SegmentwiseApproachUDHuerta()
         
        end
        
        
        function [PenaltyValues] = computePenaltyValues(~,TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,~,~,Mesh)
          
           

            
            %% Compute matrices A and B  of the eigenvalue problem      
            %Intialization values for the loops over segments and intergration points:
            PenaltyValues(length(BoundaryGeometryAllSegments))=0;
            PenaltyValues=PenaltyValues*NaN;
            
            for seg=1:length(BoundaryGeometryAllSegments)
                          
                %BoundaryGeometry of current segment:
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);
              
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(1,:));
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                
                Beig=Element.calcBdotBUD();

                Aeig=0;
                
                % loop over all integration points
                for i = 1:size(IntegrationPoints,1)
                    
                    GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                    Element = Mesh.findElementByPoint(GlobalCoordinate);
                    
                    LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
                    
                    detJ = BoundaryGeometry.calcDetJacobian([-1 -1 -1]);
                    
                    B = Element.getB(LocalCoordinate);
                    
                 
                    NormalVectorVoigt = TractionOperand.getNormalVectorVoigt(IntegrationPoints(i,:),BoundaryGeometry);
                    a = detJ*IntegrationWeights(i)  * ((B'*NormalVectorVoigt)*(B'*NormalVectorVoigt)');
                    Aeig=a+Aeig;                                      
                               
                end
                %% Compute Eigenvalues and Penalty Values
                eigenv=eig(Aeig,Beig);
                eigenv(isinf(eigenv))=[];
                Eigenv=max(eigenv);   
                
                PenaltyValues(seg)= 2*Eigenv^2+1;  
                
                
            end        
        end
    end
end

  
  