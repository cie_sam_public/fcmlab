%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%          
         
classdef ElementwiseNormalShearApproachUD < AbsNormalShearPenaltyValueApproach
    methods (Access = public)
        %% constructor
        function obj = ElementwiseNormalShearApproachUD()
         
        end
        
        
        function [ShearPenaltyValues] = computeShearPenaltyValues(~,TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,~,~,Mesh)
            numberOfElements=  Mesh.getNumberOfElements;
            % Initialize vector containing eigen- and penalty values for each element
            % penalty values not intersected by the boundary are assigned to NaN
            Eigenv(numberOfElements)=0;
            Betas(numberOfElements)=0;
            ShearPenaltyValues(length(BoundaryGeometryAllSegments))=0;
            ShearPenaltyValues=ShearPenaltyValues*NaN;
            
            %Get the #Dofs for every direction
            Element=Mesh.getElement(1); 
            SpaceDimension = Element.getSpaceDimension;
            DofsPerDir = (Element.getNumberOfDofsPerDirection)^SpaceDimension;
            
            %Initialize matrices A and B of the genaralized eigenvalue problem 
            Aeig=zeros(SpaceDimension*DofsPerDir,SpaceDimension*DofsPerDir,numberOfElements);
            Beig=zeros(SpaceDimension*DofsPerDir,SpaceDimension*DofsPerDir,numberOfElements);
            
            %% Compute matrices A and B  of the eigenvalue problem      
            %Intialization values for the loops over segments and intergration points:
            elementIntersected(numberOfElements)=false;

            for seg=1:length(BoundaryGeometryAllSegments)
                
                %BoundaryGeometry of current segment:
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);
                
                % loop over all integration points
                for i = 1:size(IntegrationPoints,1)
                    
                    GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                    Element = Mesh.findElementByPoint(GlobalCoordinate);
                    elementId=Element.getId;
                    
                    LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
                    
                    detJ = BoundaryGeometry.calcDetJacobian([-1 -1 -1]);
                    
                    B = Element.getB(LocalCoordinate);
                    
                
                    NormalVectorVoigt = TractionOperand.getNormalVectorVoigt(IntegrationPoints(i,:),BoundaryGeometry);
                            
                    a = detJ*IntegrationWeights(i)  * (B'*NormalVectorVoigt)  * (B'*NormalVectorVoigt)';
                    Aeig(:,:,elementId) = Aeig(:,:,elementId)+a;
                
                    % execute domain integration only once for every intersected element
                    
                    if ~(elementIntersected(elementId))
                        elementIntersected(elementId)=true;
                        
                        b=Element.calcBdotBUD();
                        Beig(:,:,elementId) = Beig(:,:,elementId)+b;
                       
                    end
                end
            end
            
            %% Compute Eigenvalues and Penalty Values 
            
            for i=1:numberOfElements
                % compute only if element is interseted by the boundary
                if (elementIntersected(i))
                    eigenv=eig(Aeig(:,:,i),Beig(:,:,i));
                    eigenv(isinf(eigenv))=[];
                    eigenv=max(eigenv);
                    Eigenv(i)=eigenv;
                    
                    Element=Mesh.getElement(i);
                    SpaceDimension = Element.getSpaceDimension;
                    h=Element.getMeshSizeParameter(SpaceDimension);
                    Material=Element.getMaterial;
                    E=Material.getYoungsModulus;
                    nu=Material.getPoissonsRatio;
                
                    lamda=E*nu/((1+nu)*(1-2*nu));
                    mu=E/(1+nu);
                
                    Betas(i)= mu * Eigenv(i)/h; 
                end
            end
            
            for seg=1:length(BoundaryGeometryAllSegments)
                
                %BoundaryGeometry of current segment
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(1,:));
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                elementId=Element.getId;
                
                ShearPenaltyValues(seg)=Betas(elementId);
            end
            
        end
        
        function [NormalPenaltyValues] = computeNormalPenaltyValues(~,TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,~,~,Mesh)
            
            numberOfElements=  Mesh.getNumberOfElements;
            Eigenv(numberOfElements)=0;
            Betas(numberOfElements)=0;
            NormalPenaltyValues(length(BoundaryGeometryAllSegments))=0;
            NormalPenaltyValues=NormalPenaltyValues*NaN;
            
            %Get the #Dofs for every direction
            Element=Mesh.getElement(1); 
            SpaceDimension = Element.getSpaceDimension;
            DofsPerDir = (Element.getNumberOfDofsPerDirection)^SpaceDimension;
            
            %Initialize matrices A and B of the genaralized eigenvalue problem 
            Aeig=zeros(SpaceDimension*DofsPerDir,SpaceDimension*DofsPerDir,numberOfElements);
            Beig=zeros(SpaceDimension*DofsPerDir,SpaceDimension*DofsPerDir,numberOfElements);
            
            %% Compute matrices A and B  of the eigenvalue problem      
            %Intialization values for the loops over segments and intergration points:
            elementIntersected(numberOfElements)=false;

            for seg=1:length(BoundaryGeometryAllSegments)
                
                %BoundaryGeometry of current segment:
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);
                
                % loop over all integration points
                for i = 1:size(IntegrationPoints,1)
                    
                    GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                    Element = Mesh.findElementByPoint(GlobalCoordinate);
                    elementId=Element.getId;

                    LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
                                       
                    detJ = BoundaryGeometry.calcDetJacobian([-1 -1 0]);
                    
                    GradQ = Element.getGradQ(LocalCoordinate);
                    NormalVectorVoigt = TractionOperand.getNormalVectorVoigt(IntegrationPoints(i,:),BoundaryGeometry);
                    NormalVector=NormalVectorVoigt(1:SpaceDimension,1:SpaceDimension);
       
                    a = detJ*IntegrationWeights(i)  * (GradQ'*NormalVector) * (GradQ'*NormalVector)';
                    Aeig(:,:,elementId) = Aeig(:,:,elementId)+a;
                
                    % execute domain integration only once for every intersected element
                    
                    if ~(elementIntersected(elementId))

                        elementIntersected(elementId)=true;
                              
                        b=Element.calcGradQdotGradQUD();
                        Beig(:,:,elementId) = Beig(:,:,elementId)+b;
                     
                    end
                end
            end
            
            %% Compute Eigenvalues and Penalty Values
            
            for i=1:numberOfElements
                % compute only if element is interseted by the boundary
                if (elementIntersected(i))
                    eigenv=eig(Aeig(:,:,i),Beig(:,:,i));
                    eigenv(isinf(eigenv))=[];
                    eigenv=max(eigenv);
                    Eigenv(i)=eigenv;
                    
                    Element=Mesh.getElement(i);
                    SpaceDimension = Element.getSpaceDimension;
                    h=Element.getMeshSizeParameter(SpaceDimension);
                    Material=Element.getMaterial;
                    E=Material.getYoungsModulus;
                    nu=Material.getPoissonsRatio;
                    
                    lamda=E*nu/((1+nu)*(1-2*nu));
                    mu=E/(1+nu);
                    
                    Betas(i)= lamda*Eigenv(i)/h;  
                    
                end
            end
              
            for seg=1:length(BoundaryGeometryAllSegments)
                
                %BoundaryGeometry of current segment
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                
                GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(1,:));
                Element = Mesh.findElementByPoint(GlobalCoordinate);
                elementId=Element.getId;
                
                NormalPenaltyValues(seg)=Betas(elementId);
            end
            
        end
        
        
        
        
    end
end

  
  