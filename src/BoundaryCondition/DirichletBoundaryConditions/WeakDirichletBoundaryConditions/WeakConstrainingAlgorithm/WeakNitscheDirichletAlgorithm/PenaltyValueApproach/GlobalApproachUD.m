%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%          
         
classdef GlobalApproachUD < AbsNonSeparatedPenaltyValueApproach
    methods (Access = public)
        %% constructor
        function obj = GlobalApproachUD()
         
        end
        
        
        function [PenaltyValues] = computePenaltyValues(~,TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,~,sizeK,Mesh)
            %Initialize matrices A and B of the genaralized eigenvalue problem 
            Aeig=zeros(sizeK);
            Beig=zeros(sizeK);
            
            %% Compute matrices A and B  of the eigenvalue problem      
            %Intialization values for the loops over segments and intergration points:
            numberOfElements=  Mesh.getNumberOfElements;
            elementIntersected(numberOfElements)=false;

            for seg=1:length(BoundaryGeometryAllSegments)
                BoundaryGeometry=BoundaryGeometryAllSegments(seg);
                IntegrationPoints = IntegrationScheme.getCoordinates(BoundaryGeometry);
                IntegrationWeights = IntegrationScheme.getWeights(BoundaryGeometry);
                
                for i = 1:size(IntegrationPoints,1)
                    
                    
                    GlobalCoordinate = BoundaryGeometry.mapLocalToGlobal(IntegrationPoints(i,:));
                    Element = Mesh.findElementByPoint(GlobalCoordinate);
                    elementId=Element.getId;

                    LocalCoordinate = Element.mapGlobalToLocal(GlobalCoordinate);
                    LocationMatrix = Element.getLocationMatrix;
                    
                    
                    detJ = BoundaryGeometry.calcDetJacobian([-1 -1 -1]);        
                    
                    B = Element.getB(LocalCoordinate);
                    
                    NormalVectorVoigt = TractionOperand.getNormalVectorVoigt(IntegrationPoints(i,:),BoundaryGeometry);
                    
                    a = detJ*IntegrationWeights(i)  * (B' *NormalVectorVoigt) * (B'*NormalVectorVoigt)';
                    Aeig = Mesh.scatterElementMatrixIntoGlobalMatrix(a,LocationMatrix,Aeig);
     
                    
                    if ~(elementIntersected(elementId))
                        elementIntersected(elementId)=true;
                        
                        b=Element.calcBdotBUD();
                        Beig = Mesh.scatterElementMatrixIntoGlobalMatrix(b,LocationMatrix,Beig);
                    end
                end
            end
            %% Compute Eigenvalues
            Aeig_old=Aeig;
            Beig_old=Beig;            
            % delete zero rows and columns (parts non intersected by the
            % boundary)
            Aeig(~(any(Aeig_old)+any(Beig_old)),:)=[];
            Aeig(:,~(any(Aeig_old)+any(Beig_old)))=[];
            Beig(~(any(Aeig_old)+any(Beig_old)),:)=[];
            Beig(:,~(any(Aeig_old)+any(Beig_old)))=[];

            %compute eigenvalues
            Eigenv=eig(Aeig,Beig);
            Eigenv(isinf(Eigenv))=[];
            Eigenv=max(Eigenv);
            
            %% Compute Penalty Values 
            numberOfElements=  Mesh.getNumberOfElements;
            Beta=0;
            
            for i=1:numberOfElements
                Element=Mesh.getElement(i);
                SpaceDimension = Element.getSpaceDimension;
                h=Element.getMeshSizeParameter(SpaceDimension);
                Material=Element.getMaterial;
                E=Material.getYoungsModulus;
                nu=Material.getPoissonsRatio;
                
                lamda=E*nu/((1+nu)*(1-2*nu));
                mu=E/(1+nu);
                
                Beta= max(Beta,mu * Eigenv/h);
               
            end
               
            
            PenaltyValues=Beta*ones(1,length(BoundaryGeometryAllSegments));
        end
    end
    
end

  
  