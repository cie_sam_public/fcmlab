%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%          

% Compute the exact value of the Penalty parameter which keeps the linear 
% system positive definite using numerical bisection

classdef BisectionApproach < AbsNonSeparatedPenaltyValueApproach
    methods (Access = public)
        %% constructor
        function obj = BisectionApproach(BetaMinimum,BetaMaximum,tolerance)
         obj.BetaMinimum=BetaMinimum; % usually set to zero
         obj.BetaMaximum=BetaMaximum; % usually set to zero
         obj.tolerance=tolerance; % a value around 0.1 is sufficient
        end
        
        
        function [Beta] = computePenaltyValues(obj,K,Kp)
            BetaMin=obj.BetaMinimum;
            BetaMax=obj.BetaMaximum;
            minimalEigenvalue=-1;
            
            Beta=0.5*(BetaMin+BetaMax);
            while (abs(Beta-BetaMax)>obj.tolerance || abs(Beta-BetaMin)>obj.tolerance || (abs(Beta-obj.BetaMaximum)>obj.tolerance && minimalEigenvalue<0))%bisection
                
                %get the middle value of the upper and lower bounds
                Beta=0.5*(BetaMax+BetaMin);

                % compute the minimal eigenvalue of the full matrix
                % (symmetric part):
                Ktemp=K+Beta*Kp;    
                Ktemp=full(Ktemp);
                Ktemp=0.5*(Ktemp+Ktemp');
                minimalEigenvalue=min(eig(Ktemp));
                
               % modify upper or lower bound
                if (minimalEigenvalue<0)
                    BetaMin=Beta;
                else
                    BetaMax=Beta;
                end
            
            end
        end
        
    end
    
    properties(Access=private)
        BetaMinimum
        BetaMaximum
        tolerance
    end

end

  
  