%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Weak Nitsche Dirichlet Algorithm with an eigenvalue based penalty value
% estimation

classdef WeakNitschePenaltyValueEstimationAlgorithm < AbsWeakConstrainingAlgorithm
    
    methods (Access = public)
        %% constructor
        function obj = WeakNitschePenaltyValueEstimationAlgorithm(PenaltyValueApproach,TractionOperand)
            obj.PenaltyValueApproach = PenaltyValueApproach;
             obj.TractionOperand = TractionOperand;
        end
        
 

        function [K , F] = modifyLinearSystem(obj,BoundaryGeometryAllSegments,IntegrationScheme,PrescribedFunction,Direction,K,F,Mesh)
              
            %persistent penalty modification parts, needed only for the
            %Bisection approach:
             persistent Kp;
             persistent Fp;
             persistent counter;
        
                
            if isa(obj.PenaltyValueApproach,'AbsNonSeparatedPenaltyValueApproach') && ~isa(obj.PenaltyValueApproach,'BisectionApproach') %all estimation algorithms without normal and shear separation
                
                NitscheAlgorithm = WeakNitscheDirichletAlgorithm(0,obj.TractionOperand);
               % compute penalty values for all boundary segments and modify the linear system 
                [PenaltyValues] =obj.PenaltyValueApproach.computePenaltyValues(obj.TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,Direction,size(K),Mesh);
                maxPenaltyValue=max(PenaltyValues);
                for seg=1:length(BoundaryGeometryAllSegments)
                    NitscheAlgorithm.resetPenaltyValue(PenaltyValues(seg));
                    [K,F] = NitscheAlgorithm.modifyLinearSystem(BoundaryGeometryAllSegments(seg),IntegrationScheme,PrescribedFunction,Direction,K,F,Mesh);
                end
             
                               
            elseif isa(obj.PenaltyValueApproach,'AbsNormalShearPenaltyValueApproach') %all estimation algorithms which use normal and shear separation
                
                NitscheAlgorithmShearPart = WeakNitscheDirichletAlgorithm(0,obj.TractionOperand);
                NitscheAlgorithmNormalPart = WeakNitscheDirichletAlgorithmNormalPart(0,obj.TractionOperand);

               % compute penalty values for the shear and the normal part separately 
               [ShearPenaltyValues] = obj.PenaltyValueApproach.computeShearPenaltyValues(obj.TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,Direction,size(K),Mesh);
              [NormalPenaltyValues] = obj.PenaltyValueApproach.computeNormalPenaltyValues(obj.TractionOperand,BoundaryGeometryAllSegments,IntegrationScheme,Direction,size(K),Mesh);                 
              
               maxPenaltyValue=max(NormalPenaltyValues);                           
              for seg=1:length(BoundaryGeometryAllSegments)                 
                 NitscheAlgorithmShearPart.resetPenaltyValue(ShearPenaltyValues(seg));
                 NitscheAlgorithmNormalPart.resetPenaltyValue(NormalPenaltyValues(seg));
                  [K,F] = NitscheAlgorithmShearPart.modifyLinearSystem(BoundaryGeometryAllSegments(seg),IntegrationScheme,PrescribedFunction,Direction,K,F,Mesh);
                  [K,F] = NitscheAlgorithmNormalPart.modifyLinearSystem(BoundaryGeometryAllSegments(seg),IntegrationScheme,PrescribedFunction,Direction,K,F,Mesh);            
              end
              
              
            elseif isa(obj.PenaltyValueApproach,'BisectionApproach')% a penalty value computation using numerical bisection
                
                maxPenaltyValue=[];
                %initialize persistent variables (counter and Penalty 
                % modification parts):
                if (isempty(Kp))
                    counter=0;
                    Kp=zeros(size(K));
                    Fp=zeros(size(K,1),1);
                end
                counter=counter+1;

                %get the Nitsche modification part:
                NitscheAlgorithm = WeakNitscheDirichletAlgorithm(0,obj.TractionOperand);
                for seg=1:length(BoundaryGeometryAllSegments)
                    [K,F] = NitscheAlgorithm.modifyLinearSystem(BoundaryGeometryAllSegments(seg),IntegrationScheme,PrescribedFunction,Direction,K,F,Mesh);
                end  

                
               %get the Penalty modification part:
                PenaltyAlgorithm=WeakPenaltyAlgorithm(1.0);
                for seg=1:length(BoundaryGeometryAllSegments)
                    [Kp,Fp] = PenaltyAlgorithm.modifyLinearSystem(BoundaryGeometryAllSegments(seg),IntegrationScheme,PrescribedFunction,Direction,Kp,Fp,Mesh);
                end
                
               % Compute the penalty value and modify the system if the last 
               % Dirichlet BC was reached:
                if counter==obj.NumberOfDirichletBC    
                    [PenaltyValue] =obj.PenaltyValueApproach.computePenaltyValues(K,Kp);
                    K=K+PenaltyValue*Kp;
                    F=F+PenaltyValue*Fp; 
                    maxPenaltyValue=PenaltyValue;
                    clear Kp;
                    clear Fp;
                    clear counter;
                end
            else
                Logger.ThrowException(['Unknown Penalty Value Approach Type !! ']);
            end
            
            Logger.Log(['Maximal Penalty Value:', num2str(maxPenaltyValue)],'release');
             
    end
       
        
    end
    
    properties (Access = protected)

        PenaltyValueApproach
        TractionOperand
    end
end

