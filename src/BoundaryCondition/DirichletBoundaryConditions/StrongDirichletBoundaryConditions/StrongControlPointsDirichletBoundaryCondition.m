%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%StrongEdgeDirichletBoundaryCondition

classdef StrongControlPointsDirichletBoundaryCondition < AbsStrongDirichletBoundaryCondition
    %%
    methods (Access = public)
        %% constructor
        function obj = StrongControlPointsDirichletBoundaryCondition(Value, Direction, StrongDirichletAlgorithm, ControlPointsFilter)
            obj = obj@AbsStrongDirichletBoundaryCondition( @(x,y,z)(Value), Direction, StrongDirichletAlgorithm );
            
            obj.Value = Value;
            obj.ControlPointsFilter = ControlPointsFilter;
        end
        
        %% interface method
        function [K, F] = modifyLinearSystem(obj,Mesh,K,F)
            
            Logger.TaskStart('Applying Strong Dirichlet Boundary Condition on Control Points','release');
            
              for iFieldComponent = 1:length(obj.Direction)
                if obj.Direction(iFieldComponent) ~= 0
                    
                    idsToConstrain = Mesh.getDegreesOfFreedom(obj.ControlPointsFilter, iFieldComponent);

                    prescribedValues = obj.Value * ones(length(idsToConstrain), 1);
                    [K, F] = obj.StrongDirichletAlgorithm.modifyLinearSystem(idsToConstrain,prescribedValues,K,F);
                    
                end
              end
              
            Logger.TaskFinish('Applying Strong Dirichlet Boundary Condition on Control Points','release');
        end
        
    end
    
    properties (Access = private)
        Value
        ControlPointsFilter
    end
    
end
