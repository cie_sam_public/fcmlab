%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef NitscheInterfaceCoupling
    
    methods (Access = public)
        function obj = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, PenaltyValue)
            obj.PenaltyInterfaceCoupling = PenaltyInterfaceCoupling(IntegrationScheme, InterfaceGeometry, PenaltyValue);
            obj.IntegrationScheme = IntegrationScheme;
            obj.InterfaceGeometry = InterfaceGeometry;
            obj.PenaltyValue = PenaltyValue;
            
        end
        
        function K = modifyLinearSystem(obj, FirstMesh, SecondMesh, K)
            K = obj.PenaltyInterfaceCoupling.modifyLinearSystem(FirstMesh, SecondMesh, K);
            
            for j = 1:length(obj.InterfaceGeometry)
                
                InterfaceSegment = obj.InterfaceGeometry(j);
                
                IntegrationPoints = obj.IntegrationScheme.getCoordinates(InterfaceSegment);
                IntegrationWeights = obj.IntegrationScheme.getWeights(InterfaceSegment);
                
                for i = 1:size(IntegrationPoints,1)
                    detJ = InterfaceSegment.calcDetJacobian(IntegrationPoints(i,:));
                    GlobalCoordinate = InterfaceSegment.mapLocalToGlobal(IntegrationPoints(i,:));
                    
                    ElementFirstMesh = FirstMesh.findElementByPoint(GlobalCoordinate);
                    ElementSecondMesh = SecondMesh.findElementByPoint(GlobalCoordinate);
                    
                    LocationMatrix = [ElementFirstMesh.getLocationMatrix ElementSecondMesh.getLocationMatrix];
                    
                    LocalCoordinateFirstMesh = ElementFirstMesh.mapGlobalToLocal(GlobalCoordinate);
                    LocalCoordinateSecondMesh = ElementSecondMesh.mapGlobalToLocal(GlobalCoordinate);
                    
                    NFirstMesh = ElementFirstMesh.getShapeFunctionsMatrix(LocalCoordinateFirstMesh);
                    NSecondMesh = ElementSecondMesh.getShapeFunctionsMatrix(LocalCoordinateSecondMesh);
                    
                    CFirstMesh = ElementFirstMesh.getMaterialAtPoint(LocalCoordinateFirstMesh).getMaterialMatrix;
                    CSecondMesh = ElementSecondMesh.getMaterialAtPoint(LocalCoordinateSecondMesh).getMaterialMatrix;
            
                    BFirstMesh = ElementFirstMesh.getB(LocalCoordinateFirstMesh);
                    BSecondMesh = ElementSecondMesh.getB(LocalCoordinateSecondMesh);
                    
                    NormalVector = InterfaceSegment.calcNormalVector(IntegrationPoints(i,:));
                    NormalVectorVoigt = [ NormalVector(1)    0;
                                  0                  NormalVector(2);
                                  NormalVector(2)    NormalVector(1)];
                    
                    Kn11 = - 0.5* BFirstMesh'*CFirstMesh'*NormalVectorVoigt*NFirstMesh*IntegrationWeights(i)* detJ;
                    Kn12 = 0.5* BFirstMesh'*CFirstMesh'*NormalVectorVoigt*NSecondMesh*IntegrationWeights(i)* detJ;
                    Kn21 = - 0.5* BSecondMesh'*CSecondMesh'*NormalVectorVoigt*NFirstMesh*IntegrationWeights(i)* detJ;
                    Kn22 = 0.5* BSecondMesh'*CSecondMesh'*NormalVectorVoigt*NSecondMesh*IntegrationWeights(i)* detJ;
                    
                    Kn = [Kn11 Kn12; Kn21 Kn22];
                    
                    K(LocationMatrix,LocationMatrix) = K(LocationMatrix,LocationMatrix) + Kn;
                    K(LocationMatrix,LocationMatrix) = K(LocationMatrix,LocationMatrix) + Kn';
                end
            end
        end
    end
    properties (Access = private)
        IntegrationScheme
        InterfaceGeometry
        PenaltyValue
        PenaltyInterfaceCoupling
    end
end