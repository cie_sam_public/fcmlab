%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Function to get all B-Spline Derivatives of the order pmax
% uses lower order B-Splines
% u: current parameter value
% U: knot Vector
% pmax: highest polynomial order


function dN=getBSplineDerivative(u,U,pmax)
 

m=length(U);

dN(m-pmax-1)=0;

if pmax >0
    NOneOrderLower=getBSpline(u,U,pmax-1);
    
    for i=1:m-pmax-1
       
        if abs(U(i+pmax)-U(i))>2*eps
            a=pmax/(U(i+pmax)-U(i));
        else
            a=0;
        end
        
        if abs(U(i+pmax+1)-U(i+1))>2*eps
        b=pmax/(U(i+pmax+1)-U(i+1));      
        else
        b=0;
        end

        
        dN(i)=a*NOneOrderLower(i)-b*NOneOrderLower(i+1);
    end
    
end

end
