function isSmallerThan = isSmallerThan( value1, value2, tolerance )

if nargin<3
    tolerance=1e-14;
end

isSmallerThan = value1<=value2+tolerance;

end

