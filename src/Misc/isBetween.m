function isBetween = isBetween( value, value1, value2, tolerance )

if nargin<4
    tolerance=1e-14;
end

isBetween = value1-tolerance <= value & value <= value2+tolerance;

end

