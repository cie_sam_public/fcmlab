function isClose = isClose( value1, value2, tolerance )

if nargin<3
    tolerance=1e-13;
end

isClose = abs(value1-value2)<tolerance;

end

