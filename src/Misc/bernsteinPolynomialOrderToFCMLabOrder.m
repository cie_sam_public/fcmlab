function result = bernsteinPolynomialOrderToFCMLabOrder(vector, numberOfDofsPerDirection)

    nodalModes = [1 numberOfDofsPerDirection];
    internalModes = 2:numberOfDofsPerDirection-1;
    result = [vector(:,nodalModes) vector(:,internalModes)];

end