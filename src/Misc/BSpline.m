function y =BSpline(xi, knotVector, i, p, diffOrder)
    y = zeros(length(i), length(xi));
 
    for ix = 1:length(i)
        j=i(ix);
        y(ix, :) = BSplineSingle(xi, knotVector, j, p, diffOrder);
    end
end


function y = BSplineSingle(xi, knotVector, i, p, diffOrder)

    xiI = knotVector(i);
    xiIp1 = knotVector(i+1);
        
    if diffOrder == 0
        
        if p==0 && i==1
           y = ( xiI <= xi & xi <= xiIp1 ) .* 1;
        elseif p==0
           y = ( xiI < xi & xi <= xiIp1 ) .* 1;
        elseif i==1
            y =     (xiI <= xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( xi - xiI                 , knotVector(i+p)   -xiI    ) .* BSpline(xi, knotVector, i  , p-1, 0);
            y = y + (xiI <= xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( knotVector(i+p+1) - xi   , knotVector(i+p+1) -xiIp1  ) .* BSpline(xi, knotVector, i+1, p-1, 0);            
        else
            y =     (xiI < xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( xi - xiI                 , knotVector(i+p)   -xiI    ) .* BSpline(xi, knotVector, i  , p-1, 0);
            y = y + (xiI < xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( knotVector(i+p+1) - xi   , knotVector(i+p+1) -xiIp1  ) .* BSpline(xi, knotVector, i+1, p-1, 0);
        end
        
    else
        
        if diffOrder == 1
            
            if p==0
               y = zeros(size(xi));
            else
                y =     (xiI <= xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( p, knotVector(i+p)   -xiI    ) .* BSpline(xi, knotVector, i  , p-1, 0);
                y = y - (xiI <= xi & xi <= knotVector(i+p+1) ) .* divideWithConvention( p, knotVector(i+p+1) -xiIp1  ) .* BSpline(xi, knotVector, i+1, p-1, 0);
            end
            
        end
        
    end
        
end

function res = divideWithConvention(dividend, divisor)
    if not (divisor==0)
        res = dividend./divisor;
    else
        res(dividend==0) = 1;
        res(dividend~=0) = 0;
    end
end
