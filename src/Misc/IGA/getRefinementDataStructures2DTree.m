function [Aa Am Ap Ea] = getRefinementDataStructures2DTree( tree, multiLevelGeometry )

% does not work for repeated internal knots

maxLevel = length(multiLevelGeometry);
p = multiLevelGeometry{1}.getPolynomialDegreeXi;

for iLevel = 1:maxLevel
    
    knotsXi = multiLevelGeometry{iLevel}.getKnotsXi;
    knotsEta = multiLevelGeometry{iLevel}.getKnotsEta;
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D(knotsXi, multiLevelGeometry{iLevel}.getPolynomialDegreeXi,knotsEta,multiLevelGeometry{iLevel}.getPolynomialDegreeEta, 1);
    
    levelKnotSize = 2^(maxLevel-iLevel);
    levelSupportSize = levelKnotSize * (p+1);
        
    numberOfBasisInX = multiLevelGeometry{iLevel}.numberOfControlPointsInXi;
    numberOfBasisInY = multiLevelGeometry{iLevel}.numberOfControlPointsInEta;
        
    numberOfElementsX = multiLevelGeometry{iLevel}.numberOfKnotSpansInXi ;
    numberOfElementsY = multiLevelGeometry{iLevel}.numberOfKnotSpansInEta;
    
    AaLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    AmLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    ApLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    EaLevel =  zeros(numberOfElementsX,numberOfElementsY);
    
    [~,multsXi] = knt2brk(knotsXi);
    [~,multsEta] = knt2brk(knotsEta);
    
    knotElementIxXi = zeros(1,sum(multsXi));
    knotElementIxEta = zeros(1,sum(multsEta));
    base = 1;
    knotIx = 1;
    for iElemXi = 1:length(multsXi)
        knotElementIxXi(knotIx + (1:multsXi(iElemXi))-1) = base;
        
        knotIx = knotIx + multsXi(iElemXi);
        base = base + levelKnotSize;
    end
    base = 1;
    knotIx = 1;
    for iElemEta = 1:length(multsEta)
        knotElementIxEta(knotIx + (1:multsEta(iElemEta))-1) = base;
        
        knotIx = knotIx + multsEta(iElemEta);
        base = base + levelKnotSize;
    end
    
    
    for iKnotY = 1:numberOfBasisInY
        for iKnotX = 1:numberOfBasisInX
%             spansX = (iKnotX-1)*levelKnotSize + (1:levelSupportSize);
%             spansY = (iKnotY-1)*levelKnotSize + (1:levelSupportSize);
            spansX = iKnotX + (0:p);
            spansY = iKnotY + (0:p);
            supportStart = [ knotElementIxXi(spansX(1))  knotElementIxEta(spansY(1))];
            supportEnd = [ knotElementIxXi(spansX(end)+1)   knotElementIxEta(spansY(end)+1) ];
            
            supportKnots = tree.levelsInBox(supportStart, supportEnd);
            if all(all( supportKnots == iLevel ))
                AaLevel(iKnotX , iKnotY ) = 1;               
            elseif any(any( supportKnots < iLevel )) && any(any( supportKnots >= iLevel ))
                AmLevel(iKnotX , iKnotY) = 1;
            elseif any(any( supportKnots > iLevel )) && any(any( supportKnots == iLevel ))
                ApLevel(iKnotX , iKnotY ) = 1;
                AaLevel(iKnotX , iKnotY ) = 1;
            end
            
        end
    end


     [elementIxXi,~] = knt2brk(knotElementIxXi);
     [elementIxEta,~] = knt2brk(knotElementIxXi);
    for iKnotY = 1:numberOfElementsY
        for iKnotX = 1:numberOfElementsX
            
            elementStart = [ elementIxXi(iKnotX)  elementIxEta(iKnotY)];
            elementEnd = [ elementIxXi(iKnotX+1)   elementIxEta(iKnotY+1) ];
            
            knotLevels = tree.levelsInBox(elementStart, elementEnd);
            %knotLevels = levels( (iKnotX-1)*levelKnotSize +(1:levelKnotSize), (iKnotY-1)*levelKnotSize +(1:levelKnotSize)  );
            
            if all(all( knotLevels == iLevel ))
                EaLevel(iKnotX,iKnotY) = 1;
            end
        end
    end
   
    
    Aa{iLevel} =  AaLevel;
    Am{iLevel} =  AmLevel;
    Ap{iLevel} =  ApLevel;
    Ea{iLevel} =  EaLevel;
    
end

end

