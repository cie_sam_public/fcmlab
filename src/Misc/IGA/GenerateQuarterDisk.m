function surface=GenerateQuarterDisk(r_inn,r_out)

        knot=struct('xi',[-1 -1 -1 1 1 1],'eta',[-1 -1 -1 1 1 1]);
        p1=[0 r_inn];
        p2=[r_inn r_inn];
        p3=[r_inn 0];
        p4=[0 r_out];
        p5=[r_out r_out];
        p6=[r_out 0];
        B=zeros(3,3,3);
        B(1,1,1:2)=p1;
        B(2,1,1:2)=p2;
        B(3,1,1:2)=p3;
        B(1,2,1:2)=(p1+p4)/2;
        B(2,2,1:2)=(p2+p5)/2;
        B(3,2,1:2)=(p3+p6)/2;
        B(1,1,3)=1;
        B(1,2,3)=1;
        B(2,1,3)=1/sqrt(2); B(2,2,3)=1/sqrt(2);
        B(3,1,3)=1;
        B(3,2,3)=1;
        B(1,3,1:2)=p4;
        B(2,3,1:2)=p5;
        B(3,3,1:2)=p6;
        B(1,3,3)=1;
        B(2,3,3)=1/sqrt(2);
        B(3,3,3)=1;
  
ControlPoints = reshape(B, size(B,1)*size(B,2), 3 );
ControlPoints(:,4) = ControlPoints(:,3);
ControlPoints(:,3) = 0;
KnotsXi = knot.xi;
PolynomialDegreeXi = 2;
KnotsEta = knot.eta;
PolynomialDegreeEta = 2;

surface = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints );
end