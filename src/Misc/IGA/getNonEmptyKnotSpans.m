function nonEmptyKnotSpans = getNonEmptyKnotSpans(XI)

i = 2;
nonEmptyKnotSpans = XI(1);

while i<length(XI)
    while i<length(XI) && nonEmptyKnotSpans(end) == XI(i)
        i=i+1;
    end
    if nonEmptyKnotSpans(end) ~= XI(i)
        nonEmptyKnotSpans = [nonEmptyKnotSpans XI(i) ];
    end
    i=i+1;
end

end