function ExtractionOperators = bezierExtractionOperator2D( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, Ea )


[extractionOperatorsXi, numberOfSpansInXi] = bezierExtractionOperator(KnotsXi, PolynomialDegreeXi);
[extractionOperatorsEta, numberOfSpansInEta] = bezierExtractionOperator(KnotsEta, PolynomialDegreeEta);

if nargin<5
    for iXi=1:numberOfSpansInXi
        for iEta=1:numberOfSpansInEta
            ExtractionOperators( :,:,(iEta-1)*numberOfSpansInXi + iXi) = kron( extractionOperatorsEta(:,:, iEta), extractionOperatorsXi(:,:, iXi)  );
        end
    end
else
    ExtractionOperators = cell(numberOfSpansInXi,numberOfSpansInEta);
    for iEta=1:numberOfSpansInEta
        for iXi=1:numberOfSpansInXi
            if Ea(iXi,iEta) ==1
                ExtractionOperators{ iXi, iEta } = kron( extractionOperatorsEta(:,:, iEta), extractionOperatorsXi(:,:, iXi)  );
            end
        end
    end
end


end

