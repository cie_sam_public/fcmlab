function IEN = extractedBSplineLocationMap(knots, p)

    numberOfControlPoints = size(knots, 2) - p - 1; 
    iSpan = p+1;
    iNonEmptySpan = 1;

    while iSpan<numberOfControlPoints+1
        
        iLocalBasisFunction=1:p+1;
        IEN( iNonEmptySpan, iLocalBasisFunction ) = ( iSpan-(p+1) ) + iLocalBasisFunction;
        
        % move to next knot span
        iSpan = iSpan+1;
        
        % skip empy spans
        while knots(iSpan+1) == knots(iSpan) && iSpan<numberOfControlPoints+1
            iSpan = iSpan+1; 
        end

        % move to next non-empty knot span
        iNonEmptySpan = iNonEmptySpan+1;
        
    end
    
end