%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Builds an equidistant open knot Vector in the range [-1,1], with equal-sized 
% sections (knot is repaeated p+1 times) 
% and equidistant knots with a certain multiplicity within EVERY section
% EXAMPLE: sections=2 knots=1 pmax=2 knotMultipliciy=1 yields:
%          U=[-1 -1 -1 -0.5 0 0 0 0.5 1 1 1]
%            /----section1----/---section2--/

function U=buildOpenEquidistantKnotVector(sections,knots,pmax,knotMultiplicity)

U=[];
for s=0:sections-1
    
    startValue=-1.0+2*s/sections; % start Value of current section
    endValue=startValue+2/sections;% end Value of current section
    
    UcurrentSection(1:pmax+1)=startValue; %repeat the  start knot p+1 times
    
    for i=1:knots
        UcurrentSection(pmax+2+(i-1)*knotMultiplicity:pmax+1+(i)*knotMultiplicity)=startValue+(-startValue+endValue)/(knots+1.0)*i; %repeat knot with a certain multiplicity
    end
    
    if s==sections-1 % if the section is the last
       UcurrentSection(pmax+2+(knots)*knotMultiplicity:2*pmax+2+(knots)*knotMultiplicity)=endValue;  %repeat the end-knot p+1 times
    end
    
    U=[U UcurrentSection];
    
end

end
