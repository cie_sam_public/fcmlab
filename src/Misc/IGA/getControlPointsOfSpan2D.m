function indices = getControlPointsOfSpan2D( iXi, iEta, KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta )

    numberOfControlPointsInXi = length( KnotsXi ) - PolynomialDegreeXi - 1;
    
    [spansXi, multiplicitiesXi ] = knt2brk( KnotsXi );
    [spansEta, multiplicitiesEta ] = knt2brk( KnotsEta );
    
    offsetXi = sum( multiplicitiesXi(1:iXi) ) - iXi - PolynomialDegreeXi ; 
    offsetEta = sum( multiplicitiesEta(1:iEta) ) -iEta - PolynomialDegreeEta ; 

    indicesXi= offsetXi + iXi -1 + (1:PolynomialDegreeXi+1);
    indicesEta= offsetEta + iEta -1 + (1:PolynomialDegreeEta+1);

    indices = repmat((indicesEta-1)*(numberOfControlPointsInXi), length(indicesXi), 1) + ( repmat(indicesXi, length(indicesEta), 1) )';
    indices = reshape( indices, size(indices,1)*size(indices,2), 1 );

end

