function IEN = extractedBSplineLocationMap2D(knotsXi, pXi, knotsEta, pEta, numberOfFieldComponents)

    if nargin<5
        numberOfFieldComponents = 2;
    end
    
    IENXi = extractedBSplineLocationMap(knotsXi, pXi);
    
    numberOfSpansInXi = size(IENXi, 1);
    numberOfLocalDofsXi = pXi+1;
    dofsInXi = max(max(IENXi));
    
    numberOfSpansInEta = getNumberOfKnotSpans(knotsEta);
    
    IEN = zeros(numberOfSpansInXi * numberOfSpansInEta, (pEta+1)*numberOfLocalDofsXi );
    
    for eXi = 1:pEta+1
        IEN(1:numberOfSpansInXi, (eXi-1)*numberOfLocalDofsXi+(1:numberOfLocalDofsXi)) = (eXi-1)*dofsInXi + IENXi;
    end
    
    nEta = size(knotsEta, 2) - pEta - 1;
    l = pEta+2;
    eEta = 2;

    while l < nEta+1
       
        mult=1;
       while knotsEta(l+1) == knotsEta(l) && l<nEta+1
           mult = mult+1;
          l = l+1; 
       end
       
       IEN((eEta-1)*numberOfSpansInXi+(1:numberOfSpansInXi), :) =  mult*dofsInXi + IEN((eEta-2)*numberOfSpansInXi+(1:numberOfSpansInXi), :);
        
       eEta = eEta+1;
       l = l+1; 
    end
    
    m = max(max(IEN));
    s = size(IEN, 2);
    IENforFieldComponent = IEN;
    for i = 2:numberOfFieldComponents
        IEN( :, (i-1)*s+(1:s) ) = (i-1)*m + IENforFieldComponent;
    end
    
    
    
end
