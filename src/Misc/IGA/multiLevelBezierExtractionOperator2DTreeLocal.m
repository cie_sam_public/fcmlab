function [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTree(  multiLevelGeometry, truncateBasis )

geometries = multiLevelGeometry.getGeometries;
PolynomialDegree = geometries{1}.getPolynomialDegreeXi;

[Aa, Am, Ap, Ea] = multiLevelGeometry.getRefinementDataStructures;

Mop = cell(1, length(geometries));
standardLocationMap = cell(1, length(geometries));
parfor iLevel = 1:length(geometries)-1
    Mop{iLevel+1} = bs2bs_local_2D(geometries{iLevel}.getPolynomialDegreeEta,geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta,geometries{iLevel+1}.getKnotsEta, geometries{iLevel}.getKnotsXi,geometries{iLevel+1}.getKnotsXi, Ea, iLevel+1);
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);
end
iLevel=length(geometries);
standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);


for iLevel =  1:length(geometries)
    
    
    if any(any(Ea{iLevel}))
        
        LevelExtractionOperators = bezierExtractionOperator2D( geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta, Ea{iLevel} );
        
        for iElementY = 1:size(Ea{iLevel},2)
            for iElementX = 1:size(Ea{iLevel},1)
                
                if Ea{iLevel}(iElementX,iElementY) == 1
                    MeExtensionOperator2 = [];
                    AmMeExtensionOperator2=[];
                    
                    numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevel}.getKnotsXi );
                    thisindices = standardLocationMap{iLevel} ( (iElementY-1)*numberOfElementsInXi + iElementX, : );
                    
                    for iLevelToCouple = 1:iLevel-1
                        previousLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iLevelToCouple));
                        previousLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iLevelToCouple));
                        
                        numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevelToCouple}.getKnotsXi );
                        previousindices = standardLocationMap{iLevelToCouple} ( (previousLevelIndexY-1)*numberOfElementsInXi + previousLevelIndexX, : );
                        
                        apLevelToCouple = Ap{iLevelToCouple}(previousindices);
                        apLevelToCouple = logical(apLevelToCouple(:));
                        amCurrentLevel = Am{iLevel}(thisindices);
                        amCurrentLevel=logical(amCurrentLevel(:));
                        
% %                         %version3: local matrix
%                         Mloc = eye((PolynomialDegree+1)^2);
%                         for iIntermediateLevel = iLevelToCouple+1:iLevel
%                             intermediateLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iIntermediateLevel));
%                             intermediateLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iIntermediateLevel));
%                             intermediateindices = standardLocationMap{iIntermediateLevel} ( (intermediateLevelIndexY-1)*size(Ea{iIntermediateLevel},1) + intermediateLevelIndexX, : );
%                             
%                             intermediateMloc = Mop{iIntermediateLevel}{intermediateLevelIndexX, intermediateLevelIndexY};
%                             
%                             Mloc =  Mloc * intermediateMloc;
%                             
%                             Mloc(~ Ap{iLevelToCouple}(previousindices)', : ) = 0;
%                             Mloc(:, ~ Am{iIntermediateLevel}(intermediateindices) ) = 0;
%                         end

                        %version3: local matrix withot zero rows
                        Mloc2 = eye((PolynomialDegree+1)^2);
                        Mloc2(~apLevelToCouple,:) = [];
                        for iIntermediateLevel = iLevelToCouple+1:iLevel
                            intermediateLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iIntermediateLevel));
                            intermediateLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iIntermediateLevel));
                            intermediateindices = standardLocationMap{iIntermediateLevel} ( (intermediateLevelIndexY-1)*size(Ea{iIntermediateLevel},1) + intermediateLevelIndexX, : );
                            
                            intermediateMloc = Mop{iIntermediateLevel}{intermediateLevelIndexX, intermediateLevelIndexY};                            
                            Mloc2 =  Mloc2 * intermediateMloc;
                            if truncateBasis
                                Mloc2(:, ~ Am{iIntermediateLevel}(intermediateindices) ) = 0;
                            end
                        end                      
                        
                        Mloc = zeros((PolynomialDegree+1)^2);
                        Mloc(apLevelToCouple,:) = Mloc2;
                        
                        %version3.1: local matrix with zeros
                        MeExtensionOperator2  = [MeExtensionOperator2; Mloc];
                        
                        %version3.2: local matrix without zeros
                        %AmElementMLevelCoupling2 = Mloc(apLevelToCouple,amCurrentLevel);
                        AmElementMLevelCoupling2 = Mloc2(:,amCurrentLevel);
                        AmMeExtensionOperator2  = [AmMeExtensionOperator2; AmElementMLevelCoupling2];
                    end
                    
                    if iLevel>4 && any(Aa{iLevel}(thisindices))
                       Aa{iLevel}(thisindices) 
                    end
                    
                    MeExtensionOperator2 = [MeExtensionOperator2; diag( Aa{iLevel}(thisindices) )];
                    
                    %MultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                    MultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators{iElementX,iElementY};
                    MultiLevelOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2;
                    %BezierExtractionOperator{iLevel}{iElementX, iElementY} = LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                    BezierExtractionOperator{iLevel}{iElementX, iElementY} = LevelExtractionOperators{iElementX,iElementY};
                    
                    AmMultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY}  = AmMeExtensionOperator2;
                end
            end
        end
        
    end
end


end