
function tensorProductTest

InnerRadius = 1;
    OuterRadius = 8;
    geometryDescription = GenerateQuarterDisk(InnerRadius, OuterRadius);
    
    
    PolynomialDegree=2;
    NumberOfXDivisions=2;
    NumberOfYDivisions=2;
    
    IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
    IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;
    
    knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
    knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);
    
    knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
    knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);
    
    geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
    geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
    
    
    NumberOfGaussPoints = PolynomialDegree+2; % number of Gauss points
    
    numberOfLevels = 2;
    
    multiLevelGeometries{1} = geometryDescription;
    for iLevel = 2:numberOfLevels
        multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
    end
    
    
%     Levels = [1 1 1 1 2 2 2 2; ...
%               1 1 1 1 2 2 2 2; ...
%               1 1 1 1 2 2 2 2; ...
%               1 1 1 1 2 2 2 2; ...
%               2 2 2 2 2 2 2 2; ...
%               2 2 2 2 2 2 2 2; ...
%               2 2 2 2 2 2 2 2; ...
%               2 2 2 2 2 2 2 2];

%     Levels = [2  2 1  1; ...
%               2 2 1 1; ...
%               2 2 2 2; ...
%               2 2 2 2];

%     Levels = [ 1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%               2 2 2 2 1 1 1 1; ...
%               2 2 2 2 1 1 1 1; ...
%               2 2 2 2 1 1 1 1; ...
%               2 2 2 2 1 1 1 1];
    Levels = [ 1 1 1 1; ...
               1 1 1 1; ...
              2 2  1 1; ...
              2 2  1 1];       

%     Levels = [ 1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%                1 1 1 1 1 1 1 1; ...
%                2 2 3 3 1 1 1 1; ...
%                2 2 3 3 1 1 1 1; ...
%                2 2 2 2 1 1 1 1; ...
%                2 2 2 2 1 1 1 1];

    flip(Levels')
          
    multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, 1, Levels);
    geometries = multiLevelGeometry.getGeometries;
    [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator]  = multiLevelBezierExtractionOperator2DTree(multiLevelGeometry);
    
    
    
    [Aa, Am, Ap, Ea] = multiLevelGeometry.getRefinementDataStructures;
    
    figure;
    for iLevel =  1:length(multiLevelGeometries)
        standardLocationMapXi{iLevel} = extractedBSplineLocationMap(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi);
         standardLocationMapEta{iLevel} = extractedBSplineLocationMap(geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta);
         
        for iElementY = 1:size(Ea{iLevel},2)
            for iElementX = 1:size(Ea{iLevel},1)
                if Ea{iLevel}(iElementX,iElementY) == 1
                    clear V;
                    
                                     MultiLevelOperator{iLevel}{iElementX, iElementY}
                            iLevel
                            iElementX
                            iElementY
                            
                    x = [-1:0.1:1];
                    y = [-1:0.1:1];
                    
                        for iy = 1:length(y)
                            for ix = 1:length(x)
                            BezierBasis =  getBernsteinPolynomialTensorProduct2D([x(ix),y(iy)], 1:PolynomialDegree+1, PolynomialDegree, 1:PolynomialDegree+1, PolynomialDegree, 0);
                            
                            %version 1: full matrix multiplication
                            BSPlineShapes = BezierBasis * MultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY}' ;
                            V(:, ix + length(x) * (iy-1)) = BSPlineShapes';
                            
                            
                            for iElementBasisLevel = 1:iLevel
                                intermediateLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iElementBasisLevel));
                                intermediateLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iElementBasisLevel));
                                
%                                 standardBSPlineShapes = BezierBasis * BezierExtractionOperator{iElementBasisLevel}{intermediateLevelIndexX, intermediateLevelIndexY}' ;
%                                 Vstd{iElementBasisLevel}(:, ix + length(x) * (iy-1)) = standardBSPlineShapes';
                                
                                knotsXi = geometries{iLevel}.getKnotsXi;
                                knotsEta = geometries{iLevel}.getKnotsEta;
                                knotsXi = unique(knotsXi);
                                knotsEta = unique(knotsEta);
                                standardBSPlineShapes2 = BSpline(knotsXi(iElementX)+ (x(ix)+1)/2 *(knotsXi(iElementX+1)-knotsXi(iElementX)), geometries{iElementBasisLevel}.getKnotsXi, standardLocationMapXi{iElementBasisLevel}(intermediateLevelIndexX,:), geometries{iElementBasisLevel}.getPolynomialDegreeXi,0)...
                                    * BSpline(knotsEta(iElementY)+ (y(iy)+1)/2 *(knotsEta(iElementY+1)-knotsEta(iElementY)), geometries{iElementBasisLevel}.getKnotsEta, standardLocationMapEta{iElementBasisLevel}(intermediateLevelIndexY,:), geometries{iElementBasisLevel}.getPolynomialDegreeEta, 0)';
                                standardBSPlineShapes2 = reshape(standardBSPlineShapes2, 1, size(standardBSPlineShapes2,1)*size(standardBSPlineShapes2,2));
                                Vstd2{iElementBasisLevel}(:, ix + length(x) * (iy-1)) = standardBSPlineShapes2';
                            end
                        end
                    end
                    
                       
                             close all;
                     [X,Y] = meshgrid(x,y);
                    for iElementBasisLevel = 1:iLevel
                        figure;
                        %title(['element ' num2str(iLevel) ' '  num2str(iElementX) ' ' num2str(iElementY) ])
                        for iBasisX = 1:PolynomialDegree+1
                            for iBasisY = 1:PolynomialDegree+1
                                subplot((PolynomialDegree+1),(PolynomialDegree+1),(iBasisY-1)*(PolynomialDegree+1)+iBasisX)
                                C = reshape( V((iElementBasisLevel-1)*(PolynomialDegree+1)^2+(iBasisY-1)*(PolynomialDegree+1)+iBasisX,:) ,length(x), length(y));
                                surf(X, Y, C);
%                                 xlabel('x');
%                                 ylabel('y');
                                title([num2str(iElementBasisLevel) ': ' num2str(iBasisX) ' ' num2str(iBasisY) ' ' num2str((iBasisY-1)*(PolynomialDegree+1)+iBasisX)]);
                            end
                        end
                    end
                                       
                    for iElementBasisLevel = 1:iLevel
                        figure;
                        %title(['element ' num2str(iLevel) ' '  num2str(iElementX) ' ' num2str(iElementY) ])
                        for iBasisX = 1:PolynomialDegree+1
                            for iBasisY = 1:PolynomialDegree+1
                                subplot((PolynomialDegree+1),(PolynomialDegree+1),(iBasisY-1)*(PolynomialDegree+1)+iBasisX)
                                
                                
                                C = reshape( Vstd2{iElementBasisLevel}((iBasisY-1)*(PolynomialDegree+1)+iBasisX,:) ,length(x), length(y));
                                surf(X, Y, C);
%                                 xlabel('x');
%                                 ylabel('y');
                                title([num2str(iElementBasisLevel) ': ' num2str(iBasisX) ' ' num2str(iBasisY) ' ' num2str((iBasisY-1)*(PolynomialDegree+1)+iBasisX)]);
                            end
                        end
                   end
                    
                end
            end
        end
    end
end



function [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator, Mopg] = multiLevelBezierExtractionOperator2DTree(  multiLevelGeometry )

geometries = multiLevelGeometry.getGeometries;
PolynomialDegree = geometries{1}.getPolynomialDegreeXi;

[Aa, Am, Ap, Ea] = multiLevelGeometry.getRefinementDataStructures;

Mop = cell(1, length(geometries));
standardLocationMap = cell(1, length(geometries));
parfor iLevel = 1:length(geometries)-1
    [Mopg{iLevel+1}] = bs2bs_global_2D(geometries{iLevel}.getPolynomialDegreeEta,geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta,geometries{iLevel+1}.getKnotsEta, geometries{iLevel}.getKnotsXi,geometries{iLevel+1}.getKnotsXi);
    [Mop{iLevel+1}, Sxi{iLevel+1}, Seta{iLevel+1}] = bs2bs_local_2D(geometries{iLevel}.getPolynomialDegreeEta,geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta,geometries{iLevel+1}.getKnotsEta, geometries{iLevel}.getKnotsXi,geometries{iLevel+1}.getKnotsXi, Ea, iLevel+1);
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);
    
     standardLocationMapXi{iLevel} = extractedBSplineLocationMap(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi);
     standardLocationMapEta{iLevel} = extractedBSplineLocationMap(geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta);
end
iLevel=length(geometries);
standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);


for iLevel =  1:length(geometries)
    
    
    if any(any(Ea{iLevel}))
        
        LevelExtractionOperators = bezierExtractionOperator2D( geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta, ones(size( Ea{iLevel})) );
        
        for iElementY = 1:size(Ea{iLevel},2)
            for iElementX = 1:size(Ea{iLevel},1)
                
                if Ea{iLevel}(iElementX,iElementY) == 1
                    MeExtensionOperator2 = [];
                    AmMeExtensionOperator2=[];
                    
                    numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevel}.getKnotsXi );
                    thisindices = standardLocationMap{iLevel} ( (iElementY-1)*numberOfElementsInXi + iElementX, : );
                    
                    for iLevelToCouple = 1:iLevel-1
                        previousLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iLevelToCouple));
                        previousLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iLevelToCouple));
                        
                        numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevelToCouple}.getKnotsXi );
                        previousindices = standardLocationMap{iLevelToCouple} ( (previousLevelIndexY-1)*numberOfElementsInXi + previousLevelIndexX, : );
                        
                        apLevelToCouple = Ap{iLevelToCouple}(previousindices);
                        apLevelToCouple = logical(apLevelToCouple(:));
                        amCurrentLevel = Am{iLevel}(thisindices);
                        amCurrentLevel=logical(amCurrentLevel(:));
                        
% %                         %version3: local matrix
%                         Mloc = eye((PolynomialDegree+1)^2);
%                         for iIntermediateLevel = iLevelToCouple+1:iLevel
%                             intermediateLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iIntermediateLevel));
%                             intermediateLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iIntermediateLevel));
%                             intermediateindices = standardLocationMap{iIntermediateLevel} ( (intermediateLevelIndexY-1)*size(Ea{iIntermediateLevel},1) + intermediateLevelIndexX, : );
%                             
%                             intermediateMloc = Mop{iIntermediateLevel}{intermediateLevelIndexX, intermediateLevelIndexY};
%                             
%                             Mloc =  Mloc * intermediateMloc;
%                             
%                             Mloc(~ Ap{iLevelToCouple}(previousindices)', : ) = 0;
%                             Mloc(:, ~ Am{iIntermediateLevel}(intermediateindices) ) = 0;
%                         end

                        %version3: local matrix withot zero rows
                        Mloc2 = eye((PolynomialDegree+1)^2);
                        %Mloc2(~apLevelToCouple,:) = [];
                        for iIntermediateLevel = iLevelToCouple+1:iLevel
                            intermediateLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iIntermediateLevel));
                            intermediateLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iIntermediateLevel));
                            intermediateindices = standardLocationMap{iIntermediateLevel} ( (intermediateLevelIndexY-1)*size(Ea{iIntermediateLevel},1) + intermediateLevelIndexX, : );
                            
                            intermediateMloc = Mop{iIntermediateLevel}{intermediateLevelIndexX, intermediateLevelIndexY};                            
                            Mloc2 =  Mloc2 * intermediateMloc;
                         %   Mloc2(:, ~ Am{iIntermediateLevel}(intermediateindices) ) = 0;
                            
%                             intermediateindicesXi = standardLocationMapXi{iIntermediateLevel};
%                             intermediateindicesEta = standardLocationMapEta{iIntermediateLevel};
%                             intermediateMlocXi = Sxi{iIntermediateLevel}; 
%                             intermediateMlocEta = Seta{iIntermediateLevel}; 
                        end                      
                        
                        %Mloc = zeros((PolynomialDegree+1)^2);
                        %Mloc(apLevelToCouple,:) = Mloc2;
                        %Mloc = Mloc2;
                        Mloc(apLevelToCouple,:) = Mloc2(apLevelToCouple,:);
                        
                        %version3.1: local matrix with zeros
                        MeExtensionOperator2  = [MeExtensionOperator2; Mloc];
                        
                        %version3.2: local matrix without zeros
                        %AmElementMLevelCoupling2 = Mloc(apLevelToCouple,amCurrentLevel);
                        AmElementMLevelCoupling2 = Mloc2(:,amCurrentLevel);
                        AmMeExtensionOperator2  = [AmMeExtensionOperator2; AmElementMLevelCoupling2];
                    end
                                        
                    MeExtensionOperator2 = [MeExtensionOperator2; diag( Aa{iLevel}(thisindices) )];
                    
                    %MultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                    MultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators{iElementX,iElementY};
                    MultiLevelOperator{iLevel}{iElementX, iElementY} = MeExtensionOperator2;
                    %BezierExtractionOperator{iLevel}{iElementX, iElementY} = LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                    BezierExtractionOperator{iLevel}{iElementX, iElementY} = LevelExtractionOperators{iElementX,iElementY};
                    
                    AmMultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY}  = AmMeExtensionOperator2;
                end
                
                BezierExtractionOperator{iLevel}{iElementX, iElementY} = LevelExtractionOperators{iElementX,iElementY};
            end
        end
        
    end
end


end