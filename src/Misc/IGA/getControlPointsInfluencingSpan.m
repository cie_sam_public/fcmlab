function indicesOfControlPointsInfluencingSpan = getControlPointsInfluencingSpan( iXi, KnotsXi, PolynomialDegreeXi )
% computes the indices of controlo points influencing a knot span of a
% BSpline
% Parameters
%  iXi : index of knot span starting from 1
%  KnotsXi: knot vector of the BSpline
%  PolynomialDegreeXi = polynomial degree of the BSpline
%
% Returns
%   row vector of indices starting from 1 of control point influencing span with index
%   iXi of the BSpline identified by KnotsXi, PolynomialDegreeXi
%
% Example
%   getControlPointsInfluencingSpan( 3, [-1 -1 -1 -0.5 0 0 0.5 1 1 1], 2 )
%   returns PolynomialDegreeXi [4 5 6]
   
 
    lm = extractedBSplineLocationMap(KnotsXi,PolynomialDegreeXi );
    indicesOfControlPointsInfluencingSpan = lm(iXi, : );
end
