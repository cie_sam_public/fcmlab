 function plotCurve( geometricSupport )
    xiPoints = -1:0.1:1;

    for iXi=1:length(xiPoints)
            coords = geometricSupport.mapLocalToGlobal([xiPoints(iXi) 0 0]);

            %C((iEta-1)*(length(xiPoints)) +iXi, :) = coords;
            
              X(iXi) = coords(1);
              Y(iXi) = coords(2);
              Z(iXi) = coords(3);
    end
    
    
    plot3(X,Y,Z);
    
    localPoint = [0.3 0 0];
     J = geometricSupport.calcJacobian(localPoint);
     coords = geometricSupport.mapLocalToGlobal(localPoint);
     
     hold on;

         quiver3(coords(1),coords(2),coords(3),J(1,1),J(1,2),J(1,3)); 
                  
    hold off;
    
 end