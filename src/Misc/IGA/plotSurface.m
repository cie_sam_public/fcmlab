function plotSurface( geometricSupport )


xiPoints = -1:0.1:1;
etaPoints = -1:0.1:1;

for iXi=1:length(xiPoints)
    for iEta=1:length(etaPoints)
        coords = geometricSupport.mapLocalToGlobal([xiPoints(iXi) etaPoints(iEta) 0]);
        
        
        C((iEta-1)*(length(xiPoints)) +iXi, :) = coords;
        
        X(iEta, iXi) = coords(1);
        Y(iEta, iXi) = coords(2);
        Z(iEta, iXi) = coords(3);
    end
end


surf(X,Y, Z);

hold on;

%plotControlPoints(geometricSupport.getControlPoints);

localPoint = [0 0 0];
J = geometricSupport.calcJacobian(localPoint);
coords = geometricSupport.mapLocalToGlobal(localPoint);



quiver3(coords(1),coords(2),coords(3),J(1,1),J(1,2),J(1,3));
quiver3(coords(1),coords(2),coords(3),J(2,1),J(2,2),J(2,3));



hold off;

end

