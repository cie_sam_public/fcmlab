function  plotControlPoints( controlPoints )

            fillColor = [1 0 0];
            edgeColor = [0.4 0 0];
            %circleRadius = min( dimensions )/30;
            circleRadius = 0.01;
            
for iControlPoint = 1:size(controlPoints, 1)
    
    controlPoint = controlPoints(iControlPoint, :);
    pos = [controlPoint(1:2)-circleRadius/2 circleRadius circleRadius];
    
    rectangle('Position',pos, ...
        'Curvature',[1 1], ...
        'FaceColor',fillColor , ...
        'EdgeColor',edgeColor ...
        );
end


end