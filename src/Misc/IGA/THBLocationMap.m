function [ LM ] = THBLocationMap( multiLevelGeometry, numberOfFieldComponents)

multiLevelGeometries = multiLevelGeometry.getGeometries;
PolynomialDegree = multiLevelGeometries{1}.getPolynomialDegreeXi;
%[Aa Am Ap Ea] = getRefinementDataStructures2DTree( refinementTree, multiLevelGeometries );

[Aa Am Ap Ea] = multiLevelGeometry.getRefinementDataStructures;

offset = 0;
activeElementIndex = 1;
for iLevel = 1:length(multiLevelGeometries)
    elementActiveShapes = Aa{iLevel};
    indexOfActiveShapes = find(elementActiveShapes);
    numberOfElementActiveShapes = sum(sum(elementActiveShapes));
    
    LMl = zeros(1, length(elementActiveShapes(:)));
    LMl(indexOfActiveShapes) = offset+(1:numberOfElementActiveShapes);
    
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D( multiLevelGeometries{iLevel}.getKnotsXi,  multiLevelGeometries{iLevel}.getPolynomialDegreeXi, multiLevelGeometries{iLevel}.getKnotsEta,  multiLevelGeometries{iLevel}.getPolynomialDegreeEta, 1);
    
    eLM{iLevel} = LMl;
    
    offset = offset + numberOfElementActiveShapes;
    
    
    levelActiveElements = Ea{iLevel};
    for iElementY = 1:size(levelActiveElements,2)
        for iElementX = 1:size(levelActiveElements,1)
            if levelActiveElements(iElementX, iElementY)
                
%                 thisindicesX = iElementX+(0:PolynomialDegree);
%                 thisindicesY = (iElementY+(0:PolynomialDegree)-1)*size(Aa{iLevel},1)+1;
%                 thisindices2=[];
%                 for i=1:length(thisindicesY)
%                     thisindices2 = [ thisindices2 thisindicesX+thisindicesY(i)-1];
%                 end
                
                     numberOfElementsInXi = getNumberOfKnotSpans( multiLevelGeometries{iLevel}.getKnotsXi );
                     thisindices = standardLocationMap{iLevel} ( (iElementY-1)*numberOfElementsInXi + iElementX, : );
                     
                     %assert(all(thisindices2==thisindices))
                
                %LM( activeElementIndex, 1:length(thisindices) ) = LMl(  thisindices );
                LM( activeElementIndex, (iLevel-1)*(PolynomialDegree+1)^2+1:(iLevel)*(PolynomialDegree+1)^2  ) = LMl(  thisindices );
                
                for iPreviousLevel = 1:iLevel-1
                    previousLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iPreviousLevel));
                    previousLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iPreviousLevel));
                    
%                     previousindicesX = previousLevelIndexX+(0:PolynomialDegree);
%                     previousindicesY = (previousLevelIndexY+(0:PolynomialDegree)-1)*size(Aa{iPreviousLevel},1)+1;
% 
%                     previousindices2=[];
%                     for i=1:length(previousindicesY)
%                         previousindices2 = [ previousindices2 previousindicesX+previousindicesY(i)-1];
%                     end
                    
                    numberOfElementsInXi = getNumberOfKnotSpans( multiLevelGeometries{iPreviousLevel}.getKnotsXi );
                    previousindices = standardLocationMap{iPreviousLevel} ( (previousLevelIndexY-1)*numberOfElementsInXi + previousLevelIndexX, : );
                    
                    %assert( all(previousindices == previousindices2) );
                    
                    LMpl = eLM{iPreviousLevel};
                    
                    LMpe = LMpl( previousindices );
%                     %truncation!!
%                     nonZeroCouplingFunctions = Am{iLevel}(thisindices);
%                     LMpe(~nonZeroCouplingFunctions) = 0;
                    
                    
                    LM( activeElementIndex, (iPreviousLevel-1)*(PolynomialDegree+1)^2+1:(iPreviousLevel)*(PolynomialDegree+1)^2 ) = LMpe;
                end
                
                activeElementIndex = activeElementIndex+1;
            end
        end
    end
end

if nargin<3
    numberOfFieldComponents=2;
end
m = max(max(LM( :,: )));
%s = size(LM, 2);

activeElementIndex=1;
for iLevel = 1:length(multiLevelGeometries)
    levelActiveElements = Ea{iLevel};
    for iElementY = 1:size(levelActiveElements,2)
        for iElementX = 1:size(levelActiveElements,1)
            if levelActiveElements(iElementX, iElementY)
                elementMaxLength = (iLevel)*(PolynomialDegree+1)^2;

                IENforFieldComponent = LM(activeElementIndex,1:elementMaxLength);
                for i = 2:numberOfFieldComponents
                    LM( activeElementIndex, (i-1)*elementMaxLength+(1:elementMaxLength) ) = (i-1)*m *double(IENforFieldComponent>0)  + IENforFieldComponent(1:elementMaxLength);
                end
                activeElementIndex=activeElementIndex+1;
            end
        end
    end
end





end

