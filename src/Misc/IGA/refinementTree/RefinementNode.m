%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef RefinementNode < handle
    
    methods (Access = public)
        
        %% constructor - pass an EmbeddedDomain object to it
        function obj = RefinementNode(parent, keyStart, keyEnd, level)
            obj.keyStart=keyStart;
            obj.keyEnd=keyEnd;
            %obj.content=content;
            obj.children=cell(0);
             obj.parent=parent;
             obj.level=level;
        end
        
        function isLeaf = isLeaf(obj)
            %isLeaf = isempty( obj.children );
            isLeaf = isempty( obj.keySplit );
        end
        
        
        function node = find(obj, key)
            node = obj;
            while ~ node.isLeaf
                node = node.selectChild(key);
            end
        end
        
        function levelOfLeaves = getLevelOfLeaves(obj)
            
            if ~ obj.isLeaf            
                levelOfLeaves=[];
                for iChild = 1:length(obj.children)
                    child = obj.children{iChild};
                    levelOfLeaves =  cat(2, levelOfLeaves, child.getLevelOfLeaves);
                end
            else
                levelOfLeaves = obj.level;
            end
        end
        
        function levelsInBox = levelsInBox(obj, keyStart, keyEnd)
            
            if obj.contained(keyStart, keyEnd)
                % node fully contained in box
                levelsInBox = obj.getLevelOfLeaves;

            elseif obj.isLeaf && obj.overlaps(keyStart, keyEnd)
                % leaf overlapping box
                levelsInBox = obj.level;
            elseif obj.overlaps(keyStart, keyEnd)
                % non-leaf overlaps box
                levelsInBox=[];
                for iChild = 1:length(obj.children)
                    child = obj.children{iChild};
                    levelsInBox =  cat(2, levelsInBox, child.levelsInBox( keyStart, keyEnd) );
                end
            else
                levelsInBox = [];
            end
            
        end
        
        function split(obj, splitKey, levels)
            leaf = obj.find(splitKey);
            leaf.splitLeaf(splitKey, levels);
        end
                
    end
    
    methods (Abstract)
        selectChild(obj, key)
        splitLeaf(obj, key, levels)
        overlaps(obj, keyStart, keyEnd)
    end
    
    properties(Access = public)
        keyStart
        keyEnd
        keySplit
        children
        parent
        level
        %content
    end
end