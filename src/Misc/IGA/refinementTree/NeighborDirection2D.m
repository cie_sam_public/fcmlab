classdef NeighborDirection2D < uint8
   enumeration
      S (1)
      E  (2)
      N (3)
      W  (4)
   end
   
   methods (Static)
       function reversedDirection = reverse(direction)
           reversedDirection = mod( direction+1, 4 ) + 1;
       end
   end
end


