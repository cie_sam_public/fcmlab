%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef RefinementNode2D < RefinementNode
    
    methods (Access = public)
        
        %% constructor - pass an EmbeddedDomain object to it
        function obj = RefinementNode2D(parent, keyStart, keyEnd, level, childIndex)
            obj@RefinementNode(parent, keyStart, keyEnd, level)
            obj.keyStart=keyStart;
            obj.keyEnd=keyEnd;
            obj.childIndex=childIndex;
        end
        
        function child = selectChild(obj, key)
            childIx = 2*( key(2) > obj.keySplit(2) ) +  (key(1) > obj.keySplit(1))+1;
            child = obj.children{childIx};
        end
            
        function hasSibling = hasSibling(obj, direction)
            siblings = [    0 1 1 0; ...
                            0 0 1 1; ...
                            1 1 0 0; ...
                            1 0 0 1; ...
                            ];
            
            hasSibling = logical( siblings(obj.childIndex, direction)  );
        end
        
        
        function splitLeaf(obj, key, levels)
            southWestChild = RefinementNode2D( obj, obj.keyStart, key, levels(1), 1  );
            southEastChild = RefinementNode2D( obj, [key(1), obj.keyStart(2)],[obj.keyEnd(1), key(2)], levels(2), 2  );
            northWestChild = RefinementNode2D( obj, [obj.keyStart(1), key(2)],[key(1), obj.keyEnd(2)],levels(3), 3 );
            northEastChild = RefinementNode2D( obj, key, obj.keyEnd,levels(4), 4  );
            
            obj.keySplit = key;
            obj.children = {southWestChild, southEastChild, northWestChild, northEastChild };
        end
        
        function overlaps = overlaps(obj, keyStart, keyEnd)
           overlaps = obj.keyStart(1) < keyEnd(1) && obj.keyEnd(1) > keyStart(1) ...
                        && obj.keyStart(2) < keyEnd(2) && obj.keyEnd(2) > keyStart(2);
        end
    
        function contained = contained(obj, keyStart, keyEnd)
            startContained = obj.keyStart(1) >=  keyStart(1) && obj.keyStart(2) >=  keyStart(2) && obj.keyStart(1) <  keyEnd(1) && obj.keyStart(2) <  keyEnd(2);
            endContained = obj.keyEnd(1) <=  keyEnd(1) && obj.keyEnd(2) <=  keyEnd(2) && obj.keyEnd(1) >  keyStart(1) && obj.keyEnd(2) >  keyStart(2);
            
           contained = startContained && endContained;
        end
        
        
%         function node = findNeighbour(obj, level, direction)
%             node = obj.parent;
%             while ~ node.hasSibling( direction )
%                 node = node.parent;
%             end
%             
%             node = node.children{direction};
%             
%             %reversedDirection = NeighborDirection2D.reverse( direction );
%             while node.level<level && ~node.isLeaf
%                 node = node.selectChild(keyStart);
%             end
%         end
                
    end
    
    properties(Access = public)
        childIndex
    end
    
end