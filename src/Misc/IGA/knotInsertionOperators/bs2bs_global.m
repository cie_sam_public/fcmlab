function S=bs2bs_global(d,t,u)
% Input
% d - polynomial degree
% t - old knot vector
% u - new knot vector

% Initialization
nt=length(t);
nu=length(u);

IJK=zeros(nu*(d+1)*3,3);
count=0;

[t_mult,t_single,nt_s]=knot_mult(d,t);
[u_mult,u_single,nu_s]=knot_mult(d,u);
st=d+1;
su=d+1;
row=1;
col=1;

Sl=bs2bs(d,t,u,st,su);
[I2 J2 K2]=find(Sl);
Sl=[I2 J2 K2];

pI=row:d+row;
pJ=col:d+col;
IJK(count+1:count+size(Sl,1),:)=[pI(Sl(:,1))' pJ(Sl(:,2))' Sl(:,3)];
count=count+size(Sl,1);

t_single(nt+1)=t(nt-d);
i=1;
for j=1:nu_s
    if (u_single(j) == t_single(i))
        st=st+t_mult(i);
        col=col+t_mult(i);
        i=i+1;
    end
    su=su+u_mult(j);
    row=row+u_mult(j);
    Sl=bs2bs(d,t,u,st,su);
    [I2 J2 K2]=find(Sl);
    Sl=[I2 J2 K2];
    
    pI=row:d+row;
    pJ=col:d+col;
    IJK(count+1:count+size(Sl,1),:)=[pI(Sl(:,1))' pJ(Sl(:,2))' Sl(:,3)];
    count=count+size(Sl,1);
end

[IJK_V IJK_R ~]=unique(IJK(:,1:2),'rows'); % delete double entries
IJK=[IJK_V IJK(IJK_R,3)];
S=sparse(IJK(IJK(:,1)~=0,1),IJK(IJK(:,1)~=0,2),IJK(IJK(:,1)~=0,3), nu-d-1, nt-d-1);
S=S';

end
