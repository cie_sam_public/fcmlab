function [S1]=bs2bs_global_2D(dv,du,v0,v1,u0,u1)
% dv ... degree v
% du ... degree u
% v0 ... coarse knot vector
% v1 ... refined knot vector
% u0 ... coarse knot vector
% u1 ... refined knot vector

% Calculate 1D transformation matrix
Sv=bs2bs_global(dv,v0,v1);
Su=bs2bs_global(du,u0,u1);

% Use tensor product for bivariate setting
S1=kron(Sv,Su);

end