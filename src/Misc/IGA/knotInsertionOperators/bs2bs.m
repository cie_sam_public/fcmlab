function S=bs2bs(d,t,u,k,l)
S=zeros(d+1);
S(1,:)=bs2bs_first_row(d,t,u,k,l);
for ir=1:d
    S(ir+1,:)=bs2bs_i_row(d,t,u,k,l,ir,S(ir,:));
end
