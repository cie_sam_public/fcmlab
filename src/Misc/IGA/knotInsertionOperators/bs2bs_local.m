function [S numberOfSpans]=bs2bs_local(d,t,u)
% Input
% d - polynomial degree
% t - old knot vector
% u - new knot vector

nt=length(t);
nu=length(u);
[t_mult,t_single,nt_s]=knot_mult(d,t);
[u_mult,u_single,nu_s]=knot_mult(d,u);
S=zeros(d+1,d+1, nu_s );
st=d+1;
su=d+1;
row=1;
col=1;


Sl=bs2bs(d,t,u,st,su);
S(:,:, 1)=Sl';
t_single(nt+1)=t(nt-d);
i=1;
for j=1:nu_s
    if (u_single(j) == t_single(i))
        st=st+t_mult(i);
        col=col+t_mult(i);
        i=i+1;
    end
    su=su+u_mult(j);
    row=row+u_mult(j);
    Sl=bs2bs(d,t,u,st,su);
    S(:,:,j+1)=Sl';
end

numberOfSpans=nu_s+1;