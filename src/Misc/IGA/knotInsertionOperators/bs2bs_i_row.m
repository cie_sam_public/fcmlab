function Si=bs2bs_i_row(d,t,u,k,l,ir,S)
Si = zeros(1,d);
Si(1)=S(1)*(t(k+1)-u(l+ir))/(t(k+1)-u(l+ir-d));
for j=1:d
    den=t(k+j+1)-u(l+ir-d);
    fact=(t(k+j+1)-t(k+j-d))/(t(k+j)-t(k+j-d-1));
    Si(j+1)=(fact*(S(j)*(u(l+ir)-t(k+j-d-1))-Si(j)*(u(l+ir-d)-t(k+j-d-1)))+S(j+1)*(t(k+j+1)-u(l+ir)))/den;
end