function S=bs2bs_first_row(d,t,u,k,l)
S=eye(1,d+1);
for h=1:d
    beta_2=0.0;
    uu=u(l+1-h);
    for j=h:-1:1
        d1=uu-t(k+j-h);
        d2=t(k+j)-uu;
        beta_1=S(j)/(d2+d1);
        S(j+1)=d1*beta_1+beta_2;
        beta_2=d2*beta_1;
    end
    S(1)=beta_2;
end