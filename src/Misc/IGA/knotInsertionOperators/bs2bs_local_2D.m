function [S, Sxi, Seta]=bs2bs_local_2D(dv,du,v0,v1,u0,u1,Ea,iLevel)
% dv ... degree v
% du ... degree u
% v0 ... coarse knot vector
% v1 ... refined knot vector
% u0 ... coarse knot vector
% u1 ... refined knot vector


% Calculate 1D transformation matrix
[Seta numberOfSpansInEta ]=bs2bs_local(dv,v0,v1);
[Sxi numberOfSpansInXi]=bs2bs_local(du,u0,u1);

%S = zeros((dv+1)*(du+1),(dv+1)*(du+1), numberOfSpansInXi*numberOfSpansInEta);
S = cell(numberOfSpansInXi,numberOfSpansInEta);
for iEta=1:numberOfSpansInEta
    for iXi=1:numberOfSpansInXi
        
        
        active =  Ea{iLevel}(iXi,iEta);
        iNextLevel=iLevel+1;
        while active == 0 && iNextLevel<=length(Ea)
            nextLevelIndexX = 1+ fix((iXi-1)/2^(iLevel-iNextLevel));
            nextLevelIndexY = 1+ fix((iEta-1)/2^(iLevel-iNextLevel));
            active = Ea{iNextLevel}(nextLevelIndexX,nextLevelIndexY);
            
            iNextLevel=iNextLevel+1;
        end
        
        if active == 1;
            %if Ea(iXi,iEta) ==1
            %S( :,:,(iEta-1)*numberOfSpansInXi + iXi) = kron( Seta(:,:, iEta), Sxi(:,:, iXi)  );
            S{iXi,iEta} = kron( Seta(:,:, iEta), Sxi(:,:, iXi)  );
        end
    end
end

end