function [t_mult,t_single,nt_s]=knot_mult(d,t)
nt=length(t);
nt_s=0;
m=1;
for i=d+2:nt-d-1
    if (t(i) < t(i+1))
        nt_s=nt_s+1;
        t_mult(nt_s)=m;
        t_single(nt_s)=t(i);
        m=1;
    else
        m=m+1;
    end
end
t_single(nt_s+1)=t(nt-d);
t_mult(nt_s+1)=0;