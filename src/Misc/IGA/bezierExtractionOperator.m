function [C span] = bezierExtractionOperator(KnotVector,PolynomialDegree)
% bezierExtractionOperator computes the Bezier extraction operators for the
% given KnotVector and PolynomialDegree

% Initializations
m=length(KnotVector);
a=PolynomialDegree+1;
b=a+1;
span=1;
C(:,:,1)=eye(PolynomialDegree+1);
while b<m
    C(:,:,span+1)=eye(PolynomialDegree+1);
    i=b;
    % Initialize the next extraction operator
    % Count multiplicity of the knot at location b
    while b<m && KnotVector(b+1)==KnotVector(b)
        b=b+1;
    end
    mult=b-i+1;
    if mult<PolynomialDegree
        numer=KnotVector(b)-KnotVector(a);
        for j=PolynomialDegree:-1:mult+1
            alphas(j-mult)=numer/(KnotVector(a+j)-KnotVector(a));
        end
        r=PolynomialDegree-mult;
        % Update the matrix coefficient for r new knots
        for j=1:r
            save=r-j+1;
            s=mult+j;
            for k=PolynomialDegree+1:-1:s+1
                alpha=alphas(k-s);
                % Form extraction operator
                C(:,k,span)=alpha*C(:,k,span)+(1-alpha)*C(:,k-1,span);
            end
            if b<m
                % Update overlapping coefficients of the next operator
                C(save:j+save,save,span+1)=C(PolynomialDegree-j+1:PolynomialDegree+1,PolynomialDegree+1,span);
            end
        end
        % Finished with the current operator.
        % Update indices for the next operator.
        span=span+1;
        if b<m
            a=b;
            b=b+1;
        end
    elseif mult==PolynomialDegree
        % In case multiplicity of knot is already p,
        % update indices for the next operator.
        span=span+1;
        if b<m
            a=b;
            b=b+1;
        end
    end
end

end