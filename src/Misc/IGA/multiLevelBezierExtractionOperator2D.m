function [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2D( levels, multiLevelGeometry )

PolynomialDegree = multiLevelGeometry{1}.getPolynomialDegreeXi;
geometries = multiLevelGeometry;
[Aa, Am, Ap, Ea] = getRefinementDataStructures2D( levels, geometries );


for iLevel = 1:length(geometries)
    
    for iLevelToCouple = 1:iLevel-1
        numberOfLevelBasisFunctions = length(Aa{iLevelToCouple}(:));
        MLeveLCoupling = eye(numberOfLevelBasisFunctions);
        for iIntermediateLevel = iLevelToCouple:iLevel-1
            
            MLeveLCoupling = MLeveLCoupling * bs2bs_global_2D(geometries{iIntermediateLevel}.getPolynomialDegreeEta,geometries{iIntermediateLevel}.getPolynomialDegreeXi, geometries{iIntermediateLevel}.getKnotsEta,geometries{iIntermediateLevel+1}.getKnotsEta, geometries{iIntermediateLevel}.getKnotsXi,geometries{iIntermediateLevel+1}.getKnotsXi);
            
            size1 = size(MLeveLCoupling,1);
            size2 = size(MLeveLCoupling,2);
            
            rap = repmat( Ap{iLevelToCouple}(:), 1, size2 );
            ram = repmat( Am{iIntermediateLevel+1}(:)', size1, 1 );
            nonZeroEntries = rap & ram;
            
            %MLeveLCoupling = full(MLeveLCoupling);
            MLeveLCoupling(~ nonZeroEntries) = 0;
            %MLeveLCoupling = sparse(MLeveLCoupling);
            
        end
        
        M{iLevel, iLevelToCouple} = full(MLeveLCoupling);
    end
    
end

%activeElementIndex=1;
for iLevel =  1:length(geometries)
    
    LevelExtractionOperators = bezierExtractionOperator2D( geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta );
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);
        
    for iElementY = 1:size(Ea{iLevel},2)
        for iElementX = 1:size(Ea{iLevel},1)
            
            if Ea{iLevel}(iElementX,iElementY) == 1
                MeExtensionOperator2 = [];
                AmMeExtensionOperator2=[];
%                 
%                 thisindicesX = iElementX+(0:PolynomialDegree);
%                 thisindicesY = (iElementY+(0:PolynomialDegree)-1)*size(Aa{iLevel},1)+1;
%                 thisindices2=[];
%                 for i=1:length(thisindicesY)
%                     thisindices2 = [ thisindices2 thisindicesX+thisindicesY(i)-1];
%                 end
                
                 numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevel}.getKnotsXi );
                 thisindices = standardLocationMap{iLevel} ( (iElementY-1)*numberOfElementsInXi + iElementX, : );
                 %assert( all(thisindices2 == thisindices) )
                
                
                for iLevelToCouple = 1:iLevel-1
                    previousLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iLevelToCouple));
                    previousLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iLevelToCouple));
                                        
                    MLeveLCoupling = M{iLevel, iLevelToCouple};
                    
%                     previousindicesX = previousLevelIndexX+(0:PolynomialDegree);
%                     previousindicesY = (previousLevelIndexY+(0:PolynomialDegree)-1)*size(Aa{iLevelToCouple},1)+1;
%                     previousindices2=[];
%                     for i=1:length(previousindicesY)
%                         previousindices2 = [ previousindices2 previousindicesX+previousindicesY(i)-1];
%                     end
                    
                    numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevelToCouple}.getKnotsXi );
                    previousindices = standardLocationMap{iLevelToCouple} ( (previousLevelIndexY-1)*numberOfElementsInXi + previousLevelIndexX, : );
                     %assert( all(previousindices == previousindices2) );
                     
                    %version1: fullmatrix
                    elementMLevelCoupling2 = MLeveLCoupling(previousindices,thisindices);
                    MeExtensionOperator2  = [MeExtensionOperator2; elementMLevelCoupling2];
                    
                    %version2: reduced matrix
                    apLevelToCouple = Ap{iLevelToCouple}(previousindices);
                    apLevelToCouple = logical(apLevelToCouple(:));
                    amCurrentLevel = Am{iLevel}(thisindices);
                    amCurrentLevel=logical(amCurrentLevel(:));
                    
                    AmElementMLevelCoupling2 = elementMLevelCoupling2(apLevelToCouple,amCurrentLevel);
                    AmMeExtensionOperator2  = [AmMeExtensionOperator2; AmElementMLevelCoupling2];
                    
%                     if size(AmMeExtensionOperator2,1) > 0 & size(AmMeExtensionOperator2,2) > 0
%                         AmMeExtensionOperator2
%                     end
%                     
                end
                
                MeExtensionOperator2 = [MeExtensionOperator2; diag( Aa{iLevel}(thisindices) )];
                
                MultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                MultiLevelOperator{iLevel, iElementX, iElementY} = MeExtensionOperator2;
                BezierExtractionOperator{iLevel, iElementX, iElementY} = LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                
                AmMultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY}  = AmMeExtensionOperator2;
                %MLBEOe{ activeElementIndex } =  MeExtensionOperator2 * LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
            end
        end
    end
end


end