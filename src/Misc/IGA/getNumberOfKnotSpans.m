function numberOfKnotSpans = getNumberOfKnotSpans(XI)

i = 1;
numberOfKnotSpans = -1;

while i<length(XI)
    while i<length(XI) && XI(i) == XI(i+1)
        i=i+1;
    end
    numberOfKnotSpans = numberOfKnotSpans+1;
    i=i+1;
end

end