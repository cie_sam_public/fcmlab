function [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTree(  multiLevelGeometry )

geometries = multiLevelGeometry.getGeometries;
PolynomialDegree = geometries{1}.getPolynomialDegreeXi;

[Aa, Am, Ap, Ea] = multiLevelGeometry.getRefinementDataStructures;


M = cell(length(geometries), length(geometries)-1);
for iLevel = 1:length(geometries)
    
    %parfor iLevelToCouple = 1:iLevel-1
    parfor iLevelToCouple = 1:iLevel-1
        numberOfLevelBasisFunctions = length(Aa{iLevelToCouple}(:));
        MLeveLCoupling = speye(numberOfLevelBasisFunctions);
        for iIntermediateLevel = iLevelToCouple:iLevel-1
            
            MLeveLCoupling = MLeveLCoupling * bs2bs_global_2D(geometries{iIntermediateLevel}.getPolynomialDegreeEta,geometries{iIntermediateLevel}.getPolynomialDegreeXi, geometries{iIntermediateLevel}.getKnotsEta,geometries{iIntermediateLevel+1}.getKnotsEta, geometries{iIntermediateLevel}.getKnotsXi,geometries{iIntermediateLevel+1}.getKnotsXi);
          
%             tic
%             size1 = size(MLeveLCoupling,1);
%             size2 = size(MLeveLCoupling,2);
%             
%              rap = repmat( Ap{iLevelToCouple}(:), 1, size2 );
%              ram = repmat( Am{iIntermediateLevel+1}(:)', size1, 1 );
%              nonZeroEntries = rap & ram;
%              
%              %MLeveLCoupling2 = MLeveLCoupling;
%              %MLeveLCoupling(~ nonZeroEntries) = 0;
%toc
MLeveLCoupling(~ Ap{iLevelToCouple}(:), : ) = 0;
MLeveLCoupling(:, ~ Am{iIntermediateLevel+1}(:)' ) = 0;
        end
        
        M{iLevel, iLevelToCouple} = MLeveLCoupling;
    end
    
    
    standardLocationMap{iLevel} = extractedBSplineLocationMap2D(geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi,geometries{iLevel}.getKnotsEta,geometries{iLevel}.getPolynomialDegreeEta, 1);
    
end

parfor iLevel =  1:length(geometries)
    
    LevelMultiLevelOperator = cell(size(Ea{iLevel},1), size(Ea{iLevel},2));
    LevelBezierExtractionOperator = cell(size(Ea{iLevel},1), size(Ea{iLevel},2));
    LevelMultiLevelBezierExtractionOperator = cell(size(Ea{iLevel},1), size(Ea{iLevel},2));
    LevelAmMultiLevelBezierExtractionOperator = cell(size(Ea{iLevel},1), size(Ea{iLevel},2));
    
    LevelExtractionOperators = bezierExtractionOperator2D( geometries{iLevel}.getKnotsXi, geometries{iLevel}.getPolynomialDegreeXi, geometries{iLevel}.getKnotsEta, geometries{iLevel}.getPolynomialDegreeEta, Ea{iLevel} );
            
    for iElementY = 1:size(Ea{iLevel},2)
        for iElementX = 1:size(Ea{iLevel},1)
            
            if Ea{iLevel}(iElementX,iElementY) == 1
                MeExtensionOperator2 = [];
                AmMeExtensionOperator2=[];
                
                 numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevel}.getKnotsXi );
                 thisindices = standardLocationMap{iLevel} ( (iElementY-1)*numberOfElementsInXi + iElementX, : );

                for iLevelToCouple = 1:iLevel-1
                    previousLevelIndexX = 1+ fix((iElementX-1)/2^(iLevel-iLevelToCouple));
                    previousLevelIndexY = 1+ fix((iElementY-1)/2^(iLevel-iLevelToCouple));
                                        
                    MLeveLCoupling = M{iLevel, iLevelToCouple};
                                        
                    numberOfElementsInXi = getNumberOfKnotSpans( geometries{iLevelToCouple}.getKnotsXi );
                    previousindices = standardLocationMap{iLevelToCouple} ( (previousLevelIndexY-1)*numberOfElementsInXi + previousLevelIndexX, : );
                     
                    %version1: fullmatrix
                    elementMLevelCoupling2 = MLeveLCoupling(previousindices,thisindices);
                    MeExtensionOperator2  = [MeExtensionOperator2; elementMLevelCoupling2];
                    
                    %version2: reduced matrix
                    apLevelToCouple = Ap{iLevelToCouple}(previousindices);
                    apLevelToCouple = logical(apLevelToCouple(:));
                    amCurrentLevel = Am{iLevel}(thisindices);
                    amCurrentLevel=logical(amCurrentLevel(:));
                    
                    AmElementMLevelCoupling2 = elementMLevelCoupling2(apLevelToCouple,amCurrentLevel);
                    AmMeExtensionOperator2  = [AmMeExtensionOperator2; AmElementMLevelCoupling2];
                end
                
                MeExtensionOperator2 = [MeExtensionOperator2; diag( Aa{iLevel}(thisindices) )];
                
%                  MultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY} = MeExtensionOperator2 * LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
%                  MultiLevelOperator{iLevel, iElementX, iElementY} = MeExtensionOperator2;
%                  BezierExtractionOperator{iLevel, iElementX, iElementY} = LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
%                  
%                  AmMultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY}  = AmMeExtensionOperator2;

                LevelMultiLevelOperator{iElementX, iElementY} = MeExtensionOperator2;
                LevelBezierExtractionOperator{iElementX, iElementY} = LevelExtractionOperators(:,:,(iElementY-1)*size(Ea{iLevel},1) + iElementX);
                LevelMultiLevelBezierExtractionOperator{iElementX, iElementY} =  LevelMultiLevelOperator{iElementX, iElementY} * LevelBezierExtractionOperator{iElementX, iElementY};
                LevelAmMultiLevelBezierExtractionOperator{iElementX, iElementY} = AmMeExtensionOperator2;
            end
        end
    end
    
    MultiLevelOperator{iLevel} = LevelMultiLevelOperator;
    BezierExtractionOperator{iLevel} = LevelBezierExtractionOperator;
    MultiLevelBezierExtractionOperator{iLevel}=LevelMultiLevelBezierExtractionOperator;
    AmMultiLevelBezierExtractionOperator{iLevel}=LevelAmMultiLevelBezierExtractionOperator;
end

% for iLevel = 1:length(geometries)
%      for iElementY = 1:size(Ea{iLevel},2)
%         for iElementX = 1:size(Ea{iLevel},1)
%             MultiLevelOperator{iLevel, iElementX, iElementY} = lMultiLevelOperator{iLevel}{iElementX, iElementY};
%             BezierExtractionOperator{iLevel, iElementX, iElementY} = lBezierExtractionOperator{iLevel}{iElementX, iElementY};
%             MultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY} = lMultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY};
%             AmMultiLevelBezierExtractionOperator{iLevel, iElementX, iElementY} = lAmMultiLevelBezierExtractionOperator{iLevel}{iElementX, iElementY};
%         end
%      end
% end

end