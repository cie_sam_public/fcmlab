function [Aa Am Ap Ea] = getRefinementDataStructures2D( levels, multiLevelGeometry )

% does not work for repeated internal knots

p = multiLevelGeometry{1}.getPolynomialDegreeXi;
maxLevel = double(max(max(levels)));

parfor iLevel = 1:maxLevel
    
    levelKnotSize = 2^(maxLevel-iLevel);
    levelSupportSize = levelKnotSize * (p+1);
    
    openKnotLevels = levelsForRepeatedKnots(levels, multiLevelGeometry, iLevel);
    
    
    numberOfSpansX = size(openKnotLevels,1)/levelKnotSize;
    numberOfSpansY = size(openKnotLevels,2)/levelKnotSize;
    
    numberOfBasisInX = numberOfSpansX-p;
    numberOfBasisInY = numberOfSpansY-p;
    
    numberOfElementsX = multiLevelGeometry{iLevel}.numberOfKnotSpansInXi ;
    numberOfElementsY = multiLevelGeometry{iLevel}.numberOfKnotSpansInEta;
    
    
    AaLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    AmLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    ApLevel =  zeros(numberOfBasisInX,numberOfBasisInY);
    EaLevel =  zeros(numberOfElementsX,numberOfElementsY);
    
    greaterMask = openKnotLevels > iLevel;
    smallerMask = openKnotLevels < iLevel;
    equalMask = openKnotLevels == iLevel;
    knotAnyGreaterMask = greaterMask((0:numberOfSpansX-1)*levelKnotSize + 1, (0:numberOfSpansY-1)*levelKnotSize + 1);
    knotAnySmallerMask = smallerMask((0:numberOfSpansX-1)*levelKnotSize + 1, (0:numberOfSpansY-1)*levelKnotSize + 1);
    knotAllEqualMask = equalMask((0:numberOfSpansX-1)*levelKnotSize + 1, (0:numberOfSpansY-1)*levelKnotSize + 1);
    knotAnyEqualMask = knotAllEqualMask;
    for iKnotSizeX = 1:levelKnotSize
        for iKnotSizeY = 1:levelKnotSize
            knotAnyGreaterMask = knotAnyGreaterMask | greaterMask((0:numberOfSpansX-1)*levelKnotSize + iKnotSizeX, (0:numberOfSpansY-1)*levelKnotSize + iKnotSizeY);
            knotAnySmallerMask = knotAnySmallerMask | smallerMask((0:numberOfSpansX-1)*levelKnotSize + iKnotSizeX, (0:numberOfSpansY-1)*levelKnotSize + iKnotSizeY);
            knotAllEqualMask = knotAllEqualMask & equalMask((0:numberOfSpansX-1)*levelKnotSize + iKnotSizeX, (0:numberOfSpansY-1)*levelKnotSize + iKnotSizeY);
            knotAnyEqualMask = knotAnyEqualMask | equalMask((0:numberOfSpansX-1)*levelKnotSize + iKnotSizeX, (0:numberOfSpansY-1)*levelKnotSize + iKnotSizeY);
        end
    end
    supportAnyGreaterMask = knotAnyGreaterMask(1:end-(p+1)+1, 1:end-(p+1)+1);
    supportAnySmallerMask = knotAnySmallerMask(1:end-(p+1)+1, 1:end-(p+1)+1);
    supportAllEqualMask = knotAllEqualMask(1:end-(p+1)+1, 1:end-(p+1)+1);
    supportAnyEqualMask = knotAnyEqualMask(1:end-(p+1)+1, 1:end-(p+1)+1);
    for iSupportSizeX = 1:(p+1)
        for iSupportSizeY = 1:(p+1)
            supportAnyGreaterMask = supportAnyGreaterMask | knotAnyGreaterMask(iSupportSizeX+(0:end-(p+1)), iSupportSizeY+(0:end-(p+1)));
            supportAnySmallerMask = supportAnySmallerMask | knotAnySmallerMask(iSupportSizeX+(0:end-(p+1)), iSupportSizeY+(0:end-(p+1)));
            supportAllEqualMask = supportAllEqualMask & knotAllEqualMask(iSupportSizeX+(0:end-(p+1)), iSupportSizeY+(0:end-(p+1)));
            supportAnyEqualMask = supportAnyEqualMask | knotAnyEqualMask(iSupportSizeX+(0:end-(p+1)), iSupportSizeY+(0:end-(p+1)));
        end
    end
    
    
    for iKnotY = 1:numberOfBasisInY
        for iKnotX = 1:numberOfBasisInX
            %             spansX = (iKnotX-1)*levelKnotSize + (1:levelSupportSize);
            %             spansY = (iKnotY-1)*levelKnotSize + (1:levelSupportSize);
            %             supportKnots = openKnotLevels(spansX,spansY);
            %
            %             diff = int8( int8(supportKnots)  - int8(iLevel) );
            %             %             maxdiff=max(max(diff));
            %             %             mindiff=min(min(diff));
            %             %
            %             %             if maxdiff>=0
            %             %                 %if ~ any(any( diff ~= 0 ))
            %             %                 if mindiff == 0 && maxdiff== 0
            %             %                     AaLevel(iKnotX , iKnotY ) = 1;
            %             %                 elseif mindiff<0
            %             %                     AmLevel(iKnotX , iKnotY) = 1;
            %             %                 elseif maxdiff>0 && any(any( supportKnots == iLevel ))
            %             %                     ApLevel(iKnotX , iKnotY ) = 1;
            %             %                     AaLevel(iKnotX , iKnotY ) = 1;
            %             %                 end
            %             %             end
            %
            %             iLevel
            %             iKnotX
            %             iKnotY
            %
            %             assert(any(any( diff>=0 )) == supportAnyGreaterMask(iKnotX, iKnotY) | supportAnyEqualMask(iKnotX, iKnotY));
            %             assert( all(all( diff == 0 )) == supportAllEqualMask(iKnotX, iKnotY));
            %             assert(any(any( diff < 0 )) == supportAnySmallerMask(iKnotX, iKnotY));
            %             assert(any(any( diff > 0 )) == supportAnyGreaterMask(iKnotX, iKnotY));
            %             assert(any(any( supportKnots == iLevel )) == supportAnyEqualMask(iKnotX, iKnotY));
            %
            %             if any(any( diff>=0 ))
            %                 %if ~ any(any( diff ~= 0 ))
            %                 if all(all( diff == 0 ))
            %                     AaLevel(iKnotX , iKnotY ) = 1;
            %                 elseif any(any( diff < 0 ))
            %                     AmLevel(iKnotX , iKnotY) = 1;
            %                 elseif any(any( diff > 0 )) && any(any( supportKnots == iLevel ))
            %                     ApLevel(iKnotX , iKnotY ) = 1;
            %                     AaLevel(iKnotX , iKnotY ) = 1;
            %                 end
            %             end
            
            if supportAnyGreaterMask(iKnotX, iKnotY) || supportAnyEqualMask(iKnotX, iKnotY)
                
                if supportAllEqualMask(iKnotX, iKnotY)
                    AaLevel(iKnotX , iKnotY ) = 1;
                elseif supportAnySmallerMask(iKnotX, iKnotY)
                    AmLevel(iKnotX , iKnotY) = 1;
                elseif supportAnyGreaterMask(iKnotX, iKnotY) && supportAnyEqualMask(iKnotX, iKnotY)
                    ApLevel(iKnotX , iKnotY ) = 1;
                    AaLevel(iKnotX , iKnotY ) = 1;
                end
            end
            
            
            
            %                         if all(all( supportKnots == iLevel ))
            %                             AaLevel(iKnotX , iKnotY ) = 1;
            %                         elseif any(any( supportKnots < iLevel )) && any(any( supportKnots >= iLevel ))
            %                             AmLevel(iKnotX , iKnotY) = 1;
            %                         elseif any(any( supportKnots > iLevel )) && any(any( supportKnots == iLevel ))
            %                             ApLevel(iKnotX , iKnotY ) = 1;
            %                             AaLevel(iKnotX , iKnotY ) = 1;
            %                         end
            
        end
    end
    
    
    equalMask = levels == iLevel;
    knotAllEqualMask = equalMask((0:numberOfElementsX-1)*levelKnotSize + 1, (0:numberOfElementsY-1)*levelKnotSize + 1);
    
    for iKnotSizeX = 1:levelKnotSize
        for iKnotSizeY = 1:levelKnotSize
            knotAllEqualMask = knotAllEqualMask & equalMask((0:numberOfElementsX-1)*levelKnotSize + iKnotSizeX, (0:numberOfElementsY-1)*levelKnotSize + iKnotSizeY);
        end
    end
    
    
    for iKnotY = 1:numberOfElementsY
        for iKnotX = 1:numberOfElementsX
%             knotLevels = levels( (iKnotX-1)*levelKnotSize +(1:levelKnotSize), (iKnotY-1)*levelKnotSize +(1:levelKnotSize)  );
%             
%             assert(all(all( knotLevels == iLevel )) == knotAllEqualMask(iKnotX, iKnotY) )
%             if all(all( knotLevels == iLevel ))
if knotAllEqualMask(iKnotX, iKnotY)
                EaLevel(iKnotX,iKnotY) = 1;
            end
        end
    end
    
    
    Aa{iLevel} =  AaLevel;
    Am{iLevel} =  AmLevel;
    Ap{iLevel} =  ApLevel;
    Ea{iLevel} =  EaLevel;
    
end

end





function repeatedLevels = levelsForRepeatedKnots(levels, multiLevelGeometry, iLevel)
maxLevel = double(max(max(levels)));

levelKnotSize = 2^(maxLevel-iLevel);
%levelSupportSize = levelKnotSize * (p+1);

[breaksXi,multsXi] = knt2brk(multiLevelGeometry{iLevel}.getKnotsXi) ;
[breaksEta,multsEta] = knt2brk(multiLevelGeometry{iLevel}.getKnotsEta) ;

%repeatedLevelsXi = zeros((sum(multsXi)-1)*levelKnotSize, size(levels,2));
repeatedLevelsXi = zeros((sum(multsXi)-1)*levelKnotSize, size(levels,2), 'int8');

iSpanXi = 1;
iNonEmptySpanXi = 1;
for iBreakXi = 1:length(breaksXi)-1
    mult = multsXi(iBreakXi);
    repeatedLevelsXi( iSpanXi+(0:(mult)*levelKnotSize-1), : ) = repmat(levels(iNonEmptySpanXi+(0:(levelKnotSize-1)),:), mult, 1);
    iSpanXi = iSpanXi + mult*levelKnotSize;
    iNonEmptySpanXi = iNonEmptySpanXi + 1*levelKnotSize;
end
iBreakXi= length(breaksXi);
iNonEmptySpanXi = iNonEmptySpanXi - 1*levelKnotSize;

mult = multsXi(iBreakXi)-1;
repeatedLevelsXi( iSpanXi+(0:(mult)*levelKnotSize-1), : ) = repmat(levels(iNonEmptySpanXi+(0:(levelKnotSize-1)),:), mult, 1);


repeatedLevels = zeros((sum(multsXi)-1)*levelKnotSize, (sum(multsEta)-1)*levelKnotSize, 'int8');
%repeatedLevels = zeros((sum(multsXi)-1)*levelKnotSize, (sum(multsEta)-1)*levelKnotSize);

iSpanEta = 1;
iNonEmptySpanEta = 1;
for iBreakEta = 1:length(breaksEta)-1
    mult = multsEta(iBreakEta);
    repeatedLevels( :, iSpanEta+(0:(mult)*levelKnotSize-1) ) = repmat( repeatedLevelsXi(:,iNonEmptySpanEta+(0:(levelKnotSize-1))), 1, mult);
    iSpanEta = iSpanEta + mult*levelKnotSize;
    iNonEmptySpanEta = iNonEmptySpanEta + 1*levelKnotSize;
end
iBreakEta= length(breaksEta);
iNonEmptySpanEta = iNonEmptySpanEta - 1*levelKnotSize;

mult = multsEta(iBreakEta)-1;
repeatedLevels( :, iSpanEta+(0:(mult)*levelKnotSize-1) ) = repmat( repeatedLevelsXi(:,iNonEmptySpanEta+(0:(levelKnotSize-1))), 1, mult);




end