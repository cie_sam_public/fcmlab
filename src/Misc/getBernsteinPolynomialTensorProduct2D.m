function Shapes = getBernsteinPolynomialTensorProduct2D( Coord, IndicesXi, PolynomialDegreeXi, IndicesEta, PolynomialDegreeEta , DiffOrder )

ShapesR = getBernsteinPolynomial(Coord(1), IndicesXi, PolynomialDegreeXi, 0);
ShapesS = getBernsteinPolynomial(Coord(2), IndicesEta, PolynomialDegreeEta, 0);

tensorProductShapes = ShapesR * ShapesS';

Shapes = reshape(tensorProductShapes, 1, length(IndicesXi)*length(IndicesEta));

if DiffOrder >= 1
    
    d_ShapesR_dr = getBernsteinPolynomial(Coord(1), IndicesXi, PolynomialDegreeXi, 1);
    d_ShapesS_ds = getBernsteinPolynomial(Coord(2), IndicesEta, PolynomialDegreeEta, 1);
    
    d_tensorProductShapes_dr = d_ShapesR_dr    * ShapesS';
    d_tensorProductShapes_ds = ShapesR         * d_ShapesS_ds';
    
    d_Shapes_dr = reshape(d_tensorProductShapes_dr, 1, length(IndicesXi)*length(IndicesEta) );
    d_Shapes_ds = reshape(d_tensorProductShapes_ds, 1, length(IndicesXi)*length(IndicesEta) );
    
    Shapes = [ Shapes; d_Shapes_dr; d_Shapes_ds];
    
    if DiffOrder == 2
        
        d2_ShapesR_dr2 = getBernsteinPolynomial(Coord(1), IndicesXi, PolynomialDegreeXi, 2);
        d2_ShapesS_ds2 = getBernsteinPolynomial(Coord(2), IndicesEta, PolynomialDegreeEta, 2);
        
        d2_tensorProductShapes_dr2 = d2_ShapesR_dr2    * ShapesS';
        d2_tensorProductShapes_ds2 = ShapesR         * d2_ShapesS_ds2';
        d2_tensorProductShapes_drds = d_ShapesR_dr         * d_ShapesS_ds';
                
        d2_tensorProductShapes_dr2 = reshape(d2_tensorProductShapes_dr2, 1, length(IndicesXi)*length(IndicesEta) );
        d2_tensorProductShapes_ds2 = reshape(d2_tensorProductShapes_ds2, 1, length(IndicesXi)*length(IndicesEta) );
        d2_tensorProductShapes_drds = reshape(d2_tensorProductShapes_drds, 1, length(IndicesXi)*length(IndicesEta) );
        
        Shapes = [ Shapes; d2_tensorProductShapes_dr2; d2_tensorProductShapes_ds2; d2_tensorProductShapes_drds ];
    end
end

end