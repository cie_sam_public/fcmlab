%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%


classdef SpaceTreeNode < TreeNode
    %SpaceTreeNode Subclass of TreeNode. Handles Datas.
    %   
    
    
    methods
        %% constructor
        function obj = SpaceTreeNode(Parent, NumberofChildren, indexGeometry, support, numberOfSeedPoints)
            obj = obj@TreeNode(Parent, NumberofChildren);
            obj.indexGeometry = indexGeometry;
            obj.support = support;
            obj.seedPoints = SeedPoint.empty(numberOfSeedPoints^2,0);
            obj.numberOfSeedPoints = numberOfSeedPoints;
        end
        
        %% createOrientedBoundingBox
        function createOrientedBoundingBox(obj, depthCounter)
            if (depthCounter==0)
                obj.createPointCloud()
                if (isempty(obj.pointCloud)==false)
                    obj.myOrientedBoundingBox = OrientedBoundingBox(obj.pointCloud, length(obj.pointCloud));
                end
            else
                for n = 1:obj.NumberOfChildren
                    if obj.Depth ~= 0
                    obj.Children{n}.createOrientedBoundingBox(depthCounter-1)
                    end
                end
            end
        end
        
        %% getAllOBBs
        function OBBs = getAllOBBs(obj, OBBs)
            if (isempty(obj.pointCloud)==false);
                OBBs=[OBBs, obj.myOrientedBoundingBox];
            else
                if (obj.Depth>1)
                    for n = 1:obj.NumberOfChildren
                        OBBs = obj.Children{n}.getAllOBBs(OBBs);
                    end
                end
            end
            OBBs;
        end
        %% getCompletePointCloud
        function pointCloud = getCompletePointCloud(obj, pointCloud)
            if (obj.Depth >1)
                for n = 1:obj.NumberOfChildren
                    pointCloud = obj.Children{n}.getCompletePointCloud(pointCloud);
                end
            end
            pointCloud= [pointCloud, obj.pointCloud];
        end
        %% getCompleteSeedPointCloud
        function seedPointCloud = getCompleteSeedPointCloud(obj, seedPointCloud)
            if (obj.Depth >0)
                for n = 1:obj.NumberOfChildren
                    seedPointCloud = obj.Children{n}.getCompleteSeedPointCloud(seedPointCloud);
                end
            end
            if (obj.Depth == 0)
                seedPointCloud= [seedPointCloud, obj.seedPoints];
            end
        end
%         function rectangles = getRectanglesOfChildren(obj)
%             r1=obj.getIndexGeometryInGlobalCoordinates(1);
%             r2=obj.getIndexGeometryInGlobalCoordinates(2);
%             r3=obj.getIndexGeometryInGlobalCoordinates(3);
%             r4=obj.getIndexGeometryInGlobalCoordinates(4);
%             rectangles = ([r1; r2; r3; r4]);
%         end
%         function rectangles = getRectanglesOfAllChildren(obj)
%             r = [];
%             if(obj.Depth>1)
%                 for n=1:4
%                     r = [r,obj.Children{n}.getRectanglesOfAllChildren()];
%                 end
%                 r1=obj.getIndexGeometryInGlobalCoordinates(1);
%                 r2=obj.getIndexGeometryInGlobalCoordinates(2);
%                 r3=obj.getIndexGeometryInGlobalCoordinates(3);
%                 r4=obj.getIndexGeometryInGlobalCoordinates(4);
%                 rectangles = ([r; r1; r2; r3; r4]);
%             end
%         end
%         function indexGeometryCoords = getIndexGeometryInGlobalCoordinates(obj, numberOfChild)
%             A=obj.Children{numberOfChild}.indexGeometry.getVerticesCoordinates();
%             P1 = obj.getGlobalCoordinatesOfSeedPoint([A(1,1),A(1,2),A(1,3)]);
%             P2 = obj.getGlobalCoordinatesOfSeedPoint([A(2,1),A(2,2),A(2,3)]);
%             P3 = obj.getGlobalCoordinatesOfSeedPoint([A(3,1),A(3,2),A(3,3)]);
%             P4 = obj.getGlobalCoordinatesOfSeedPoint([A(4,1),A(4,2),A(4,3)]);
%             indexGeometryCoords = [{P1},{P2},{P3},{P4}];
%         end
%         

        %% getSubDomains
        function subDomains = getSubDomains(obj, subDomains)
            if (obj.Depth == 0)
                subDomains = [subDomains, obj.indexGeometry];
            else
                for n = 1:obj.NumberOfChildren
                    subDomains = obj.Children{n}.getSubDomains(subDomains);
                end
            end
        end
        
        %% getIndexGeometries
        function rectangles = getIndexGeometries(obj)
              
            localRectangles = Rectangle.empty(1,0);
            indexGeometries = obj.getAllIndexGeometries(localRectangles);
            rectangles = RectangleCoordinates.empty(length(indexGeometries),0);
            for n = 1:length(indexGeometries)
                A = indexGeometries(n).getVerticesCoordinates();
                P1 = obj.getGlobalCoordinatesOfSeedPoint([A(1,1),A(1,2),A(1,3)]);
                P2 = obj.getGlobalCoordinatesOfSeedPoint([A(2,1),A(2,2),A(2,3)]);
                P3 = obj.getGlobalCoordinatesOfSeedPoint([A(3,1),A(3,2),A(3,3)]);
                P4 = obj.getGlobalCoordinatesOfSeedPoint([A(4,1),A(4,2),A(4,3)]);
                rectangles(n) = RectangleCoordinates(P1, P2, P3, P4);
            end
        end
        %% getAllIndexGeometries
        function rectangles = getAllIndexGeometries(obj, rectangles)
            if (obj.Depth > 1)
                for n = 1:obj.NumberOfChildren
                    rectangles = obj.Children{n}.getAllIndexGeometries(rectangles);
                end
            end
            if (obj.Depth > 0)
            myrecs =  [obj.Children{1}.indexGeometry,...
                obj.Children{2}.indexGeometry,...
                obj.Children{3}.indexGeometry,...
                obj.Children{4}.indexGeometry];
            rectangles = [rectangles, myrecs];
            end
        end
        
%         function rectangles = getIndexGeometries(obj)
%             rectangles = RectangleCoordinates.empty(1,0);
%             if (obj.Depth~=0)
%             for n = 1:obj.NumberOfChildren
%                 A = obj.Children{n}.indexGeometry.getVerticesCoordinates();
%                 P1 = obj.getGlobalCoordinatesOfSeedPoint([A(1,1),A(1,2),A(1,3)]);
%                 P2 = obj.getGlobalCoordinatesOfSeedPoint([A(2,1),A(2,2),A(2,3)]);
%                 P3 = obj.getGlobalCoordinatesOfSeedPoint([A(3,1),A(3,2),A(3,3)]);
%                 P4 = obj.getGlobalCoordinatesOfSeedPoint([A(4,1),A(4,2),A(4,3)]);
%                 rectangles(n) = RectangleCoordinates(P1, P2, P3,P4);
%             end
%             end
            
%         end

        function createPointCloud(obj)
            localPointCloud = SeedPoint.empty(1,0);
            for n = 1:obj.numberOfSeedPoints^2
                if (obj.seedPoints(n).domainIndex == 2)
                    localPointCloud(length(localPointCloud)+1) = obj.seedPoints(n);
                end
            end
            for k = 1:length(localPointCloud)
                localPointCloud(k).coords = obj.getGlobalCoordinatesOfSeedPoint(localPointCloud(k).coords);
            end
            if (obj.isIntersected())
                for k = 1:obj.NumberOfChildren
                    seedPointsOfChildren = obj.Children{k}.getMySeedPoints();
                    for j = 1:length(seedPointsOfChildren)
                        if (seedPointsOfChildren(j).domainIndex == 2)
                            localPointCloud(length(localPointCloud)+1) = seedPointsOfChildren(j);
                        end
                    end
                end
            end

            obj.pointCloud = localPointCloud;
        end
        
        function localSeedPoints = getMySeedPoints(obj)
            localSeedPoints = SeedPoint.empty(1,0);
            for n = 1:(obj.numberOfSeedPoints^2)
                localSeedPoints(n) = obj.seedPoints(n);
            end
            for k = 1:length(localSeedPoints)
                localSeedPoints(k).coords = obj.getGlobalCoordinatesOfSeedPoint(localSeedPoints(k).coords);
            end
            if (obj.isLeaf==false)
                for i = 1:obj.NumberOfChildren
                    seedPointsOfChildren = obj.Children{i}.getMySeedPoints();
                    for j = 1:length(seedPointsOfChildren)
                        localSeedPoints(length(localSeedPoints)+1) = seedPointsOfChildren(j);
                    end
                end
            end

        end
        function check = isIntersected(obj)
            
            initialDomainIndex = obj.seedPoints(1).domainIndex;
            check = false;
            
            for i = 1:obj.numberOfSeedPoints^2
                currentDomainIndex = obj.seedPoints(i).domainIndex;
                if (initialDomainIndex ~= currentDomainIndex)
                    check = true;
                    break;
                end
            end
        end
%         function globalCoordinates = getGlobalCoordinatesFromIndexNumberOfSeedPoint(obj, n)
%             globalCoordinates = obj.getGlobalCoordinatesOfSeedPoint(obj.seedPoints(n).coords);
%         end
        
        function globalCoordinates = getGlobalCoordinatesOfSeedPoint(obj, seedPointCoordinates)            
            localCoordinates = obj.indexGeometry.mapLocalToGlobal(seedPointCoordinates);
            globalCoordinates = obj.support.mapLocalToGlobal(localCoordinates);
        end
    end
    properties
        indexGeometry
        support
        seedPoints
        numberOfSeedPoints
        pointCloud
        myOrientedBoundingBox
    end
end

