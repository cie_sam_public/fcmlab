%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%


classdef SpaceQuadTree<AbsSpaceTreeCtrl
    %SpaceQuadTree
    
    properties
    end
    
    methods (Access = public)
        function obj = SpaceQuadTree(embeddedDomain, geometry)
            obj = obj@AbsSpaceTreeCtrl(embeddedDomain, geometry);
        end
        %% partition
        function partition(obj, desiredDepth, desiredNumberOfSeedPoints)
            
            obj.treeRoot = SpaceTreeNode(0, 4, obj.indexGeometry, obj.geometry, desiredNumberOfSeedPoints);
            obj.getSeedPoints(obj.treeRoot);
            obj.partitionRecursively(obj.treeRoot, desiredDepth);
            
        end
        %% getSubIndexGeometry
        function SubIndexGeometry = getSubIndexGeometry(obj, Node)
            
            % 7--5--8--6--9
            % |     |     |
            % 10    11    12
            % |     |     |
            % 4--3--5--4--6
            % |     |     |
            % 7     8     9
            % |     |     |
            % 1--1--2--2--3
            
            vertices = Vertex.empty(9,0);
            
            
            vertices(1) = Vertex(Node.indexGeometry.mapLocalToGlobal([-1.0,-1.0]));
            vertices(2) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 0.0,-1.0]));
            vertices(3) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 1.0,-1.0]));
            vertices(4) = Vertex(Node.indexGeometry.mapLocalToGlobal([-1.0, 0.0]));
            vertices(5) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 0.0, 0.0]));
            vertices(6) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 1.0, 0.0]));
            vertices(7) = Vertex(Node.indexGeometry.mapLocalToGlobal([-1.0, 1.0]));
            vertices(8) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 0.0, 1.0]));
            vertices(9) = Vertex(Node.indexGeometry.mapLocalToGlobal([ 1.0, 1.0]));
            
            lines = Line.empty(12,0);
            lines(1) = Line( vertices(1), vertices(2) );
            lines(2) = Line( vertices(2), vertices(3) );
            lines(3) = Line( vertices(4), vertices(5) );
            lines(4) = Line( vertices(5), vertices(6) );
            lines(5) = Line( vertices(7), vertices(8) );
            lines(6) = Line( vertices(8), vertices(9) );
            lines(7) = Line( vertices(1), vertices(4) );
            lines(8) = Line( vertices(2), vertices(5) );
            lines(9) = Line( vertices(3), vertices(6) );
            lines(10) = Line( vertices(4), vertices(7) );
            lines(11) = Line( vertices(5), vertices(8) );
            lines(12) = Line( vertices(6), vertices(9) );
            
            SubIndexGeometry = Rectangle.empty(4,0);
            SubIndexGeometry(1) = Rectangle( vertices([1 2 5 4]), lines([1 8 3 7]) );
            SubIndexGeometry(2) = Rectangle( vertices([2 3 6 5]), lines([2 9 4 8]) );
            SubIndexGeometry(3) = Rectangle( vertices([4 5 8 7]), lines([3 11 5 10]) );
            SubIndexGeometry(4) = Rectangle( vertices([5 6 9 8]), lines([4 12 6 11]) );
            
        end
        
        %% getSeedPoints
        function getSeedPoints(obj, Node)
            localSeedPoints = linspace(-1,1,Node.numberOfSeedPoints);
            for i = 1:Node.numberOfSeedPoints
                for j = 1:Node.numberOfSeedPoints
                    n = length(Node.seedPoints)+1;
                    localSeedPointCoordinates = [localSeedPoints(i),localSeedPoints(j),0];
                    Node.seedPoints(n) = SeedPoint(...
                        obj.getDomainIndexForSeedPoint(...
                        Node.getGlobalCoordinatesOfSeedPoint(localSeedPointCoordinates)...
                        ),...
                        localSeedPointCoordinates);               
                end
            end
        end
        function subDomains = getSubDomains(obj)
            subDomains = [];
            subDomains = obj.treeRoot.getSubDomains(subDomains);
        end
        function OBBs = getAllOBBs(obj)
            OBBs = OrientedBoundingBox.empty(1,0);
            OBBs = obj.treeRoot.getAllOBBs(OBBs);
        end
        %%
        function createOrientedBoundingBoxes(obj, depthCounter)
            obj.treeRoot.createOrientedBoundingBox(depthCounter)
        end
        %% getCompletePointCloud
        function pointCloud = getCompletePointCloud(obj)
            pointCloud = SeedPoint.empty(1,0);
            pointCloud = obj.treeRoot.getCompletePointCloud(pointCloud);
        end
        %% getSeedPointCloud
        function seedPointCloud = getCompleteSeedPointCloud(obj)
            seedPointCloud = SeedPoint.empty(1,0);
            seedPointCloud = obj.treeRoot.getCompleteSeedPointCloud(seedPointCloud);
        end
        function rectangles = getAllIndexGeometries(obj)
            rectangles = obj.treeRoot.getIndexGeometries();
        end
        %% getIndexGeometry
        function indexGeometry = getIndexGeometry(obj)
            indexGeometry = Rectangle([...
                Vertex([-1.0,-1.0,0.0]),...
                Vertex([ 1.0,-1.0,0.0]),...
                Vertex([ 1.0, 1.0,0.0]),...
                Vertex([-1.0, 1.0,0.0])],[]);
        end
    end
    
    
end

