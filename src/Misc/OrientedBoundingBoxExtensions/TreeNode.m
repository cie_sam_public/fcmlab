%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%



classdef TreeNode < hgsetget
    %TreeNode Part of tree. 
    
    
    properties %(Dependent, Hidden)
        Children
        NumberOfChildren
        Parent
        Depth = 0;
        DepthFromTop
    end
    methods
        %% constructor
        function obj = TreeNode(Parent, NumberOfChildren)
            obj.Parent = Parent;
            obj.NumberOfChildren = NumberOfChildren;
            obj.Children = cell(NumberOfChildren,1);
            if (obj.Parent == 0)
                obj.DepthFromTop = 0;
            else
                obj.DepthFromTop = obj.Parent.DepthFromTop+1;
            end
        end
        
        %% Building functions
        function SetChildren(obj, ChildID, Child)
            obj.Children(ChildID) = {Child};  
            if (Child.Depth+1>obj.Depth)
                obj.Depth = Child.Depth+1;
                if (obj.Parent~=0)
                    obj.Parent.higherDepthRecursively(obj.Depth)
                end
            end
            obj.Children{ChildID}.higherDepthFromTopRecursively(obj.DepthFromTop)
        end
        
        %% Get- and set- functions
           
        
        function higherDepth(obj)
            obj.Depth = obj.Depth +1;
        end
        function Depth = get.Depth(obj)
            Depth = obj.Depth;
        end
        
%         function obj = set.Parent(obj, PA)
%             obj.Parent = PA;
%         end
        function Parent = get.Parent(obj)
            Parent = obj.Parent;
        end
        
        function higherDepthRecursively(obj, newDepth)
            if (newDepth>=obj.Depth)
            obj.Depth = newDepth + 1;
            if (obj.Parent ~= 0)
            obj.Parent.higherDepthRecursively(obj.Depth)
            end
            end
        end
        
        
        function higherDepthFromTopRecursively(obj, newDepthFromTop)
            obj.DepthFromTop = newDepthFromTop + 1;
            if (obj.getNumberOfAllChildren() ~= 0)
                
            for n = 1:obj.NumberOfChildren
                obj.Children{n}.higherDepthFromTopRecursively(obj.DepthFromTop)
            end
            end
            
        end
        
        %% getNumberOfAllChildren
        function NumberOfAllChildren = getNumberOfAllChildren(obj)
            if (obj.Depth == 0)
                NumberOfAllChildren = 0;
            else
                Variable = 0;
                for n = 1:obj.NumberOfChildren
                    Variable = Variable + obj.Children{n,1}.getNumberOfAllChildren();
                end
                NumberOfAllChildren = Variable + obj.NumberOfChildren;
            end
        end
        %% isLeaf
        function bool = isLeaf(obj)
            if (obj.Depth == 0)
                bool = true;
            else
                bool = false;
            end
        end
    end
        
end

