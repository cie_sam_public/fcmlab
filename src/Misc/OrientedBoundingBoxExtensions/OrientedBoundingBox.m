%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Handles Oriented Bounding Boxes

classdef OrientedBoundingBox < hgsetget
    %OrientedBoundingBox 
    
    properties %(Access = private)
        lengths%lengths of Oriented Bounding Box
        vectors%holds direction
        center%center of OBB in real Coordinates
        barycenter
        covarianceMatrix
        extremePoints
        quadIndexGeometry
    end
    
    methods (Access = public)
        %% Constructor
        function obj = OrientedBoundingBox(pointCloudOrCompleteQuad, numberOfPoints)
            switch nargin
            case 2
            bc = obj.getBarycenter(pointCloudOrCompleteQuad, numberOfPoints);
            obj.barycenter = [bc(1),bc(2)];
            obj.covarianceMatrix = obj.getCovarianceMatrix(obj.barycenter, pointCloudOrCompleteQuad, numberOfPoints);
            [V, D] = eig(obj.covarianceMatrix);
            obj.vectors = transpose(inv(V));
            obj.extremePoints = obj.findExtremePoints(...
                obj.transformPoints(pointCloudOrCompleteQuad, obj.barycenter, obj.vectors));
            v1 = [obj.extremePoints(4), obj.extremePoints(5)];
            v2 = [obj.extremePoints(1), obj.extremePoints(5)];
            v3 = [obj.extremePoints(1), obj.extremePoints(2)];
            v4 = [obj.extremePoints(4), obj.extremePoints(2)];
            obj.lengths(1) = obj.extremePoints(1) - obj.extremePoints(4);
            obj.lengths(2) = obj.extremePoints(2) - obj.extremePoints(5);
            vc = [(obj.extremePoints(4) + obj.lengths(1)/2),(obj.extremePoints(5) + obj.lengths(2)/2)];
            coords1 = obj.LocalToWorld(v1, obj.barycenter, obj.vectors);
            coords2 = obj.LocalToWorld(v2, obj.barycenter, obj.vectors);
            coords3 = obj.LocalToWorld(v3, obj.barycenter, obj.vectors);
            coords4 = obj.LocalToWorld(v4, obj.barycenter, obj.vectors);
            obj.center = obj.LocalToWorld(vc, obj.barycenter, obj.vectors);
            
            obj.quadIndexGeometry = Quad([...
                Vertex([coords1(1),coords1(2),0]),...
                Vertex([coords2(1),coords2(2),0]),...
                Vertex([coords3(1),coords3(2),0]),...
                Vertex([coords4(1),coords4(2),0])...
                ],[]);
            case 1
                obj.quadIndexGeometry = pointCloudOrCompleteQuad;
                LocalCenter = pointCloudOrCompleteQuad.calcCentroid;
                obj.center = [LocalCenter(1), LocalCenter(2)]';
                corners = pointCloudOrCompleteQuad.getVerticesCoordinates;
                directions1 = ((corners(1,:) - corners(2,:))/norm(corners(1,:) - corners(2,:)));
                directions2 = ((corners(1,:) - corners(4,:))/norm(corners(1,:) - corners(4,:)));
                obj.vectors = [[directions1(1),directions2(1)];...
                    [directions1(2),directions2(2)]];
                obj.lengths(1) = norm(corners(1,:) - corners(2,:));
                obj.lengths(2) = norm(corners(1,:) - corners(4,:));
            end
        end
        
            

        
                %%Intersection tests
        function bool = hasCollisionWith(obj, OBB)
            bool = true;
            AxisDirections = [obj.vectors, OBB.vectors];
            HalfLengths = [obj.lengths, OBB.lengths]*0.5;
            T = [obj.center - OBB.center; 0];
            for n = 1:4
                L = [AxisDirections(:,n);0];
                LengthOfAxis = norm(cross(L,T));
                AxisTestLength = (norm(cross([(HalfLengths(1)*AxisDirections(:,1));0],L)) + ...
                    norm(cross([(HalfLengths(2)*AxisDirections(:,2));0],L)) + ...
                    norm(cross([(HalfLengths(3)*AxisDirections(:,3));0],L)) + ...
                    norm(cross([(HalfLengths(4)*AxisDirections(:,4));0],L)));
                if AxisTestLength < LengthOfAxis
                    bool = false;
                    break;
                end
            end
        end
    end
    methods
        %% set get functions
        function lengths = get.lengths(obj)
            lengths = obj.lengths;
        end
        function vectors = get.vectors(obj)
            vectors = obj.vectors;
        end
        function center = get.center(obj)
            center = obj.center;
        end
        function quadIndexGeometry = get.quadIndexGeometry(obj)
            quadIndexGeometry = obj.quadIndexGeometry;
        end
    end
    methods (Access = private)
        %% Barycenter
        function barycenter = getBarycenter(obj, pointCloud, numberOfPoints)
            barycenter = [];
            barycenter(1) = 0;%x
            barycenter(2) = 0;%y
            for n = 1:numberOfPoints
                barycenter(1) = barycenter(1) + pointCloud(n).coords(1);%x
                barycenter(2) = barycenter(2) + pointCloud(n).coords(2);%y
            end
            barycenter(1) = barycenter(1)/numberOfPoints;%x
            barycenter(2) = barycenter(2)/numberOfPoints;%y
        end
        %%
        function covarianceMatrix = getCovarianceMatrix(obj, barycenter, pointCloud, numberOfPoints)
            myCovariance1 = 0;
            myCovariance2 = 0;
            myCovariance4 = 0;
            for n = 1:numberOfPoints
                x1 = pointCloud(n).coords(1) - barycenter(1);
                x2 = pointCloud(n).coords(2) - barycenter(2);
                
                myCovariance1 = myCovariance1 + x1*x1;
                myCovariance2 = myCovariance2 + x2*x2;
                myCovariance4 = myCovariance4 + x1*x2;
            end
            myCovariance1 = myCovariance1/numberOfPoints;
            myCovariance2 = myCovariance2/numberOfPoints;
            myCovariance4 = myCovariance4/numberOfPoints;
            covarianceMatrix = [...
                myCovariance1 myCovariance4;...
                myCovariance4 myCovariance2];
        end
        %%
        function pointCloud = transformPoints(obj,localPointCloud, barycenter, TransformationMatrix)           
            pointCloud = SeedPoint.empty(1,0);
            for n = 1:length(localPointCloud)
                pointCloud(n) = SeedPoint(true,((TransformationMatrix)*(transpose([localPointCloud(n).coords(1),localPointCloud(n).coords(2)])-transpose(barycenter))));
            end
        end
        %%
        function transformatedVector = LocalToWorld(obj, Vector, barycenter, TransformationMatrix)
            transformatedVector = inv(TransformationMatrix)*transpose(Vector) + transpose(barycenter);
            
        end
        %%
        function extremePoints = findExtremePoints(obj, localPointCloud)
            safety = 0.01;
            extremePoints = [];
            extremePoints(1) = localPointCloud(1).coords(1);
            extremePoints(2) = localPointCloud(1).coords(2);
            extremePoints(4) = localPointCloud(1).coords(1);
            extremePoints(5) = localPointCloud(1).coords(2);
            for n = 1:length(localPointCloud)
                if (extremePoints(1) < localPointCloud(n).coords(1))
                    extremePoints(1) =  localPointCloud(n).coords(1);
                end
                if (extremePoints(2) < localPointCloud(n).coords(2))
                    extremePoints(2) =  localPointCloud(n).coords(2);
                end
                if (extremePoints(4) > localPointCloud(n).coords(1))
                    extremePoints(4) =  localPointCloud(n).coords(1);
                end
                if (extremePoints(5) > localPointCloud(n).coords(2))
                    extremePoints(5) =  localPointCloud(n).coords(2);
                end
            end
            extremePoints(1) = extremePoints(1) + safety;
            extremePoints(2) = extremePoints(2) + safety;
            extremePoints(4) = extremePoints(4) - safety;
            extremePoints(5) = extremePoints(5) - safety;
        end        
    end
end

