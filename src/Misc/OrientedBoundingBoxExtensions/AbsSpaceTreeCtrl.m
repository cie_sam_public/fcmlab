classdef AbsSpaceTreeCtrl<handle
    %AbsSpaceTreeCtrl Controls Tree with datas
    %   Detailed explanation goes here
    
    properties
        treeRoot
        embeddedDomain
        geometry
        indexGeometry
    end
    
    methods (Access = public)
        %% constructor
        function obj = AbsSpaceTreeCtrl(embeddedDomain, geometry)
            obj.embeddedDomain = embeddedDomain;
            obj.geometry = geometry;
            obj.indexGeometry = obj.getIndexGeometry();
        end
    end
    methods    
        %% partitionRecursivley
        function partitionRecursively(obj, Node, depthCounter)
            check = obj.isIntersected(Node);
            if (check && depthCounter ~= 0)
                
                SubIndexGeometry = obj.getSubIndexGeometry(Node);
                
                depthCounter = depthCounter-1;
                
                for n= 1:length(SubIndexGeometry)
                    Node.SetChildren(n, SpaceTreeNode(Node, Node.NumberOfChildren, SubIndexGeometry(n), Node.support, Node.numberOfSeedPoints));
                    obj.getSeedPoints(Node.Children{n,1});
                    obj.partitionRecursively(Node.Children{n,1},depthCounter);
                end
            end
        end
%         % returns true if it is completly or half intersected
%         function check = isIntersected(obj, Node)
%             check = false;
%             for i = 1:Node.numberOfSeedPoints^2
%                 
%                 if (Node.seedPoints(i).domainIndex == 2)
%                     check = true;
%                     break;
%                 end
%             end
%         end
        function check = isIntersected(obj, Node)
            
            initialDomainIndex = Node.seedPoints(1).domainIndex;
            check = false;
            
            for i = 1:Node.numberOfSeedPoints^2
                currentDomainIndex = Node.seedPoints(i).domainIndex;
                if (initialDomainIndex ~= currentDomainIndex)
                    check = true;
                    break;
                end
            end
        end

        %% getDomainIndexForSeedPoint
        function domainIndex = getDomainIndexForSeedPoint(obj,seedPointCoordinates)
            domainIndex = obj.embeddedDomain.getDomainIndex(seedPointCoordinates);
        end

    end
    
end

