%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
function y = getBernsteinPolynomial(xi, i, p, diffOrder, binomialCoefficientEvaluator) 
% Function to compute the Bernstein Polynomials on [-1, 1] starting from index
% i=1
% Parameters
%   xi : coordinate at which evaluate the Bersntein polynomials
%   i  : indices of the Bernstein Polynomials to evaluate
%   p  : degree of the Bernstein Polynomials
%   diffOrder : derivative to evaluate ( 0 = evaluate bernstein polynomials, 1 = evaluate first derivative of Bernstein Polynomials)
%
% Returns
%   a column vector of evaluated Bernstein Polynomials
%
% Example
%   getBernsteinPolynomial(0, 2:3, 2, 0) evaluates the 2nd and 3rd bernstein polynomial of degree 2 at 0 
%   getBernsteinPolynomial(0, 2:3, 2, 1) evaluates the derivative 2nd and 3rd bernstein polynomial of degree 2 at 0

    y = zeros(length(i), length(xi));
        
    %ix = 1;
    %for j=i
    %parfor ix = 1:length(i)
    for ix = 1:length(i)
        j=i(ix);
        y(ix, :) = bernsteinPolynomial01((xi+1)/2, j-1, p, diffOrder, binomialCoefficientEvaluator) ./2^diffOrder;
        %ix = ix+1;
    end
    
    %y=y(unique([1 size(y,1) 2:size(y,1)-1]),:);
end


% Function to get the Bernstein Polynomials on [0, 1] starting from index
% i=0
function y = bernsteinPolynomial01(xi, i, p, diffOrder, binomialCoefficientEvaluator)

% persistent binomTable;
% if isempty(binomTable) 
%     binomTable = zeros(20,20);
%     
%     for d=1:size(binomTable,1)
%         for j=0:d
%             binomTable(d,j+1) = nchoosek(d,j);
%         end
%     end
% end
    
    
    if not (0<=i && i<=p)
         y = zeros(size(xi));
         return;
    end
            
    if diffOrder == 0
        
        if p==0
            y = ones(size(xi));
        else
            %y = 0.5 .* (  (1-xi) .* bernsteinPolynomial01(xi, i, p-1, diffOrder) + (1+xi) .*  bernsteinPolynomial01(xi, i-1, p-1, diffOrder) );
            %y = double(nchoosek(uint8(p),uint8(i))) .* (xi).^i .* (1-xi).^(p-i);
            %y = nchoosekmod(p,i) .* (xi).^i .* (1-xi).^(p-i);
            %y = binomTable(p,i+1) * (xi).^i .* (1-xi).^(p-i);
            
            coeff = nchoosek20(p,i);
            %coeff = binomialCoefficientEvaluator.nchoosek(p,i);
            y = coeff * (xi).^i .* (1-xi).^(p-i);
        end
        
    else
        
        if diffOrder == 1
            
            if p==0 
               y = zeros(size(xi));
            else
                y = p .* ( bernsteinPolynomial01(xi, i-1, p-1, 0, binomialCoefficientEvaluator) - bernsteinPolynomial01(xi, i, p-1, 0, binomialCoefficientEvaluator) );
            end
            
        end
        
    end
        
end