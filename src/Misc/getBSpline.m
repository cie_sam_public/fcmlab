%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Function to get all B-Spline Functions of the order pmax
% uses recursion
% u: current parameter value
% U: knot Vector
% pmax: highest polynomial order

function Npmax=getBSpline(u,U,pmax)

 
m=length(U);

N(m-1,pmax+1)=0;

% Order p=0
for i=1:m-1
    if u>=U(i) && u<=U(i+1)
        N(i,1)=1;
    end
    
end

%Order p>0
for p=2:pmax+1
    
    for i=1:m-p
       
        if abs(U(i+p-1)-U(i))>2*eps
            a=(u-U(i))/(U(i+p-1)-U(i));
        else
            a=0;
        end
        
        if abs(U(i+p)-U(i+1))>2*eps
        b=(U(i+p)-u)/(U(i+p)-U(i+1));      
        else
        b=0;
        end
        
        N(i,p)=a*N(i,p-1)+b*N(i+1,p-1);
        N(i,p)=min(N(i,p),1);
    
    end
end



Npmax=N(1:m-pmax-1,pmax+1); %select only shape functions of the order pmax
end
