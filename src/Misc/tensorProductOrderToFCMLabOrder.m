function result = tensorProductOrderToFCMLabOrder(vector, numberOfDofsPerDirection)

    nodalModes = [1 numberOfDofsPerDirection numberOfDofsPerDirection^2 (numberOfDofsPerDirection-1)*numberOfDofsPerDirection+1 ];
    internalModes = 2:numberOfDofsPerDirection-1;
    edgeModes = [internalModes (internalModes)*numberOfDofsPerDirection internalModes+(numberOfDofsPerDirection-1)*numberOfDofsPerDirection (internalModes-1)*numberOfDofsPerDirection+1];

    faceModes=[];
    for i=numberOfDofsPerDirection * (internalModes-1)
        for j=internalModes
            faceModes = [faceModes i+j];
        end
    end
    
    for i = 1:size(vector,2)/numberOfDofsPerDirection^2
        result(:,(i-1)*numberOfDofsPerDirection^2+(1:numberOfDofsPerDirection^2)) = [vector(:, (i-1)*numberOfDofsPerDirection^2+ nodalModes) vector(:, (i-1)*numberOfDofsPerDirection^2+ edgeModes) vector(:, (i-1)*numberOfDofsPerDirection^2+ faceModes)];
    end

end