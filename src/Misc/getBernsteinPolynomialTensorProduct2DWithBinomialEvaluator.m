function Shapes = getBernsteinPolynomialTensorProduct2D( Coord, IndicesXi, PolynomialDegreeXi, IndicesEta, PolynomialDegreeEta , DiffOrder, binomialCoefficientEvaluator )

    ShapesR = getBernsteinPolynomialWithBinomialEvaluator(Coord(1), IndicesXi, PolynomialDegreeXi, 0, binomialCoefficientEvaluator);
    ShapesS = getBernsteinPolynomialWithBinomialEvaluator(Coord(2), IndicesEta, PolynomialDegreeEta, 0, binomialCoefficientEvaluator);

    tensorProductShapes = ShapesR * ShapesS';

    Shapes = reshape(tensorProductShapes, 1, length(IndicesXi)*length(IndicesEta));
    
    if DiffOrder > 0 
         
        d_ShapesR_dr = getBernsteinPolynomialWithBinomialEvaluator(Coord(1), IndicesXi, PolynomialDegreeXi, DiffOrder, binomialCoefficientEvaluator);
        d_ShapesS_ds = getBernsteinPolynomialWithBinomialEvaluator(Coord(2), IndicesEta, PolynomialDegreeEta, DiffOrder, binomialCoefficientEvaluator);

        d_tensorProductShapes_dr = d_ShapesR_dr    * ShapesS';
        d_tensorProductShapes_ds = ShapesR         * d_ShapesS_ds';
        
        d_Shapes_dr = reshape(d_tensorProductShapes_dr, 1, length(IndicesXi)*length(IndicesEta) );
        d_Shapes_ds = reshape(d_tensorProductShapes_ds, 1, length(IndicesXi)*length(IndicesEta) );
        
        Shapes = [ Shapes; d_Shapes_dr; d_Shapes_ds];
    end

end