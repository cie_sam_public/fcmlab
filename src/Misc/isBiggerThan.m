function isBiggerThan = isBiggerThan( value1, value2, tolerance )

if nargin<3
    tolerance=1e-14;
end

isBiggerThan = value1>=value2-tolerance;

end

