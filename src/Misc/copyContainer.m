function copiedCont = copyContainer(cont2copy)
% This function copies a container.map object by VALUE with all attributes

    keys2copy = keys(cont2copy);            % get all the keys of the original container
    keyType2Copy = cont2copy.KeyType;       % get the keyType of the original container
    valueType2Copy = cont2copy.ValueType;   % get the valueType of the original container
     
    % create a copy of the original container using the same attributes
    copiedCont = containers.Map('KeyType',keyType2Copy,'ValueType',valueType2Copy);
    
    % loop over the entries of the container to copy them one by one using
    % the correct key
    for i = 1:length(cont2copy)
        index = cell2mat(keys2copy(i)); % get key of original container
        copiedCont(index) = cont2copy(index);   % copy the value using the key
    end
end

