%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% function getGradQ
% It returns the gradient of the standard interpolation matrix 
% for eigenvalues calculations 


function GradQ = getGradQ(obj,Coord)

    % Calculating derivatives of shape functions divided by Jacobian
    % Derivatives in r and s direction (returns a vector 1xM)
    DerivShapeFunction = obj.evalDerivOfShapeFunct(Coord);
    
    Jacobian = obj.calcJacobian(Coord);
    
    numberOfDofsPerFieldComponent = size(DerivShapeFunction,2)/2;
    dNdr = DerivShapeFunction(1,1:numberOfDofsPerFieldComponent);
    dNds = DerivShapeFunction(1,numberOfDofsPerFieldComponent+1:2*numberOfDofsPerFieldComponent);
    
     dNdR = [ dNdr; dNds ];
    
     % Dividing by Jacobian
    dNdX = Jacobian(1:2,1:2) \ dNdR ;
    
    GradQ = zeros(2,obj.DofDimension*numberOfDofsPerFieldComponent);
    
    % Assembling derivatives into Q matrix
    GradQ(1,1:SizeMPerDofDimension) = dNdX(1,:);
    GradQ(2,SizeMPerDofDimension+1:obj.DofDimension*SizeMPerDofDimension) = dNdX(2,:);
 
end