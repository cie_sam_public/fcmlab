%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef PlasticBar < AbsPlasticElement
    
    methods(Access = public)
        %% constructor
        function obj = PlasticBar(Nodes,Edges,Material,Integrator,...
                PolynomialDegree,DofDimension,Domain)
            SpaceDimension = 1;
            numberOfDofsPerDirection=PolynomialDegree+1;
            
            obj = obj@AbsPlasticElement(Nodes,Edges,[],[],Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain);
            obj.geometricSupport = Edges(1).getLine();
            
            % initialize tempCont container
            obj.ID_EpsilonElast = 1;
            obj.ID_EpsilonPlast = 2;
            obj.ID_Stress = 3;
            obj.ID_HardeningVar = 4;
            obj.initContainer = containers.Map('KeyType','int32','ValueType','any');
            obj.initContainer(obj.ID_EpsilonElast) = 0; % Elastic Strain
            obj.initContainer(obj.ID_EpsilonPlast) = 0; % Plastic Strain
            obj.initContainer(obj.ID_Stress) = 0;       % Stress
            obj.initContainer(obj.ID_HardeningVar) = 0; % Hardening Variable
        end
        
        %%
        N = evalShapeFunct(obj,Coord)
        dN = evalDerivOfShapeFunct(obj,Coord)
        B = getB(obj,Coord)
        
        LocalCoord = mapGlobalToLocal(obj,GlobalCoord)
        GlobalCoord = mapLocalToGlobal(obj,LocalCoord)
        S = getStress(obj,Coord)
        
        %% calculate the Jacobian
        function J = calcJacobian(obj,Coord)
            J = obj.edges.calcJacobian(Coord);
        end
        
        %% calculate the determinant of the Jacobian
        function detJ = calcDetJacobian(obj,Coord)
            detJ = obj.edges.calcDetJacobian(Coord);
        end
        
        %% get ID of Stress in Container
        function ID = getStressID(obj)
            ID = obj.ID_Stress;
        end
        
        %% get ID of Elastic Strain in Container
        function ID = getEpsilonElastID(obj)
            ID = obj.ID_EpsilonElast;
        end
        
        %% get ID of Plastic Strain in Container
        function ID = getEpsilonPlastID(obj)
            ID = obj.ID_EpsilonPlast;
        end  
    end
    
    methods (Access = protected)
        %%
        NodalModes = evalNodalModes(obj,Coord);
        EdgeModes = evalEdgeModes(obj,Coord);
        
        NodalModesDerivR = evalNodalModesDeriv(obj,Coord);
        EdgeModesDerivR = evalEdgeModesDeriv(obj,Coord);
        
        function FaceModes = evalFaceModes(obj)
            FaceModes = [];
        end
        
        function VolumeModes = evalVolumeModes(obj)
            VolumeModes = [];
        end
        
        function FaceModesDeriv = evalFaceModesDeriv(obj)
            FaceModesDeriv = [];
        end
        
        function VolumeModesDeriv = evalVolumeModesDeriv(obj)
            VolumeModesDeriv = [];
        end 
    end    
    
    properties (Access = private)
        % indices where the variables are stored in the "tempCont"
        % Container => to avoid messing things up
        ID_EpsilonElast
        ID_EpsilonPlast
        ID_Stress
        ID_HardeningVar
    end
end