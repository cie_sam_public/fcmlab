function [ID_E_El, ID_E_Pl, ID_S, ID_HV] = getIndicesOfTempCont(obj)
% This function returns the indices of where the variables are stored in
% the "tempCont" - Container
    ID_E_El = obj.ID_EpsilonElast;
    ID_E_Pl = obj.ID_EpsilonPlast;
    ID_S = obj.ID_Stress;
    ID_HV = obj.ID_HardeningVar;
end

