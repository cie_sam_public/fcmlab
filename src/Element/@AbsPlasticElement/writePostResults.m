%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function writePostResults(obj, loadCase, loadIncrement, GPID, container)
% This function saved the history variables for later post processing:
% It extracts the tmp variables from the computation and saves them
% properly. This is done for each Gauss Point => loadstep => load increment

% Create the container of the Gauss Point if it does not exist yet, otherwise
% extract it form the PostResults-Container

    if ~isKey(obj.PostResults,GPID)
        % Check if container for GP already exists and create it if not
        GPCont = containers.Map('KeyType','int32','ValueType','any');
        % Create load case container
        loadCaseCont = containers.Map('KeyType','int32','ValueType','any');
    else
        GPCont = obj.PostResults(GPID);
        % Check if container for load case already exists and create it if not
        if ~isKey(GPCont,loadCase);
            loadCaseCont = containers.Map('KeyType','int32','ValueType','any');
        else
            loadCaseCont = GPCont(loadCase);
        end
    end
    
    % Load increment key will never exist as this function is only called
    % once each load increment
    % store results of this load increment with the load increment as key
    % in the loadcase container
    loadCaseCont(loadIncrement) = copyContainer(container);
    
    GPCont(loadCase) = loadCaseCont;
    
    obj.PostResults(GPID) = GPCont;
end