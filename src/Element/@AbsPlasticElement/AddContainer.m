%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function AddContainer(obj,Coord)
% This function checks if the container of this GP already exists and
% creates it if not

    GPX = Coord(1);
    GPY = Coord(2);
    GPZ = Coord(3);

    isContainedX = false;
    isContainedY = false;
    isContainedZ = false;

    if isKey(obj.GPIDCont,GPX);
        ContainerY = obj.GPIDCont(GPX);
        isContainedX = true;

        if isKey (ContainerY,GPY)
            isContainedY = true;
            ContainerZ = ContainerY(GPY);

            if isKey(ContainerZ,GPZ)
                isContainedZ = true;
            end
        end
    end

    if ~isContainedZ % GP is not yet contained in the container, so it has to be added

        obj.GP_ID = obj.GP_ID + 1; % must be in critical section
        
        if ~isContainedY
            ContainerZ = containers.Map('KeyType','double','ValueType','any');
        end

        ContainerZ(GPZ) = obj.GP_ID;

        if ~isContainedX
            ContainerY = containers.Map('KeyType','double','ValueType','any');
        end

        ContainerY(GPY) = ContainerZ;

        obj.GPIDCont(GPX) = ContainerY;
        
        obj.tempCont(obj.GP_ID) = copyContainer(obj.initContainer);
        obj.permCont(obj.GP_ID) = copyContainer(obj.initContainer);

        % save GPCoord in other direction => Key = ID, Coord = Value
        obj.GPCoordCont(obj.GP_ID) = Coord;

        % save B-operater for later use in B-Container
        B = obj.getB(Coord);
        obj.BCont(obj.GP_ID) = B;
    end
end