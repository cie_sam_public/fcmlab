%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%   This class is the interface of geometrically linear plastic elements

classdef AbsPlasticElement < AbsNonlinElement
    
    methods (Access = public)
        %% constructor
        function obj = AbsPlasticElement(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain)
            
            obj = obj@AbsNonlinElement(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain);
            
            obj.GP_ID = 0;
            obj.GPIDCont = containers.Map('KeyType','double','ValueType','any');
            obj.GPCoordCont = containers.Map('KeyType','int32','ValueType','any');
            obj.tempCont = containers.Map('KeyType','int32','ValueType','any');
            obj.permCont = containers.Map('KeyType','int32','ValueType','any');
            obj.BCont = containers.Map('KeyType','int32','ValueType','any');
            obj.PostResults = containers.Map('KeyType','int32','ValueType','any');
            obj.oldDofVector  = zeros(obj.DofDimension*(length(obj.nodes) + ...
                      (obj.PolynomialDegree - 1) * length(obj.edges) + ...
                      (obj.PolynomialDegree - 1)^2 * length(obj.faces) + ...
                      (obj.PolynomialDegree - 1)^3 * length(obj.solid)),1);
            obj.postProcessContainer = containers.Map('KeyType','int32','ValueType','any');
        end
        
        AddContainer(obj,Coord);
        GP_ID = getGPID(obj,Coord);
        B = getBAtGP(obj, Coord);
        GPPermCont = getPermContAtGP(obj, Coord);
        GPTempCont = getTempContAtGP(obj, Coord);
        setTempContAtGP(obj, Coord, container);
        UpdatePermanentContainer(obj, loadCase, loadIncrement);
        UpdateTemporaryContainer(obj,loadcase);
        
        integrandFint = calcIntegrandFint(obj,Coord);
        writePostResults(obj, loadCase, loadIncrement, GPID, container);
        
        %% get Post Results Container
        function PostResults = getPostResults(obj)
          PostResults = obj.PostResults;
        end
        
        %% get old DofVector
        function oldDofVector = getOldDofVector(obj)
          oldDofVector = obj.oldDofVector;
        end
        
        %% set old DofVector
        function setOldDofVector(obj,vector)
          obj.oldDofVector = vector;
        end
        
        %% get postProcessContainer
        function container = getPostProcessContainer(obj)
          container = obj.postProcessContainer;
        end
        
        %% set postProcessContainer
        function setPostProcessContainer(obj, container)
          obj.postProcessContainer = copyContainer(container);
        end
    end
    
    methods(Abstract, Access = public)
        %%
        N = evalShapeFunct(obj,Coord);
        dN = evalDerivOfShapeFunct(obj,Coord);
        B = getB(obj,Coord);
        J = calcJacobian(obj);
        detJ = calcDetJacobian(obj,LocalCoords);
        
        LocalCoord = mapGlobalToLocal(obj,GlobalCoord);
        GlobalCoord = mapLocalToGlobal(obj,LocalCoord);
        
        ID = getStressID(obj);
        ID = getEpsilonElastID(obj);
        ID = getEpsilonPlastID(obj);
        S = getStress(obj,Coord);
    end
    
    %% Properties
    properties(Access = protected)
        GP_ID
        GPIDCont
        GPCoordCont
        BCont
        oldDofVector
        PostResults
        tempCont
        permCont
        initContainer 
        postProcessContainer
    end
end