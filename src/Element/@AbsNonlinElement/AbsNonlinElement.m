%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%   abstract element handle class for a pFEM/FCM combination
%   This class defines the interface for the nonlinear elements.
%   Specific element types and their behaviour need to be specified in
%   subclasses.

classdef AbsNonlinElement < AbsElement
    
    methods (Access = public)
        %% constructor
        function obj = AbsNonlinElement(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain)
            
            obj = obj@AbsElement(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain);
        end
        
        %mainly used for testing purposes
        function initializeDOFVector(obj, setOfSolutionVector)        
            obj.dofVector = setOfSolutionVector;        
        end
        
        %%
        Fe_int = calcInternalForceVector(obj);
        Ke = calcStiffnessMatrix(obj);
    end
    
    methods(Abstract, Access = public)
        N = evalShapeFunct(obj,Coord);
        dN = evalDerivOfShapeFunct(obj,Coord);
        B = getB(obj,Coord);
        J = calcJacobian(obj);
        detJ = calcDetJacobian(obj,LocalCoords);
        
        LocalCoord = mapGlobalToLocal(obj,GlobalCoord);
        GlobalCoord = mapLocalToGlobal(obj,LocalCoord);
        S = getStress(obj,Coord);
        integrandFint = calcIntegrandFint(obj,Coord); 
    end
end