
classdef ElementFactoryPlasticBar < AbsElementFactory
    
    methods
        %% constructor
        function obj = ElementFactoryPlasticBar(Material,integrationOrder)
            integrator = Integrator(NoPartition(),integrationOrder);
            Domain = PseudoDomain();
            obj = obj@AbsElementFactory(Material,integrator,Domain);
        end
        
        %% create Element
        function Element = createElement(obj,Nodes,Edges,~,~,...
                PolynomialDegree,DofDimension)
            Element = PlasticBar(Nodes,Edges,obj.material,obj.integrator,...
                PolynomialDegree,DofDimension,obj.domain);
        end
        
        function numberOfDofsPerDirection = getNumberOfDofsPerDirection(~,PolynomialDegree)
            numberOfDofsPerDirection=PolynomialDegree+1;
        end
    end
    
    properties (Access = private)
    end
    
end

