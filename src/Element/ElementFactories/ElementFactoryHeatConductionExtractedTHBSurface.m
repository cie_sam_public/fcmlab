%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Element Factor Elastic Quad class

classdef ElementFactoryHeatConductionExtractedTHBSurface < AbsElementFactory
    
    methods
        %% constructor
        function obj = ElementFactoryHeatConductionExtractedTHBSurface(Material,NumberOfGaussPoints, MultiLevelGeometry, MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator)
            integrator = Integrator(NoPartition(),NumberOfGaussPoints);
            Domain = PseudoDomain(); % domain returns always 2
            obj = obj@AbsElementFactory(Material,integrator,Domain);
            
            obj.MultiLevelGeometries=MultiLevelGeometry.getGeometries;
            obj.RefinementTree=MultiLevelGeometry.getRefinementTree;
            
            for iLevel = 1:length(obj.MultiLevelGeometries)
                KnotsXi{iLevel} = obj.MultiLevelGeometries{iLevel}.getKnotsXi;
                KnotsEta{iLevel} = obj.MultiLevelGeometries{iLevel}.getKnotsEta;
            end
            
            PolynomialDegree = obj.MultiLevelGeometries{1}.getPolynomialDegreeXi;
            
            if nargin<4
                %[obj.ExtractionOperators obj.MultiLevelOperators obj.BezierExtractionOperators obj.AmMultiLevelBezierExtractionOperators] = multiLevelBezierExtractionOperator2DTree( obj.RefinementTree, obj.MultiLevelGeometries );
            else
                obj.ExtractionOperators  = MultiLevelBezierExtractionOperator;
                obj.MultiLevelOperators  = MultiLevelOperator;
                obj.BezierExtractionOperators  = BezierExtractionOperator;
                obj.AmMultiLevelBezierExtractionOperators = AmMultiLevelBezierExtractionOperator;
            end
            
            
            [obj.Aa, obj.Am, obj.Ap, ~] = MultiLevelGeometry.getRefinementDataStructures;
            
            for iLevel = 1:length(obj.MultiLevelGeometries)
                obj.StandardLocationMap{iLevel} = extractedBSplineLocationMap2D(obj.MultiLevelGeometries{iLevel}.getKnotsXi, obj.MultiLevelGeometries{iLevel}.getPolynomialDegreeXi,obj.MultiLevelGeometries{iLevel}.getKnotsEta,obj.MultiLevelGeometries{iLevel}.getPolynomialDegreeEta, 1);
            end
        end
        
        %% create Element
        function Element = createElement(obj,Nodes,Edges,Faces,~, ~,DofDimension, ElementIndex, Level)
            
            BSplineSurface = obj.MultiLevelGeometries{Level};
            
            iXi = mod(ElementIndex-1, BSplineSurface.numberOfKnotSpansInXi)+1;
            iEta = floor((ElementIndex-1)/ BSplineSurface.numberOfKnotSpansInXi) +1;
            ElementExtractionOperator = obj.ExtractionOperators{Level}{iXi, iEta};
            
            PolynomialDegree=BSplineSurface.getPolynomialDegreeXi;
            %              thisindicesX = iXi+(0:PolynomialDegree);
            %                 thisindicesY = (iEta+(0:PolynomialDegree)-1)*size(obj.Am{Level},1)+1;
            %                 thisindices=[];
            %                 for i=1:length(thisindicesY)
            %                     thisindices = [ thisindices thisindicesX+thisindicesY(i)-1];
            %                 end
            thisindices = obj.StandardLocationMap{Level} ( (iEta-1)*BSplineSurface.numberOfKnotSpansInXi + iXi, : );
            
            MultiLevelOperator = obj.MultiLevelOperators{Level}{iXi, iEta};
            BezierExtractionOperator = obj.BezierExtractionOperators{Level}{iXi, iEta};
            AmMultiLevelBezierExtractionOperator = obj.AmMultiLevelBezierExtractionOperators{Level}{iXi, iEta};
            Index = [iXi iEta];
            Element = HeatConductionExtractedTHBSurface(Nodes,Edges,Faces,obj.material,obj.integrator,BSplineSurface.getPolynomialDegreeXi,DofDimension, obj.domain, ElementExtractionOperator, obj.Aa{Level}(thisindices), obj.Am{Level}(thisindices), obj.Ap{Level}(thisindices), MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator, Level, Index);
            
            disp( sprintf( 'level %d span %d', Level-1, ElementIndex-1 ) )
            MultiLevelOperator
            M = MultiLevelOperator* BezierExtractionOperator;
            M( ~any(M,2), : ) = []
            
            
            
        end
        
        function numberOfDofsPerDirection = getNumberOfDofsPerDirection(~,PolynomialDegree)
            numberOfDofsPerDirection=PolynomialDegree+1;
        end
        
    end
    
    properties (Access = public)
        MultiLevelGeometries
        ExtractionOperators
        RefinementTree
        Aa
        Am
        Ap
        
        MultiLevelOperators
        BezierExtractionOperators
        AmMultiLevelBezierExtractionOperators
        
        StandardLocationMap
    end
    
end

