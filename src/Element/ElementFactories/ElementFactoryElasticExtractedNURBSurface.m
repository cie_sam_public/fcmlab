%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Element Factor Elastic Quad class


classdef ElementFactoryElasticExtractedNURBSurface < AbsElementFactory
    
    methods
        %% constructor
        function obj = ElementFactoryElasticExtractedNURBSurface(Material, integrationOrder, KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, Weights)
            integrator = Integrator(NoPartition(),integrationOrder);
            Domain = PseudoDomain(); % domain returns always 2
            obj = obj@AbsElementFactory(Material,integrator,Domain);
                        
            obj.ExtractionOperators = bezierExtractionOperator2D( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta );
            obj.Weights = Weights;
            
            spansXi = knt2brk( KnotsXi );
            obj.NumberOfSpansInXi = length(spansXi)-1;
            
            assert( PolynomialDegreeXi == PolynomialDegreeEta, 'just isotropic tensor space allowed');
            
            obj.KnotsXi = KnotsXi;
            obj.PolynomialDegreeXi = PolynomialDegreeXi;
            obj.KnotsEta = KnotsEta;
            obj.PolynomialDegreeEta = PolynomialDegreeEta;
        end
        
        %% create Element
        function Element = createElement(obj,Nodes,Edges,Faces,~, PolynomialDegree,DofDimension, ElementIndex, ~)
            ElementExtractionOperator = obj.ExtractionOperators( :,:, ElementIndex );
            
            iXi = mod(ElementIndex-1, obj.NumberOfSpansInXi)+1;
            iEta = floor((ElementIndex-1)/ obj.NumberOfSpansInXi) +1;
            
            controlPointsIndices = getControlPointsOfSpan2D ( iXi, iEta, obj.KnotsXi, obj.PolynomialDegreeXi, obj.KnotsEta, obj.PolynomialDegreeEta );
            
            Element = ElasticExtractedNURBSurface(Nodes,Edges,Faces,obj.material,obj.integrator,obj.PolynomialDegreeXi,DofDimension, obj.domain, obj.Weights(controlPointsIndices), ElementExtractionOperator,  1, [iXi iEta]);

        end
        
        function numberOfDofsPerDirection = getNumberOfDofsPerDirection(~,PolynomialDegree)
            numberOfDofsPerDirection=PolynomialDegree+1;
        end
        
    end
    
    properties (Access = private)
        ExtractionOperators
        Weights
        NumberOfSpansInXi
        
        KnotsXi
        PolynomialDegreeXi
        KnotsEta
        PolynomialDegreeEta
    end
    
end

