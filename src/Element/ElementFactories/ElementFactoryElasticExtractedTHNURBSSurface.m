%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef ElementFactoryElasticExtractedTHNURBSSurface < AbsElementFactory
    
    methods
        %% constructor
        function obj = ElementFactoryElasticExtractedTHNURBSSurface(Material,NumberOfGaussPoints, MultiLevelGeometry, MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator)
            integrator = Integrator(NoPartition(),NumberOfGaussPoints);
            Domain = PseudoDomain();
            obj = obj@AbsElementFactory(Material,integrator,Domain);
            
             obj.MultiLevelGeometry = MultiLevelGeometry;
            obj.MultiLevelGeometries=MultiLevelGeometry.getGeometries;
            
            
            obj.ExtractionOperators = MultiLevelBezierExtractionOperator;
            obj.MultiLevelOperators = MultiLevelOperator;
            obj.BezierExtractionOperators = BezierExtractionOperator;
            obj.AmMultiLevelBezierExtractionOperators = AmMultiLevelBezierExtractionOperator;
            

%             [obj.ExtractionOperators, ...
%             obj.MultiLevelOperators, ...
%             obj.BezierExtractionOperators, ...
%             obj.AmMultiLevelBezierExtractionOperators] = multiLevelBezierExtractionOperator2D( obj.Levels, obj.MultiLevelGeometries );
            
            [obj.Aa, obj.Am, obj.Ap, ~] = MultiLevelGeometry.getRefinementDataStructures;
            
            for iLevel = 1:length(obj.MultiLevelGeometries)
                obj.StandardLocationMap{iLevel} = extractedBSplineLocationMap2D(obj.MultiLevelGeometries{iLevel}.getKnotsXi, obj.MultiLevelGeometries{iLevel}.getPolynomialDegreeXi,obj.MultiLevelGeometries{iLevel}.getKnotsEta,obj.MultiLevelGeometries{iLevel}.getPolynomialDegreeEta, 1);
            end
        end
        
        %% create Element
        function Element = createElement(obj,Nodes,Edges,Faces,~, ~,DofDimension, ElementIndex, Level)
            
            BSplineSurface = obj.MultiLevelGeometries{Level};
            
            iXi = mod(ElementIndex-1, BSplineSurface.numberOfKnotSpansInXi)+1;
            iEta = floor((ElementIndex-1)/ BSplineSurface.numberOfKnotSpansInXi) +1;
            ElementExtractionOperator = obj.ExtractionOperators{Level}{iXi, iEta};
            
            thisindices = obj.StandardLocationMap{Level} ( (iEta-1)*BSplineSurface.numberOfKnotSpansInXi + iXi, : );
            
            MultiLevelOperator = obj.MultiLevelOperators{Level}{iXi, iEta};
            BezierExtractionOperator = obj.BezierExtractionOperators{Level}{iXi, iEta};
            AmMultiLevelBezierExtractionOperator = obj.AmMultiLevelBezierExtractionOperators{Level}{iXi, iEta};
            
            elementControlPoints = obj.MultiLevelGeometry.getControlPointsOfSpan(iXi, iEta, Level);
            elementWeights = elementControlPoints(:,4);
            
            
            Element = ElasticExtractedTHNURBSSurface(Nodes,Edges,Faces,obj.material,obj.integrator,BSplineSurface.getPolynomialDegreeXi,DofDimension, obj.domain, elementWeights, ElementExtractionOperator, obj.Aa{Level}(thisindices), obj.Am{Level}(thisindices), obj.Ap{Level}(thisindices), MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator, Level, [iXi iEta]);
            
        end
        
        function numberOfDofsPerDirection = getNumberOfDofsPerDirection(~,PolynomialDegree)
            numberOfDofsPerDirection=PolynomialDegree+1;
        end
        
    end
    
    properties (Access = public)
        MultiLevelGeometries
        ExtractionOperators
        Levels
        Aa
        Am
        Ap
        
        MultiLevelGeometry
        MultiLevelOperators
        BezierExtractionOperators
        AmMultiLevelBezierExtractionOperators
        
        StandardLocationMap
    end
    
end

