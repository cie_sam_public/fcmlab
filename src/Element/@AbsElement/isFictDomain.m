function fictDomain = isFictDomain(obj, Coord)
% This function checks whether the GP (Coord) is in the ficticious domain
    Mat = obj.getMaterialAtPoint(Coord);
    fictDomain = Mat.isFictDomain;
end

