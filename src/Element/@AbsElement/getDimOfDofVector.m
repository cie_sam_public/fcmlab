function d2 = getDimOfDofVector(obj)
% This function returns the current number of solutions in the dof vector
    [~, d2] = size(obj.dofVector);
end

