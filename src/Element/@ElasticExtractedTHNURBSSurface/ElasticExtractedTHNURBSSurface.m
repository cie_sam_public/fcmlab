%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Class ElasticQuad
% A 2D-rectangular element is defined for pFEM and FCM analysis
% It is a plane element without any thickness defined

classdef ElasticExtractedTHNURBSSurface < Abs2DElement
    
    methods(Access = public)
        %% constructor
        function obj = ElasticExtractedTHNURBSSurface(Nodes,Edges,Faces,Material,Integrator,...
            PolynomialDegree,DofDimension,Domain, Weights, extractionOperator, Aa, Am, Ap, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator,  Level, Index)
            NumberOfDofsPerDirection=PolynomialDegree+1;
            
            obj = obj@Abs2DElement(Nodes,Edges,Faces,Material,Integrator,...
                PolynomialDegree,NumberOfDofsPerDirection,DofDimension,Domain);
            
            obj.ExtractionOperator = extractionOperator;
            obj.Aa = Aa;
            obj.Am = Am;
            obj.Ap = Ap;
            obj.MultiLevelOperator=MultiLevelOperator;
            obj.BezierExtractionOperator=BezierExtractionOperator;
            obj.AmMultiLevelBezierExtractionOperator=AmMultiLevelBezierExtractionOperator;
            
            obj.AmMultiLevelBezierExtractionOperator(~any(obj.AmMultiLevelBezierExtractionOperator,2),:) = [];
            
            obj.Weights = Weights;
            obj.Level = Level;
            obj.Index = Index;
        end
        
        %%
        N = evalShapeFunct(obj,Coord);
        dN = evalDerivOfShapeFunct(obj,Coord);
        
        
        function setLocationMatrix(obj, LocationMatrix)
              obj.LocationMatrix = LocationMatrix;
             
              % hack for handling truncated splines
              extractionOperatorZeroRows = repmat( ~any(obj.ExtractionOperator,2)', 1, obj.DofDimension );
              
              obj.LocationMatrix( extractionOperatorZeroRows ) = 0;
              
              obj.NonZeroShapesMask = obj.LocationMatrix(1: length(obj.LocationMatrix)/obj.DofDimension) > 0;
              
              obj.ReducedExtractionOperator = obj.ExtractionOperator(obj.NonZeroShapesMask,:);
        end    
    end
    
     properties (Access = public)
            Level
        Index
     end
    
    properties (Access = private)
        ExtractionOperator
        Weights
        
        ReducedExtractionOperator
        NonZeroShapesMask
        Aa
        Am
        Ap
        MultiLevelOperator
        BezierExtractionOperator
        AmMultiLevelBezierExtractionOperator
    end
        
end

