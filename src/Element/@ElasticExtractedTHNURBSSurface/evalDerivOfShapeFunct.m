%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% evaluate the derivative of a shape function in direction r and s
% This function evaluates the derivatives of the nodal, edge, face modes
% in direction r and s at a local coordinate.

function d_R = evalDerivOfShapeFunct(obj,Coord)

    BezierBasis =  getBernsteinPolynomialTensorProduct2D(Coord, 1:obj.numberOfDofsPerDirection, obj.PolynomialDegree, 1:obj.numberOfDofsPerDirection, obj.PolynomialDegree, 1);

    %version 1: full matrix multiplication
    % BSPlineShapes = BezierBasis * obj.ReducedExtractionOperator';

    %version 2: decomposed matrix multiplication
    LeafLevelBSplines = BezierBasis * obj.BezierExtractionOperator';

    activeShapes = logical(obj.Aa);
    if size(obj.AmMultiLevelBezierExtractionOperator,1)>0  && size(obj.AmMultiLevelBezierExtractionOperator,2)>0
        BSPlineShapes = LeafLevelBSplines(:,logical(obj.Am)) * obj.AmMultiLevelBezierExtractionOperator';
        BSPlineShapes(:,size(BSPlineShapes,2) + (1:sum(activeShapes))) = LeafLevelBSplines(:,activeShapes) ;
    else
        BSPlineShapes = LeafLevelBSplines(:,activeShapes) ;
    end

    if size(BSPlineShapes,2) ~= size(obj.Weights(obj.NonZeroShapesMask),1)
        obj.Weights
    end
    
    Weights = BSPlineShapes * obj.Weights(obj.NonZeroShapesMask);
    
    N      = BSPlineShapes(1,:);
    d_N_dr = BSPlineShapes(2,:);
    d_N_ds = BSPlineShapes(3,:);
    
    W      = Weights(1);
    d_W_dr = Weights(2);
    d_W_ds = Weights(3);
   
    d_R_dr = ( d_N_dr * W - N * d_W_dr ) .* obj.Weights(obj.NonZeroShapesMask)' / W^2;
    d_R_ds = ( d_N_ds * W - N * d_W_ds ) .* obj.Weights(obj.NonZeroShapesMask)' / W^2;
  
    d_R = [  d_R_dr, d_R_ds ];
end