%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% evaluate edge modes derivatives in direction r, s and t
%  This function evaluates the edge modes derivatives with respect to 
%  direction r, s and t at local coordinate.

function EdgeModesDeriv = evalEdgeModesDeriv(~,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal)
    
    DofsPerEdge=length(BSpRInternal);
    % initializing a matrix to store all the edge modes over each edge
    EdgeModesDeriv = zeros(1,36*DofsPerEdge);
    
         % wrt r

    EdgeModesDeriv(1,1:DofsPerEdge) = BSpSLeft*BSpTLeft*DerivBSpRInternal; % edge 1 wrt r
    EdgeModesDeriv(1,DofsPerEdge+1:2*DofsPerEdge) = DerivBSpRRight*BSpTLeft*BSpSInternal; % edge 2 wrt r
    EdgeModesDeriv(1,2*DofsPerEdge+1:3*DofsPerEdge) = BSpSRight*BSpTLeft*DerivBSpRInternal; % edge 3 wrt r
    EdgeModesDeriv(1,3*DofsPerEdge+1:4*DofsPerEdge) = DerivBSpRLeft*BSpTLeft*BSpSInternal;  % edge 4 wrt r 
    
    EdgeModesDeriv(1,4*DofsPerEdge+1:5*DofsPerEdge) = DerivBSpRLeft*BSpSLeft*BSpTInternal; % edge 5 wrt r
    EdgeModesDeriv(1,5*DofsPerEdge+1:6*DofsPerEdge) = DerivBSpRRight*BSpSLeft*BSpTInternal; % edge 6 wrt r
    EdgeModesDeriv(1,6*DofsPerEdge+1:7*DofsPerEdge) = DerivBSpRRight*BSpSRight*BSpTInternal; % edge 7 wrt r
    EdgeModesDeriv(1,7*DofsPerEdge+1:8*DofsPerEdge) = DerivBSpRLeft*BSpSRight*BSpTInternal;  % edge 8 wrt r 
    
    EdgeModesDeriv(1,8*DofsPerEdge+1:9*DofsPerEdge) = BSpSLeft*BSpTRight*DerivBSpRInternal; % edge 9 wrt r
    EdgeModesDeriv(1,9*DofsPerEdge+1:10*DofsPerEdge) = DerivBSpRRight*BSpTRight*BSpSInternal; % edge 10 wrt r
    EdgeModesDeriv(1,10*DofsPerEdge+1:11*DofsPerEdge) = BSpSRight*BSpTRight*DerivBSpRInternal; % edge 11 wrt r
    EdgeModesDeriv(1,11*DofsPerEdge+1:12*DofsPerEdge) = DerivBSpRLeft*BSpTRight*BSpSInternal;  % edge 12 wrt r 
        
           % wrt s

    EdgeModesDeriv(1,12*DofsPerEdge+1:13*DofsPerEdge) = DerivBSpSLeft*BSpTLeft*BSpRInternal; % edge 1 wrt s
    EdgeModesDeriv(1,13*DofsPerEdge+1:14*DofsPerEdge) = BSpRRight*BSpTLeft*DerivBSpSInternal; % edge 2 wrt s
    EdgeModesDeriv(1,14*DofsPerEdge+1:15*DofsPerEdge) = DerivBSpSRight*BSpTLeft*BSpRInternal; % edge 3 wrt s
    EdgeModesDeriv(1,15*DofsPerEdge+1:16*DofsPerEdge) = BSpRLeft*BSpTLeft*DerivBSpSInternal;  % edge 4 wrt s 
    
    EdgeModesDeriv(1,16*DofsPerEdge+1:17*DofsPerEdge) = DerivBSpSLeft*BSpRLeft*BSpTInternal; % edge 5 wrt s
    EdgeModesDeriv(1,17*DofsPerEdge+1:18*DofsPerEdge) = DerivBSpSLeft*BSpRRight*BSpTInternal; % edge 6 wrt s
    EdgeModesDeriv(1,18*DofsPerEdge+1:19*DofsPerEdge) = DerivBSpSRight*BSpRRight*BSpTInternal; % edge 7 wrt s
    EdgeModesDeriv(1,19*DofsPerEdge+1:20*DofsPerEdge) = DerivBSpSRight*BSpRLeft*BSpTInternal;  % edge 8 wrt s 
    
    EdgeModesDeriv(1,20*DofsPerEdge+1:21*DofsPerEdge) = DerivBSpSLeft*BSpTRight*BSpRInternal; % edge 9 wrt s
    EdgeModesDeriv(1,21*DofsPerEdge+1:22*DofsPerEdge) = BSpRRight*BSpTRight*DerivBSpSInternal; % edge 10 wrt s
    EdgeModesDeriv(1,22*DofsPerEdge+1:23*DofsPerEdge) = DerivBSpSRight*BSpTRight*BSpRInternal; % edge 11 wrt s
    EdgeModesDeriv(1,23*DofsPerEdge+1:24*DofsPerEdge) = BSpRLeft*BSpTRight*DerivBSpSInternal;  % edge 12 wrt s 
    
         % wrt t

    EdgeModesDeriv(1,24*DofsPerEdge+1:25*DofsPerEdge) = DerivBSpTLeft*BSpSLeft*BSpRInternal; % edge 1 wrt t
    EdgeModesDeriv(1,25*DofsPerEdge+1:26*DofsPerEdge) = DerivBSpTLeft*BSpRRight*BSpSInternal; % edge 2 wrt t
    EdgeModesDeriv(1,26*DofsPerEdge+1:27*DofsPerEdge) = DerivBSpTLeft*BSpSRight*BSpRInternal; % edge 3 wrt t
    EdgeModesDeriv(1,27*DofsPerEdge+1:28*DofsPerEdge) = DerivBSpTLeft*BSpRLeft*BSpSInternal;  % edge 4 wrt t 
    
    EdgeModesDeriv(1,28*DofsPerEdge+1:29*DofsPerEdge) = BSpRLeft*BSpSLeft*DerivBSpTInternal; % edge 5 wrt t
    EdgeModesDeriv(1,29*DofsPerEdge+1:30*DofsPerEdge) = BSpRRight*BSpSLeft*DerivBSpTInternal; % edge 6 wrt t
    EdgeModesDeriv(1,30*DofsPerEdge+1:31*DofsPerEdge) = BSpRRight*BSpSRight*DerivBSpTInternal; % edge 7 wrt t
    EdgeModesDeriv(1,31*DofsPerEdge+1:32*DofsPerEdge) = BSpRLeft*BSpSRight*DerivBSpTInternal;  % edge 8 wrt t 
    
    EdgeModesDeriv(1,32*DofsPerEdge+1:33*DofsPerEdge) = DerivBSpTRight*BSpSLeft*BSpRInternal; % edge 9 wrt t
    EdgeModesDeriv(1,33*DofsPerEdge+1:34*DofsPerEdge) = DerivBSpTRight*BSpRRight*BSpSInternal; % edge 10 wrt t
    EdgeModesDeriv(1,34*DofsPerEdge+1:35*DofsPerEdge) = DerivBSpTRight*BSpSRight*BSpRInternal; % edge 11 wrt t
    EdgeModesDeriv(1,35*DofsPerEdge+1:36*DofsPerEdge) = DerivBSpTRight*BSpRLeft*BSpSInternal;  % edge 12 wrt t 
    
    
   
        
        
     
end

