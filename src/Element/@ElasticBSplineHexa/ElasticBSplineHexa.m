%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Class ElasticBSplineHexa
% A 3D-hexahedral element for pFEM and FCM is defined
 
classdef ElasticBSplineHexa < AbsElement
    
    methods(Access = public)
        %% constructor
        function obj = ElasticBSplineHexa(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,U,DofDimension,Domain)
            SpaceDimension = 3;
            numberOfDofsPerDirection=length(U)-PolynomialDegree-1;

           obj = obj@AbsElement(Nodes,Edges,Faces,Solid,Material,Integrator,...
                PolynomialDegree,numberOfDofsPerDirection,DofDimension,SpaceDimension,Domain);
            obj.geometricSupport = Solid(1).getSolid();
            obj.knotVector=U;
        end
        
        %%
        N = evalShapeFunct(obj,Coord);
        dN = evalDerivOfShapeFunct(obj,Coord);
        B = getB(obj,Coord);
        
        LocalCoord = mapGlobalToLocal(obj,GlobalCoord);
        GlobalCoord = mapLocalToGlobal(obj,LocalCoord);
        
        %% calculate the jacobian of the element
        function J = calcJacobian(obj,LocalCoord)
            J = obj.solid.calcJacobian(LocalCoord);
        end
        
        %% calculate det jacobian
        function detJ = calcDetJacobian(obj,LocalCoords)
            detJ = obj.solid.calcDetJacobian(LocalCoords);
        end
        
        
        function h=getMeshSizeParameter(obj,SpaceDimension)       
            h=obj.getCharacteristicMeshSize(SpaceDimension);     
        end
        
    end
    
    methods (Access = protected)
        
        function BSp = evalBSpline(obj,pmax,u)
             BSp=getBSpline(u,obj.knotVector,pmax);
        end
        
        function dBSp = evalDerivativeBSpline(obj,pmax,u)
             dBSp=getBSplineDerivative(u,obj.knotVector,pmax);
        end
        
    
    NodalModes = evalNodalModes(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight);
    EdgeModes = evalEdgeModes(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,BSpRInternal,BSpSInternal,BSpTInternal); 
    FaceModes = evalFaceModes(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,BSpRInternal,BSpSInternal,BSpTInternal); 
    VolumeModes = evalVolumeModes(obj,BSpRInternal,BSpSInternal,BSpTInternal);
        
    NodalModesDeriv = evalNodalModesDeriv(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight);
    EdgeModesDeriv = evalEdgeModesDeriv(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    FaceModesDeriv = evalFaceModesDeriv(obj,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    VolumeModesDeriv = evalVolumeModesDeriv(obj,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    end
    
    properties (Access = private)
        knotVector
    end
    
end

