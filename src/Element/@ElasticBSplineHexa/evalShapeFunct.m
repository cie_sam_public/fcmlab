%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% evaluate the shape function
% This function evaluates a specified shape function at a local coordinate.

function N = evalShapeFunct(obj,Coord)

    DofsLocalDirection = obj.numberOfDofsPerDirection;
    

    BSpR = zeros(1,DofsLocalDirection);
    BSpS = zeros(1,DofsLocalDirection);
    BSpT = zeros(1,DofsLocalDirection);

    BSpR(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(1));
    BSpS(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(2));
    BSpT(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(3));

   % shape functions on nodes
    BSpRLeft=BSpR(1,1);
    BSpRRight=BSpR(1,DofsLocalDirection);
    BSpSLeft=BSpS(1,1);
    BSpSRight=BSpS(1,DofsLocalDirection);
    BSpTLeft=BSpT(1,1);
    BSpTRight=BSpT(1,DofsLocalDirection);
    
       % internal shape functions 
    BSpRInternal=BSpR(1,2:DofsLocalDirection-1);
    BSpSInternal=BSpS(1,2:DofsLocalDirection-1);
    BSpTInternal=BSpT(1,2:DofsLocalDirection-1);


    
    NodalModes = obj.evalNodalModes(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight);

    EdgeModes = obj.evalEdgeModes(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,BSpRInternal,BSpSInternal,BSpTInternal);
    
    FaceModes = obj.evalFaceModes(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,BSpRInternal,BSpSInternal,BSpTInternal);
    
    VolumeModes = obj.evalVolumeModes(BSpRInternal,BSpSInternal,BSpTInternal);
    
    N = [NodalModes EdgeModes FaceModes VolumeModes];

end