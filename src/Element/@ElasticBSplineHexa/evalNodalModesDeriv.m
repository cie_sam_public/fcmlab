%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% evaluate nodal modes derivatives in direction r, s and t
%  This function evaluates the nodal modes derivatives with respect to 
%  direction r, s and t at local coordinate.

function NodalModesDeriv = evalNodalModesDeriv(~,BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight)
    
    dN1dr = DerivBSpRLeft*BSpSLeft*BSpTLeft;
    dN2dr = DerivBSpRRight*BSpSLeft*BSpTLeft;
    dN3dr = DerivBSpRRight*BSpSRight*BSpTLeft;
    dN4dr = DerivBSpRLeft*BSpSRight*BSpTLeft;
    dN5dr = DerivBSpRLeft*BSpSLeft*BSpTRight;
    dN6dr = DerivBSpRRight*BSpSLeft*BSpTRight;
    dN7dr = DerivBSpRRight*BSpSRight*BSpTRight;
    dN8dr = DerivBSpRLeft*BSpSRight*BSpTRight;
    
    dN1ds = DerivBSpSLeft*BSpRLeft*BSpTLeft;
    dN2ds = DerivBSpSLeft*BSpRRight*BSpTLeft;
    dN3ds = DerivBSpSRight*BSpRRight*BSpTLeft;
    dN4ds = DerivBSpSRight*BSpRLeft*BSpTLeft;
    dN5ds = DerivBSpSLeft*BSpRLeft*BSpTRight;
    dN6ds = DerivBSpSLeft*BSpRRight*BSpTRight;
    dN7ds = DerivBSpSRight*BSpRRight*BSpTRight;
    dN8ds = DerivBSpSRight*BSpRLeft*BSpTRight;
    
    dN1dt = DerivBSpTLeft*BSpRLeft*BSpSLeft;
    dN2dt = DerivBSpTLeft*BSpRRight*BSpSLeft;
    dN3dt = DerivBSpTLeft*BSpRRight*BSpSRight;
    dN4dt = DerivBSpTLeft*BSpRLeft*BSpSRight;
    dN5dt = DerivBSpTRight*BSpRLeft*BSpSLeft;
    dN6dt = DerivBSpTRight*BSpRRight*BSpSLeft;
    dN7dt = DerivBSpTRight*BSpRRight*BSpSRight;
    dN8dt = DerivBSpTRight*BSpRLeft*BSpSRight;
    
    
    
    
    
    
    NodalModesDeriv = [dN1dr, dN2dr, dN3dr, dN4dr, ...
                       dN5dr, dN6dr, dN7dr, dN8dr, ...
                       dN1ds, dN2ds, dN3ds, dN4ds, ...
                       dN5ds, dN6ds, dN7ds, dN8ds, ...
                       dN1dt, dN2dt, dN3dt, dN4dt, ...
                       dN5dt, dN6dt, dN7dt, dN8dt];
    
end