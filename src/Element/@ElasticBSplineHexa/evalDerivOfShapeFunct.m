%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% evaluate the derivative of a shape function in direction r, s and t
% This function evaluates the derivative with respect to direction r, s and
% t of a specified shape function at a local coordinate.

function dN = evalDerivOfShapeFunct(obj,Coord)

    DofsLocalDirection = obj.numberOfDofsPerDirection;
    InternalDofsLocalDirection=obj.numberOfDofsPerDirection-2;
    
    BSpR = zeros(1,DofsLocalDirection);
    BSpS = zeros(1,DofsLocalDirection);
    BSpT = zeros(1,DofsLocalDirection);

    
    DerivBSpR = zeros(1,DofsLocalDirection);
    DerivBSpS = zeros(1,DofsLocalDirection);
    DerivBSpT = zeros(1,DofsLocalDirection);

    
    
    DerivBSpR(1,:) = obj.evalDerivativeBSpline(obj.PolynomialDegree,Coord(1));
    BSpR(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(1));
    DerivBSpS(1,:) = obj.evalDerivativeBSpline(obj.PolynomialDegree,Coord(2));
    BSpS(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(2));
    DerivBSpT(1,:) = obj.evalDerivativeBSpline(obj.PolynomialDegree,Coord(3));
    BSpT(1,:) = obj.evalBSpline(obj.PolynomialDegree,Coord(3));
    
  % shape functions on nodes
    BSpRLeft=BSpR(1,1);
    BSpRRight=BSpR(1,DofsLocalDirection);
    BSpSLeft=BSpS(1,1);
    BSpSRight=BSpS(1,DofsLocalDirection);
    BSpTLeft=BSpT(1,1);
    BSpTRight=BSpT(1,DofsLocalDirection);
    
    DerivBSpRLeft=DerivBSpR(1,1);
    DerivBSpRRight=DerivBSpR(1,DofsLocalDirection);
    DerivBSpSLeft=DerivBSpS(1,1);
    DerivBSpSRight=DerivBSpS(1,DofsLocalDirection);
    DerivBSpTLeft=DerivBSpT(1,1);
    DerivBSpTRight=DerivBSpT(1,DofsLocalDirection);
    
       % internal shape functions 
    BSpRInternal=BSpR(1,2:DofsLocalDirection-1);
    BSpSInternal=BSpS(1,2:DofsLocalDirection-1);
    BSpTInternal=BSpT(1,2:DofsLocalDirection-1);

    DerivBSpRInternal=DerivBSpR(1,2:DofsLocalDirection-1);
    DerivBSpSInternal=DerivBSpS(1,2:DofsLocalDirection-1);
    DerivBSpTInternal=DerivBSpT(1,2:DofsLocalDirection-1);

    NodalModesDeriv = obj.evalNodalModesDeriv(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight);
    EdgeModesDeriv = obj.evalEdgeModesDeriv(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    FaceModesDeriv = obj.evalFaceModesDeriv(BSpRLeft,BSpRRight,BSpSLeft,BSpSRight,BSpTLeft,BSpTRight,DerivBSpRLeft,DerivBSpRRight,DerivBSpSLeft,DerivBSpSRight,DerivBSpTLeft,DerivBSpTRight,BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    VolumeModesDeriv = obj.evalVolumeModesDeriv(BSpRInternal,BSpSInternal,BSpTInternal,DerivBSpRInternal,DerivBSpSInternal,DerivBSpTInternal);
    
    dN = [NodalModesDeriv(1,1:8) EdgeModesDeriv(1,1:12*InternalDofsLocalDirection) FaceModesDeriv(1,1:6*InternalDofsLocalDirection^2) VolumeModesDeriv(1,1:InternalDofsLocalDirection^3) ...
        NodalModesDeriv(1,9:16) EdgeModesDeriv(1,12*InternalDofsLocalDirection+1:24*InternalDofsLocalDirection) FaceModesDeriv(1,6*InternalDofsLocalDirection^2+1:12*InternalDofsLocalDirection^2) VolumeModesDeriv(1,InternalDofsLocalDirection^3+1:2*InternalDofsLocalDirection^3) ...
        NodalModesDeriv(1,17:24) EdgeModesDeriv(1,24*InternalDofsLocalDirection+1:36*InternalDofsLocalDirection) FaceModesDeriv(1,12*InternalDofsLocalDirection^2+1:18*InternalDofsLocalDirection^2) VolumeModesDeriv(1,2*InternalDofsLocalDirection^3+1:3*InternalDofsLocalDirection^3)];
    
end


