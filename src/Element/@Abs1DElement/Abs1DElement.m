%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
  
% Class Abs2DElement

classdef Abs1DElement < AbsElement
    
    methods(Access = public)
        %% constructor
        function obj = Abs1DElement(Nodes, Edges, Material, Integrator, PolynomialDegree, NumberOfDofsPerDirection, DofDimension, Domain)
            SpaceDimension = 1;

            obj = obj@AbsElement(Nodes,Edges,[],[],Material,Integrator,...
                PolynomialDegree,NumberOfDofsPerDirection,DofDimension,SpaceDimension,Domain);
            obj.geometricSupport = Edges(1).getLine();
        end
                    
        %%
        B = getB(obj,Coord);
        GradQ = getGradQ(obj,Coord);
        LocalCoord = mapGlobalToLocal(obj,GlobalCoord);
        GlobalCoord = mapLocalToGlobal(obj,LocalCoord);
        
        %% calculate the Jacobian
        function J = calcJacobian(obj,Coord)
            J = obj.edges.calcJacobian(Coord);
        end
        
        %% calculate the determinant of the Jacobian
        function detJ = calcDetJacobian(obj,Coord)
            detJ = obj.edges.calcDetJacobian(Coord);
        end
        
        function h=getMeshSizeParameter(obj,SpaceDimension)
            h=obj.getCharacteristicMeshSize(SpaceDimension);    
        end
        
    end

    methods(Abstract, Access = public)
        %%
        N = evalShapeFunct(obj,Coord);
        dN = evalDerivOfShapeFunct(obj,Coord);        
    end
    
end

