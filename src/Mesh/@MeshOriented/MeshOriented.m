%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%


classdef MeshOriented < Mesh
    %Mesh for oriented systems (not aligned to major axis)
    

    
    methods(Access = public)
        function obj = MeshOriented(MeshFactory)
            obj = obj@Mesh(MeshFactory);
            obj.TransformMatrix = MeshFactory.getTransformMatrix;
            obj.CenterOfCoordinates = MeshFactory.getCenterOfCoordinates;
        end
        
        function Node = findNode(obj,Coords)
            if ~obj.checkPointIsNode(Coords)
                Logger.ThrowException('(Error in node load definition)');
            end
            
            [i,j,k] = obj.convertCoordinatesIntoIndices(Coords,@round);
         
            % Node index
            NodeId = (i+1) + j*(obj.NumberOfXDivisions+1) ...
                + k*(obj.NumberOfXDivisions+1)*(obj.NumberOfYDivisions+1);
            
            % Return node
            Node = obj.Nodes(NodeId);
        end
        function IsNode = checkPointIsNode(obj,Coords)
            
            IsNode = true;
            
            Coords = inv(obj.TransformMatrix)*(Coords-obj.CenterOfCoordinates)';

            Coords = Coords - obj.MeshOrigin;
            
            if ( Coords(1) >obj.Lx || ...
                    Coords(2) >obj.Ly || ...
                    Coords(3) >obj.Lz )
                IsNode = false;
            end
            
            % Negative values in the input ?
            if ( Coords(1) <0 || ...
                    Coords(2) <0 || ...
                    Coords(3) <0 )
                IsNode = false;
            end
            
            % Is a real node?
            i = mod(Coords(1),(obj.Lx/obj.NumberOfXDivisions));
            j = mod(Coords(2),(obj.Ly/obj.NumberOfYDivisions));
            k = mod(Coords(3),(obj.Lz/obj.NumberOfZDivisions));
            
            tolerance = 1e-8;
            if ( ( i > tolerance && (obj.Lx/obj.NumberOfXDivisions) - i > tolerance ) || ...
                    ( j > tolerance && (obj.Ly/obj.NumberOfYDivisions) - j > tolerance ) || ...
                    ( k > tolerance && (obj.Lz/obj.NumberOfZDivisions) - k > tolerance ) )
                IsNode = false;
            end
        end
        function [i,j,k] = convertCoordinatesIntoIndices(obj,Coords,roundingFunctionHandle)

            Coords = inv(obj.TransformMatrix)*(Coords-obj.CenterOfCoordinates)';

            Coords = Coords' - obj.MeshOrigin;

%         disp(['After: ', num2str(Coords)])

            i = roundingFunctionHandle(Coords(1)*(obj.NumberOfXDivisions/obj.Lx));
            
            if i > obj.NumberOfXDivisions
                i = obj.NumberOfXDivisions;
            end
            
            j = roundingFunctionHandle(Coords(2)*(obj.NumberOfYDivisions/obj.Ly));
            
            if j > obj.NumberOfYDivisions
                j = obj.NumberOfYDivisions;
            end
            
            k = roundingFunctionHandle(Coords(3)*(obj.NumberOfZDivisions/obj.Lz));
            
            if k > obj.NumberOfZDivisions
                k = obj.NumberOfZDivisions;
            end
        end
    end

    
    properties %(Access = protected)
        TransformMatrix
        CenterOfCoordinates
    end
end

