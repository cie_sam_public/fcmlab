%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Mesh factory for a 2D Uniform case
%   This class creates a uniform interface mesh for a 2D element, it is derived from
%   the MeshFactory. It also creates and reduces the internal mesh,
%   calculates its fixed interface modes. Further it creates the interface
%   Element Factory.

classdef MeshFactoryAdaptiveIGA2D < AbsMeshFactory
    
    methods (Access = public)
        %% constructor

        function obj = MeshFactoryAdaptiveIGA2D( MultiLevelGeometry, DofDimension, ElementFactory, GeometryFactory,...
                MeshOrigin, Lx, Ly )
            obj = obj@AbsMeshFactory(MultiLevelGeometry.getGeometries{1}.getPolynomialDegreeXi,DofDimension,...
                [],ElementFactory);
            obj.MultiLevelGeometry = MultiLevelGeometry;
            obj.MultiLevelGeometries = MultiLevelGeometry.getGeometries;
            obj.DofDimension = DofDimension;
            obj.ElementFactory = ElementFactory;
            obj.GeometryFactory = GeometryFactory;
            
            if nargin < 7
                MeshOrigin = [0 0];
                Lx = 1;
                Ly = 1;
            end
            obj.MeshOrigin = MeshOrigin;
            obj.Lx = Lx;
            obj.Ly = Ly;
                
            %obj.numberOfDofsPerDirection=ElementFactory.getNumberOfDofsPerDirection( obj.MultiLevelGeometries{1}.getPolynomialDegreeXi() );
            
            for iLevel = 1:length( obj.MultiLevelGeometries)
                KnotsXi{iLevel} = obj.MultiLevelGeometries{iLevel}.getKnotsXi;
                KnotsEta{iLevel} = obj.MultiLevelGeometries{iLevel}.getKnotsEta;
            end
            
            PolynomialDegree = obj.MultiLevelGeometries{1}.getPolynomialDegreeXi;
            locationMapForTensorProductOrder = THBLocationMap( obj.MultiLevelGeometry, obj.DofDimension );
            
            obj.locationMap = locationMapForTensorProductOrder;
        end
        
       
        
        
        %% create Mesh
        function mesh = createMesh(obj)
            mesh = MeshIGA(obj);
        end
        
        function geometry = getGeometry(obj)
            geometry = obj.MultiLevelGeometry;
        end
        
        function DofDimension = getDofDimension(obj)
            DofDimension = obj.DofDimension;
        end
        
        function Edges = getEdges(obj)
            Edges = obj.Edges;
        end
        
        %% To be implemented to inherit from AbsMesh
        function Nodes = createNodes(obj)
            Nodes = [];
        end
        
        function Edges = createEdges(obj)
            Edges = obj.getEdges();
        end
        
        function Faces = createFaces(obj)
            Faces = [];
        end
        
        function Solids = createSolids(obj)
            Solids = [];
        end
        
        function ExtractedAssemblyAlgorithm = getExtractedAssemblyAlgorithm(obj)
            ExtractedAssemblyAlgorithm = @(a,b)a;
        end
      

                
        function Elements = createElements(obj)
            Logger.Log('Creating elements......','release');
            
            obj.Edges = [];

            [Aa, Am, Ap, Ea] = obj.MultiLevelGeometry.getRefinementDataStructures;
            
            activeElementIndex=1;
            for iLevel = 1:length( obj.MultiLevelGeometries);
                for iEta = 1:obj.MultiLevelGeometries{iLevel}.numberOfKnotSpansInEta
                    for iXi = 1:obj.MultiLevelGeometries{iLevel}.numberOfKnotSpansInXi
                        
                        if Ea{iLevel}(iXi,iEta) == 1
                        
                            iSpan = (iEta-1)*obj.MultiLevelGeometries{iLevel}.numberOfKnotSpansInXi+iXi;

                            geometricFace = obj.GeometryFactory.createArea( iSpan, iLevel);
                            
                            topologicEdges =  obj.getEdgesOfFace( geometricFace );
                            %obj.Edges = [obj.Edges topologicEdges ];
                            obj.Edges{(activeElementIndex-1)*4 +1} = topologicEdges(1);
                            obj.Edges{(activeElementIndex-1)*4 +2} = topologicEdges(2);
                            obj.Edges{(activeElementIndex-1)*4 +3} = topologicEdges(3);
                            obj.Edges{(activeElementIndex-1)*4 +4} = topologicEdges(4);

                            topologicFace = Face([], topologicEdges, obj.numberOfDofsPerDirection, obj.DofDimension, geometricFace );

                            Elements(activeElementIndex) = obj.ElementFactory.createElement([], topologicEdges, topologicFace, [], obj.MultiLevelGeometries{iLevel}.getPolynomialDegreeXi, obj.DofDimension, iSpan, iLevel);

                            elementLM = obj.locationMap(activeElementIndex,1:obj.DofDimension*iLevel*obj.numberOfDofsPerDirection^2);                            
                            Elements(activeElementIndex).setLocationMatrix( elementLM )

                            
                            
                            fprintf('%d %d [%d]:',iLevel-1, iXi-1+ obj.MultiLevelGeometries{iLevel}.numberOfKnotSpansInXi*( iEta-1), length(Elements(activeElementIndex).getLocationMatrix))
                            for a =  Elements(activeElementIndex).getLocationMatrix - 1
                                fprintf('%d ',a)
                            end
                            fprintf('\n');
                            
                            %[iLevel iXi iEta]
                            %Elements(activeElementIndex).getLocationMatrix - 1
                            
                            if iLevel==2 && iEta==1 && iXi==2
                                eta=-1:0.1:1; a=1; 
                                clear r;
                                for ieta=eta
                                    r(a,:)=Elements(activeElementIndex).evalShapeFunct([ieta -1 0]);
                                    a=a+1; 
                                end
                                figure(5)
                                for ieta=1:size(r,2)
                                    plot(eta, r(:,ieta)); axis([-1 1 0 1])
                                end
                            end
                            
                            
                            
                            
                        
                            activeElementIndex=activeElementIndex+1;
                        end
                    end
                end
            end
        end
    end
    
    methods(Access = private)
        
        function topologicEdges =  getEdgesOfFace(obj, geometricFace )
            geometricEdges = geometricFace.getLines();
            for iGeometricEdge = 1:length(geometricEdges)
               topologicEdges(iGeometricEdge) = Edge([], obj.numberOfDofsPerDirection, obj.DofDimension, geometricEdges(iGeometricEdge));
            end          
        end
        
    end
    
    properties (Access = private)
        
        MultiLevelGeometry
        MultiLevelGeometries

        GeometryFactory
        
        locationMap
               
        Edges
    end
    
    properties (Dependent)
        ControlPoints

        PolynomialDegreeXi
        KnotsXi
        PolynomialDegreeEta
        KnotsEta

        numberOfControlPointsInXi
        numberOfControlPointsInEta
        numberOfKnotSpansInXi
        numberOfKnotSpansInEta
    end
    
end

