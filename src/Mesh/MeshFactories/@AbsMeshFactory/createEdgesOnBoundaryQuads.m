%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% creates nodes on a boundary line
function EdgesOnBoundaryQuads = createEdgesOnBoundaryQuads(obj,Nodes)


Coordinate = zeros(4*length(obj.Boundary),3);

%% get the face of the first quad
Area = obj.Boundary(1);
    %% get the edges of the first quad
    Line = Area.getLines();
        % get nodes of the first edge
            Vertex = Line(1).getVertices();
            %1st node
                X1 = Vertex(1).getX();
                Y1 = Vertex(1).getY();
                Z1 = Vertex(1).getZ();

                Coordinate(1,:) = [X1, Y1, Z1];

                NodesOnBoundaryQuads(1)= Nodes(1);
             %2nd node
                X2 = Vertex(2).getX();
                Y2 = Vertex(2).getY();
                Z2 = Vertex(2).getZ();

                Coordinate(2,:) = [X2, Y2, Z2];

                NodesOnBoundaryQuads(2)= Nodes(2);
        %get 1st edge
         nodePair = NodesOnBoundaryQuads(1:2);
         EdgesOnBoundaryQuads(1) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
        % get nodes of the second edge
            Vertex = Line(2).getVertices();
            %3rd node
                X3 = Vertex(2).getX();
                Y3 = Vertex(2).getY();
                Z3 = Vertex(2).getZ();

                Coordinate(3,:) = [X3, Y3, Z3];

                NodesOnBoundaryQuads(3)= Nodes(3);
            % get nodes of the third edge
            Vertex = Line(3).getVertices();
            %3rd node
                X4 = Vertex(2).getX();
                Y4 = Vertex(2).getY();
                Z4 = Vertex(2).getZ();

                Coordinate(4,:) = [X4, Y4, Z4];

                NodesOnBoundaryQuads(4)= Nodes(4);
          %get 2nd edge
          nodePair = NodesOnBoundaryQuads(2:3);
          EdgesOnBoundaryQuads(2) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
          
          %get 3rd edge
          EdgesOnBoundaryQuads(3) = Edge([Nodes(3) Nodes(4)],obj.numberOfDofsPerDirection,obj.DofDimension);
          %get 4th edge
          EdgesOnBoundaryQuads(4) = Edge([Nodes(4) Nodes(1)],obj.numberOfDofsPerDirection,obj.DofDimension);
  

%% get the vector of nodes along the Boundary
NodesID = 5;
countNodes = 5;
countEdges = 5;
countCoord = 5;
countRep = 0;
 for i=2:length(obj.Boundary)
        Area = obj.Boundary(i);
        Line = Area.getLines();
        % get nodes of the first edge
        Vertex = Line(1).getVertices();
            % get first node
            X1 = Vertex(1).getX();
            Y1 = Vertex(1).getY();
            Z1 = Vertex(1).getZ();

            Coordinate(countCoord,:) = [X1, Y1, Z1];
            
            testerNode1 = 1.0;
            RepeatedNodes = 0;
            for j=1:length(NodesOnBoundaryQuads)
                
                if (Coordinate(countCoord,1)~= Coordinate(j,1) || Coordinate(countCoord,2)~= Coordinate(j,2) || Coordinate(countCoord,3)~= Coordinate(j,3))
                    testerNode1= testerNode1+1;
                    if (testerNode1 >= 2 )
                        if ( i > obj.NumberOfSegments*obj.NumberOfSegments-obj.NumberOfSegments+2)
                            for k=1:testerNode1-1
                                if(Coordinate(testerNode1,1)== Coordinate(k,1) && Coordinate(testerNode1,2)== Coordinate(k,2) && Coordinate(testerNode1,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        else
                            for k=1:testerNode1-2
                                if(Coordinate(testerNode1,1)== Coordinate(k,1) && Coordinate(testerNode1,2)== Coordinate(k,2) && Coordinate(testerNode1,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        end
                    end
                elseif(Coordinate(countCoord,1)== Coordinate(j,1) && Coordinate(countCoord,2)== Coordinate(j,2) && Coordinate(countCoord,3)== Coordinate(j,3))                                        

                    NodesOnBoundaryQuads(NodesID) = Nodes(testerNode1-(RepeatedNodes));

                    NodesID = NodesID+1;
                    RepeatedNodes = RepeatedNodes+1;
                    countNodes = countNodes+1;
                    countRep = countRep+1;
                    break;
                end
            end   
 
            if (testerNode1 == length(NodesOnBoundaryQuads)+1)
                NodesOnBoundaryQuads(NodesID) = Nodes(countNodes-countRep);
                NodesID = NodesID+1;
                countNodes = countNodes+1;
            end
            
            countCoord = countCoord+1;
            
             % get second node
            X2 = Vertex(2).getX();
            Y2 = Vertex(2).getY();
            Z2 = Vertex(2).getZ();

            Coordinate(countCoord,:) = [X2, Y2, Z2];
            
           testerNode2 = 1.0;
           RepeatedNodes = 0;
            for j=1:length(NodesOnBoundaryQuads) 
                if (Coordinate(countCoord,1)~= Coordinate(j,1) || Coordinate(countCoord,2)~= Coordinate(j,2) || Coordinate(countCoord,3)~= Coordinate(j,3))
                    testerNode2= testerNode2+1;
                    if (testerNode2 >= 2 )
                        if ( i > obj.NumberOfSegments*obj.NumberOfSegments-obj.NumberOfSegments+2)
                            for k=1:testerNode2-1
                                if(Coordinate(testerNode2,1)== Coordinate(k,1) && Coordinate(testerNode2,2)== Coordinate(k,2) && Coordinate(testerNode2,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        else
                            for k=1:testerNode2-2
                                if(Coordinate(testerNode2,1)== Coordinate(k,1) && Coordinate(testerNode2,2)== Coordinate(k,2) && Coordinate(testerNode2,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        end
                    end
                elseif(Coordinate(countCoord,1)== Coordinate(j,1) && Coordinate(countCoord,2)== Coordinate(j,2) && Coordinate(countCoord,3)== Coordinate(j,3))                                        

                    NodesOnBoundaryQuads(NodesID) = Nodes(testerNode2-(RepeatedNodes));

                    NodesID = NodesID+1;
                    RepeatedNodes = RepeatedNodes+1;
                    countNodes = countNodes+1;
                    countRep = countRep+1;
                    break;
                end
            end   
 
            if (testerNode2 == length(NodesOnBoundaryQuads)+1)
                NodesOnBoundaryQuads(NodesID) = Nodes(countNodes-countRep);
                NodesID = NodesID+1;
                countNodes = countNodes+1;
            end
            
            countCoord = countCoord+1;
           
            
            if (testerNode2 == length(NodesOnBoundaryQuads) ...
                    || testerNode1 == length(NodesOnBoundaryQuads)-1)
                nodePair = NodesOnBoundaryQuads(countNodes-2:countNodes-1);
                EdgesOnBoundaryQuads(countEdges) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
                countEdges = countEdges +1;
            end
            
            

        % get nodes of the second edge
        Vertex = Line(2).getVertices();

            
             % get Third node
            X3 = Vertex(2).getX();
            Y3 = Vertex(2).getY();
            Z3 = Vertex(2).getZ();

            Coordinate(countCoord,:) = [X3, Y3, Z3];
            
            testerNode3 = 1.0;
            RepeatedNodes = 0;
            for j=1:length(NodesOnBoundaryQuads)
                 
                if (Coordinate(countCoord,1)~= Coordinate(j,1) || Coordinate(countCoord,2)~= Coordinate(j,2) || Coordinate(countCoord,3)~= Coordinate(j,3))
                    testerNode3= testerNode3+1;
                    if (testerNode3 >= 2 )
                        if ( i > obj.NumberOfSegments*obj.NumberOfSegments-obj.NumberOfSegments+2)
                            for k=1:testerNode3-2
                                if(Coordinate(testerNode3,1)== Coordinate(k,1) && Coordinate(testerNode3,2)== Coordinate(k,2) && Coordinate(testerNode3,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        else
                            for k=1:testerNode3-2
                                if(Coordinate(testerNode3,1)== Coordinate(k,1) && Coordinate(testerNode3,2)== Coordinate(k,2) && Coordinate(testerNode3,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        end
                    end
                elseif(Coordinate(countCoord,1)== Coordinate(j,1) && Coordinate(countCoord,2)== Coordinate(j,2) && Coordinate(countCoord,3)== Coordinate(j,3))                                        

                    NodesOnBoundaryQuads(NodesID) = Nodes(testerNode3-(RepeatedNodes));

                    NodesID = NodesID+1;
                    RepeatedNodes = RepeatedNodes+1;
                    countNodes = countNodes+1;
                    countRep = countRep+1;
                    break;
                end
            end   
 
            if (testerNode3 == length(NodesOnBoundaryQuads)+1)
                NodesOnBoundaryQuads(NodesID) = Nodes(countNodes-countRep);
                NodesID = NodesID+1;
                countNodes = countNodes+1;
            end
            
            countCoord = countCoord+1;
            
            if (testerNode3 == length(NodesOnBoundaryQuads) ...
                    || testerNode2 == length(NodesOnBoundaryQuads)-1)
                nodePair = NodesOnBoundaryQuads(countNodes-2:countNodes-1);
                EdgesOnBoundaryQuads(countEdges) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
                countEdges = countEdges +1;
            end
            
            


        % get nodes of the third edge
        Vertex = Line(3).getVertices();
            % get first node
            X4 = Vertex(2).getX();
            Y4 = Vertex(2).getY();
            Z4 = Vertex(2).getZ();

            Coordinate(countCoord,:) = [X4, Y4, Z4];

            testerNode4 = 1.0;
            RepeatedNodes = 0;
            for j=1:length(NodesOnBoundaryQuads)
                
                if (Coordinate(countCoord,1)~= Coordinate(j,1) || Coordinate(countCoord,2)~= Coordinate(j,2) || Coordinate(countCoord,3)~= Coordinate(j,3))
                    testerNode4= testerNode4+1;
                    if (testerNode4 >= 2 )
                        if ( i > obj.NumberOfSegments*obj.NumberOfSegments-obj.NumberOfSegments+2)
                            for k=1:testerNode4-1
                                if(Coordinate(testerNode4,1)== Coordinate(k,1) && Coordinate(testerNode4,2)== Coordinate(k,2) && Coordinate(testerNode4,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        else
                            for k=1:testerNode4-2
                                if(Coordinate(testerNode4,1)== Coordinate(k,1) && Coordinate(testerNode4,2)== Coordinate(k,2) && Coordinate(testerNode4,3)== Coordinate(k,3))
                                    RepeatedNodes = RepeatedNodes+1;
                                    break;
                                end
                            end
                        end
                    end
                elseif(Coordinate(countCoord,1)== Coordinate(j,1) && Coordinate(countCoord,2)== Coordinate(j,2) && Coordinate(countCoord,3)== Coordinate(j,3))                                        

                    NodesOnBoundaryQuads(NodesID) = Nodes(testerNode4-(RepeatedNodes));

                    NodesID = NodesID+1;
                    RepeatedNodes = RepeatedNodes+1;
                    countNodes = countNodes+1;
                    countRep = countRep+1;
                    break;
                end
            end   
 
            if (testerNode4 == length(NodesOnBoundaryQuads)+1)
                NodesOnBoundaryQuads(NodesID) = Nodes(countNodes-countRep);
                NodesID = NodesID+1;
                countNodes = countNodes+1;
            end
            
            if (testerNode4 == length(NodesOnBoundaryQuads)...
                    || testerNode3 == length(NodesOnBoundaryQuads)-1)
                nodePair = NodesOnBoundaryQuads(countNodes-2:countNodes-1);
                EdgesOnBoundaryQuads(countEdges) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
                countEdges = countEdges +1;
            end
            
            if (testerNode4 == length(NodesOnBoundaryQuads) ...
                    || testerNode1 == length(NodesOnBoundaryQuads)-3)
                nodePair = [NodesOnBoundaryQuads(countNodes-4) ...
                    NodesOnBoundaryQuads(countNodes-1)];
                EdgesOnBoundaryQuads(countEdges) = Edge(nodePair,obj.numberOfDofsPerDirection,obj.DofDimension);
                countEdges = countEdges +1;
            end

            for j=1:length(EdgesOnBoundaryQuads)
                EdgeDebug = EdgesOnBoundaryQuads(j).getNodes();
                V1(j)=EdgeDebug(1).getVertex;
                V2(j)=EdgeDebug(2).getVertex;
            end

            countCoord = countCoord+1;

end
