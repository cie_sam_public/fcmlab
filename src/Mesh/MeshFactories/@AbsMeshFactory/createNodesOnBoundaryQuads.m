%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% creates nodes on a boundary line
function NodesOnBoundaryQuads = createNodesOnBoundaryQuads(obj)


NodesOnBoundaryQuads=[];

%% get the nodes of the first quad
Vertex = obj.Boundary(1).getVertices();
% get first node
X1 = Vertex(1).getX();
Y1 = Vertex(1).getY();
Z1 = Vertex(1).getZ();

Coordinate(1,:) = [X1, Y1, Z1];

NodesOnBoundaryQuads= [NodesOnBoundaryQuads Node(Coordinate(1,:),obj.DofDimension)];
% get second node
X2 = Vertex(2).getX();
Y2 = Vertex(2).getY();
Z2 = Vertex(2).getZ();

Coordinate(2,:) = [X2, Y2, Z2];

NodesOnBoundaryQuads= [NodesOnBoundaryQuads...
    Node(Coordinate(2,:),obj.DofDimension)];
% get third node
X3 = Vertex(3).getX();
Y3 = Vertex(3).getY();
Z3 = Vertex(3).getZ();

Coordinate(3,:) = [X3, Y3, Z3];

% get fourth node
X4 = Vertex(4).getX();
Y4 = Vertex(4).getY();
Z4 = Vertex(4).getZ();

Coordinate(4,:) = [X4, Y4, Z4];

NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
    Node(Coordinate(3,:),obj.DofDimension)];
NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
    Node(Coordinate(4,:),obj.DofDimension)];
%% get the vector of nodes along the Boundary
countNodes = 5;
CountCoord = 5;
count = 0;
   for i=2:length(obj.Boundary)
        Vertex = obj.Boundary(i).getVertices();
        % get first node
        X1 = Vertex(1).getX();
        Y1 = Vertex(1).getY();
        Z1 = Vertex(1).getZ();
        
        Coordinate(CountCoord,:) = [X1, Y1, Z1];
        testerNode1 = 1.0;
            for j=1:length(Coordinate)
                if (Coordinate(CountCoord,1)~= Coordinate(j,1) || Coordinate(CountCoord,2)~=...
                        Coordinate(j,2) || Coordinate(CountCoord,3)~= Coordinate(j,3))
                    testerNode1 = testerNode1+1;
                elseif (Coordinate(CountCoord,1)== Coordinate(j,1) && Coordinate(CountCoord,2)==...
                        Coordinate(j,2) && Coordinate(CountCoord,3)== Coordinate(j,3))
                    break;
                end
            end   
            if(testerNode1 == length(Coordinate))
                NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
                    Node(Coordinate(CountCoord-count,:),obj.DofDimension)];
                    countNodes = countNodes+1;
            end
            CountCoord = CountCoord+1;
             
        % get second node
        X2 = Vertex(2).getX();
        Y2 = Vertex(2).getY();
        Z2 = Vertex(2).getZ();
        
        Coordinate(CountCoord,:) = [X2, Y2, Z2];
         testerNode2 = 1.0;
            for j=1:length(Coordinate)
                if (Coordinate(CountCoord,1)~= Coordinate(j,1) || Coordinate(CountCoord,2)~=...
                        Coordinate(j,2) || Coordinate(CountCoord,3)~= Coordinate(j,3))
                    testerNode2 = testerNode2+1;
                elseif (Coordinate(CountCoord,1)== Coordinate(j,1) && Coordinate(CountCoord,2)==...
                        Coordinate(j,2) && Coordinate(CountCoord,3)== Coordinate(j,3))
                    break;
                end
            end   
            if(testerNode2 == length(Coordinate))
                NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
                    Node(Coordinate(CountCoord-count,:),obj.DofDimension)];
                    countNodes = countNodes+1;
            end
            CountCoord = CountCoord+1;
        % get third node
        X3 = Vertex(3).getX();
        Y3 = Vertex(3).getY();
        Z3 = Vertex(3).getZ();
        
        Coordinate(CountCoord,:) = [X3, Y3, Z3];
           testerNode3 = 1.0;
            for j=1:length(Coordinate)
                if (Coordinate(CountCoord,1)~= Coordinate(j,1) || Coordinate(CountCoord,2)~=...
                        Coordinate(j,2) || Coordinate(CountCoord,3)~= Coordinate(j,3))
                    testerNode3 = testerNode3+1;
                elseif (Coordinate(CountCoord,1)== Coordinate(j,1) && Coordinate(CountCoord,2)==...
                        Coordinate(j,2) && Coordinate(CountCoord,3)== Coordinate(j,3))
                    break;
                end
            end   
            if(testerNode3 == length(Coordinate))
                NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
                    Node(Coordinate(CountCoord,:),obj.DofDimension)];
                    countNodes = countNodes+1;
            end
            CountCoord = CountCoord+1;   
        % get fourth node
        X4 = Vertex(4).getX();
        Y4 = Vertex(4).getY();
        Z4 = Vertex(4).getZ();
        
        Coordinate(CountCoord,:) = [X4, Y4, Z4];
           testerNode4 = 1.0;
            for j=1:length(Coordinate)
                if (Coordinate(CountCoord,1)~= Coordinate(j,1) || Coordinate(CountCoord,2)~=...
                        Coordinate(j,2) || Coordinate(CountCoord,3)~= Coordinate(j,3))
                    testerNode4 = testerNode4+1;
                elseif (Coordinate(CountCoord,1)== Coordinate(j,1) && Coordinate(CountCoord,2)==...
                        Coordinate(j,2) && Coordinate(CountCoord,3)== Coordinate(j,3))
                    break;
                end
            end   
            if(testerNode4 == length(Coordinate))
                NodesOnBoundaryQuads = [NodesOnBoundaryQuads...
                    Node(Coordinate(CountCoord,:),obj.DofDimension)];
                    countNodes = countNodes+1;
            end
            CountCoord = CountCoord+1;
            
            for i=1:length(NodesOnBoundaryQuads)
             VertexNode(i)=NodesOnBoundaryQuads(i).getVertex;
            end
   end
end
