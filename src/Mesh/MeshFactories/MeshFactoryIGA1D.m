%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 

classdef MeshFactoryIGA1D < AbsMeshFactory
    
    methods (Access = public)
        %% constructor

        function obj = MeshFactoryIGA1D( GeometryDescription, DofDimension, ElementFactory, GeometryFactory )
            obj = obj@AbsMeshFactory(GeometryDescription.getPolynomialDegreeXi,DofDimension,[],ElementFactory);
           
            
            obj.GeometryDescription = GeometryDescription;
            %obj.DofDimension = DofDimension;
            %obj.ElementFactory = ElementFactory;
            obj.GeometryFactory = GeometryFactory;
            
            %obj.numberOfDofsPerDirection=ElementFactory.getNumberOfDofsPerDirection( GeometryDescription.getPolynomialDegreeXi() );
            
             locationMapInBernsteinPolynomialOrder = extractedBSplineLocationMap( GeometryDescription.getKnotsXi, GeometryDescription.getPolynomialDegreeXi);
             indicesInFCMLabOrder = bernsteinPolynomialOrderToFCMLabOrder(1:size(locationMapInBernsteinPolynomialOrder,2), size(locationMapInBernsteinPolynomialOrder,2));
             obj.locationMap = locationMapInBernsteinPolynomialOrder(:, indicesInFCMLabOrder);
        end

        %% create Mesh
        function mesh = createMesh(obj)
            mesh = MeshIGA(obj);
        end
        
        function geometry = getGeometry(obj)
            geometry = obj.GeometryDescription;
        end
        
        function DofDimension = getDofDimension(obj)
            DofDimension = obj.DofDimension;
        end
        
        function Edges = getEdges(obj)
            Edges = obj.Edges;
        end
        function Nodes = getNodes(obj)
            Nodes = obj.Nodes;
        end
        
        function Nodes = createNodes(obj)
            Nodes = obj.getNodes();
        end
        
        function Edges = createEdges(obj)
            Edges = obj.getEdges();
        end
        
         function Faces = createFaces(obj)
            Faces = [];
        end
        
        function Solids = createSolids(obj)
            Solids = [];
        end
        
        function ExtractedAssemblyAlgorithm = getExtractedAssemblyAlgorithm(obj)
            ExtractedAssemblyAlgorithm = @assembleExtractedStiffnessMatrix1D;
        end
      
        function Elements = createElements(obj)
            Logger.Log('Creating elements......','release');
            
            obj.Edges = [];                    
            for iSpan = 1:obj.GeometryDescription.numberOfKnotSpansInXi
               
                geometricLine = obj.GeometryFactory.createCurve( iSpan );

                %topologicNodes =  obj.getNodesOfLine( geometricLine );
                topologicNodes =  [];
                topologicEdge = Edge(topologicNodes, obj.numberOfDofsPerDirection, obj.DofDimension, geometricLine );
                
                obj.Edges = [obj.Edges topologicEdge ];
                %obj.Nodes = [obj.Nodes topologicNodes ];

                Elements(iSpan) = obj.ElementFactory.createElement(topologicNodes, topologicEdge, [], [], obj.GeometryDescription.getPolynomialDegreeXi, obj.DofDimension, iSpan);
                Elements(iSpan).setLocationMatrix( obj.locationMap(iSpan,:) )
            end
        end
    end
    
%    methods(Access = private)
%         
%         function topologicNodes =  getNodesOfLine(obj, geometricLine )
%             geometricVertices = geometricLine.getVertices();
%             for iGeometricVertex = 1:length( geometricVertices )
%                topologicNodes( iGeometricVertex ) = Node( geometricVertices.getCoords(), obj.DofDimension );
%             end          
%         end
        
%    end
    
    properties (Access = private)
        
        GeometryDescription
        %numberOfDofsPerDirection

        %ElementFactory
        GeometryFactory
        
        locationMap
        
        %DofDimension
       
        Edges
        Nodes
    end
    
    properties (Dependent)
        ControlPoints
    end
end

