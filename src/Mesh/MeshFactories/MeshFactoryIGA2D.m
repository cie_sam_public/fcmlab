%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Mesh factory for a 2D Uniform case
%   This class creates a uniform interface mesh for a 2D element, it is derived from
%   the MeshFactory. It also creates and reduces the internal mesh,
%   calculates its fixed interface modes. Further it creates the interface
%   Element Factory.

classdef MeshFactoryIGA2D < AbsMeshFactory
    
    methods (Access = public)
        %% constructor

        function obj = MeshFactoryIGA2D( SurfaceDescription, DofDimension, ElementFactory, GeometryFactory )
            obj = obj@AbsMeshFactory(SurfaceDescription.getPolynomialDegreeXi,DofDimension,[],ElementFactory);
                       
            obj.SurfaceDescription = SurfaceDescription;
            %obj.DofDimension = DofDimension;
            %obj.ElementFactory = ElementFactory;
            obj.GeometryFactory = GeometryFactory;
            
            %obj.numberOfDofsPerDirection=ElementFactory.getNumberOfDofsPerDirection( SurfaceDescription.getPolynomialDegreeXi() );
            obj.locationMap = extractedBSplineLocationMap2D( SurfaceDescription.getKnotsXi, SurfaceDescription.getPolynomialDegreeXi, SurfaceDescription.getKnotsEta, SurfaceDescription.getPolynomialDegreeEta, DofDimension);
        end
        
       
        
        
        %% create Mesh
        function mesh = createMesh(obj)
            mesh = MeshIGA(obj);
        end
        
        function geometry = getGeometry(obj)
            geometry = obj.SurfaceDescription;
        end
        
        function DofDimension = getDofDimension(obj)
            DofDimension = obj.DofDimension;
        end
        
        function Edges = getEdges(obj)
            Edges = obj.Edges;
        end
        
        function Nodes = getNodes(obj)
            Nodes = [];
        end
        
        function Nodes = createNodes(obj)
            Nodes = getNodes();
        end
        
        function Edges = createEdges(obj)
            Edges = obj.getEdges();
        end
        
        function Faces = createFaces(obj)
            Faces = [];
        end
        
        function Solids = createSolids(obj)
            Solids = [];
        end
        
        function ExtractedAssemblyAlgorithm = getExtractedAssemblyAlgorithm(obj)
            ExtractedAssemblyAlgorithm = @(a,b)a;
        end
      

                
        function Elements = createElements(obj)
            Logger.Log('Creating elements......','release');
            
            %Elements( obj.SurfaceDescription.numberOfKnotSpansInXi * obj.SurfaceDescription.numberOfKnotSpansInEta ) = HeatConductionExtractedBSplineSurface();
            %obj.Edges = Edge(1,obj.SurfaceDescription.numberOfKnotSpansInXi * obj.SurfaceDescription.numberOfKnotSpansInEta * 4);
            %obj.Edges = [];                    
            for iEta = 1:obj.SurfaceDescription.numberOfKnotSpansInEta
                for iXi = 1:obj.SurfaceDescription.numberOfKnotSpansInXi
                    iSpan = (iEta-1)*obj.SurfaceDescription.numberOfKnotSpansInXi+iXi;

                    geometricFace = obj.GeometryFactory.createArea( iSpan);
                    
                    topologicEdges =  obj.getEdgesOfFace( geometricFace );
                    %obj.Edges = cat(2, obj.Edges, topologicEdges);
                    obj.Edges{(iSpan-1)*4 +1} = topologicEdges(1);
                    obj.Edges{(iSpan-1)*4 +2} = topologicEdges(2);
                    obj.Edges{(iSpan-1)*4 +3} = topologicEdges(3);
                    obj.Edges{(iSpan-1)*4 +4} = topologicEdges(4);
                    %obj.Edges((iSpan-1)*4 + (1:4))= topologicEdges;
                    
                    topologicFace = Face([], topologicEdges, obj.numberOfDofsPerDirection, obj.DofDimension, geometricFace );
                    
                    element = obj.ElementFactory.createElement([], topologicEdges, topologicFace, [], obj.SurfaceDescription.getPolynomialDegreeXi, obj.DofDimension, iSpan, 1);
                    Elements(iSpan) = element;
                    Elements(iSpan).setLocationMatrix( obj.locationMap(iSpan,:) )
                end
            end
        end
    end
    
    methods(Access = private)
        
        function topologicEdges =  getEdgesOfFace(obj, geometricFace )
            geometricEdges = geometricFace.getLines();
            for iGeometricEdge = 1:length(geometricEdges)
               topologicEdges(iGeometricEdge) = Edge([], obj.numberOfDofsPerDirection, obj.DofDimension, geometricEdges(iGeometricEdge));
            end          
        end
        
    end
    
    properties (Access = private)
        
        SurfaceDescription
        %numberOfDofsPerDirection

        %ElementFactory
        GeometryFactory
        
        locationMap
        
        %DofDimension
       
        Edges
    end
    
    properties (Dependent)
        ControlPoints

        PolynomialDegreeXi
        KnotsXi
        PolynomialDegreeEta
        KnotsEta

        numberOfControlPointsInXi
        numberOfControlPointsInEta
        numberOfKnotSpansInXi
        numberOfKnotSpansInEta
    end
    
end

