%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function  [fixedInterfaceModes, eigenvaluesDiagonal,fixedInterfaceStiffnessMatrix] = calculateFixedInterfaceModes(obj)

    NumberOfGaussPoints = obj.PolynomialDegree + 1;
    IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
    
    % Selet constraining strategy
    ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( obj.PenaltyValue );
    
    % Give analytical description of boundary values
    u = @(x,y,z) 0;
    
    lengthInterface = length(obj.InterfaceGeometry);
    
    for i = 1:lengthInterface
        
        % Create boundary condition in X
        ConstrainedDirections = [1 1];
        BoundaryCondition = WeakDirichletBoundaryCondition( u, ConstrainedDirections, ...
            IntegrationScheme, obj.InterfaceGeometry(i), ConstrainingAlgorithm );
        
        % Modify Linear System
        [fixedInterfaceStiffnessMatrix, ~ ] = BoundaryCondition.modifyLinearSystem(obj.InternalMesh,obj.InternalStiffnessMatrix,obj.InternalLoadVector);
    end
    
    % Solve for Eigenmodes
    
    Logger.TaskStart('Solving for Eigenmodes','release');
    
    [fixedInterfaceModes, eigenvaluesDiagonal] = eigs(fixedInterfaceStiffnessMatrix,obj.InternalMassMatrix,obj.NumberOfModes,'SM');
    
    Logger.TaskFinish('Solving for Eigenmodes','release');

end