%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Mesh factory for a 2D Uniform case
%   This class creates a uniform interface mesh for a 2D element, it is derived from
%   the MeshFactory. It also creates and reduces the internal mesh,
%   calculates its fixed interface modes. Further it creates the interface
%   Element Factory.

classdef MeshFactory2DUniformCraigBamptonMethod
    
    methods (Access = public)
        %% constructor
        function obj = MeshFactory2DUniformCraigBamptonMethod(PolynomialDegree,NumberingScheme,DofDimension, ...
                InternalMeshFactory, InterfaceGeometry, NumberOfModes, PenaltyValue)
            obj.PolynomialDegree = PolynomialDegree;
            obj.DofDimension = DofDimension;
            obj.NumberingScheme = NumberingScheme;
            obj.ElementFactory = obj.createInterfaceElementFactory();
            obj.InterfaceGeometry = InterfaceGeometry;
            obj.NumberOfModes = NumberOfModes;
            obj.PenaltyValue = PenaltyValue;
            
            obj.InternalMesh = Mesh(InternalMeshFactory);
            obj.InternalStiffnessMatrix = obj.InternalMesh.assembleStiffnessMatrix();
            obj.InternalMassMatrix = obj.InternalMesh.assembleMassMatrix();
            obj.InternalLoadVector = zeros(length(obj.InternalStiffnessMatrix),1);
            
            [obj.FixedInterfaceModes, obj.EigenvaluesDiagonal, obj.FixedInterfaceStiffnessMatrix] = obj.calculateFixedInterfaceModes();
            
            
        end
        
        %% create Mesh
        function mesh = createMesh(obj)
            mesh = MeshCraigBamptonMethod(obj);
        end
        
        %% get InterfaceGeometry
        function interfaceGeometry = getInterfaceGeometry(obj)
            interfaceGeometry = obj.InterfaceGeometry;
        end
        
        %% get InternalMesh
        function internalMesh = getInternalMesh(obj)
            internalMesh = obj.InternalMesh;
        end
        
        %% get NumberOfModes
        function numberOfModes = getNumberOfModes(obj)
            numberOfModes = obj.NumberOfModes;
        end
        
        %% get PolynomialDegree
        function polynomialDegree = getPolynomialDegree(obj)
            polynomialDegree = obj.PolynomialDegree;
        end
        
        %% assign degrees of freedom, and return number of dofs
        function NumberOfDofs = assignDofs(obj,Nodes,Edges,Faces,Solids)
            NumberOfDofs = obj.NumberingScheme.assignDofs(Nodes,Edges,Faces,Solids,...
                obj.PolynomialDegree,obj.DofDimension);
        end
        
        %% createNodes
        function Nodes = createNodes(obj,InterfaceGeometry)
            
            for j = 1:length(InterfaceGeometry)
                
                Line = InterfaceGeometry(j);
                Vertices = Line.getVertices(); 
                
                Nodes(j) = Node(Vertices(1).getCoords(),obj.DofDimension);
            end
            
            Nodes(end+1) = Node(Vertices(2).getCoords(),obj.DofDimension);
            
            Logger.Log('Creating nodes......','release');
        end
        %% createEdges
        function Edges = createEdges(obj,Nodes)
            for j = 1:length(Nodes)-1
                nodePair = [Nodes(j) Nodes(j+1)];
                Edges(j) = Edge(nodePair,obj.PolynomialDegree+1,obj.DofDimension);
            end
            Logger.Log('Creating edges......','release');
        end
        %% createFaces
        function Faces = createFaces(obj,Nodes,Edges)
            Faces = [];
        end
        
        %% create solids
        function solids = createSolids(obj,Nodes,Edges,Faces)
            solids = [];
        end
        
        %% createElements
        function Elements = createElements(obj,Nodes,Edges,Faces,solids)
            Logger.Log('Creating elements......','release');
            for i=1:length(Edges)
                NodePair = Nodes(i:i+1);
                OneEdge = Edges(i);                
                Elements(i) = obj.ElementFactory.createElement(NodePair,...
                    OneEdge,[],[],obj.PolynomialDegree,obj.DofDimension);
            end
           
        end
    end
    
    properties (Access = private)
        PolynomialDegree
        DofDimension
        NumberingScheme
        ElementFactory
        InternalMesh
        InternalStiffnessMatrix
        InternalMassMatrix
        InternalLoadVector
        FixedInterfaceModes
        EigenvaluesDiagonal
        InterfaceGeometry
        NumberOfModes
        PenaltyValue
        FixedInterfaceStiffnessMatrix
    end
end

