%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function  [ReducedMassMatrix, ReducedStiffnessMatrix] = reduceInternalMesh(obj, MeshCB)

    Logger.TaskStart('Calculating reduced stiffness and mass matrix','release');
    
    NumberOfGaussPoints = obj.PolynomialDegree + 1;
    IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
    
    %% Coupling InternalMesh and Interfaces
    
    coupledStiffnessMatrix = zeros(obj.InternalMesh.getNumberOfDofs+MeshCB.getNumberOfDofs-obj.NumberOfModes);
    coupledStiffnessMatrix(1:size(obj.InternalStiffnessMatrix,1),1:size(obj.InternalStiffnessMatrix,1)) = obj.InternalStiffnessMatrix;
    
    for i = 1:length(obj.InterfaceGeometry)
        
        InterfaceCoupling = PenaltyInterfaceCoupling(IntegrationScheme, obj.InterfaceGeometry(i), obj.PenaltyValue);
        coupledStiffnessMatrix = InterfaceCoupling.modifyLinearSystemCraigBamptonMethod(MeshCB, obj.InternalMesh, coupledStiffnessMatrix);
        
    end
    
    %% Divide stiffness matrix in boundary(Kbb), internal(Kii) and mixed (Kib,Kbi) parts
    
    DofsInternalMesh = obj.InternalMesh.getNumberOfDofs;
    DofsBoundaryMesh = MeshCB.getNumberOfDofs-obj.getNumberOfModes;
    
    Kbb = coupledStiffnessMatrix(DofsInternalMesh+1:DofsInternalMesh+DofsBoundaryMesh, DofsInternalMesh+1:DofsInternalMesh+DofsBoundaryMesh);
    Kib = coupledStiffnessMatrix(1:DofsInternalMesh, DofsInternalMesh+1:DofsInternalMesh+DofsBoundaryMesh);
    Kbi = coupledStiffnessMatrix(DofsInternalMesh+1:DofsInternalMesh+DofsBoundaryMesh, 1:DofsInternalMesh);
    Kii = coupledStiffnessMatrix(1:DofsInternalMesh,1:DofsInternalMesh); 
    
    rigidBodyModes = -inv(Kii)*Kib;
        
    %% assemble reduced stiffness matrix and reduced mass matrix
    
    ReducedStiffnessMatrix = [obj.FixedInterfaceModes'*Kii*obj.FixedInterfaceModes zeros(obj.NumberOfModes,MeshCB.getNumberOfDofs-obj.NumberOfModes); ...
            zeros(MeshCB.getNumberOfDofs-obj.NumberOfModes,obj.NumberOfModes) Kbb+Kbi*rigidBodyModes];

    ReducedMassMatrix = [obj.FixedInterfaceModes'*obj.InternalMassMatrix*obj.FixedInterfaceModes obj.FixedInterfaceModes'*obj.InternalMassMatrix*rigidBodyModes; ...
        rigidBodyModes'*obj.InternalMassMatrix*obj.FixedInterfaceModes rigidBodyModes'*obj.InternalMassMatrix*rigidBodyModes];

    Logger.TaskFinish('Calculating reduced stiffness and mass matrix','release');
      
    TransformationMatrix = [obj.FixedInterfaceModes rigidBodyModes; zeros(MeshCB.getNumberOfDofs-obj.NumberOfModes,obj.NumberOfModes) eye(MeshCB.getNumberOfDofs-obj.getNumberOfModes)];
    MeshCB.setTransformationMatrix(TransformationMatrix);

end