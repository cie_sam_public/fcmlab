classdef MeshFactory1DGraded < AbsMeshFactory
    
    methods(Access = public)
        %% constructor
        function obj = MeshFactory1DGraded(NumberOfElements,GradingFactor,...
                MaxPolynomialDegree,NumberingScheme,DofDimension,MeshOrigin,...
                Lx,ElementFactory)
            obj = obj@AbsMeshFactory(MaxPolynomialDegree,DofDimension,...
                NumberingScheme,ElementFactory);
            obj.MeshOrigin = MeshOrigin;
            obj.Lx = Lx;
            obj.Ly = 1.0;
            obj.Lz = 1.0;
            obj.NumberOfXDivisions = NumberOfElements;
            obj.NumberOfYDivisions = 0;
            obj.NumberOfZDivisions = 0;
            obj.gradingFactor = GradingFactor;
            obj.PolynomialDegree = round(linspace(1, ...
                obj.MaxPolynomialDegree, obj.NumberOfXDivisions));

        end
        
        %% create nodes on line for hp graded refinement 
        function Nodes = createNodes(obj)
            Logger.Log('Creating nodes......','release');
            
            StartPoint = [0 0 0] + obj.MeshOrigin;
            EndPoint = [obj.Lx 0 0] + obj.MeshOrigin;
            Coordinate = StartPoint;
            
            %loop over the nodes
            for i = 1:obj.NumberOfXDivisions + 1  
                Nodes(i)= Node(Coordinate, obj.DofDimension);
                Coordinate(1) = (EndPoint(1) - StartPoint(1)) * ...
                    obj.gradingFactor ^ (obj.NumberOfXDivisions-i);
            end
                        
        end
        
        %% createEdges
        function Edges = createEdges(obj,Nodes)
            Logger.Log('Creating edges......','release');

            % linear distribution of the polynomial degree
            for i=1:length(Nodes)-1
                nodePair = Nodes(i:i+1);
                pEdge = obj.PolynomialDegree(i);
                Edges(i) = Edge(nodePair,pEdge,obj.DofDimension);
            end
            
        end
        
        %% create Faces
        function Faces = createFaces(obj,Nodes,Edges)
            Faces = [];
        end
        
        %% create solids
        function solids = createSolids(obj,Nodes,Edges,Faces)
            solids = [];
        end
        
        %% createElements
        function Elements = createElements(obj,Nodes,Edges,Faces,solids)
            Logger.Log('Creating elements......','release');

            for i=1:obj.NumberOfXDivisions
                NodePair = Nodes(i:i+1);
                OneEdge = Edges(i);
                pElement = obj.PolynomialDegree(i);
                Elements(i) = obj.ElementFactory.createElement(NodePair,...
                    OneEdge,[],[],pElement,obj.DofDimension);
            end
            
            if obj.flagResetElementCounter
                Elements(i).resetIncrementId; 
                obj.flagResetElementCounter=false;
            end
        end
    end

    properties(Access = public)
        gradingFactor;
        PolynomialDegree;
    end
        
end
