%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					          %
%                                                                       %
%=======================================================================%
 
%assembleStiffnessMatrix function
%   This function assembles the element stiffness matrix and get the 
%   global one.
%   The global stiffness matrix is initalized with the number of degrees
%   of freedom, then the element values are taken and assembled into the 
%   global stiffness matrix, this is done using the location matrix LM.

function K = assembleExtractedStiffnessMatrix1D(Mesh, K)
    Logger.TaskStart('Calculating global stiffness matrix K','release');
    
    if nargin < 2
        K = sparse(Mesh.NumberOfDofs,Mesh.NumberOfDofs);    % Initializing global stiffness matrix K
    end
    
    Logger.Log(['nDofs = ' num2str(Mesh.NumberOfDofs)],'release');

    % create reference Element
    PolynomialDegree = Mesh.PolynomialDegreeXi;
    referenceElement = getReferenceElement( Mesh );

    % compute bezier extraction operator: use bezierExtractionOperator, Mesh.KnotsXi, Mesh.PolynomialDegreeXi
    C = bezierExtractionOperator(Mesh.KnotsXi,Mesh.PolynomialDegreeXi);
    KeRef = referenceElement.calcStiffnessMatrix;
    
    permute = bernsteinPolynomialOrderToFCMLabOrder( 1:PolynomialDegree+1, PolynomialDegree+1 );   
    for i = 1:length(Mesh.Elements)

        Ce = C(:,:,i);
        Ce = Ce(permute,:);
        Ce = Ce(:,permute);
        
        Jac = Mesh.Elements(i).calcJacobian(0);
        Ke =  Ce * KeRef / Jac(1) * Ce';
        
        assertElementsAlmostEqual(Ke,Mesh.Elements(i).calcStiffnessMatrix)

        % Element location matrix LM
        LM = Mesh.Elements(i).getLocationMatrix;
              
        K = Mesh.scatterElementMatrixIntoGlobalMatrix(Ke,LM,K);
    end
    
    Logger.TaskFinish('Calculating global stiffness matrix K','release');

end


function referenceElement = getReferenceElement( Mesh )

    referenceControlPoints = [ -1 0 0; 1 0 0 ];
    referenceElementSupport1 = BSplineCurve( [-1 -1 1 1], 1, referenceControlPoints );
    referenceElementSupport1.elevateOrder( Mesh.PolynomialDegreeXi-1 );
    
    Mat1D = Hooke1D(1,1,1,1);
    Mat1D = [Mat1D Mat1D];
    integrator = Integrator(NoPartition(),Mesh.PolynomialDegreeXi+1);
    Domain = PseudoDomain();

    referenceElementSupport = BezierCurve( referenceElementSupport1.getControlPoints );
    
    nodes = [ Node([ -1 0 0], 1), Node([ 1 0 0], 1) ];
    edge = Edge(nodes,Mesh.PolynomialDegreeXi+1,1, referenceElementSupport);

    referenceElement = ElasticBezierBar(nodes, edge, Mat1D,integrator,Mesh.PolynomialDegreeXi,1,Domain );
    
end