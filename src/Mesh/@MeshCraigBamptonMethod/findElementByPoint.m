%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Returns an element given a Point's coordinates [1x3] row vector

function Element = findElementByPoint(obj,Point)

exit = 0;
ElementId = 0;
for i = 1:length(obj.InterfaceGeometry)
    Interface = obj.InterfaceGeometry(i);
    for j = 1:length(Interface)
        ElementId = ElementId + 1;
        Line = Interface(j);
        Vertices = Line.getVertices();
        x1 = Vertices(1).getX;
        y1 = Vertices(1).getY;
        x2 = Vertices(2).getX;
        y2 = Vertices(2).getY;
        
        if x2-x1 == 0 && Point(1) == x1 && (Point(2)-y1)*(Point(2)-y2) <= 0
            exit = 1;
            break
        elseif y2-y1 == 0 &&  Point(2) == y1 && (Point(1)-x1)*(Point(1)-x2) <= 0
            exit = 1;
            break
        elseif (Point(1)-x1)/(x2-x1)==(Point(2)-y1)/(y2-y1) && (Point(1)-x1)/(x2-x1)>=0 && (Point(1)-x1)/(x2-x1)<1
            exit = 1;
            break
        end
        
    end
    if exit == 1
        break
    end
end
% Return element
Element = obj.Elements(ElementId);

end