%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Mesh Class
%   This class contains elements, nodes, edges, faces, volumes and
%   number of Dofs of the mesh also assembles the Global Stiffness matrix,
%   Global Mass Matrix and Global Load Vector.

classdef AbsMesh < handle
  
    methods (Access = public)
        %% Constructor
        function obj = AbsMesh( Elements )
           
            obj.Elements = Elements;
            
            MaxDofId = max(obj.Elements(1).getLocationMatrix());
            
            firstLocationMatrix = obj.Elements(1).getLocationMatrix();
            obj.MinDofId = min(firstLocationMatrix(firstLocationMatrix>0));
            for i=2:length(obj.Elements)
               lm =  obj.Elements(i).getLocationMatrix();
               MaxDofId = max(MaxDofId, max(lm));
               obj.MinDofId = min(obj.MinDofId, min(lm(lm>0)));
            end
            
            obj.NumberOfDofs = MaxDofId - obj.MinDofId + 1;
            
        end
        
        %% Assembly of the system
        K = assembleStiffnessMatrix(obj,K)
        M = assembleMassMatrix(obj,M)
        F = assembleLoadVector(obj,LoadCase,F,MeshId)
        F_int = assembleInternalForceVector(obj)
        
        %% Scattering Solution
        scatterSolution(obj,SolutionVector)
        
        %% Get Number of Dofs
        function n = getNumberOfDofs(obj)
            n = obj.NumberOfDofs;
        end
        %% Get Elements
        function Elements = getElements(obj)
            Elements = obj.Elements;
        end
        %% get Element
        function Element = getElement(obj,id)
            Element = obj.Elements(id);
        end
        %% get Number Of Elements
        function NumberOfElements = getNumberOfElements(obj)
            NumberOfElements = length(obj.Elements);
        end
        %%
        function subDomains = getIntegrationCells( obj )
            subDomains = [];
            for i = 1:length(obj.Elements)
                subDomains = [ subDomains, obj.Elements(i).getIntegrationCells( ) ];
            end
        end
    end
    
    %%
    methods (Access = public, Static)
        GlobalMatrix = scatterElementMatrixIntoGlobalMatrix(ElementMatrix,LocationMatrix,GlobalMatrix)
        GlobalVector = scatterElementVectorIntoGlobalVector(ElementVector,LocationMatrix,GlobalVector)
    end
    
   methods ( Abstract)
        %% Find Functions
        % used when defining the loads or boundary conditions
        
        % Returns an element given a Point's coordinates [1x3] row vector
        Element = findElementByPoint(obj,Point);
    end
    
    %%
    properties(Access = public)
        NumberOfDofs
        Elements
        MinDofId
    end
    
end
