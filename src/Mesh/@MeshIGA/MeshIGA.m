%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef MeshIGA < AbsMesh
  
    methods (Access = public)
        %% Constructor
        function obj = MeshIGA( meshFactory )
            obj = obj@AbsMesh( meshFactory.createElements() );
                        
            obj.Geometry = meshFactory.getGeometry();
            obj.Edges = meshFactory.getEdges();
            obj.Nodes =[];% meshFactory.getNodes();
            obj.ExtractedAssemblyAlgorithm = meshFactory.getExtractedAssemblyAlgorithm();
            obj.MeshOrigin = meshFactory.getMeshOrigin();
            obj.Lx = meshFactory.getLX();
            obj.Ly = meshFactory.getLY();
            obj.Lz = meshFactory.getLZ();
        end
      
         function dofNumbers = getDegreesOfFreedom(obj, ControlPointsFilter, fieldComponentIndex)
            numberOfDofsPerFieldComponent = size(obj.ControlPoints,1);
            dofNumbers = find(ControlPointsFilter(obj.ControlPoints)) + (fieldComponentIndex-1)*numberOfDofsPerFieldComponent + obj.MinDofId-1 ;
        end

        function edges = getEdges(obj)
            edges = obj.Edges;
        end
        function nodes = getNodes(obj)
            nodes = obj.Nodes;
        end
                
        function edges = findEdges(obj, filter)
    
            edges=[];
           for iEdge = 1:length(obj.Edges)
               edge=obj.Edges{iEdge};
              edgeSupport = edge.getLine;
              edgeControlPoints = edgeSupport.getControlPoints;

              interpolatoryControlPoints = edgeControlPoints( [1 size(edgeControlPoints,1)] , : );      
              matchedControlPoints = find(filter(interpolatoryControlPoints));

              if length(matchedControlPoints) == 2
                  edges = [edges edge];
              end
           end
        end
        
        
	    %% get Mesh Origin
        function Origin = getMeshOrigin(obj)
            Origin = obj.MeshOrigin;
        end

        %% get Lx
        function Lx = getLX(obj)
            Lx = obj.Lx;
        end
        %% get Ly
        function Ly = getLY(obj)
            Ly = obj.Ly;
        end
        %% get Lz
        function Lz = getLZ(obj)
            Lz = obj.Lz;
        end
        
%         function nodes = findNodes(obj, filter)
%                         
%             nodes=[];
%            for node = obj.Nodes
%                coords = node.getCoords
%                if lenght( filter( coords ) ) > 0
%                   nodes = [nodes node];
%                end
%            end
%            
%         end

         function Geometry = getGeometry(obj)
              Geometry = obj.Geometry;
          end
               
    end
    

    %%
    properties(Access = protected)
        Geometry
        Edges
        Nodes
        
        ExtractedAssemblyAlgorithm
        
        MeshOrigin
        Lx
        Ly
        Lz
    end
    
    properties (Dependent)
        ControlPoints

        PolynomialDegreeXi
        KnotsXi
        PolynomialDegreeEta
        KnotsEta

        numberOfControlPointsInXi
        numberOfControlPointsInEta
        numberOfKnotSpansInXi
        numberOfKnotSpansInEta
       
    end
   
    methods
          function KnotsXi = get.KnotsXi(obj)
              KnotsXi = obj.Geometry.getKnotsXi();
          end
          function PolynomialDegreeXi = get.PolynomialDegreeXi(obj)
              PolynomialDegreeXi = obj.Geometry.getPolynomialDegreeXi();
          end
          function KnotsEta = get.KnotsEta(obj)
              KnotsEta = obj.Geometry.geKnotsEta();
          end
          function PolynomialDegreeEta = get.PolynomialDegreeEta(obj)
              PolynomialDegreeEta = obj.Geometry.getPolynomialDegreeEta();
          end
          function ControlPoints = get.ControlPoints(obj)
              ControlPoints = obj.Geometry.getControlPoints();
          end
          function numberOfControlPointsInXi = get.numberOfControlPointsInXi(obj)
              numberOfControlPointsInXi = obj.Geometry.numberOfControlPointsInXi();
          end
          function numberOfControlPointsInEta = get.numberOfControlPointsInEta(obj)
              numberOfControlPointsInEta = obj.Geometry.numberOfControlPointsInEta();
          end
          function numberOfKnotSpansInXi = get.numberOfKnotSpansInXi(obj)
              numberOfKnotSpansInXi = obj.Geometry.numberOfKnotSpansInXi();
          end
          function numberOfKnotSpansInEta = get.numberOfKnotSpansInEta(obj)
              numberOfKnotSpansInEta = obj.Geometry.numberOfKnotSpansInEta();
          end
    end
    
end
