%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 

classdef EmbeddedRotatedRectangle < AbsEmbeddedDomain
%%       
    methods (Access = public)
        % constructor
        function obj = EmbeddedRotatedRectangle( center, lengths, theta )
            switch nargin
                case 3
            transformMatrix = [[cos(theta), -sin(theta), 0];[sin(theta),cos(theta),0];[0,0,1]];
% transformMatrix = rotz((theta/pi)*180);
            firstPoint = Vertex((transformMatrix*[-lengths(1)/2 -lengths(2)/2 0.0]')' + center);
            secoundPoint = Vertex((transformMatrix*[lengths(1)/2 -lengths(2)/2 0.0]')' + center);
            thirdPoint = Vertex((transformMatrix*[lengths(1)/2 lengths(2)/2 0.0]')' + center);
            fourthPoint = Vertex((transformMatrix*[-lengths(1)/2 lengths(2)/2 0.0]')' + center);

            obj.ThisQuad = Quad([firstPoint secoundPoint thirdPoint fourthPoint],[]);
                case 1
                    obj.ThisQuad = center;
            end
        end
        
        
       domainIndex = getDomainIdndex(obj,coords)        
    
    end
%%
    properties (Access = private)
        ThisQuad;            
    end
  
end