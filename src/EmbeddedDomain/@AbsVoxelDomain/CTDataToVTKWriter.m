%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

function obj = CTDataToVTKWriter( obj,Iterationstep, Sensors )





            origin = obj.VoxelOffset(:);
            spacing = [1 1 1];
            endPoint = origin + obj.Index(:);
            numberOfPointsInXYZ = floor(ceil( endPoint - origin));
            
            s = size(Sensors,1);
            dimensionOfResults=1;
            coordsAndResults=zeros(s,dimensionOfResults+3);

            for i=1:s
                coordsAndResults(i,1:dimensionOfResults)=obj.getValueAtPoint(Sensors(i,:));
                coordsAndResults(i,dimensionOfResults+1:dimensionOfResults+3)=Sensors(i,:);
            end   
            
            coordsAndResults=unique(coordsAndResults,'rows');
            
            A=sortrows(coordsAndResults,linspace(dimensionOfResults+3,dimensionOfResults+1,3));
            filename = ['Iterationsschritt',num2str(Iterationstep),'default.vtk'];
            h = fopen(filename,'wt');
            fprintf(h, '# vtk DataFile Version 3.0\n');
            fprintf(h, 'NewmarkWaveEquation\n');
            fprintf(h, 'ASCII\n');
            fprintf(h, 'DATASET STRUCTURED_POINTS\n');
            fprintf(h, 'DIMENSIONS %d %d %d\n',numberOfPointsInXYZ(1),numberOfPointsInXYZ(2),numberOfPointsInXYZ(3));
            fprintf(h, 'ORIGIN %d %d %d\n',origin(1),origin(2),origin(3) ) ;
            fprintf(h, 'SPACING %d %d %d\n',spacing(1),spacing(2),spacing(3));
            fprintf(h, 'POINT_DATA %d\n', numberOfPointsInXYZ(1)*numberOfPointsInXYZ(2)*numberOfPointsInXYZ(3) );
            fprintf(h, 'SCALARS Dichte double %d\n',dimensionOfResults);
            fprintf(h, 'LOOKUP_TABLE default\n');
            for i=1:size(A,1)
                fprintf(h, '%d ',A(i,1) );
                fprintf(h,'\n');
            end
            fclose(h);
            



end

