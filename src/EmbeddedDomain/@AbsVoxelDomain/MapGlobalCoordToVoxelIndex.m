%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013                   %
%                                                                       %
%=======================================================================%

function [ output_args ] = MapGlobalCoordToVoxelIndex(obj,Coord)

Eps = 0.5;
relativeX =   Coord(1) - obj.VoxelOffset(1);
VoxelX    =  (Coord(1) - obj.VoxelOffset(1))/obj.VoxelSpacing(1);
relativeY =   Coord(2) - obj.VoxelOffset(2);
VoxelY    =  (Coord(2) - obj.VoxelOffset(2))/obj.VoxelSpacing(2);
relativeZ =   Coord(3) - obj.VoxelOffset(3);
VoxelZ    =  (Coord(3) - obj.VoxelOffset(3))/obj.VoxelSpacing(3);

% myLengthX = obj.Index(1) * obj.VoxelSpacing(1);
% myLengthY = obj.Index(2) * obj.VoxelSpacing(2);
% myLengthZ = obj.Index(3) * obj.VoxelSpacing(3);

if CompareGreaterEqual(VoxelX ,Eps);
    
        currentVoxelX = ceil(abs(relativeX)/obj.VoxelSpacing(1));
        
elseif CompareLessEqual(VoxelX, Eps);
    
        currentVoxelX = floor(abs(relativeX)/obj.VoxelSpacing(1));
        
end

if CompareGreaterEqual(VoxelY, Eps);
    
       currentVoxelY = ceil(abs(relativeY)/obj.VoxelSpacing(2));
       
elseif CompareLessEqual(VoxelY, Eps);
    
       currentVoxelY = floor(abs(relativeY)/obj.VoxelSpacing(2));
     
end

if CompareGreaterEqual(VoxelZ, Eps);
    
       currentVoxelZ = ceil(abs(relativeZ)/obj.VoxelSpacing(3));
       
elseif CompareLessEqual(VoxelZ, Eps);
    
       currentVoxelZ = floor(abs(relativeZ)/obj.VoxelSpacing(3));
   
end
   
output_args(1) = currentVoxelX;
output_args(2) = currentVoxelY;
output_args(3) = currentVoxelZ;

end

