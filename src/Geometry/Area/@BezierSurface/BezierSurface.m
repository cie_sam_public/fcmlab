%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Class for the quadrilaterals

classdef BezierSurface < AbsArea
    
    methods (Access = public)
        %% constructor
        function obj = BezierSurface( ControlPoints, numberOfControlPointsInXi, numberOfControlPointsInEta )
            
            bottomEdgeControlPoints = ControlPoints( 1:numberOfControlPointsInXi, : );
            rightEdgeControlPoints = ControlPoints(  numberOfControlPointsInXi*(1:numberOfControlPointsInEta), : );
            topEdgeControlPoints = ControlPoints( (numberOfControlPointsInEta-1)*numberOfControlPointsInXi+(1:numberOfControlPointsInXi), : );
            leftEdgeControlPoints = ControlPoints( numberOfControlPointsInXi*(0:numberOfControlPointsInEta-1)+1, : );
            
            Edges = [ BezierCurve(bottomEdgeControlPoints) BezierCurve(rightEdgeControlPoints) BezierCurve(topEdgeControlPoints) BezierCurve(leftEdgeControlPoints) ];
            
            obj = obj@AbsArea([], Edges);
            
            obj.ControlPoints = ControlPoints;
            obj.numberOfControlPointsInXi =  numberOfControlPointsInXi;
            obj.numberOfControlPointsInEta =  numberOfControlPointsInEta;
        end
        
        %%
        g1 = calcBaseVector1(obj,LocalCoords);
        g2 = calcBaseVector2(obj,LocalCoords);
                
        GlobalCoords = mapLocalToGlobal(obj,LocalCoords);

        function ControlPoints = getControlPoints(obj)
            ControlPoints = obj.ControlPoints;
        end     
    end
    
    properties (Dependent, Access = public)
      DegreeXi
      DegreeEta
   end
    
    methods
        function DegreeXi = get.DegreeXi(obj)
            DegreeXi =  obj.numberOfControlPointsInXi-1;
        end
        
        function DegreeEta = get.DegreeEta(obj)
            DegreeEta =  obj.numberOfControlPointsInEta-1;
        end
    end
    
    properties (Access = private)
        ControlPoints
        
        numberOfControlPointsInXi
        numberOfControlPointsInEta
        BinomialEvaluator
    end
    

end

