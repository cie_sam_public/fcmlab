%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% map global coordinates to local coordinates
function xi = mapGlobalToLocal(obj,GlobalCoords)


 %Logger.ThrowException('mapGlobalToLocal not implemented');

toleranceError = (10^-7);
xi = [0 0];
Q = obj.mapLocalToGlobal(xi);
fmin = norm([Q(1)-GlobalCoords(1), Q(2)-GlobalCoords(2)]);
%determine initial guess for Newton-Raphson method

for w = 0.10:0.1:1.0
    for l = 1:2
        for m = 1:2
            xi_n = transpose([((-1)^l)*w; ((-1)^m)*w]);
            Q_n = obj.mapLocalToGlobal(xi_n);
            f = norm([Q_n(1)-GlobalCoords(1), Q_n(2)-GlobalCoords(2)]);
            if f < fmin
                fmin = f;
                xi = xi_n;
            end
        end
    end
end
%Newton-Raphson method with damping
Q = obj.mapLocalToGlobal(xi);
for i = 1:20
    
    error = norm([Q(1)-GlobalCoords(1), Q(2)-GlobalCoords(2)]);
    
    if (error < toleranceError)
        break;
    end
    
    J = obj.calcJacobian(xi);
    JT = (J(1:2,1:2));
    f = ([Q(1)-GlobalCoords(1), Q(2)-GlobalCoords(2)]);
    incrementxi = -f/JT;
    xi = xi + incrementxi;
    Q = obj.mapLocalToGlobal(xi);
end

end