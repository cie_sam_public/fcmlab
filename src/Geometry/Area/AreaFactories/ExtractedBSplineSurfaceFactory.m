%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Class for the quadrilaterals

classdef ExtractedBSplineSurfaceFactory < AbsAreaFactory
    
    methods (Access = public)
        %% constructor
        function obj = ExtractedBSplineSurfaceFactory( BSplineSurface )
            obj.BSplineSurface = BSplineSurface;
            obj.ExtractionOperators = bezierExtractionOperator2D( obj.BSplineSurface.getKnotsXi, obj.BSplineSurface.getPolynomialDegreeXi, obj.BSplineSurface.getKnotsEta, obj.BSplineSurface.getPolynomialDegreeEta );
        end
 
        function Area = createArea(obj, areaIndex)

            iXi = mod(areaIndex-1, obj.BSplineSurface.numberOfKnotSpansInXi)+1;
            iEta = floor((areaIndex-1)/ obj.BSplineSurface.numberOfKnotSpansInXi) +1;
            controlPointsIndices = getControlPointsOfSpan2D ( iXi, iEta, obj.BSplineSurface.getKnotsXi, obj.BSplineSurface.getPolynomialDegreeXi, obj.BSplineSurface.getKnotsEta, obj.BSplineSurface.getPolynomialDegreeEta );
            controlPoints = obj.BSplineSurface.getControlPoints;
            
            Area = ExtractedBSplineSurface( controlPoints(controlPointsIndices,:) , obj.ExtractionOperators(:,:,areaIndex), obj.BSplineSurface.getPolynomialDegreeXi+1, obj.BSplineSurface.getPolynomialDegreeEta+1);
        end
    end
    
    properties (Access = private)
        ExtractionOperators
        BSplineSurface
    end
    
    
end


