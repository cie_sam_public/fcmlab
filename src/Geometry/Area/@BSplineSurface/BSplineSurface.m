%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Class for the quadrilaterals

classdef BSplineSurface < handle
    
    methods (Access = public)
        %% constructor
        function obj = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints )
            obj.KnotsXi=KnotsXi;
            obj.PolynomialDegreeXi=PolynomialDegreeXi;
            obj.KnotsEta=KnotsEta;
            obj.PolynomialDegreeEta=PolynomialDegreeEta;
            obj.ControlPoints=ControlPoints;
        end
        
        g1 = calcBaseVector1(obj,LocalCoords);
        g2 = calcBaseVector2(obj,LocalCoords);
                
        GlobalCoords = mapLocalToGlobal(obj,LocalCoords);
        
        function obj = insertKnots(obj, KnotToInsertXi, KnotToInsertEta)
            
            ControlPointsTensor = reshape( obj.ControlPoints, size(obj.ControlPoints,1)/obj.numberOfControlPointsInEta, size(obj.ControlPoints,1)/obj.numberOfControlPointsInXi, size(obj.ControlPoints,2) );
            
            [ ControlPointsTensor, obj.KnotsXi, obj.KnotsEta ] = knot_refine_surf(obj.PolynomialDegreeXi, obj.PolynomialDegreeEta, obj.KnotsXi, obj.KnotsEta, ControlPointsTensor, KnotToInsertXi, KnotToInsertEta);
            
            obj.ControlPoints = reshape( ControlPointsTensor, size(ControlPointsTensor,1) * size(ControlPointsTensor,2) , size(obj.ControlPoints,2));
        end
        
        function obj = elevateOrder(obj, increaseXi, increaseEta)
            ControlPointsTensor = reshape( obj.ControlPoints, size(obj.ControlPoints,1)/obj.numberOfControlPointsInEta, size(obj.ControlPoints,1)/obj.numberOfControlPointsInXi, size(obj.ControlPoints,2));
            
            [ ControlPointsTensor, obj.KnotsXi, obj.KnotsEta, obj.PolynomialDegreeXi, obj.PolynomialDegreeEta ] = degree_elevate_surf( obj.PolynomialDegreeXi, obj.PolynomialDegreeEta, obj.KnotsXi, obj.KnotsEta, ControlPointsTensor, increaseXi, increaseEta);
            
            obj.ControlPoints = reshape( ControlPointsTensor, size(ControlPointsTensor,1) * size(ControlPointsTensor,2) , size(obj.ControlPoints,2));
        end
        
        
        function KnotsXi = getKnotsXi(obj)
           KnotsXi = obj.KnotsXi; 
        end
        
        function PolynomialDegreeXi = getPolynomialDegreeXi(obj)
           PolynomialDegreeXi = obj.PolynomialDegreeXi; 
        end
        
        function KnotsEta = getKnotsEta(obj)
           KnotsEta = obj.KnotsEta; 
        end
        
        function PolynomialDegreeEta = getPolynomialDegreeEta(obj)
           PolynomialDegreeEta = obj.PolynomialDegreeEta; 
        end
        
        function ControlPoints = getControlPoints(obj)
           ControlPoints = obj.ControlPoints; 
        end
        
        function removeWeights(obj)
           obj.ControlPoints =obj. ControlPoints(:, 1:3);
        end
        
        function removeMultiplicities(obj)
            localNonEmptySpansInXi = obj.nonEmptySpansInXi;
            obj.KnotsXi = [ repmat(localNonEmptySpansInXi(1), 1, obj.PolynomialDegreeXi) localNonEmptySpansInXi repmat(localNonEmptySpansInXi(end), 1, obj.PolynomialDegreeXi)  ];
           
            localNonEmptySpansInEta = obj.nonEmptySpansInEta;
            obj.KnotsEta = [ repmat(localNonEmptySpansInEta(1), 1, obj.PolynomialDegreeEta) localNonEmptySpansInEta repmat(localNonEmptySpansInEta(end), 1, obj.PolynomialDegreeEta)  ];
        end
        
        function refinedSurface = refineBisect(obj)
            refinedSurface = BSplineSurface( obj.KnotsXi,  obj.PolynomialDegreeXi,  obj.KnotsEta,  obj.PolynomialDegreeEta,  obj.ControlPoints );
            
            nonEmptySpansXi = obj.nonEmptySpansInXi;
            nonEmptySpansEta = obj.nonEmptySpansInEta;
            
            knotsToInsertXi = (nonEmptySpansXi(1:end-1) + nonEmptySpansXi(2:end))/2;
             knotsToInsertEta = (nonEmptySpansEta(1:end-1) + nonEmptySpansEta(2:end))/2;
            refinedSurface.insertKnots(knotsToInsertXi,knotsToInsertEta);
        end
        
    end
    
   properties (Dependent)
      numberOfControlPoints
      numberOfControlPointsInXi
      numberOfControlPointsInEta
      
      numberOfKnotSpansInXi
      numberOfKnotSpansInEta
      
      nonEmptySpansInXi
      nonEmptySpansInEta
       end
   
    methods
        
        function numberOfControlPoints = get.numberOfControlPoints(obj)
            numberOfControlPoints = size(obj.ControlPoints, 1);
        end
        
        function numberOfControlPointsInXi = get.numberOfControlPointsInXi(obj)
            numberOfControlPointsInXi = length(obj.KnotsXi) - obj.PolynomialDegreeXi - 1;
        end
        
        function numberOfControlPointsInEta = get.numberOfControlPointsInEta(obj)
           numberOfControlPointsInEta = length(obj.KnotsEta)   - obj.PolynomialDegreeEta - 1;
        end
        
        function numberOfKnotSpansInXi = get.numberOfKnotSpansInXi(obj)
            numberOfKnotSpansInXi = getNumberOfKnotSpans( obj.KnotsXi );
        end
        
        function numberOfKnotSpansInEta = get.numberOfKnotSpansInEta(obj)
            numberOfKnotSpansInEta = getNumberOfKnotSpans( obj.KnotsEta );
        end
        
        function nonEmptySpansInXi = get.nonEmptySpansInXi (obj)
            nonEmptySpansInXi = getNonEmptyKnotSpans( obj.KnotsXi );
        end
        
        function nonEmptySpansInEta = get.nonEmptySpansInEta (obj)
            nonEmptySpansInEta = getNonEmptyKnotSpans( obj.KnotsEta );
        end
    end
    
    properties (Access = private)
        KnotsXi
        PolynomialDegreeXi
        KnotsEta
        PolynomialDegreeEta
        ControlPoints
    end
    

    
end

