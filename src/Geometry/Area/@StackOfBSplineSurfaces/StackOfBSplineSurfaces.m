%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Class for the quadrilaterals

classdef StackOfBSplineSurfaces < handle
    
    methods (Access = public)
        %% constructor
        function obj = StackOfBSplineSurfaces( Geometries, RefinementTree, Levels )
            obj.Geometries=Geometries;
            obj.RefinementTree = RefinementTree;
            
            %[obj.Aa obj.Am obj.Ap obj.Ea] = getRefinementDataStructures2DTree( RefinementTree, obj.Geometries );
            [obj.Aa obj.Am obj.Ap obj.Ea] = getRefinementDataStructures2D( Levels, obj.Geometries );
        end
        
        function Geometries = getGeometries(obj)
            Geometries=obj.Geometries;
        end
        
        function Aa = getAa(obj)
            Aa=obj.Aa;
        end
        
        function Am = getAm(obj)
            Am=obj.Am;
        end
        function Ap = getAp(obj)
            Ap=obj.Ap;
        end
        function Ea = getEa(obj)
            Ea=obj.Ea;
        end
        
        function [Aa Am Ap Ea] = getRefinementDataStructures(obj)
            Aa=obj.Aa;
            Am=obj.Am;
            Ap=obj.Ap;
            Ea=obj.Ea;
        end
        
        function RefinementTree = getRefinementTree(obj)
            RefinementTree=obj.RefinementTree;
        end
        
        function ControlPoints = getControlPoints(obj)
            ControlPoints = [];
            
            for iGeometry = 1:length(obj.Geometries)
                LevelControlPoints = obj.Geometries{iGeometry}.getControlPoints();
                assert( length(obj.Aa{iGeometry}(:)) == size(LevelControlPoints,1 ) );
                ControlPoints = [ControlPoints; LevelControlPoints(logical(obj.Aa{iGeometry}(:)),:) ];
            end
        end
        
        function ControlPoints = getControlPointsOfSpan(obj, iXi,iEta,level)
            
            ControlPoints=[];
            for iPreviousLevel = 1:level
                previousLevelIndexX = 1+ fix((iXi-1)/2^(level-iPreviousLevel));
                previousLevelIndexY = 1+ fix((iEta-1)/2^(level-iPreviousLevel));
                
                controlPointsIndices = getControlPointsOfSpan2D ( previousLevelIndexX, previousLevelIndexY, obj.Geometries{iPreviousLevel}.getKnotsXi, obj.Geometries{iPreviousLevel}.getPolynomialDegreeXi, obj.Geometries{iPreviousLevel}.getKnotsEta, obj.Geometries{iPreviousLevel}.getPolynomialDegreeEta );
                levelControlPoints = obj.Geometries{iPreviousLevel}.getControlPoints;
                ControlPoints=[ControlPoints; levelControlPoints(controlPointsIndices,:)];
            end
        end
        
        function ControlPoints = getLevelControlPointsOfSpan(obj, iXi,iEta,level)
            ControlPoints=[];
            for iPreviousLevel = level:level
                previousLevelIndexX = 1+ fix((iXi-1)/2^(level-iPreviousLevel));
                previousLevelIndexY = 1+ fix((iEta-1)/2^(level-iPreviousLevel));
                
                controlPointsIndices = getControlPointsOfSpan2D ( previousLevelIndexX, previousLevelIndexY, obj.Geometries{iPreviousLevel}.getKnotsXi, obj.Geometries{iPreviousLevel}.getPolynomialDegreeXi, obj.Geometries{iPreviousLevel}.getKnotsEta, obj.Geometries{iPreviousLevel}.getPolynomialDegreeEta );
                levelControlPoints = obj.Geometries{iPreviousLevel}.getControlPoints;
                ControlPoints=[ControlPoints; levelControlPoints(controlPointsIndices,:)];
            end
        end
        
    end
    
    properties (Access = private)
        Geometries
        RefinementTree
        Aa
        Am
        Ap
        Ea
    end
    
    
end


