%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Class for circular arcs
%  Arcs with an angle between 0 and pi


classdef Arc < AbsCurve
    
    methods (Access = public)
         %% constructor
        function obj = Arc(Vertex1,Vertex2,VertexCenter)
            obj = obj@AbsCurve(Vertex1,Vertex2);
            obj.VertexCenter=VertexCenter;
            obj.Radius=obj.calcRadius;
            obj.RotationAxis=obj.calcRotationAxis;
            obj.Angle=obj.calcAngle;
        end
        
        
        %%
        Centroid = calcCentroid(obj);
        J = calcJacobian(obj,LocalCoords);
        detJ = calcDetJacobian(obj,LocalCoords);
        NormalVector = calcNormalVector(obj,LocalCoords);
        GlobalCoords = mapLocalToGlobal(obj,LocalCoords);
        LocalCoords = mapGlobalToLocal(obj,GlobalCoords);
        
        %% get VertexCenter
        function VertexCenter = getCenter(obj)
            VertexCenter = obj.VertexCenter;
        end      
    end
    
    methods (Access = protected)
           %%
        function radius=calcRadius(obj)
            radius=norm(obj.Vertices(1).getCoords-obj.VertexCenter.getCoords);        
        end    
           %%
        function RotationAxis=calcRotationAxis(obj)
            Vector1=obj.Vertices(1).getCoords-obj.VertexCenter.getCoords;
            Vector2=obj.Vertices(2).getCoords-obj.VertexCenter.getCoords;
            
            RotationAxis=cross(Vector2,Vector1);%rotation axis vector
            if ~norm(RotationAxis) && abs(Vector1(3)-Vector2(3))<1E-5
                RotationAxis=[0 0 -1]; %assume z as rotation axis
            elseif ~norm(RotationAxis)
                Logger.ThrowException(['Arc points are colinear']);
            end
            RotationAxis=RotationAxis/norm(RotationAxis);        
        end     
        
            %%
        function angle=calcAngle(obj)
            Vector1=obj.Vertices(1).getCoords-obj.VertexCenter.getCoords;
            Vector2=obj.Vertices(2).getCoords-obj.VertexCenter.getCoords;
            angle=acos(Vector1*Vector2'/obj.Radius^2);% angle of rotation between Vertex1 and Vertex2 
        end    
            %%
        function R=evalRotationMatrix(obj,LocalCoords)     
            n=obj.RotationAxis;
            Omega=obj.Angle;
            sa=sin(((1/2)*LocalCoords+1/2)*Omega);
            ca=cos(((1/2)*LocalCoords+1/2)*Omega);
            R=[n(1)^2*(1-ca)+ca, n(1)*n(2)*(1-ca)-n(3)*sa, n(1)*n(3)*(1-ca)+n(2)*sa;... 
                n(2)*n(1)*(1-ca)+n(3)*sa, n(2)^2*(1-ca)+ca, n(2)*n(3)*(1-ca)-n(1)*sa;...
                n(3)*n(1)*(1-ca)-n(2)*sa, n(3)*n(2)*(1-ca)+n(1)*sa, n(3)^2*(1-ca)+ca]; %rotation matrix for a rotation around axis n 
        end
        %%
        function dR=evalDerivative(obj,LocalCoords)     
            n=obj.RotationAxis;
            Omega=obj.Angle;
            sa=sin(((1/2)*LocalCoords+1/2)*Omega);
            ca=cos(((1/2)*LocalCoords+1/2)*Omega);

            dR=[(1/2)*sa*Omega*(n(1)^2-1),  -(1/2)*Omega*(-n(1)*n(2)*sa+n(3)*ca), (1/2)*Omega*(n(1)*n(3)*sa+n(2)*ca);...
                (1/2)*Omega*(n(1)*n(2)*sa+n(3)*ca), (1/2)*sa*Omega*(n(2)^2-1),  (1/2)*Omega*(n(2)*n(3)*sa-n(1)*ca);... 
                (1/2)*Omega*(n(1)*n(3)*sa-n(2)*ca), (1/2)*Omega*(n(2)*n(3)*sa+n(1)*ca), (1/2)*sa*Omega*(n(3)^2-1)]; % derivative of rotation matrix 
        end
    end
    
    properties (Access = private)
        VertexCenter
        Radius
        RotationAxis
        Angle
    end
    
end

