%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef BSplineCurve < handle
    
        properties (Access = private)
        KnotsXi
        PolynomialDegreeXi
        KnotsEta
        PolynomialDegreeEta
        ControlPoints
    end
    
   properties (Dependent)
      numberOfControlPoints
      numberOfControlPointsInXi
      numberOfControlPointsInEta
      
      numberOfKnotSpansInXi
      numberOfKnotSpansInEta
   end
    
    methods (Access = public)
        %% constructor
        function obj = BSplineCurve( KnotsXi, PolynomialDegreeXi, ControlPoints )
            obj.KnotsXi=KnotsXi;
            obj.PolynomialDegreeXi=PolynomialDegreeXi;
            obj.ControlPoints=ControlPoints;
        end
        
        function obj = insertKnots(obj, KnotToInsertXi)
            
            [TrasposedControlPoints, obj.KnotsXi] = bspkntins( obj.PolynomialDegreeXi, obj.ControlPoints', obj.KnotsXi, KnotToInsertXi);
            
            obj.ControlPoints=TrasposedControlPoints';

        end
        
        function obj = elevateOrder(obj, increaseXi)
            
            [TrasposedControlPoints, obj.KnotsXi] = bspdegelev( obj.PolynomialDegreeXi, obj.ControlPoints', obj.KnotsXi, increaseXi);
            obj.ControlPoints=TrasposedControlPoints';
            obj.PolynomialDegreeXi =  obj.PolynomialDegreeXi+increaseXi;
        end
        
        
        function KnotsXi = getKnotsXi(obj)
           KnotsXi = obj.KnotsXi; 
        end
        
        function PolynomialDegreeXi = getPolynomialDegreeXi(obj)
           PolynomialDegreeXi = obj.PolynomialDegreeXi; 
        end
        
        function KnotsEta = getKnotsEta(obj)
           KnotsEta = obj.KnotsEta; 
        end
        
        function PolynomialDegreeEta = getPolynomialDegreeEta(obj)
           PolynomialDegreeEta = obj.PolynomialDegreeEta; 
        end
        
        function ControlPoints = getControlPoints(obj)
           ControlPoints = obj.ControlPoints; 
        end
        
        function removeWeights(obj)
           obj.ControlPoints =obj. ControlPoints(:, 1:3);
        end
        
    end
    
    methods
        
        function numberOfControlPoints = get.numberOfControlPoints(obj)
            numberOfControlPoints = size(obj.ControlPoints, 1);
        end
        
        function numberOfControlPointsInXi = get.numberOfControlPointsInXi(obj)
            numberOfControlPointsInXi = length(obj.KnotsXi) - obj.PolynomialDegreeXi - 1;
        end
        
        function numberOfControlPointsInEta = get.numberOfControlPointsInEta(obj)
           numberOfControlPointsInEta = length(obj.KnotsEta)   - obj.PolynomialDegreeEta - 1;
        end
        
        function numberOfKnotSpansInXi = get.numberOfKnotSpansInXi(obj)
            numberOfKnotSpansInXi = getNumberOfKnotSpans( obj.KnotsXi );
        end
        
        function numberOfKnotSpansInEta = get.numberOfKnotSpansInEta(obj)
            numberOfKnotSpansInEta = getNumberOfKnotSpans( obj.KnotsEta );
        end
    end
    

    
end

