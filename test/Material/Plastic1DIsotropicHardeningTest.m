%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class Plastic1DIsotropicHardening

classdef Plastic1DIsotropicHardeningTest < TestCase
    
    properties
        Mat
        PlasticElem
        Integrator4
        GPCoord
    end
    
    methods
        
        %% constructor
        function obj = Plastic1DIsotropicHardeningTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            obj.Integrator4 = Integrator(NoPartition(),3);
            obj.Mat = Plastic1DIsotropicHardening(1,20,1.0,100,1,1);
            NoDomain = PseudoDomain();
            
            Node1 = Node([0 0 0],1);
            Node1.getDof().setId(1);
            Node1.getDof().setValue(1,1);
            Node2 = Node([1 0 0],1);
            Node2.getDof().setId(4);
            Node2.getDof().setValue(2,1);
            Edge3 = Edge([Node1 Node2],9,1);
            
            obj.PlasticElem = PlasticBar([Node1 Node2], Edge3, obj.Mat, obj.Integrator4,...
                2,1,NoDomain);
            
            obj.PlasticElem.AddContainer([-7.745966692414834e-01, 0, 0]);
            obj.PlasticElem.AddContainer([0, 0, 0]);
            obj.PlasticElem.AddContainer([7.745966692414834e-01, 0, 0]);
            
            % to initialize internal variables
            initContainer = containers.Map('KeyType','int32','ValueType','any');
            initContainer(1) = 0;   % Elastic Strain
            initContainer(2) = 0;   % Plastic Strain
            initContainer(3) = 200; % Stress
            initContainer(4) = 0;   % Hardening Variable
            
            obj.GPCoord = [-7.745966692414834e-01, 0, 0];
            
            obj.PlasticElem.setTempContAtGP([-7.745966692414834e-01, 0, 0], initContainer);
            obj.PlasticElem.setTempContAtGP([0, 0, 0], initContainer);
            obj.PlasticElem.setTempContAtGP([7.745966692414834e-01, 0, 0], initContainer);
            
            obj.PlasticElem.initializeDOFVector([0;100;50]); 
        end
        
        function tearDown(obj)
        end
        
        function testReturnMapping(obj)            
            [epsilonElast, epsilonPlast, stress, hardeningVar] =obj.Mat.returnMapping(1.1, 2.4, 3.9, 4.16);
            vectorOfSolutions = [epsilonElast, epsilonPlast, stress, hardeningVar];
            assertVectorsAlmostEqual( vectorOfSolutions, [0.0678 3.9522 1.3550 4.2123],'absolute',1e-4);
        end
        
        function testCalcYieldStress(obj)           
            yieldStress = obj.Mat.calcYieldStress(5.255);
            assertVectorsAlmostEqual(yieldStress, 1.055315789473684e+02, 'absolute', 1e-4);
        end
        
        function testGetMaterialMat(obj)    
            MaterialMatrix = obj.Mat.getMaterialMatrix(obj.PlasticElem, obj.GPCoord);            
            assertVectorsAlmostEqual(MaterialMatrix, 1.0, 'absolute', 1e-4);
        end
        
        function testCalcEpsilonElastTrial(obj)
            epsilonElastOld = [0;0;0];
            epsilonElastTrial = obj.Mat.calcEpsilonElastTrial(obj.PlasticElem, obj.GPCoord, epsilonElastOld);            
            assertVectorsAlmostEqual(epsilonElastTrial, [5.131670194948597; 5.131670194948597; 5.131670194948597], 'absolute', 1e-4);
        end
    end 
end