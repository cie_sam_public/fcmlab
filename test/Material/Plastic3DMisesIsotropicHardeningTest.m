%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class Plastic3DMisesIsotropicHardening

classdef Plastic3DMisesIsotropicHardeningTest < TestCase
    
    properties
        Mat
        PlasticElem
        Integrator1
        PD1
        DofDimension
        GPCoord
    end
    
    methods
        
        %% constructor
        function obj = Plastic3DMisesIsotropicHardeningTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            obj.Integrator1 = Integrator(NoPartition,2);
            obj.PD1= 1;
            obj.DofDimension = 3;
            NoDomain = PseudoDomain();
            
            Node1 = Node([3 5 0],3);
            Node2 = Node([5 5 0],3);
            Node3 = Node([5 8 0],3);
            Node4 = Node([3 8 0],3);
            Node5 = Node([3 5 9],3);
            Node6 = Node([5 5 9],3);
            Node7 = Node([5 8 9],3);
            Node8 = Node([3 8 9],3);
            Edge1 = Edge([Node1 Node2],3,3);
            Edge2 = Edge([Node2 Node3],3,3);
            Edge3 = Edge([Node4 Node3],3,3);
            Edge4 = Edge([Node1 Node4],3,3);
            Edge5 = Edge([Node1 Node5],3,3);
            Edge6 = Edge([Node2 Node6],3,3);
            Edge7 = Edge([Node3 Node7],3,3);
            Edge8 = Edge([Node4 Node8],3,3);
            Edge9 = Edge([Node5 Node6],3,3);
            Edge10 = Edge([Node6 Node7],3,3);
            Edge11 = Edge([Node8 Node7],3,3);
            Edge12 = Edge([Node5 Node8],3,3);
            Face1 = Face([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],3,3);
            Face2 = Face([Node1 Node2 Node6 Node5], [Edge1 Edge5 Edge6 Edge9],3,3);
            Face3 = Face([Node2 Node3 Node7 Node6], [Edge2 Edge6 Edge7 Edge10],3,3);
            Face4 = Face([Node3 Node4 Node8 Node7], [Edge3 Edge7 Edge8 Edge11],3,3);
            Face5 = Face([Node1 Node4 Node8 Node5], [Edge4 Edge5 Edge8 Edge12],3,3);
            Face6 = Face([Node5 Node6 Node7 Node8], [Edge9 Edge10 Edge11 Edge12],3,3);
            
            solid1 = Solid([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6],3,3);
                        
            % to test plasticity
            obj.Mat = Plastic3DMisesIsotropicHardening(1,20,1,100,1,1);
            obj.PlasticElem = PlasticHexa([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6], solid1, obj.Mat,obj.Integrator1, ...
                obj.PD1,obj.DofDimension,PseudoDomain());
            
            %8 GPs for the PD = 1 hexa
            GP1 = [-5.773502691896258e-01 -5.773502691896258e-01 -5.773502691896258e-01];
            GP2 = [5.773502691896258e-01 -5.773502691896258e-01 -5.773502691896258e-01];
            
            GP3 = [-5.773502691896258e-01 5.773502691896258e-01 -5.773502691896258e-01];
            GP4 = [5.773502691896258e-01 5.773502691896258e-01 -5.773502691896258e-01];
            
            
            GP5 = [-5.773502691896258e-01 -5.773502691896258e-01 5.773502691896258e-01];
            GP6 = [5.773502691896258e-01 -5.773502691896258e-01 5.773502691896258e-01];
            
            GP7 = [-5.773502691896258e-01 5.773502691896258e-01  5.773502691896258e-01];
            GP8 = [5.773502691896258e-01 5.773502691896258e-01  5.773502691896258e-01];
            
            obj.PlasticElem.AddContainer(GP1);
            obj.PlasticElem.AddContainer(GP2);
            obj.PlasticElem.AddContainer(GP3);
            obj.PlasticElem.AddContainer(GP4);
            obj.PlasticElem.AddContainer(GP5);
            obj.PlasticElem.AddContainer(GP6);
            obj.PlasticElem.AddContainer(GP7);
            obj.PlasticElem.AddContainer(GP8);           
            
            % to initialize internal variables
            initContainer = containers.Map('KeyType','int32','ValueType','any');
            initContainer(1) = zeros(3);   % Elastic Strain
            initContainer(2) = 0;   % Plastic Strain
            initContainer(3) = 200*eye(3); % Stress            
            
            obj.PlasticElem.setTempContAtGP(GP1, initContainer);
            obj.PlasticElem.setTempContAtGP(GP2, initContainer);
            obj.PlasticElem.setTempContAtGP(GP3, initContainer);
            obj.PlasticElem.setTempContAtGP(GP4, initContainer);
            obj.PlasticElem.setTempContAtGP(GP5, initContainer);
            obj.PlasticElem.setTempContAtGP(GP6, initContainer);
            obj.PlasticElem.setTempContAtGP(GP7, initContainer);
            obj.PlasticElem.setTempContAtGP(GP8, initContainer);
            
            obj.PlasticElem.initializeDOFVector([1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24]')
            
            obj.GPCoord = GP1;    
            
        end
        
        function tearDown(obj)
        end
                
        function testReturnMapping(obj)            
            
            [epsilonElast, epsilonPlast, stress, hardeningVar] =obj.Mat.returnMapping(1.1, 2.4, 3.9, 4.16, 5.25, 6.36);
            
            assertVectorsAlmostEqual(epsilonElast, 1.026666666666667, 'absolute', 1e-4);
            assertElementsAlmostEqual(epsilonPlast, [84.647500000000008  79.397500000000008  79.397500000000008; 
                                                     79.397500000000008  84.647500000000008  79.397500000000008;
                                                     79.397500000000008  79.397500000000008  84.647500000000008], 'absolute', 1e-4);
            assertVectorsAlmostEqual(stress, 7.386666666666667, 'absolute', 1e-4);
            assertElementsAlmostEqual(hardeningVar, [7.940833333333334   3.780833333333333   3.780833333333333;
                                                     3.780833333333333   7.940833333333334   3.780833333333333;
                                                     3.780833333333333   3.780833333333333   7.940833333333334], 'absolute', 1e-4);
        end
        
        function testCalcYieldStress(obj)           
            yieldStress = obj.Mat.calcYieldStress(5.255);
            assertVectorsAlmostEqual(yieldStress, 1.052550000000000e+02, 'absolute', 1e-4);
        end
        
        function testGetMaterialMat(obj)    
            MaterialMatrix = obj.Mat.getMaterialMatrix(obj.PlasticElem, obj.GPCoord); 
            answer = [0.023199023199023  -0.024420024420024  -0.024420024420024         0                   0                   0;
           -0.024420024420024   0.023199023199023  -0.024420024420024                   0                   0                   0;
           -0.024420024420024  -0.024420024420024   0.023199023199023                   0                   0                   0;
                            0                   0                   0   0.023809523809524                   0                   0;
                            0                   0                   0                   0   0.023809523809524                   0;
                            0                   0                   0                   0                   0   0.023809523809524];
            assertElementsAlmostEqual(MaterialMatrix, answer, 'absolute', 1e-4);
        end
        
        function testCalcEpsilonElastTrial(obj)
            epsilonElastOld = zeros(3);
            answer = [0.288675134594813   0.573895945495678   0.366559789519629;
                      0.573895945495678   0.859116756396542   0.651780600420494;
                      0.366559789519629   0.651780600420494   0.444444444444444];
            epsilonElastTrial = obj.Mat.calcEpsilonElastTrial(obj.PlasticElem, obj.GPCoord, epsilonElastOld);         
            assertElementsAlmostEqual(epsilonElastTrial, answer, 'absolute', 1e-4);
        end
        
        function testCalcDevHydVariables(obj)
            
            tensor = [0.288675134594813   0.573895945495678   0.366559789519629;
                      0.573895945495678   0.859116756396542   0.651780600420494;
                      0.366559789519629   0.651780600420494   0.444444444444444];
            [tensorDev, tensorHyd] = obj.Mat.calcDevHydVariables(tensor);     
            ans_tensorDev= [-0.242070310550453   0.573895945495678   0.366559789519629;
                             0.573895945495678   0.328371311251276   0.651780600420494;
                             0.366559789519629   0.651780600420494  -0.086301000700822];
            ans_tensorHyd = 0.530745445145266;
            assertElementsAlmostEqual(tensorDev, ans_tensorDev, 'absolute', 1e-4);
            assertVectorsAlmostEqual(tensorHyd, ans_tensorHyd, 'absolute', 1e-4);
        end
        
        function testCalcPlastModulus(obj)
            epsilonElastDevTrial = [0.288675134594813   0.573895945495678   0.366559789519629;
                                    0.573895945495678   0.859116756396542   0.651780600420494;
                                    0.366559789519629   0.651780600420494   0.444444444444444];
            answer = [ 0.022468815417687  -0.024280419254420  -0.024187233071994  -0.000257934618394  -0.000292939481043  -0.000164748435967;
                     -0.024280419254420   0.021959119138366  -0.024384596097013  -0.000767630897715  -0.000871807740307  -0.000490302506061;
                     -0.024187233071994  -0.024384596097013   0.022379917043376  -0.000397116323635  -0.000451009835297  -0.000253646810278;
                     -0.000257934618394  -0.000767630897715  -0.000397116323635   0.022284956297407  -0.001164747221349  -0.000655050942029;
                     -0.000292939481043  -0.000871807740307  -0.000451009835297  -0.001164747221349   0.021987704237913  -0.000743949316339;
                     -0.000164748435967  -0.000490302506061  -0.000253646810278  -0.000655050942029  -0.000743949316339   0.022892126567271];
            PlastModulus = obj.Mat.calcPlastModulus(3.34, 0.98, epsilonElastDevTrial);
            
            assertElementsAlmostEqual(PlastModulus, answer, 'absolute', 1e-4);
        end
        
        function testCalcUnitFlowVector(obj)
            epsilonElastDevTrial = [-0.242070310550453   0.573895945495678   0.366559789519629;
                                    0.573895945495678   0.328371311251276   0.651780600420494;
                                    0.366559789519629   0.651780600420494  -0.086301000700822];
            N = obj.Mat.calcUnitFlowVector(epsilonElastDevTrial);
            answer = [-0.173307726330324   0.235094032042416 -0.061786305712091   0.821749691136265   0.933271111754499   0.524869353381759];
            assertElementsAlmostEqual(N, answer, 'absolute', 1e-4);
        end
        
        function testCalcVMStress(obj)
            dS = [-0.242070310550453   0.573895945495678   0.366559789519629;
                   0.573895945495678   0.328371311251276   0.651780600420494;
                   0.366559789519629   0.651780600420494  -0.086301000700822];
            stress_VM = obj.Mat.calcVMStress(dS);
            assertVectorsAlmostEqual(stress_VM, 1.710681789210932, 'absolute', 1e-4);
        end
    end 
end