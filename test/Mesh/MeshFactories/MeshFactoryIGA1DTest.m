%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% testing of class MeshFactory2DUniform

classdef MeshFactoryIGA1DTest < TestCase
    
       
    methods
        %% constructor
        function obj = MeshFactoryIGA1DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            Mat1D = Hooke1D(1,1,1,1);
            Mat1D = [Mat1D Mat1D]; % to make it work with FCM
            PolynomialDegree = 3;
            NumberGP = PolynomialDegree+1;
            DofDimension = 1;
            NumberOfXDivisions = 3;
            
          ControlPoints = [0 0 0;
                           1 0 0];

            curve = BSplineCurve( [-1 -1 1 1], 1, ControlPoints );
            knots = linspace(-1, 1, NumberOfXDivisions+1);
            curve.elevateOrder( PolynomialDegree-1 );
            curve.insertKnots( knots(2:length(knots)-1) );

            MyElementFactory = ElementFactoryElasticExtractedBSplineBar(Mat1D,NumberGP, curve.getKnotsXi(), curve.getPolynomialDegreeXi);
            MyGeometryFactory = ExtractedBSplineCurveFactory( curve );
            obj.MeshFactory = MeshFactoryIGA1D( curve, DofDimension, MyElementFactory, MyGeometryFactory);
            
            obj.Elements = obj.MeshFactory.createElements();
            obj.Edges = obj.MeshFactory.getEdges();
            
            obj.Nodes = [ ...
                0 0 0;
                1/3 0 0;
                2/3 0 0;
                1 0 0
            ];
        
        end
        
        function tearDown(obj)
        end
        
               
        %% test createEdges
        function testGetEdges(obj)
            
            function testEdgeExists(startCoord, endCoord)
                found = 0;
                for edge = obj.Edges
                    edgeStart = edge.mapLocalToGlobal([-1 0 0]);
                    edgeEnd = edge.mapLocalToGlobal([1 0 0]);
                    
                    if min(isClose(edgeStart, startCoord)) &&  min(isClose(edgeEnd, endCoord))
                        found = 1;
                        break;
                    end
                end
                
                assertEqual(found, 1);
            end
                        
            testEdgeExists(obj.Nodes(1,:), obj.Nodes(2,:));
            testEdgeExists(obj.Nodes(2,:), obj.Nodes(3,:));
            testEdgeExists(obj.Nodes(3,:), obj.Nodes(4,:));
        end
                
        %% test createElements
        function testCreateElements(obj)
            % Check size of elements
            assertEqual( length(obj.Elements), 3 );
                        
            function checkElement( element, nodes )
                
                edge = element.getEdges;
                edgeStart = edge.mapLocalToGlobal([-1 0 0]);
                edgeEnd = edge.mapLocalToGlobal([1 0 0]);
                assertEqual( all(isClose(edgeStart, nodes(1,:))) &&  all(isClose(edgeEnd, nodes(2,:))), true);
                
            end
            
            checkElement( obj.Elements(1), obj.Nodes(1:2,:) );
            checkElement( obj.Elements(2), obj.Nodes(2:3,:) );
            checkElement( obj.Elements(3), obj.Nodes(3:4,:) );
            
        end
        
    end
    
    properties
       MeshFactory
       Elements
       Edges
       
       Nodes
    end
end

