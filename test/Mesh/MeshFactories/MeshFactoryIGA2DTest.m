%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% testing of class MeshFactory2DUniform

classdef MeshFactoryIGA2DTest < TestCase
    
       
    methods
        %% constructor
        function obj = MeshFactoryIGA2DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            Material = HookePlaneStrain(1,0.5,7,1);
            IntegrationOrder = 4;
            DofDimension = 2;
            
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            geometryDescription.removeWeights();
            
            BSplineSurface = ExtractedBSplineSurface(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            
            geometryDescription.insertKnots( 0,  0 );
            geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );
            elementFactory = ElementFactoryElasticExtractedBSplineSurface( Material, IntegrationOrder, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta ); 
            
            % Creating MeshFactory 2D
            obj.MeshFactory = MeshFactoryIGA2D( geometryDescription, DofDimension, elementFactory, geometryFactory);
            obj.Elements = obj.MeshFactory.createElements();
            obj.Edges = obj.MeshFactory.getEdges();
            
            nodes = [ ...
                0 1 0;
                3/4 3/4 0;
                1 0 0;
                
                0 7/4 0;
                21/16 21/16 0;
                7/4 0 0;
                
                0 2.5 0;
                15/8 15/8 0;
                2.5 0 0;
            ];
        
            obj.Nodes = reshape(nodes',3,3,3);
        end
        
        function tearDown(obj)
        end
        
               
        %% test createEdges
        function testGetEdges(obj)
            
            function testEdgeExists(startCoord, endCoord)
                found = 0;
                for iEdge = 1:numel(obj.Edges)
                    edge = obj.Edges{iEdge};
                    edgeStart = edge.mapLocalToGlobal([-1 0 0]);
                    edgeEnd = edge.mapLocalToGlobal([1 0 0]);
                    
                    if min(isClose(edgeStart, startCoord)) &&  min(isClose(edgeEnd, endCoord))
                        found = 1;
                        break;
                    end
                end
                
                assertEqual(found, 1);
            end
                        
            testEdgeExists(obj.Nodes(:,1,3)', obj.Nodes(:,2,3)');
            testEdgeExists(obj.Nodes(:,2,3)', obj.Nodes(:,3,3)');
            
            testEdgeExists(obj.Nodes(:,1,2)', obj.Nodes(:,2,2)');
            testEdgeExists(obj.Nodes(:,2,2)', obj.Nodes(:,3,2)');
            
            testEdgeExists(obj.Nodes(:,1,1)', obj.Nodes(:,2,1)');
            testEdgeExists(obj.Nodes(:,2,1)', obj.Nodes(:,3,1)');
            
            
            testEdgeExists(obj.Nodes(:,1,1)', obj.Nodes(:,1,2)');
            testEdgeExists(obj.Nodes(:,1,2)', obj.Nodes(:,1,3)');
            
            testEdgeExists(obj.Nodes(:,2,1)', obj.Nodes(:,2,2)');
            testEdgeExists(obj.Nodes(:,2,2)', obj.Nodes(:,2,3)');
            
            testEdgeExists(obj.Nodes(:,3,1)', obj.Nodes(:,3,2)');
            testEdgeExists(obj.Nodes(:,3,2)', obj.Nodes(:,3,3)');
        end
                
        %% test createElements
        function testCreateElements(obj)
            % Check size of elements
            assertEqual( length(obj.Elements), 4 );
            
            function testEdgeExists(element, startCoord, endCoord)
                found = 0;
                for edge = element.getEdges
                    edgeStart = edge.mapLocalToGlobal([-1 0 0]);
                    edgeEnd = edge.mapLocalToGlobal([1 0 0]);
                    
                    if min(isClose(edgeStart, startCoord)) &&  min(isClose(edgeEnd, endCoord))
                        found = 1;
                        break;
                    end
                end
                
                assertEqual(found, 1);
            end
            
            function checkElement( element, nodes )
                testEdgeExists(element, nodes(:,1,1)', nodes(:,2,1)');
                testEdgeExists(element, nodes(:,1,2)', nodes(:,2,2)');
                
                testEdgeExists(element, nodes(:,1,1)', nodes(:,1,2)');
                testEdgeExists(element, nodes(:,2,1)', nodes(:,2,2)');
            end
            
            checkElement( obj.Elements(1), obj.Nodes(:,1:2,1:2) );
            checkElement( obj.Elements(2), obj.Nodes(:,2:3,1:2) );
            checkElement( obj.Elements(3), obj.Nodes(:,1:2,2:3) );
            checkElement( obj.Elements(4), obj.Nodes(:,2:3,2:3) );   
        end
        
    end
    
    properties
       MeshFactory
       Elements
       Edges
       
       Nodes
    end
end

