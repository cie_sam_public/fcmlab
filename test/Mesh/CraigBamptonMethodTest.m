%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% testing the Craig Bampton Method 
%% comparing standard FCM and FCM using Craig Bampton Method

classdef CraigBamptonMethodTest < TestCase
    
    properties
        SolutionVectors
        InternalSolutionVectorCraigBamptonMethod
    end
    
    methods
        
        %% constructor
        function obj = CraigBamptonMethodTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
        %% Standard FCM
            %% Embedded Domain Setup
            
            Origin = [0.0 0.0 0.0];
            Lengths = [1.0 1.0];
            
            %Create the domain
            Rectangle = EmbeddedRectangle( Origin, Lengths);
            
            %% Material Setup
            YoungsModulus = 1.0;
            PoissonsRatio = 0.0;
            Density = 1.0;
            Alpha = 0;
            
            % Material for the void (background) domain
            Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            PolynomialDegree = 1;
            NumberOfGaussPoints = PolynomialDegree+1;
            SpaceTreeDepth = 1;
            
            ElementFactory = ElementFactoryElasticQuadFCM( Materials, Rectangle, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 1.5;
            MeshLengthY = 1;
            NumberOfXDivisions = 2;
            NumberOfYDivisions = 1;
            DofDimension = 2;
            
            MeshFactory = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory);
            
            %% Analysis Setup
            
            numberOfTimeSteps= 1;
            timeState = TimeState( 0.0 );
            endTime= 1.0;
            dt = endTime/numberOfTimeSteps;
            initialcondition=[0 0 0];
            Analysis = NewmarkWaveEquation( MeshFactory,numberOfTimeSteps,endTime,initialcondition,timeState );
            
            %% Apply weak Dirichlet boundary conditions
            IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of boundary
            StartPoint = [0.0 0.0 0.0];
            EndPoint = [0.0 1.0 0.0];
            NumberOfBoundarySegments = 1;
            BoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
            Boundary = BoundaryFactory.getBoundary();
            
            % Selet constraining strategy
            PenaltyValue = 1e5;
            ConstrainingAlgorithm = WeakPenaltyAlgorithm( PenaltyValue );
            
            % Give analytical description of boundary values
            ux = @(x,y,z) 0;
            uy = @(x,y,z,t) 0.1;
            uy  = SpaceTimeFunction(timeState,uy);
            
            % Create boundary condition in X
            BoundaryConditionXMeshId = 1;
            ConstrainedDirections = [1 0];
            BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId = 1;
            ConstrainedDirections = [0 1];
            BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Register conditions at analysis
            Analysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );
            Analysis.addDirichletBoundaryCondition( BoundaryConditionY, BoundaryConditionYMeshId );
            
            %% Add load case
            
            loadCase = LoadCase();
            Analysis.addLoadCases(loadCase);
            
            %% Run the analysis
            
            obj.SolutionVectors = Analysis.solve();
            
        %% Craig Bampton Method
            %% Embedded Domain Setup
            
            OriginCraigBamptonMethod = [0.0 0.0 0.0];
            LengthsCraigBamptonMethod = [1.0 1.0];
            
            %Create the domain
            RectangleCraigBamptonMethod = EmbeddedRectangle( OriginCraigBamptonMethod, LengthsCraigBamptonMethod);
            
            %% Material Setup
            
            % Material for the void (background) domain
            MaterialsCraigBamptonMethod(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            MaterialsCraigBamptonMethod(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            
            ElementFactoryCraigBamptonMethod = ElementFactoryElasticQuadFCM( MaterialsCraigBamptonMethod, RectangleCraigBamptonMethod, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            
            NumberOfInterfaceDivisions = 1; 
            NumberOfModes = 10;
            InterfaceFactory = StraightLineBoundaryFactory( [0.0 1.0 0.0], [0.0 0.0 0.0], NumberOfInterfaceDivisions);
            InterfaceGeometry = InterfaceFactory.getBoundary();
            
            InternalMeshFactoryCraigBamptonMethod = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactoryCraigBamptonMethod);
            MeshFactoryCraigBamptonMethod = MeshFactory2DUniformCraigBamptonMethod(PolynomialDegree,TopologicalSorting,DofDimension, ...
                InternalMeshFactoryCraigBamptonMethod, InterfaceGeometry, NumberOfModes, PenaltyValue);
            
            %% Analysis Setup
            numberOfTimeSteps= 1;
            timeState = TimeState( 0.0 );
            endTime= 1.0;
            dt = endTime/numberOfTimeSteps;
            initialcondition=[0 0 0];
            AnalysisCraigBamptonMethod = NewmarkWaveEquation( MeshFactoryCraigBamptonMethod,numberOfTimeSteps,endTime,initialcondition,timeState );
            
            %% Apply weak Dirichlet boundary conditions
            
            % Get geometrical description of boundary
            StartPoint = [0.0 1.0 0.0];
            EndPoint = [0.0 0.0 0.0];
            NumberOfBoundarySegments = 1;
            BoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
            Boundary = BoundaryFactory.getBoundary();
            
            % Selet constraining strategy
            ConstrainingAlgorithm = WeakPenaltyAlgorithm( PenaltyValue );
            
            % Give analytical description of boundary values
            ux = @(x,y,z) 0;
            uy = @(x,y,z,t) 0.1;
            uy  = SpaceTimeFunction(timeState,uy);
            
            % Create boundary condition in X
            BoundaryConditionXMeshId = 1;
            ConstrainedDirections = [1 0];
            BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId = 1;
            ConstrainedDirections = [0 1];
            BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Register conditions at analysis
            AnalysisCraigBamptonMethod.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );
            AnalysisCraigBamptonMethod.addDirichletBoundaryCondition( BoundaryConditionY, BoundaryConditionYMeshId );
            
            %% Add load case
            
            loadCase = LoadCase();
            AnalysisCraigBamptonMethod.addLoadCases(loadCase);
            
            %% Run the analysis
            
            solutionVectorsCraigBamptonMethod = AnalysisCraigBamptonMethod.solve();
            
            MeshCraigBamptonMethod = AnalysisCraigBamptonMethod.getMesh();
            InternalMeshCraigBamptonMethod = MeshCraigBamptonMethod.getInternalMesh;
            
            TransformationMatrix = MeshCraigBamptonMethod.getTransformationMatrix();
            obj.InternalSolutionVectorCraigBamptonMethod = TransformationMatrix(1:InternalMeshCraigBamptonMethod.getNumberOfDofs,:)* ...
                solutionVectorsCraigBamptonMethod(MeshCraigBamptonMethod.getFirstReducedDof:MeshCraigBamptonMethod.getFirstReducedDof+MeshCraigBamptonMethod.getNumberOfDofs-1,:);
            
        end
        
        function testSolutionVectors(obj)
            
            assertVectorsAlmostEqual(obj.SolutionVectors, obj.InternalSolutionVectorCraigBamptonMethod, 'absolute', 0.0001)
            
        end
    end
end