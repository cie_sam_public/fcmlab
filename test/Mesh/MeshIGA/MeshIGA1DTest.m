%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Testing the Mesh class
classdef MeshIGA1DTest < TestCase
   
    properties
        MyMesh
    end
    
    methods
        %% Constructor
        function obj = MeshIGA1DTest(name)
            obj = obj@TestCase(name);        
        end
        
        %% Set Up
        function setUp(obj)
            Mat1D = Hooke1D(1,1,1,1);
            Mat1D = [Mat1D Mat1D]; % to make it work with FCM
            PolynomialDegree = 3;
            NumberGP = PolynomialDegree+1;
            DofDimension = 1;
            NumberOfXDivisions = 3;
            
          ControlPoints = [0 0 0;
                           1 0 0];

            curve = BSplineCurve( [-1 -1 1 1], 1, ControlPoints );
            knots = linspace(-1, 1, NumberOfXDivisions+1);
            curve.elevateOrder( PolynomialDegree-1 );
            curve.insertKnots( knots(2:length(knots)-1) );

            MyElementFactory = ElementFactoryElasticExtractedBSplineBar(Mat1D,NumberGP, curve.getKnotsXi(), curve.getPolynomialDegreeXi);
            MyGeometryFactory = ExtractedBSplineCurveFactory( curve );
            MyMeshFactory = MeshFactoryIGA1D( curve, DofDimension, MyElementFactory, MyGeometryFactory);
            
            obj.MyMesh = MeshIGA(MyMeshFactory);
        end
        
        %% Tear Down
        function tearDown(obj)
        end
        
        %% Testing get number of Dofs
        function testNumberOfDofs(obj)
            assertEqual(obj.MyMesh.getNumberOfDofs, 6);
        end
        
        
        %% Testing get edges
        function testGetEdges(obj)
            assertEqual( length(obj.MyMesh.getEdges), 3);
        end
        
        %% Testing get elements
        function testGetElements(obj)
            assertEqual( length(obj.MyMesh.getElements), 3);
            assertEqual( obj.MyMesh.getNumberOfElements, 3);
        end
                
    end
    
end