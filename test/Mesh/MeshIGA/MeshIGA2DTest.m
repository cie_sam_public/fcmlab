%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% Testing the Mesh class
classdef MeshIGA2DTest < TestCase
   
    properties
        MyMesh
    end
    
    methods
        %% Constructor
        function obj = MeshIGA2DTest(name)
            obj = obj@TestCase(name);        
        end
        
        %% Set Up
        function setUp(obj)
            Material = HookePlaneStrain(1,0.5,7,1);
            IntegrationOrder = 4;
            DofDimension = 2;
            
            geometryDescription = GenerateQuarterDisk(1, 2.5);
                    
           
            %BSplineSurface = ExtractedNURBSSurface(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            
            geometryDescription.insertKnots( 0,  0 );
            
             ControlPoints = geometryDescription.getControlPoints;
            geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );
            elementFactory = ElementFactoryElasticExtractedNURBSurface( Material, IntegrationOrder, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta, ControlPoints(:,4) ); 
            
            % Creating MeshFactory 2D
            MeshFactory = MeshFactoryIGA2D( geometryDescription, DofDimension, elementFactory, geometryFactory);
            obj.MyMesh = MeshIGA(MeshFactory);
        end
        
        %% Tear Down
        function tearDown(obj)
        end
        
        %% Testing get number of Dofs
        function testNumberOfDofs(obj)
            assertEqual(obj.MyMesh.getNumberOfDofs, 16*2);
        end
        
        
        %% Testing get edges
        function testGetEdges(obj)
            assertEqual( length(obj.MyMesh.getEdges), 4*4);
        end
        
        %% Testing get elements
        function testGetElements(obj)
            assertEqual( length(obj.MyMesh.getElements), 4);
            assertEqual( obj.MyMesh.getNumberOfElements, 4);
        end
        
        function testFindEdges(obj)
            OuterRadius = 2.5;
            outerControlPointsFilter=@(ControlPoints)( ControlPoints(:,1).^2 + ControlPoints(:,2).^2 >= OuterRadius^2 -1e-9 );
            edgesFound = obj.MyMesh.findEdges(outerControlPointsFilter);
            
            assertEqual( length(edgesFound), 2);
        end
        
    end
    
end