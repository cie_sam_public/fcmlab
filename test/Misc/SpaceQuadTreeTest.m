%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% testing of class SpaceQuadTree

classdef SpaceQuadTreeTest < TestCase
    
    properties
        realGeometry
        quadTree
        EmbedDomain
    end
    
    methods
        %% constructor
        function obj = SpaceQuadTreeTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            obj.EmbedDomain = AnnularPlate(0, 0.7, 0.5);
            obj.realGeometry = Rectangle([...
                Vertex([-1, -1.0,0.0]),...
                Vertex([ 1.0,-1.0,0.0]),...
                Vertex([ 1.0, 1.0,0.0]),...
                Vertex([-1.0, 1.0,0.0])],[]);
            
            obj.quadTree= SpaceQuadTree(obj.EmbedDomain, obj.realGeometry);

            obj.quadTree.partition(4,5)
            obj.quadTree.createOrientedBoundingBoxes(1.0)
        end
        
        %% test getCompleteSeedPointCloud
        function testgetCompleteSeedPointCloud(obj)
            assertEqual(length(obj.quadTree.getCompleteSeedPointCloud()),3400);
        end
        %% test getAllIndexGeometries
        function getAllIndexGeometries(obj)
            assertEqual(length(obj.quadTree.getAllIndexGeometries()),180);
        end
        
        %% test getIndexGeometry
        function testgetIndexGeometry(obj)
            assertEqual(obj.quadTree.getIndexGeometry.getVertices.getCoords,[-1 -1 0]);
        end
        
        %% test getAllOBBs
        function testgetAllOBBs(obj)
            assertEqual(length(obj.quadTree.getAllOBBs()),4);
        end
    end
    
end