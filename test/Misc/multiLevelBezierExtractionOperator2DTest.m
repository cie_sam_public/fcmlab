%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing getLegendrePolynomial function

classdef multiLevelBezierExtractionOperator2DTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = multiLevelBezierExtractionOperator2DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            Levels = ones(8,8);
            Levels(3:8,3:8) = 2;
            
            PolynomialDegree=2;
            
            KnotVectorXi{1} = [ 0 0 linspace(0,1,5) 1 1];
            KnotVectorXi{2} = [ 0 0 linspace(0,1,9) 1 1];
            
            KnotVectorEta{1} = [ 0 0 linspace(0,1,5) 1 1];
            KnotVectorEta{2} = [ 0 0 linspace(0,1,9) 1 1];
            
            
           geometryDescription1 = BSplineSurface( KnotVectorXi{1}, PolynomialDegree, KnotVectorEta{1}, PolynomialDegree, [] );
           geometryDescription2 = BSplineSurface( KnotVectorXi{2}, PolynomialDegree, KnotVectorEta{2}, PolynomialDegree, [] );          
                       
            multiLeverBezierExtractionOperators = multiLevelBezierExtractionOperator2D(  Levels,  {geometryDescription1, geometryDescription2} );
        end
        
        function tearDown(obj)
        end
        
        function  testDegreeNoRepeatedKnot(obj)

            
        end
        
      

    end
    
end

