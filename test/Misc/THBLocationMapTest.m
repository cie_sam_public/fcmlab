%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% testing getLegendrePolynomial function

classdef THBLocationMapTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = THBLocationMapTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
        end
        
        function tearDown(obj)
        end
        
        function  testDegreeNoRepeatedKnot(obj)
            
            
 Levels = ones(8,4);
 Levels(3:6,3:4) = 2;
  
 KnotsXi=[-1.0000   -1.0000   -1.0000   -0.5000         0         0    0.5000    1.0000    1.0000    1.0000];
 KnotsEta=[-1    -1    -1     0     1     1     1];
 PolynomialDegreeXi=2;
 PolynomialDegreeEta=2;
 geometryDescription1 = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, [] );
 
  KnotsXi=[-1.0000   -1.0000   -1.0000   -0.7500   -0.5000   -0.2500         0         0    0.2500    0.5000    0.7500    1.0000    1.0000    1.0000];
 KnotsEta=[-1.0000   -1.0000   -1.0000   -0.5000         0    0.5000    1.0000    1.0000    1.0000];
 PolynomialDegreeXi=2;
 PolynomialDegreeEta=2;
 geometryDescription2 = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, [] );
 
 
mlGeometry{1} = geometryDescription1;
mlGeometry{2} = geometryDescription2;
            
stack = StackOfBSplineSurfaces( mlGeometry, [], Levels );
            LM = THBLocationMap( stack, 1 );
            
            LMTest14 = [
                1 2 3 8 9 10 15 16 17; 
                2 3 4 9 10 11 16 17 18;
                4 5 6 11 12 13 18 19 20;
                ] ;
            
            assert( all(all( LM(1:3,1:length(LMTest14)) == LMTest14) ));
            
            LMTest6 = [12 13 14 19 20 21 25 26 27];
            assert( all(all( LM(6,1:length(LMTest6)) == LMTest6) ));
            
        end
        
        
    end
    
end

