%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% testing getLegendrePolynomial function

classdef getRefinementDataStructuresTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = getRefinementDataStructuresTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
        end
        
        function tearDown(obj)
        end
        
        function  testDegreeNoRepeatedKnot(obj)
            
            %             levelsCenter = 2 * ones(4,4);
            %             %levelsCenter(3:4,3:4) = 3 * ones(2,2);
            %             %levelsCenter(1:2,1:2) = 3 * ones(2,2);
            %
            %             %levels = ones(28,28);
            %             %levels(13:16,13:16) = levelsCenter;
            %
            %             levels = ones(12,12);
            %             levels(5:8,5:8) = levelsCenter;
            %
            %             flip(levels')
            
            
            levels = ones(8,8);
            levels(3:6,3:6) = 2;
            
                        PolynomialDegree=2;
            
            KnotVectorXi{1} = [ 0 0 linspace(0,1,5) 1 1];
            KnotVectorXi{2} = [ 0 0 linspace(0,1,9) 1 1];
            
            KnotVectorEta{1} = [ 0 0 linspace(0,1,5) 1 1];
            KnotVectorEta{2} = [ 0 0 linspace(0,1,9) 1 1];
            
            
           geometryDescription1 = BSplineSurface( KnotVectorXi{1}, PolynomialDegree, KnotVectorEta{1}, PolynomialDegree, [] );
           geometryDescription2 = BSplineSurface( KnotVectorXi{2}, PolynomialDegree, KnotVectorEta{2}, PolynomialDegree, [] ); 
                       
            [Aa, Am, Ap, Ea] = getRefinementDataStructures2D(levels,{geometryDescription1, geometryDescription2});
            
            Ap1Test = zeros(6,6);
            Ap1Test(2:5,2:5) = 1;

            Ea1Test = ones(4,4);
            Ea1Test(2:3,2:3) = 0;
            
            assertEqual( Aa{1}, ones(6,6));
            assertEqual( Am{1}, zeros(6,6));
            assertEqual( Ap{1}, Ap1Test);
            assertEqual( Ea{1}, Ea1Test);
            
            Am2Test = zeros(10,10);
            Am2Test(3:4,3:8) = 1;
            Am2Test(3:8,3:4) = 1;
            
            Am2Test(5:8,7:8) = 1;
            Am2Test(7:8,5:8) = 1;
            
            Ea2Test = zeros(8,8);
            Ea2Test(3:6,3:6) = 1;
            
            assertEqual( Am{2}, Am2Test);
            assertEqual( Ap{2},  zeros(10,10));
            assertEqual( Aa{2}(5:6,5:6), ones(2,2) );
            assertEqual( Ea{2}, Ea2Test);
            
            
        end
        
        
    end
    
end

