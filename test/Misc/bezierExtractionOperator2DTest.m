%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing getLegendrePolynomial function

classdef bezierExtractionOperator2DTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = bezierExtractionOperator2DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
        end
        
        function tearDown(obj)
        end
        
        function  testTensorProductDegree2(obj)

            C1 = [ ...
    1.0000         0         0         0         0         0; ...
         0    1.0000    0.5000         0         0         0; ...
         0         0    0.5000         0         0         0; ...
         0         0         0    1.0000         0         0; ...
         0         0         0         0    1.0000    0.5000; ...
         0         0         0         0         0    0.5000 ];


 C2 = [ ...

    0.5000         0         0         0         0         0; ...
    0.5000    1.0000         0         0         0         0; ...
         0         0    1.0000         0         0         0; ...
         0         0         0    0.5000         0         0; ...
         0         0         0    0.5000    1.0000         0; ...
         0         0         0         0         0    1.0000];

 C3 = [ ...

    1.0000         0         0         0         0         0; ...
         0    1.0000    0.5000         0         0         0; ...
         0         0    0.5000         0         0         0; ...
         0         0         0    1.0000         0         0; ...
         0         0         0         0    1.0000    0.5000; ...
         0         0         0         0         0    0.5000];


 C4 = [ ...

    0.5000         0         0         0         0         0; ...
    0.5000    1.0000         0         0         0         0; ...
         0         0    1.0000         0         0         0; ...
         0         0         0    0.5000         0         0; ...
         0         0         0    0.5000    1.0000         0; ...
         0         0         0         0         0    1.0000];

     
     C = bezierExtractionOperator2D([-1 -1 -1 0 1 1 1], 2, [-1 -1 0 1 1],1);
     
     assertEqual(size(C), [6 6 4]);
     
     assertElementsAlmostEqual(C(:,:,1),C1);
     assertElementsAlmostEqual(C(:,:,2),C2);
     assertElementsAlmostEqual(C(:,:,3),C3);
     assertElementsAlmostEqual(C(:,:,4),C4);
     

        end
        
        

        
        

    end
    
end

