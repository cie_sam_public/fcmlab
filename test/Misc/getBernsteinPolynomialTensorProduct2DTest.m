%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing getLegendrePolynomial function

classdef getBernsteinPolynomialTensorProduct2DTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = getBernsteinPolynomialTensorProduct2DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
        end
        
        function tearDown(obj)
        end
        
        function  testTensorProductDegree2(obj)
            LocalCoords = [0.5 -0.3 0];
            
            DegreeXi = 2;
            DegreeEta = 2;
            ShapeIndicesXi = 1:3;
            ShapeIndicesEta = 1:3;
            
            rightTensorProduct = [0.02640625  0.1584375  0.23765625   0.0284375   0.170625   0.2559375   0.00765625 0.0459375  0.06890625];
            
           assertElementsAlmostEqual( getBernsteinPolynomialTensorProduct2D( LocalCoords, ShapeIndicesXi, DegreeXi, ShapeIndicesEta,  DegreeEta, 0 ), rightTensorProduct )
        end
        
        
        function  testTensorProductDerivativeDegree2(obj)
            LocalCoords = [0.5 -0.3 0];
            
            DegreeXi = 2;
            DegreeEta = 2;
            ShapeIndicesXi = 1:3;
            ShapeIndicesEta = 1:3;
            
            rightTensorProduct = [...
                                   0.02640625   0.1584375  0.23765625  0.0284375   0.170625   0.2559375   0.00765625  0.0459375   0.06890625; ...
                                  -0.105625  -0.211250   0.316875  -0.113750  -0.227500   0.341250  -0.030625 -0.06125   0.091875; ...
                                  -0.040625  -0.243750  -0.365625   0.018750   0.112500   0.168750   0.021875  0.13125   0.196875 ...
                                  ];
            
           assertElementsAlmostEqual( getBernsteinPolynomialTensorProduct2D( LocalCoords, ShapeIndicesXi, DegreeXi, ShapeIndicesEta,  DegreeEta, 1 ), rightTensorProduct )
        end
        
        
        

    end
    
end

