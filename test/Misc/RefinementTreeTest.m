%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
classdef RefinementTreeTest < TestCase
    
    properties
        root
        root2
    end
    
    methods
        %% constructor
        function obj = RefinementTreeTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            obj.root = RefinementNode2D(0, [0 0], [10 10], 1, 0);
            obj.root.split([2 3], 2*ones(1,4));
            
            obj.root2 = RefinementNode2D(0, [0 0], [10 10], 1, 0);
            obj.root2.split([2 3], 2*ones(1,4));
            obj.root2.split([4 5], [1 2 3 4]);
            obj.root2.split([6 2], [3 4 11 1]);
            obj.root2.split([3 2.5], [2 2 3 4]);
        end
        
        function testIsLeaf(obj)
            assertEqual( obj.root.isLeaf, false)
        end
        
        function testFindSw(obj)
            sw = obj.root.find([1 2]);
            assertEqual( sw.keyStart, [ 0 0])
            assertEqual( sw.keyEnd, [ 2 3])
        end        
        function testFindNw(obj)
            nw = obj.root.find([2 4]);
            assertEqual( nw.keyStart, [ 0 3 ])
            assertEqual( nw.keyEnd, [ 2 10 ])
        end        
        function testFindSe(obj)
            se = obj.root.find([6 1]);
            assertEqual( se.keyStart, [ 2 0])
            assertEqual( se.keyEnd, [ 10 3])
        end        
        function testFindNe(obj)
            ne = obj.root.find([9 7]);
            assertEqual( ne.keyStart, [ 2 3])
            assertEqual( ne.keyEnd, [ 10 10])
        end
        
        function testFind2(obj)
            ne = obj.root2.find([3 4]);
            assertEqual( ne.keyStart, [ 2 3])
            assertEqual( ne.keyEnd, [ 4 5])
        end
        
        function testFind22(obj)
            ne = obj.root2.find([2.75 2.75]);
            assertEqual( ne.keyStart, [2 2.5])
            assertEqual( ne.keyEnd, [ 3 3])
        end
        
        function testgetLevelOfLeaves(obj)
            levelOfLeaves = obj.root.getLevelOfLeaves;
            assertEqual( levelOfLeaves, 2*ones(1,4))
        end
        
        function testgetLevelOfLeaves2(obj)
            levelOfLeaves = obj.root2.getLevelOfLeaves;
            assertEqual( levelOfLeaves, [ 2 3 4 2 2 3 4 1 2 1 2 3 4  ] )
        end
        
        function testlevelsInBox(obj)
            levelOfLeaves = obj.root.levelsInBox([0 0], [10 10]);
            assertEqual( levelOfLeaves, [2 2 2 2] )
        end
        
        function testlevelsInBox2(obj)
            levelOfLeaves = obj.root2.levelsInBox([0 0 ], [2 3]);
            assertEqual( levelOfLeaves, 2 )
        end
        
        function testlevelsInBox22(obj)
            levelOfLeaves = obj.root2.levelsInBox([2 3], [4 5]);
            assertEqual( levelOfLeaves, 1 )
        end
        function testlevelsInBox23(obj)
            levelOfLeaves = obj.root2.levelsInBox([0 0], [2 10]);
            assertEqual( levelOfLeaves, [2 2] )
        end
        
        function testlevelsInBox24(obj)
            levelOfLeaves = obj.root2.levelsInBox([0 3], [10 10]);
            assertEqual( levelOfLeaves, [ 2 1 2 3 4 ] )
        end
        
        function testlevelsInBox25(obj)
            levelOfLeaves = obj.root2.levelsInBox([2 2.5], [3 3]);
            assertEqual( levelOfLeaves, 3 )
        end
        
        function testlevelsInBox26(obj)
            levelOfLeaves = obj.root2.levelsInBox([0 0], [10 10]);
            assertEqual( levelOfLeaves, [ 2 3 4 2 2 3 4 1 2 1 2 3 4 ]  )
        end
        
        function testlevelsInBox27(obj)
            levelOfLeaves = obj.root2.levelsInBox([2 0], [10 4]);
            assertEqual( levelOfLeaves, [ 3 4 2 2 3 4 1 1 2 ]  )
        end
        
        function testlevelsInBox28(obj)
            levelOfLeaves = obj.root2.levelsInBox([2.25 2.25], [7.11 3.001]);
            assertEqual( levelOfLeaves, [ 2 2 3 4 1 1 2 ]  )
        end
        
    end
    
end

