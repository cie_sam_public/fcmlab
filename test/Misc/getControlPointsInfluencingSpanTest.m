%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing getLegendrePolynomial function

classdef getControlPointsInfluencingSpanTest < TestCase
    
    
    methods
        
        %% constructor
        function obj = getControlPointsInfluencingSpanTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
        end
        
        function tearDown(obj)
        end
        
        function  testDegree2(obj)
            
            PolynomialDegreeXi = 2;
            
            KnotsXi = [-1 -1 -1 -0.5 0 0.5 1 1 1];
            assertEqual( getControlPointsInfluencingSpan( 1, KnotsXi, PolynomialDegreeXi ), [1 2 3] );
            assertEqual( getControlPointsInfluencingSpan( 2, KnotsXi, PolynomialDegreeXi ), [2 3 4] );
            assertEqual( getControlPointsInfluencingSpan( 3, KnotsXi, PolynomialDegreeXi ), [3 4 5] );
            assertEqual( getControlPointsInfluencingSpan( 4, KnotsXi, PolynomialDegreeXi ), [4 5 6] );
            
            KnotsXi = [-1 -1 -1 -0.5 0 0 0.5 1 1 1];
            assertEqual( getControlPointsInfluencingSpan( 1, KnotsXi, PolynomialDegreeXi ), [1 2 3] );
            assertEqual( getControlPointsInfluencingSpan( 2, KnotsXi, PolynomialDegreeXi ), [2 3 4] );
            assertEqual( getControlPointsInfluencingSpan( 3, KnotsXi, PolynomialDegreeXi ), [4 5 6] );
            
        end
        
        function  testDegree3(obj)
            
            PolynomialDegreeXi = 3;
            
            KnotsXi = [-1 -1 -1 -1 -0.5 0 0 0 0.5 1 1 1 1];
            assertEqual( getControlPointsInfluencingSpan( 1, KnotsXi, PolynomialDegreeXi ), [1 2 3 4] );
            assertEqual( getControlPointsInfluencingSpan( 2, KnotsXi, PolynomialDegreeXi ), [2 3 4 5 ] );
            assertEqual( getControlPointsInfluencingSpan( 3, KnotsXi, PolynomialDegreeXi ), [ 5 6 7 8] );
            assertEqual( getControlPointsInfluencingSpan( 4, KnotsXi, PolynomialDegreeXi ), [6 7 8 9] );
            
        end

    end
    
end

