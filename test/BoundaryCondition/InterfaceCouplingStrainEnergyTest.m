%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% testing interface coupling of two rectangles
classdef InterfaceCouplingStrainEnergyTest < TestCase
    
    properties
        testAnalysis
    end
    
    methods
        
        %% constructor
        function obj = InterfaceCouplingStrainEnergyTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            %% First Mesh
            %% Embedded Domain Setup
            
            Origin = [0.0 0.0 0.0];
            Lengths = [0.5 1.0];
            
            %Create the domain
            Rectangle = EmbeddedRectangle( Origin, Lengths);
            
            %% Material Setup
            YoungsModulus = 1.0;
            PoissonsRatio = 0.3;
            Density = 1.0;
            Alpha = 0;
            
            % Material for the void (background) domain
            Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            PolynomialDegree = 5;
            NumberOfGaussPoints = PolynomialDegree+1;
            SpaceTreeDepth = 1;
            
            ElementFactory = ElementFactoryElasticQuadFCM( Materials, Rectangle, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 0.5;
            MeshLengthY = 1.0;
            NumberOfXDivisions = 1;
            NumberOfYDivisions = 2;
            DofDimension = 2;
            
            MeshFactories{1} = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory);
            
            %% Second Mesh
            %% Embedded Domain Setup
            
            Origin2 = [0.5 0.0 0.0];
            Lengths2 = [0.5 1.0];
            
            %Create the domain
            Rectangle2 = EmbeddedRectangle( Origin2, Lengths2);
            
            %% Material Setup
            YoungsModulus2 = 1.0;
            PoissonsRatio2 = 0.3;
            Density2 = 1.0;
            Alpha2 = 0;
            
            % Material for the void (background) domain
            Materials2(1) = HookePlaneStress( YoungsModulus2, PoissonsRatio2, Density2, Alpha2);
            
            % Material for the actual domain
            Materials2(2) = HookePlaneStress( YoungsModulus2, PoissonsRatio2, Density2, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            PolynomialDegree2 = 5;
            NumberOfGaussPoints2 = PolynomialDegree2+1;
            SpaceTreeDepth2 = 1;
            
            ElementFactory2 = ElementFactoryElasticQuadFCM( Materials2, Rectangle2, ...
                NumberOfGaussPoints2, SpaceTreeDepth2 );
            
            % Step 2: create the mesh
            MeshOrigin2 = [0.5 0.0 0.0];
            MeshLengthX2 = 0.5;
            MeshLengthY2 = 1.0;
            NumberOfXDivisions2 = 1;
            NumberOfYDivisions2 = 2;
            DofDimension2 = 2;
            
            MeshFactories{2} = MeshFactory2DUniform( NumberOfXDivisions2, ...
                NumberOfYDivisions2, PolynomialDegree2, TopologicalSorting, ...
                DofDimension2, MeshOrigin2, MeshLengthX2, MeshLengthY2, ElementFactory2);
            
            %% Analysis Setup
            
            % Decide on anlysis type
            obj.testAnalysis = QuasiStaticAnalysis( MeshFactories);
            
            %% Points
            
            BottomLeft = [ 0.0 0.0 0.0 ];
            BottomMiddle = [ 0.5 0.0 0.0 ];
            BottomRight = [ 1.0 0.0 0.0 ];
            TopLeft = [ 0.0 1.0 0.0 ];
            TopMiddle = [ 0.5 1.0 0.0 ];
            TopRight = [ 1.0 1.0 0.0 ];
            
            %% Coupling
            
            % Select integration scheme
            IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of interface
            NumberOfBoundarySegments = 3;
            InterfaceBoundaryFactory = StraightLineBoundaryFactory( BottomMiddle, TopMiddle, NumberOfBoundarySegments);
            InterfaceGeometry = InterfaceBoundaryFactory.getBoundary();
            
            % Create Coupling condition
            Beta = 1e5;
            FirstMeshId = 1;
            SecondMeshId = 2;
            InterfaceCoupling = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
            
            % Register conditions at analysis
            obj.testAnalysis.addInterfaceCoupling( InterfaceCoupling, FirstMeshId, SecondMeshId);
            
            %% Apply Body Load (Mesh 1)
            
            % Analytical description of body force
            bx  = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 35 * power( x, 4 ) * power( y, 6 ) + ...
                6 * power( x, 2 ) * y ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 35 * PoissonsRatio * power( x, 4 ) * power( y, 6 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 2 * power( y, 3 ) * YoungsModulus ) / ( 1 - PoissonsRatio * PoissonsRatio );
            by = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 20 * power( x, 3 ) * power( y, 7 ) + ...
                6 * x * power( y, 2 ) ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 42 * power( x, 5 ) * power( y, 5 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 6 * PoissonsRatio * x * power( y, 2 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio );
            
            % Transform to vector function
            BodyForce1 = @(x,y,z) [ bx(x,y,z); by(x,y,z)];
            
            % Create new load case
            loadCase = LoadCase();
            loadCase.addBodyLoad( BodyForce1 ,1 );
            
            
            %% Apply Body Load (Mesh 2)
            
            % Analytical description of body force
            bx2  = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 35 * power( x, 4 ) * power( y, 6 ) + ...
                6 * power( x, 2 ) * y ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 35 * PoissonsRatio * power( x, 4 ) * power( y, 6 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 2 * power( y, 3 ) * YoungsModulus ) / ( 1 - PoissonsRatio * PoissonsRatio );
            by2 = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 20 * power( x, 3 ) * power( y, 7 ) + ...
                6 * x * power( y, 2 ) ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 42 * power( x, 5 ) * power( y, 5 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 6 * PoissonsRatio * x * power( y, 2 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio );
            
            % Transform to vector function
            BodyForce2 = @(x,y,z) [ bx2(x,y,z); by2(x,y,z)];
            
            % Create new load case
            
            loadCase.addBodyLoad( BodyForce2, 2 );
            
            %% Register load case at analysis (Mesh 1 & 2)
            obj.testAnalysis.addLoadCases(loadCase);
            
            %% Apply weak Dirichlet boundary conditions (Mesh 1)
            
            % Select integration scheme
            IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of boundary
            NumberOfBoundarySegments = 3;
            BoundaryFactoryBottom = StraightLineBoundaryFactory( BottomMiddle, BottomLeft, NumberOfBoundarySegments);
            BoundaryFactoryLeft = StraightLineBoundaryFactory( BottomLeft, TopLeft, NumberOfBoundarySegments);
            BoundaryFactoryTop = StraightLineBoundaryFactory( TopLeft, TopMiddle, NumberOfBoundarySegments);
            Boundary = BoundaryFactoryBottom.getBoundary();
            Boundary = [ Boundary BoundaryFactoryLeft.getBoundary() ];
            Boundary = [ Boundary BoundaryFactoryTop.getBoundary() ];
            
            % Selet constraining strategy
            Beta = 1e8;
            ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );
            
            % Give analytical description of boundary values
            ux = @(x,y,z) power( x, 2 ) * power( y, 3 );
            uy = @(x,y,z) power( x, 5 ) * power( y, 7 );
            
            % Create boundary condition in X
            BoundaryConditionXMeshId = 1;
            ConstrainedDirections = [1 0];
            BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId = 1;
            ConstrainedDirections = [0 1];
            BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
                IntegrationScheme, Boundary, ConstrainingAlgorithm );
            
            % Register conditions at analysis
            obj.testAnalysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );
            obj.testAnalysis.addDirichletBoundaryCondition( BoundaryConditionY, BoundaryConditionYMeshId );
            
            %% Apply weak Dirichlet boundary conditions (Mesh 2)
            
            % Select integration scheme
            IntegrationScheme2 = GaussLegendre( NumberOfGaussPoints2 );
            
            % Get geometrical description of boundary
            NumberOfBoundarySegments2 = 3;
            BoundaryFactoryTop2 = StraightLineBoundaryFactory( TopMiddle, TopRight, NumberOfBoundarySegments2);
            BoundaryFactoryRight2 = StraightLineBoundaryFactory( TopRight, BottomRight, NumberOfBoundarySegments2);
            BoundaryFactoryBottom2 = StraightLineBoundaryFactory( BottomRight, BottomMiddle, NumberOfBoundarySegments2);
            Boundary2 = BoundaryFactoryTop2.getBoundary();
            Boundary2 = [ Boundary2 BoundaryFactoryRight2.getBoundary() ];
            Boundary2 = [ Boundary2 BoundaryFactoryBottom2.getBoundary() ];
            
            % Selet constraining strategy
            Beta2 = 1.0e8;
            ConstrainingAlgorithm2 = WeakNitscheDirichlet2DAlgorithm( Beta2 );
            
            % Give analytical description of boundary values
            ux2 = @(x,y,z) power( x, 2 ) * power( y, 3 );
            uy2 = @(x,y,z) power( x, 5 ) * power( y, 7 );
            
            % Create boundary condition in X
            BoundaryConditionXMeshId2 = 2;
            ConstrainedDirections2 = [1 0];
            BoundaryConditionX2 = WeakDirichletBoundaryCondition( ux2, ConstrainedDirections2, ...
                IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId2 = 2;
            ConstrainedDirections2 = [0 1];
            BoundaryConditionY2 = WeakDirichletBoundaryCondition( uy2, ConstrainedDirections2, ...
                IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );
            
            % Register conditions at analysis
            obj.testAnalysis.addDirichletBoundaryCondition( BoundaryConditionX2, BoundaryConditionXMeshId2 );
            obj.testAnalysis.addDirichletBoundaryCondition( BoundaryConditionY2, BoundaryConditionYMeshId2 );
            
            
        end
        
        
        
        function tearDown(obj)
        end
        
        function testStrainEnergy(obj)
            
            %% Run the analysis
            
            obj.testAnalysis.solve();
            
            %% Perform Integration Post Processing (First Mesh)
            
            FeMesh = obj.testAnalysis.getMesh();
            
            % Create result point processors
            IndexOfPhysicalDomain = 2;
            LoadCaseToVisualize = 1;
            
            % Energy
            strainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );
            
            % Decide on post processor type
            PostProcessor = IntegrationPostProcessor( );
            
            % Register result point processors
            PostProcessor.registerPointProcessor( strainEnergy );
            
            % Integrate the results over the domain
            NumericalStrainEnergyFirstMesh = PostProcessor.integrate( FeMesh{1} );
            
            %% Perform Integration Post Processing (Second Mesh)
            
            % Create result point processors
            IndexOfPhysicalDomain = 2;
            LoadCaseToVisualize = 1;
            
            % Energy
            strainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );
            
            % Decide on post processor type
            PostProcessor = IntegrationPostProcessor( );
            
            % Register result point processors
            PostProcessor.registerPointProcessor( strainEnergy );
            
            % Integrate the results over the domain
            NumericalStrainEnergySecondMesh = PostProcessor.integrate( FeMesh{2} );
            
            assertVectorsAlmostEqual( NumericalStrainEnergyFirstMesh+NumericalStrainEnergySecondMesh, 0.546, 'absolute', 0.001)
        end
    end
end

