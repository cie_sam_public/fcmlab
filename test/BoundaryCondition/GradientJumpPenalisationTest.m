%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% testing
classdef GradientJumpPenalisationTest < TestCase
    
    properties
        analysis
    end
    
    methods
        %% constructor
        function obj = GradientJumpPenalisationTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            
            %% Input parameters
            % Numerical parameters
            meshOrigin = [0.0 0.0 0.0];
            meshLengthX = 1.7;
            meshLengthY = 1.7;
            numberOfXDivisions = 2;
            numberOfYDivisions = 2;
            polynomialDegree = 4;
            numberOfGaussPoints = polynomialDegree+1;
            
            % Mechanical parameters
            youngsModulus = 1.0;
            poissonsRatio = 0.3;
            density = 1.0;
            
            % Creation of Materials
            materials(1) = HookePlaneStress( ...
                youngsModulus, poissonsRatio, ...
                density, 0 );
            materials(2) = HookePlaneStress( ...
                youngsModulus, poissonsRatio, ...
                density, 1 );
            
            
            % Creation of Domain Geoemtry
            center = [ 0.0 0.0 0.0 ];
            lengths = [ 1 1 ];
            myRectangle = EmbeddedRectangle( center, lengths );
            
            
            % FCM Parameter
            spaceTreeDepth = 1;
             % Creation of the FEM system
            DofDimension = 2;
            
            
            %My Partitioner
            quadPartitioner = QuadTree(myRectangle,spaceTreeDepth);
            
            elementFactory = ElementFactoryElasticQuadFCMArbitraryPart(materials,myRectangle,quadPartitioner,...
                numberOfGaussPoints);
            
            meshFactory = MeshFactory2DUniform(numberOfXDivisions,...
                numberOfYDivisions,polynomialDegree,TopologicalSorting,...
                DofDimension,meshOrigin,meshLengthX,meshLengthY,elementFactory);
            meshFactory.resetElementCounter();
            
            
            
            % Create Analysis
            obj.analysis = QuasiStaticAnalysis( meshFactory );
            
            loadCase = LoadCase();
            loadCase.addBodyLoad( @(x,y,z) 0);
            obj.analysis.addLoadCases( loadCase );
            
        end
        
        function tearDown(obj)
        end
        
        function testOne(obj)
            
            integrationScheme = GaussLegendre(4);
            SpaceDim = 2;
            domainIndex = 2;
            resolution = [10 10];
            ratio = 1;
            penaltyValue = 1000;
            
            GJP = GradientJumpPenalisation(integrationScheme, SpaceDim, domainIndex, resolution, ratio, penaltyValue);
            obj.analysis.addGradientJumpPenalisation(GJP);
            
            
            % Run analysis
            obj.analysis.solve();
            
            
        end
        
        
        
    end
    
end

