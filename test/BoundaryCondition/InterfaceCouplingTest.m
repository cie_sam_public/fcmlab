%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% testing interface coupling of two quad elements
classdef InterfaceCouplingTest < TestCase
    
    properties
        TestAnalysis
    end
    
    methods
        
        %% constructor
        function obj = InterfaceCouplingTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            %% Left Element
            %% Embedded Domain Setup
            
            OriginLeftElement = [0.0 0.0 0.0];
            LengthsLeftElement = [1.0 1.0];
            
            %Create the domain
            RectangleLeftElement = EmbeddedRectangle( OriginLeftElement, LengthsLeftElement);
            
            %% Material Setup
            YoungsModulus = 1.0;
            PoissonsRatio = 0.0;
            Density = 1.0;
            Alpha = 0;
            
            % Material for the void (background) domain
            MaterialsLeftElement(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            MaterialsLeftElement(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            PolynomialDegree = 1;
            NumberOfGaussPoints = PolynomialDegree+1;
            SpaceTreeDepth = 1;
            
            ElementFactory = ElementFactoryElasticQuadFCM( MaterialsLeftElement, RectangleLeftElement, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            MeshOriginLeftElement = [0.0 0.0 0.0];
            MeshLengthX = 1;
            MeshLengthY = 1;
            NumberOfXDivisions = 1;
            NumberOfYDivisions = 1;
            DofDimension = 2;
            
            MeshFactories{1} = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOriginLeftElement, MeshLengthX, MeshLengthY, ElementFactory);
            
            %% Right Element
            %% Embedded Domain Setup
            
            OriginRightElement = [1.0 0.0 0.0];
            LengthsRightElement = [1.0 1.0];
            
            %Create the domain
            RectangleRightElement = EmbeddedRectangle( OriginRightElement, LengthsRightElement);
            
            %% Material Setup
            
            % Material for the void (background) domain
            MaterialsRightElement(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            MaterialsRightElement(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            
            ElementFactoryRightElement = ElementFactoryElasticQuadFCM( MaterialsRightElement, RectangleRightElement, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            MeshOriginRightElement = [1.0 0.0 0.0];
            
            MeshFactories{2} = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOriginRightElement, MeshLengthX, MeshLengthY, ElementFactoryRightElement);
            
            %% Analysis Setup
            
            % Decide on anlysis type
            obj.TestAnalysis = QuasiStaticAnalysis( MeshFactories);
            
        end
        
        function tearDown(obj)
        end
        
        %% TestPenaltyInterfaceCoupling
        function testPenaltyCouplingMatrix(obj)
            
            % Select integration scheme
            NumberOfGaussPoints = 2;
            IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of interface
            StartPoint = [1.0 0.0 0.0];
            EndPoint = [1.0 1.0 0.0];
            NumberOfBoundarySegments = 1;
            InterfaceBoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
            InterfaceGeometry = InterfaceBoundaryFactory.getBoundary();
            
            % Create Coupling condition
            Beta = 1;
            FirstMeshId = 1;
            SecondMeshId = 2;
            Coupling = PenaltyInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
            
            % Register conditions at analysis
            obj.TestAnalysis.addInterfaceCoupling( Coupling, FirstMeshId, SecondMeshId);
            
            
            % Register load case at analysis
            EmptyLoadCase = LoadCase();
            obj.TestAnalysis.addLoadCases(EmptyLoadCase);
            
            % Compute CouplingMatrix
            
            TotalNumberOfDofs = obj.TestAnalysis.getTotalNumberOfDofs();
            CouplingMatrix = obj.TestAnalysis.applyInterfaceCoupling(zeros(TotalNumberOfDofs,TotalNumberOfDofs));
            
            % Compare with reference matrix
            
            CouplingMatrixRef = [ ...
                1/3 1/6 0 0 -1/3 -1/6 0 0; ...
                1/6 1/3 0 0 -1/6 -1/3 0 0; ...
                0 0 1/3 1/6 0 0 -1/3 -1/6; ...
                0 0 1/6 1/3 0 0 -1/6 -1/3; ...
                -1/3 -1/6 0 0 1/3 1/6 0 0; ...
                -1/6 -1/3 0 0 1/6 1/3 0 0; ...
                0 0 -1/3 -1/6 0 0 1/3 1/6; ...
                0 0 -1/6 -1/3 0 0 1/6 1/3 ];
            
            assertVectorsAlmostEqual(CouplingMatrix([2 4 6 8 9 11 13 15],[2 4 6 8 9 11 13 15]), CouplingMatrixRef, 'absolute', 0.0001)
        end
        
        %% TestNitscheInterfaceCoupling
        function testNitscheCouplingMatrix(obj)
            
            % Select integration scheme
            NumberOfGaussPoints = 2;
            IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of interface
            StartPoint = [1.0 0.0 0.0];
            EndPoint = [1.0 1.0 0.0];
            NumberOfBoundarySegments = 1;
            InterfaceBoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
            InterfaceGeometry = InterfaceBoundaryFactory.getBoundary();
            
            % Create Coupling condition
            Beta = 0;
            FirstMeshId = 1;
            SecondMeshId = 2;
            Coupling = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
            
            % Register conditions at analysis
            obj.TestAnalysis.addInterfaceCoupling( Coupling, FirstMeshId, SecondMeshId);
            
            
            % Register load case at analysis
            EmptyLoadCase = LoadCase();
            obj.TestAnalysis.addLoadCases(EmptyLoadCase);
            
            % Compute CouplingMatrix
            
            TotalNumberOfDofs = obj.TestAnalysis.getTotalNumberOfDofs();
            CouplingMatrix = obj.TestAnalysis.applyInterfaceCoupling(zeros(TotalNumberOfDofs,TotalNumberOfDofs));
            
            % Compare with reference matrix
            
            CouplingMatrixRef = [ ...
                0 1/6 0 1/12 0 0 0 0 -1/6 0 -1/12 0 0 0 0 0; ...
                0 -1/6 0 -1/12 0 1/8 0 1/8 1/6 0 1/12 0 -1/8 0 -1/8 0; ...
                0 1/12 0 1/6 0 0 0 0 -1/12 0 -1/6 0 0 0 0 0; ...
                0 -1/12 0 -1/6 0 -1/8 0 -1/8 1/12 0 1/6 0 1/8 0 1/8 0; ...
                0 0 0 0 0 1/12 0 1/24 0 0 0 0 -1/12 0 -1/24 0; ...
                0 0 0 0 0 -1/12 0 -1/24 0 0 0 0 1/12 0 1/24 0; ...
                0 0 0 0 0 1/24 0 1/12 0 0 0 0 -1/24 0 -1/12 0; ...
                0 0 0 0 0 -1/24 0 -1/12 0 0 0 0 1/24 0 1/12 0; ...
                0 1/6 0 1/12 0 1/8 0 1/8 -1/6 0 -1/12 0 -1/8 0 -1/8 0; ...
                0 -1/6 0 -1/12 0 0 0 0 1/6 0 1/12 0 0 0 0 0; ...
                0 1/12 0 1/6 0 -1/8 0 -1/8 -1/12 0 -1/6 0 1/8 0 1/8 0; ...
                0 -1/12 0 -1/6 0 0 0 0 1/12 0 1/6 0 0 0 0 0; ...
                0 0 0 0 0 1/12 0 1/24 0 0 0 0 -1/12 0 -1/24 0; ...
                0 0 0 0 0 -1/12 0 -1/24 0 0 0 0 1/12 0 1/24 0; ...
                0 0 0 0 0 1/24 0 1/12 0 0 0 0 -1/24 0 -1/12 0; ...
                0 0 0 0 0 -1/24 0 -1/12 0 0 0 0 1/24 0 1/12 0 ];
            
            CouplingMatrixRef = CouplingMatrixRef+CouplingMatrixRef';
            
            assertVectorsAlmostEqual(CouplingMatrix, CouplingMatrixRef, 'absolute', 0.0001)
        end
    end
end

