classdef TimeStateTest < TestCase
    
    properties
        TestTimeState
    end
    
    methods
        
        %% constructor
        function obj = TimeStateTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
           startTime = 0.0;
           obj.TestTimeState = TimeState(startTime);
            
        end
        
        function tearDown(obj)
        end
        
        function testAnalysis(obj)
            timeIncrement = 0.5;
            obj.TestTimeState.updateTime(timeIncrement);
            assertEqual(obj.TestTimeState.currentTime, 0.5);            
        end       
    end
    
end
