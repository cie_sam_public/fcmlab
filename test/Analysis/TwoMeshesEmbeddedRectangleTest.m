%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef TwoMeshesEmbeddedRectangleTest < TestCase
    
    properties
        TestAnalysis
        IntegrationSchemeFirstMesh
        IntegrationSchemeSecondMesh
        BoundaryFirstMesh
        BoundarySecondMesh
    end
    
    methods
        
        %% constructor
        function obj = TwoMeshesEmbeddedRectangleTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            
            %% First Mesh
            %% Embedded Domain Setup
            
            Origin = [0.0 0.0 0.0];
            Lengths = [1.0 1.0];
            
            %Create the domain
            RectangleFirstMesh = EmbeddedRectangle( Origin, Lengths);
            
            %% Material Setup
            YoungsModulus = 1.0;
            PoissonsRatio = 0.3;
            Density = 1.0;
            Alpha = 0;
            
            % Material for the void (background) domain
            MaterialsFirstMesh(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            MaterialsFirstMesh(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            PolynomialDegree = 1;
            NumberOfGaussPoints = PolynomialDegree+1;
            SpaceTreeDepth = 1;
            
            ElementFactoryFirstMesh = ElementFactoryElasticQuadFCM( MaterialsFirstMesh, RectangleFirstMesh, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 4.0/3.0;
            MeshLengthY = 4.0/3.0;
            NumberOfXDivisions = 1;
            NumberOfYDivisions = 1;
            DofDimension = 2;
            
            MeshFactories{1} = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactoryFirstMesh);
            
            %% Second Mesh
            %% Embedded Domain Setup
            
            %Create the domain
            RectangleSecondMesh = EmbeddedRectangle( Origin, Lengths);
            
            % Material for the void (background) domain
            MaterialsSecondMesh(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, Alpha);
            
            % Material for the actual domain
            MaterialsSecondMesh(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, Density, 1);
            
            %% Mesh Setup
            
            % Step 1: decide on element type
            
            ElementFactorySecondMesh = ElementFactoryElasticQuadFCM( MaterialsSecondMesh, RectangleSecondMesh, ...
                NumberOfGaussPoints, SpaceTreeDepth );
            
            % Step 2: create the mesh
            
            MeshFactories{2} = MeshFactory2DUniform( NumberOfXDivisions, ...
                NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactorySecondMesh);
            
            %% Analysis Setup
            obj.TestAnalysis = QuasiStaticAnalysis(MeshFactories);
            
            %% Create Boundary (Mesh 1)
            % Select integration scheme
            obj.IntegrationSchemeFirstMesh = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of boundary
            NumberOfBoundarySegments = 3;
            BoundaryFactoryFirstMesh = RectangleBoundaryFactory( Origin, Lengths, NumberOfBoundarySegments );
            obj.BoundaryFirstMesh = BoundaryFactoryFirstMesh.getBoundary();
            
            %% Create Boundary (Mesh 2)
            % Select integration scheme
            obj.IntegrationSchemeSecondMesh = GaussLegendre( NumberOfGaussPoints );
            
            % Get geometrical description of boundary
            BoundaryFactorySecondMesh = RectangleBoundaryFactory( Origin, Lengths, NumberOfBoundarySegments );
            obj.BoundarySecondMesh = BoundaryFactorySecondMesh.getBoundary();
        end
        
        function tearDown(obj)
        end
        
        function testBodyLoad(obj)
            %% Apply Body Load 
            PoissonsRatio = 0.3;
            YoungsModulus = 1.0;
            % Analytical description of body force
            bx  = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 35 * power( x, 4 ) * power( y, 6 ) + ...
                6 * power( x, 2 ) * y ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 35 * PoissonsRatio * power( x, 4 ) * power( y, 6 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 2 * power( y, 3 ) * YoungsModulus ) / ( 1 - PoissonsRatio * PoissonsRatio );
            by = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 20 * power( x, 3 ) * power( y, 7 ) + ...
                6 * x * power( y, 2 ) ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 42 * power( x, 5 ) * power( y, 5 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 6 * PoissonsRatio * x * power( y, 2 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio );
            
            % Transform to vector function
            BodyForceFirstLoadCase = @(x,y,z) [ bx(x,y,z); by(x,y,z)];
            BodyForceSecondLoadCase = @(x,y,z) 0.5*[ bx(x,y,z); by(x,y,z)];
            
            % Create first load case 
            FirstLoadCase = LoadCase();
            FirstLoadCase.addBodyLoad(BodyForceFirstLoadCase, 1);
            FirstLoadCase.addBodyLoad(BodyForceFirstLoadCase, 2);
            
            %Create second load case
            SecondLoadCase = LoadCase();
            SecondLoadCase.addBodyLoad(BodyForceSecondLoadCase, 1);
            SecondLoadCase.addBodyLoad(BodyForceSecondLoadCase, 1);
            SecondLoadCase.addBodyLoad(BodyForceSecondLoadCase, 2);
            SecondLoadCase.addBodyLoad(BodyForceSecondLoadCase, 2);
            
            %% Register load case at analysis 
            obj.TestAnalysis.addLoadCases(FirstLoadCase);
            obj.TestAnalysis.addLoadCases(SecondLoadCase);
            
            LoadVector = obj.TestAnalysis.computeLoadVectors(obj.TestAnalysis.getTotalNumberOfDofs());
            assertVectorsAlmostEqual(LoadVector(:,1), LoadVector(:,2));
        end       
        function testNeumannBoundaryCondition(obj)
            %% Apply weak Neumann boundary conditions
            
            % Give analytical description of boundary values
            LoadFunctionFirstLoadCase = @(x,y,z) [10*x;2*y]; 
            LoadFunctionSecondLoadCase = @(x,y,z) [5*x;y]; 
            
            % Create boundary condition
            NeumannBoundaryConditionFirstLoadCaseFirstMesh = WeakNeumannBoundaryCondition(LoadFunctionFirstLoadCase,obj.IntegrationSchemeFirstMesh,obj.BoundaryFirstMesh);
            NeumannBoundaryConditionFirstLoadCaseSecondMesh = WeakNeumannBoundaryCondition(LoadFunctionFirstLoadCase,obj.IntegrationSchemeSecondMesh,obj.BoundarySecondMesh);
            NeumannBoundaryConditionSecondLoadCaseFirstMesh = WeakNeumannBoundaryCondition(LoadFunctionSecondLoadCase,obj.IntegrationSchemeFirstMesh,obj.BoundaryFirstMesh);
            NeumannBoundaryConditionSecondLoadCaseSecondMesh = WeakNeumannBoundaryCondition(LoadFunctionSecondLoadCase,obj.IntegrationSchemeSecondMesh,obj.BoundarySecondMesh);

            % Create first load case 
            FirstLoadCase = LoadCase();
            FirstLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionFirstLoadCaseFirstMesh, 1);
            FirstLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionFirstLoadCaseSecondMesh, 2);
            
            %Create second load case
            SecondLoadCase = LoadCase();
            SecondLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionSecondLoadCaseFirstMesh, 1);
            SecondLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionSecondLoadCaseFirstMesh, 1);
            SecondLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionSecondLoadCaseSecondMesh, 2);
            SecondLoadCase.addNeumannBoundaryCondition(NeumannBoundaryConditionSecondLoadCaseSecondMesh, 2);
            
            %% Register load case at analysis 
            obj.TestAnalysis.addLoadCases(FirstLoadCase);
            obj.TestAnalysis.addLoadCases(SecondLoadCase);
            
            %% Compute and compare load vectors
            LoadVector = obj.TestAnalysis.computeLoadVectors(obj.TestAnalysis.getTotalNumberOfDofs());
            assertVectorsAlmostEqual(LoadVector(:,1), LoadVector(:,2));
        end
        function testSolution(obj)
            %% Apply Body Load 
            PoissonsRatio = 0.3;
            YoungsModulus = 1.0;
            % Analytical description of body force
            bx  = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 35 * power( x, 4 ) * power( y, 6 ) + ...
                6 * power( x, 2 ) * y ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 35 * PoissonsRatio * power( x, 4 ) * power( y, 6 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 2 * power( y, 3 ) * YoungsModulus ) / ( 1 - PoissonsRatio * PoissonsRatio );
            by = @(x,y,z) ...
                - ( ( 1 - PoissonsRatio ) * ( 20 * power( x, 3 ) * power( y, 7 ) + ...
                6 * x * power( y, 2 ) ) * YoungsModulus ) / ...
                ( 2 * ( 1 - PoissonsRatio * PoissonsRatio ) ) - ...
                ( 42 * power( x, 5 ) * power( y, 5 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio ) ...
                - ( 6 * PoissonsRatio * x * power( y, 2 ) * YoungsModulus ) / ...
                ( 1 - PoissonsRatio * PoissonsRatio );
            
            % Transform to vector function
            BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];
            % Create load case 
            loadCase = LoadCase();
            loadCase.addBodyLoad(BodyForce, 1);
            loadCase.addBodyLoad(BodyForce, 2);
            
            %% Register load case at analysis 
            obj.TestAnalysis.addLoadCases(loadCase);
            
            %% Apply weak Dirichlet boundary conditions
            
            %Selet constraining strategy
            Beta = 0.0;
            ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );
            
            % Give analytical description of boundary values
            ux = @(x,y,z) power( x, 2 ) * power( y, 3 );
            uy = @(x,y,z) power( x, 5 ) * power( y, 7 );
            
            % Create boundary condition in X
            BoundaryConditionXMeshId = 1;
            ConstrainedDirections = [1 0];
            BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
                obj.IntegrationSchemeFirstMesh, obj.BoundaryFirstMesh, ConstrainingAlgorithm );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId = 1;
            ConstrainedDirections = [0 1];
            BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
                obj.IntegrationSchemeFirstMesh, obj.BoundaryFirstMesh, ConstrainingAlgorithm );
            
            % Register conditions at analysis
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionY, BoundaryConditionYMeshId );
            
            %% Apply weak Dirichlet boundary conditions (Mesh 2)
            
            % Selet constraining strategy
            Beta2 = 0.0;
            ConstrainingAlgorithm2 = WeakNitscheDirichlet2DAlgorithm( Beta2 );
            
            % Create boundary condition in X
            BoundaryConditionXMeshId2 = 2;
            ConstrainedDirections2 = [1 0];
            BoundaryConditionX2 = WeakDirichletBoundaryCondition( ux, ConstrainedDirections2, ...
                obj.IntegrationSchemeSecondMesh, obj.BoundarySecondMesh, ConstrainingAlgorithm2 );
            
            % Create boundary condition in Y
            BoundaryConditionYMeshId2 = 2;
            ConstrainedDirections2 = [0 1];
            BoundaryConditionY2 = WeakDirichletBoundaryCondition( uy, ConstrainedDirections2, ...
                obj.IntegrationSchemeSecondMesh, obj.BoundarySecondMesh, ConstrainingAlgorithm2 );
            % Register conditions at analysis
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionX2, BoundaryConditionXMeshId2 );
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionY2, BoundaryConditionYMeshId2 );
            
            %% Run the analysis
            TotalNumberOfDofs = obj.TestAnalysis.getTotalNumberOfDofs();
            
            MassMatrix = obj.TestAnalysis.computeMassMatrix(TotalNumberOfDofs);
            StiffnessMatrix = obj.TestAnalysis.computeStiffnessMatrix(TotalNumberOfDofs);
            LoadVector = obj.TestAnalysis.computeLoadVectors(TotalNumberOfDofs);
            SolutionVector = obj.TestAnalysis.solve();
            
            % Compare Vectors and Matrices
            
            for i = 1:(TotalNumberOfDofs/2)
                for j = 1:(TotalNumberOfDofs/2)
                    assertVectorsAlmostEqual(MassMatrix(i,j),MassMatrix(i+(TotalNumberOfDofs/2),j+(TotalNumberOfDofs/2)));
                    assertVectorsAlmostEqual(MassMatrix(i+(TotalNumberOfDofs/2),j),0);
                    assertVectorsAlmostEqual(MassMatrix(i,j+(TotalNumberOfDofs/2)),0);
                    assertVectorsAlmostEqual(StiffnessMatrix(i,j),StiffnessMatrix(i+(TotalNumberOfDofs/2),j+(TotalNumberOfDofs/2)));
                    assertVectorsAlmostEqual(StiffnessMatrix(i+(TotalNumberOfDofs/2),j),0);
                    assertVectorsAlmostEqual(StiffnessMatrix(i,j+(TotalNumberOfDofs/2)),0);
                end
                assertVectorsAlmostEqual(LoadVector(i),LoadVector(i+(TotalNumberOfDofs/2)));
                assertVectorsAlmostEqual(SolutionVector(i), SolutionVector(i+(TotalNumberOfDofs/2)));
            end
        end
    end
end

