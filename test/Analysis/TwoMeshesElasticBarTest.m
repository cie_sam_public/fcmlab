%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef TwoMeshesElasticBarTest < TestCase
    
    properties
        TestAnalysis
    end
    
    methods
        
        %% constructor
        function obj = TwoMeshesElasticBarTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
         
            %% Material Setup
                        
            Area=1.0;
            E=1.0;
            Density=1.0;
            ScalingFactor=1.0;
            
            MaterialsFirstMesh(1) = Hooke1D( Area,E,Density,ScalingFactor);
            MaterialsSecondMesh(1) = Hooke1D( Area,E,Density,ScalingFactor);
            
            %% Mesh Setup
            
            PolynomialDegree = 1;
            NumberOfGaussPoints = PolynomialDegree+1;
            
            ElementFactoryFirstMesh = ElementFactoryElasticBar( MaterialsFirstMesh, NumberOfGaussPoints);
            ElementFactorySecondMesh = ElementFactoryElasticBar( MaterialsSecondMesh, NumberOfGaussPoints);
            
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 1.0;
            NumberOfXDivisions = 4;
            DofDimension = 1;
            
            meshFactory{1} = MeshFactory1DUniform( NumberOfXDivisions, ...
                PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, ElementFactoryFirstMesh );
            meshFactory{2} = MeshFactory1DUniform( NumberOfXDivisions, ...
                PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, ElementFactorySecondMesh );
            
            %% Analysis Setup
            
            obj.TestAnalysis = QuasiStaticAnalysis(meshFactory);
            
            %% Apply Body Load 

            BodyForce = @(x,y,z)  6*x ;
            loadCase = LoadCase();
            loadCase.addBodyLoad( BodyForce, 1 );
            loadCase.addBodyLoad( BodyForce, 2 );
            
            %% Apply Neumann boundary conditions 
            Position = [1.0 0.0 0.0];
            LoadVector = @(x,y,z) -3;
            NeumannBCFirstMesh = NodalNeumannBoundaryCondition(Position,LoadVector);
            loadCase.addNeumannBoundaryCondition(NeumannBCFirstMesh,1);
            NeumannBCSecondMesh = NodalNeumannBoundaryCondition(Position,LoadVector);
            loadCase.addNeumannBoundaryCondition(NeumannBCSecondMesh,2);
            
            %% Register load case at analysis
            obj.TestAnalysis.addLoadCases(loadCase);
            
            %% Apply Dirichlet boundary conditions 
            
            Position = [0.0 0.0 0.0];
            PrescribedValue = 0.0;
            Direction = [1.0 0.0 0.0];
            
            StrongDirichletAlgorithmFirstMesh = StrongDirectConstrainingAlgorithm;
            BoundaryConditionFirstMesh = StrongNodeDirichletBoundaryCondition(Position,PrescribedValue,Direction,StrongDirichletAlgorithmFirstMesh);
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionFirstMesh,1);
            
            StrongDirichletAlgorithmSecondMesh = StrongDirectConstrainingAlgorithm;
            BoundaryConditionSecondMesh = StrongNodeDirichletBoundaryCondition(Position,PrescribedValue,Direction,StrongDirichletAlgorithmSecondMesh);
            obj.TestAnalysis.addDirichletBoundaryCondition( BoundaryConditionSecondMesh,2);
            
        end
        
        function tearDown(obj)
        end
        
        function testSolutionVector(obj)
            assertVectorsAlmostEqual(obj.TestAnalysis.solve(), [0 ; -1/64 ; -8/64 ; -27/64 ; -1; 0; -1/64 ; -8/64 ; -27/64 ; -1 ] , 'absolute', 0.0001)            
        end       
    end
    
end

