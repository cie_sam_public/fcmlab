classdef NewmarkWaveEquationTest < TestCase
    %
    properties
        TestAnalysis
    end
    
    methods
        
        %% constructor
        function obj = NewmarkWaveEquationTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
           
            %% Material Setup
                        
            Area=1.0;
            E=1.0;
            Density=1.0;
            ScalingFactor=1.0;
            
            Materials(1) = Hooke1D( Area,E,Density,ScalingFactor);
                        
            %% Mesh Setup
            
            PolynomialDegree = 1;
            NumberOfGaussPoints = PolynomialDegree+1;
            
            ElementFactory = ElementFactoryElasticBar( Materials, NumberOfGaussPoints);
            
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 1.0;
            NumberOfXDivisions = 2;
            DofDimension = 1;
            
            meshFactory = MeshFactory1DUniform( NumberOfXDivisions, ...
                PolynomialDegree, TopologicalSorting, ...
                DofDimension, MeshOrigin, MeshLengthX, ElementFactory );
            
            %% Analysis Setup
            
            numberOfTimeSteps=1;
            startTime=0.0;
            endTime=0.1;
            d0=1/50;
            v0=1/5;
            a0=6;
            initialCondition=[d0 v0 a0];
            time=TimeState(startTime);
            
            obj.TestAnalysis = NewmarkWaveEquation(meshFactory,numberOfTimeSteps,endTime,initialCondition,time);
            
            loadCase = LoadCase();
            obj.TestAnalysis.addLoadCases(loadCase);
            
        end
        
        function tearDown(obj)
        end
        
        function testAnalysis(obj)
               
            assertVectorsAlmostEqual(obj.TestAnalysis.solve(), [1/50 11/200 ; 1/50 11/200 ; 1/50 11/200] , 'absolute', 10^-10)            
        end       
    end
    
end

