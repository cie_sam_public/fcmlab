%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

classdef BoundaryRecoveryElementwiseFactoryTest  < TestCase
    
    properties
        
        TestedFactory
        Geometry
        BoundingBoxX
        BoundingBoxY
        RecoveredBoundary
        PartitionDepth
        SeedPoints
        BoundaryNormals
        Analysis
        ElementsForBoundary
        
    end
    
    methods
        
        %% constructor
        function obj = BoundaryRecoveryElementwiseFactoryTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            
            %% Input parameters
            % Numerical parameters
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 4.0/3.0;
            MeshLengthY = 4.0/3.0;
            NumberOfXDivisions = 4;
            NumberOfYDivisions = 4;
            PolynomialDegree = 7;
            NumberOfGaussPoints = PolynomialDegree+1;
            
            % Mechanical parameters
            YoungsModulus = 1.0;
            PoissonsRatio = 0.3;
            Density = 1.0;
            
            % FCM Parameter
            SpaceTreeDepth = 1;
            Alpha = 0;
            
            
            %% Instanciation of the problem
            
            % Creation of Materials
            Materials(1) = HookePlaneStress( ...
                YoungsModulus, PoissonsRatio, ...
                Density, Alpha );
            Materials(2) = HookePlaneStress( ...
                YoungsModulus, PoissonsRatio, ...
                Density, 1 );
            
            % Creation of Domain Geoemtry
            Center = [ 0.0 0.0 0.0 ];
            Lengths = [ 1.0 1.0 ];
            Rectangle = EmbeddedRectangle( Center, Lengths );
            
            
            
            % Creation of the FEM system
            DofDimension = 2;
            
            ElementFactory = ElementFactoryElasticQuadFCM(Materials,Rectangle,...
                NumberOfGaussPoints,SpaceTreeDepth);
            
            MeshFactory = MeshFactory2DUniform(NumberOfXDivisions,...
                NumberOfYDivisions,PolynomialDegree,TopologicalSorting,...
                DofDimension,MeshOrigin,MeshLengthX,MeshLengthY,ElementFactory);
            
            % Create Analysis
            obj.Analysis = QuasiStaticAnalysis( MeshFactory );
            
            
            %             %Original Test Case
            obj.BoundingBoxX=[.1, 1.1];
            obj.BoundingBoxY=[.6, 1.2];
            
            %            % Special Test High
            %             obj.BoundingBoxX=[.35, .65];
            %             obj.BoundingBoxY=[.2, 1.2];
            %
            %             Special Test Wide
            %             obj.BoundingBoxX=[.1, 1.1];
            %             obj.BoundingBoxY=[.35, .65];
            
            %             Special Test box inside one element
            %             obj.BoundingBoxX=[.67, .9];
            %             obj.BoundingBoxY=[.67, .9];
            
            
            %             domainFunctionHandle = @(x,y,z) ...
            %                 2 - ((x+y)<0.3125);
            
            
            %             domainFunctionHandle = @(x,y,z) ...
            %                 2 - ((x>=0&&y>=0&&x<=1&&y<=1));
            
            domainFunctionHandle = @(x,y,z) ...
                2 - ((x^2+y^2)<1);
            
            
            obj.Geometry = FunctionHandleDomain(domainFunctionHandle);
            
            
            obj.PartitionDepth=3;
            obj.SeedPoints=0;
            
            
            obj.TestedFactory=BoundaryRecoveryElementwiseFactory(obj.Geometry, obj.BoundingBoxX, obj.BoundingBoxY, obj.PartitionDepth, obj.SeedPoints, obj.Analysis);
            
            
            
            
            
        end
        
        function tearDown(obj)
        end
        
        function testGetElements(obj)
            obj.ElementsForBoundary=obj.TestedFactory.getElements();
            assertEqual(length(obj.ElementsForBoundary),12);
            
            
        end
        function testBoundaryRecovery(obj)
            % RecoveredBoundary is an Array containing Boundary Lines
            % Elementwise - making sure that no Boundary Line will cross
            % over an Element border.
            
            obj.RecoveredBoundary=obj.TestedFactory.getBoundary();
            %drawBoundary(obj.RecoveredBoundary)
            assertEqual(length(obj.RecoveredBoundary),35);
            
            
            
        end
        
    end
end