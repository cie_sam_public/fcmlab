classdef ConformingMeshWrappedBoundaryFactory2DTest < TestCase
    
    properties
        BoundaryFactory
    end
    
    methods
        
        %% constructor
        function obj = ConformingMeshWrappedBoundaryFactory2DTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            close all;
            MeshOrigin = [0.0 0.0 0.0];
            MeshLengthX = 2.3;
            MeshLengthY = 2.3;
            NumberOfXDivisions = 4;
            NumberOfYDivisions = 4;
            
            vertex1 = Vertex([1 0 0]);
            vertex2 = Vertex([2 1 0]);
            vertex3 = Vertex([1 2 0]);
            vertex4 = Vertex([0 1 0]);
            Boundary(1) = Line(vertex1,vertex2);
            Boundary(2) = Line(vertex2,vertex3);
            Boundary(3) = Line(vertex3,vertex4);
            Boundary(4) = Line(vertex4,vertex1);
            
            obj.BoundaryFactory = ConformingMeshWrappedBoundaryFactory2D(Boundary,MeshOrigin,MeshLengthX, MeshLengthY, NumberOfXDivisions, NumberOfYDivisions);
            
        end
        function tearDown(obj)
        end
        
        function testBoundary(obj)
            
            MyBoundary = obj.BoundaryFactory.getBoundary();
            
            for i=1:length(MyBoundary)
                vert = MyBoundary(i).getVertices();
                coords(1,:) = vert(1).getCoords();
                coords(2,:) = vert(2).getCoords();
                if mod(i,2)== 0
                    plot3(coords(:,1),coords(:,2),coords(:,3),'r-');
                else
                    plot3(coords(:,1),coords(:,2),coords(:,3),'g-');
                end
            end
             assertEqual(length(MyBoundary),16);
            
            
        end
    end
end