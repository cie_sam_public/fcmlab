%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef STLwithConformingMeshBoundaryFactoryTest < TestCase
    
    properties
        TestFactory
    end
    
    methods
        
        %% constructor
        function obj = STLwithConformingMeshBoundaryFactoryTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            tolerance = 1e-7;
            obj.TestFactory = STLwithConformingMeshBoundaryFactory('Test.stl',tolerance);
        end
        
        function tearDown(obj)
        end
        
        function testSTLread(obj)
            
            
            Origin = [0 0 0];
            Lx =1.0;
            Ly =1.0;
            Lz =1.0;
            
            NumOfXDivisions = 2;
            NumOfYDivisions = 2;
            NumOfZDivisions = 2;

            planes = obj.TestFactory.extractPlanesFromMeshData(Origin, NumOfXDivisions, NumOfYDivisions, NumOfZDivisions, Lx, Ly, Lz );
            %assertEqual(length(planes),3);
            
            obj.TestFactory.processBoundary();
            processedSTLBoundary = obj.TestFactory.getProcessedBoundary;
            
            % just testing that stlread works and testing one set of coords
            assertElementsAlmostEqual(processedSTLBoundary(1).getVerticesCoordinates(),...
                [-8.9062 -57.6562 142.500;-9.6875 -57.6562 142.500;-9.6875 -58.4375 142.500;-9.6875 -58.4375 142.500],'absolute',1E-3);
            
        end

    end
    
end