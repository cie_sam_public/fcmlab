%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%


classdef EmbeddedRotatedRectangleTest < TestCase
    
    properties
        EmbeddedDomain
    end
    
    methods
        % constructor
        function obj = EmbeddedRotatedRectangleTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % Creation of Domain Geoemtry
            Center = [ 3.0 0.0 0.0 ];
            Lengths = [ 1.20 0.40 ];
            Theta = (2/10)*3.14;
            
            obj.EmbeddedDomain = EmbeddedRotatedRectangle( Center, Lengths, Theta );
        end
        
        function tearDown(obj)
        end
        
        function testGetDomainIndex(obj)
            assertEqual(obj.EmbeddedDomain.getDomainIndex([ 3.0 0 0]), 2);
            assertEqual(obj.EmbeddedDomain.getDomainIndex([-1.5 0 0]), 1);            
            assertEqual(obj.EmbeddedDomain.getDomainIndex([2.7320 -0.4144 0]), 2);            
            assertEqual(obj.EmbeddedDomain.getDomainIndex([2.3970 -0.1907 0]), 2);
            assertEqual(obj.EmbeddedDomain.getDomainIndex([3.6030 0.1907 0]), 2);
        end
    end
    
end

