%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
classdef EmbeddedRectangleWithCircularHolesTest < TestCase
    
    properties  
        origin;
        lengths;
        holeCenter1;
        holeRadius1;
        holeCenter2;
        holeRadius2;
        EmbeddedDomain;
    end
    
    methods
        % constructor
        function obj = EmbeddedRectangleWithCircularHolesTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            obj.origin = [0.0 0.0];
            width = 20;
            height = 10;
            obj.lengths = [width height];
            obj.holeCenter1 = [5 5];
            obj.holeRadius1 = 3.0;
            holes{1} = {obj.holeCenter1 obj.holeRadius1};
            
            obj.holeCenter2 = [15 5];
            obj.holeRadius2 = 2;
            holes{2} = {obj.holeCenter2 obj.holeRadius2};
            
            obj.EmbeddedDomain = EmbeddedRectangleWithCircularHoles(obj.origin,...
               obj.lengths, holes);
        end
        
        function tearDown(obj)
        end
        
        function testGetDomainIndex(obj)
            delta = 0.0001;
            
%           Points outside the box           
            point = [0-delta 5];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [20 + delta 5];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
  
            point = [10 0-delta];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [10 10+delta];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
%             Points inside the hole 1

            point = [5+delta 5 ];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [5 5+delta];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [5 + 0.5 * sqrt(2) * (3 - delta)
                     5 + 0.5 * sqrt(2) * (3 - delta)];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
                 
%             Points inside the hole 2

            point = [15+delta 5 ];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [15 5+delta];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
            point = [15 + 0.5 * sqrt(2) * (2 - delta)
                     5 + 0.5 * sqrt(2) * (2 - delta)];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 1);
            
%             Points inside the plate

            point = [5 + 0.5 * sqrt(2) * (3 + delta)
                     5 + 0.5 * sqrt(2) * (3 + delta)];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 2);
            
            point = [15 + 0.5 * sqrt(2) * (2 + delta)
                     5 + 0.5 * sqrt(2) * (2 + delta)];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 2);
            
            point = [10 5];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 2);
            
            point = [1.5 6];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 2);
            
            point = [17.5 4];
            assertEqual(obj.EmbeddedDomain.getDomainIndex( point ), 2);
        end
    end
    
end

