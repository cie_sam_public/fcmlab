%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class VoxelDomainSTLBoundaryTest

classdef VoxelDomainTest < TestCase
    
    properties
        TestDomain
        TestDomain2
    end
    
    methods
        
        %% constructor
        function obj = VoxelDomainTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            obj.TestDomain = VoxelDomain('VoxelData.txt');
            obj.TestDomain2 = VoxelDomain('VoxelData2.txt');
        end
        
        function tearDown(obj)
        end
        
        function testDomain(obj)
            % Test f�r getDomainIndex
            assertEqual(obj.TestDomain.getDomainIndex([-7 3 0]),2);
            assertEqual(obj.TestDomain.getDomainIndex([-7 4.5 0]),1);
            assertEqual(obj.TestDomain.getDomainIndex([-1 4.5 0]),1);
            assertEqual(obj.TestDomain.getDomainIndex([-1 3 0]),2);
            assertEqual(obj.TestDomain.getDomainIndex([-4.6 4.4 0.1]),2);
            assertEqual(obj.TestDomain.getDomainIndex([-3.4 3.9 0]),1);
            % Test f�r getValueAtPoint
            assertEqual(obj.TestDomain2.getValueAtPoint([3 2 1]),8);
            assertEqual(obj.TestDomain2.getValueAtPoint([5 1 1]),5);
            assertEqual(obj.TestDomain2.getValueAtPoint([5 3 1]),15);
            assertEqual(obj.TestDomain2.getValueAtPoint([3 1 2]),1);
            assertEqual(obj.TestDomain2.getValueAtPoint([1 2 2]),1);
            assertEqual(obj.TestDomain2.getValueAtPoint([2 1 2]),0);
            % Test f�r steValueAtPoint
            obj.TestDomain2.setValueAtPoint([3 2 1],0.8,1e-10,25,17,1.5);
            assertEqual(obj.TestDomain2.getValueAtPoint([3 2 1]),0.8);
            obj.TestDomain2.setValueAtPoint([3 2 1],0.8,1e-10,25,50,1.5);
            assertEqual(obj.TestDomain2.getValueAtPoint([4 2 1]),1);
            assertEqual(obj.TestDomain2.getValueAtPoint([3 2 2]),1);
            % Test f�r getAllVoxelPoints
            MyVector = obj.TestDomain2.getAllVoxelPoints();
            assertEqual(MyVector(1,:),[1 1 1]);
            
           
            
        end

    end
    
end

