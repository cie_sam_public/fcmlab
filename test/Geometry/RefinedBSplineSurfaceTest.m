%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of quad class

classdef RefinedBSplineSurfaceTest < TestCase
    
    properties
        MyNonRefinedBSplineSurface
        MyBSplineSurface11
        MyBSplineSurface21
        MyBSplineSurface12
        MyBSplineSurface22
    end
    
    methods
        %% Constructor
        function obj = RefinedBSplineSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        %% Set up
        function setUp(obj)         
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            geometryDescription.removeWeights();
            
            obj.MyNonRefinedBSplineSurface = ExtractedBSplineSurface(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            
NumberOfXDivisions=2;
NumberOfYDivisions=2;
PolynomialDegree = 3;
 
IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );
            
            obj.MyBSplineSurface11 = geometryFactory.createArea( 1 );
            obj.MyBSplineSurface12 = geometryFactory.createArea( 2 );
            obj.MyBSplineSurface21 = geometryFactory.createArea( 3 );
            obj.MyBSplineSurface22 = geometryFactory.createArea( 4 );
        end
        
               
%         %% Test get lines
%         function testLines(obj)
%             lines = obj.MyBSplineSurface.getLines();
%             
%             line =  lines(1);
%             assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 1 0] );
%             assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [351/400, 231/400, 0] );
%             assertEqual( line.mapLocalToGlobal([1 0 0]), [1 0 0] );
%              
%             line =  lines(2);
%             assertEqual( line.mapLocalToGlobal([-1 0 0]), [1 0 0] );
%             assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [79/40, 0, 0] );
%             assertEqual( line.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
%             
%             line =  lines(3);
%             assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 2.5 0] );
%             assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [351/160, 231/160, 0] );
%             assertEqual( line.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
%             
%             line =  lines(4);
%             assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 1 0] );
%             assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [0, 79/40, 0] );
%             assertEqual( line.mapLocalToGlobal([1 0 0]), [0 2.5 0] );
%         end
%         
%         
        %% Test calculate base vector1
        function testCalcBaseVector1_11(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyBSplineSurface11.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,[49/64, -(147/320), 0]/2);
        end
        
        function testCalcBaseVector1_12(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyBSplineSurface12.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,[49/320, -(343/320), 0]/2);
        end
        
        function testCalcBaseVector1_21(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyBSplineSurface21.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,[79/64, -(237/320), 0]/2);
        end
        
        function testCalcBaseVector1_22(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyBSplineSurface22.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,[79/320, -(553/320), 0]/2);
        end
        
         %% Test calculate base vector2
        function testCalcBaseVector2_11(obj)
            LocalCoords = [5/10, -4/10];
            g2 = obj.MyBSplineSurface11.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,[117/256, 165/256, 0]/2);
        end
        
        function testCalcBaseVector2_12(obj)
            LocalCoords = [5/10, -4/10];
            g2 = obj.MyBSplineSurface12.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,[189/256, 45/256, 0]/2);
        end
        
        function testCalcBaseVector2_21(obj)
            LocalCoords = [5/10, -4/10];
            g2 = obj.MyBSplineSurface21.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,[117/256, 165/256, 0]/2);
        end
        
        function testCalcBaseVector2_22(obj)
            LocalCoords = [5/10, -4/10];
            g2 = obj.MyBSplineSurface22.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,[189/256, 45/256, 0]/2);
        end
         
         %% Test calculate normal vector
        function testCalcNormalVector11(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyBSplineSurface11.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
         
        function testCalcNormalVector12(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyBSplineSurface12.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
        
        function testCalcNormalVector21(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyBSplineSurface21.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
        
        function testCalcNormalVector22(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyBSplineSurface22.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
        
         %% Test calculate jacobian
        function testCalcJacobian11(obj)
            LocalCoords = [-6/10, 7/10];
            J = obj.MyBSplineSurface11.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [[1179/800, -(131/800) 0.0];[57/400, 297/400, 0]]/2);
        end
        
        function testCalcJacobian12(obj)
            LocalCoords = [-6/10, 7/10];
            J = obj.MyBSplineSurface12.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [[131/200, -(393/400), 0.0];[63/100, 12/25, 0]]/2);
        end
        
        function testCalcJacobian21(obj)
            LocalCoords = [-6/10, 7/10];
            J = obj.MyBSplineSurface21.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [[1719/800, -(191/800), 0.0];[57/400, 297/400, 0]]/2);
        end
        
        function testCalcJacobian22(obj)
            LocalCoords = [-6/10, 7/10];
            J = obj.MyBSplineSurface22.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [[191/200, -(573/400), 0.0];[63/100, 12/25, 0]]/2);
        end
         
        %% Test calculate det jacobian
        function testCalcDetJacobian11(obj)
            LocalCoords = [-6/10, 7/10];
            detJ = obj.MyBSplineSurface11.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,35763/32000/4);
        end  
        
        function testCalcDetJacobian12(obj)
            LocalCoords = [-6/10, 7/10];
            detJ = obj.MyBSplineSurface12.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,7467/8000/4);
        end  
        
        function testCalcDetJacobian21(obj)
            LocalCoords = [-6/10, 7/10];
            detJ = obj.MyBSplineSurface21.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,52143/32000/4);
        end  
        
        function testCalcDetJacobian22(obj)
            LocalCoords = [-6/10, 7/10];
            detJ = obj.MyBSplineSurface22.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,10887/8000/4);
        end  
        
%        %% Test Map from quad coordinates (r,s) to global coordinates
       function testMapLocalToGlobalCoords11(obj)
           function testSubSurface (  subSurface, convertCoords, coords )
            assertElementsAlmostEqual(subSurface.mapLocalToGlobal(coords), obj.MyNonRefinedBSplineSurface.mapLocalToGlobal(convertCoords(coords)));
           end
          
           convertCoords = @(coords) (coords-1)/2;
           
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [-1 -1] );
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [1 -1] );
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [1 1] );
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [-1 1] );
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [5/10, -4/10] );
            testSubSurface( obj.MyBSplineSurface11, convertCoords, [0, 0] );
       end
       
      function testMapLocalToGlobalCoords12(obj)
          function testSubSurface (  subSurface, convertCoords, coords )
            assertElementsAlmostEqual(subSurface.mapLocalToGlobal(coords), obj.MyNonRefinedBSplineSurface.mapLocalToGlobal(convertCoords(coords)));
          end
        
           convertCoords = @(coords) [(coords(1)+1)/2 (coords(2)-1)/2 ] ;
           
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [-1 -1] );
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [1 -1] );
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [1 1] );
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [-1 1] );
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [5/10, -4/10] );
            testSubSurface( obj.MyBSplineSurface12, convertCoords, [0, 0] );
      end
       
        function testMapLocalToGlobalCoords21(obj)
          function testSubSurface (  subSurface, convertCoords, coords )
            assertElementsAlmostEqual(subSurface.mapLocalToGlobal(coords), obj.MyNonRefinedBSplineSurface.mapLocalToGlobal(convertCoords(coords)));
          end
        
           convertCoords = @(coords) [(coords(1)-1)/2 (coords(2)+1)/2 ] ;
           
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [-1 -1] );
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [1 -1] );
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [1 1] );
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [-1 1] );
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [5/10, -4/10] );
            testSubSurface( obj.MyBSplineSurface21, convertCoords, [0, 0] );
        end
       
        function testMapLocalToGlobalCoords22(obj)
          function testSubSurface (  subSurface, convertCoords, coords )
            assertElementsAlmostEqual(subSurface.mapLocalToGlobal(coords), obj.MyNonRefinedBSplineSurface.mapLocalToGlobal(convertCoords(coords)));
          end
        
           convertCoords =  @(coords) (coords+1)/2 ;
           
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [-1 -1] );
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [1 -1] );
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [1 1] );
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [-1 1] );
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [5/10, -4/10] );
            testSubSurface( obj.MyBSplineSurface22, convertCoords, [0, 0] );
       end
       
       

    end

end