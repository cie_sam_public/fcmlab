%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of line class

classdef ArcTest < TestCase
    
    properties
        MyArc
    end
    
    methods
         %% Constructor
        function obj = ArcTest(name)
            obj = obj@TestCase(name);
        end
        
       %% Set up
       function setUp(obj)
           Vertex1 = Vertex([0 2 1]);
           Vertex2 = Vertex([-2 0 1]);
           VertexCenter = Vertex([0 0 1]);
           obj.MyArc = Arc(Vertex1,Vertex2,VertexCenter);
       end
       
       %% Tear Down
       function tearDown(obj)
       end
       
       %% Test get vertices
       function testGetVertices(obj)
           assertEqual(obj.MyArc.getCenter, Vertex([0 0 1]));
           assertEqual(obj.MyArc.getVertices, [Vertex([0 2 1]) Vertex([-2 0 1])]);
       end
       
       %% Test calc centroid
       function testCalcCentroid(obj)
           assertElementsAlmostEqual(obj.MyArc.calcCentroid, [-sqrt(2) sqrt(2) 1]);
       end
       
       %% Test Calculate Jacobian
       function testCalcJacobian(obj)
           anyLocalCoord = 0;
           assertElementsAlmostEqual(obj.MyArc.calcJacobian(anyLocalCoord), [-1.110720734539592 -1.110720734539592 0]);
       end
       
       %% Test Calculate det Jacobian
       function testCalcDetJacobian(obj)
           anyLocalCoord = 0.4;
           assertElementsAlmostEqual(obj.MyArc.calcDetJacobian(anyLocalCoord),pi/2);
       end
       
       %% Test Normal vector
       function testNormalVector(obj)
           anyLocalCoord = 0;
           NormalVector = obj.MyArc.calcNormalVector(anyLocalCoord);
           assertElementsAlmostEqual(NormalVector,[-1 1 0]/sqrt(2));
       end
       
       %% Test Map from local coordinates to global coordinates
       function testMapLocalToGlobalCoords(obj)
           GlobalCoordinates1 = obj.MyArc.mapLocalToGlobal(-1);
           GlobalCoordinates2 = obj.MyArc.mapLocalToGlobal(0);
           GlobalCoordinates3 = obj.MyArc.mapLocalToGlobal(1);
           assertElementsAlmostEqual(GlobalCoordinates1,[0 2 1]);
           assertElementsAlmostEqual(GlobalCoordinates2,[-sqrt(2) sqrt(2) 1]);
           assertElementsAlmostEqual(GlobalCoordinates3,[-2 0 1]);
       end
       
       %% Test Map from global coordinates to local coordinates
       function testMapGlobalToLocalCoords(obj)
           % centroid
           Centroid = obj.MyArc.calcCentroid();
           LocalCoordCentroid = obj.MyArc.mapGlobalToLocal(Centroid);
           assertElementsAlmostEqual(LocalCoordCentroid,0);
           % vertex 1
           Vertices = obj.MyArc.getVertices;
           LocalCoordVertex1 = obj.MyArc.mapGlobalToLocal(Vertices(1).getCoords());
           assertElementsAlmostEqual(LocalCoordVertex1,-1);
           % vertex 2
           LocalCoordVertex2 = obj.MyArc.mapGlobalToLocal(Vertices(2).getCoords());
           assertElementsAlmostEqual(LocalCoordVertex2,1);
       end
   end
end