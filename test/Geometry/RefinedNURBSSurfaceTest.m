%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of quad class

classdef RefinedNURBSSurfaceTest < TestCase
    
    properties
        MyNURBSSurface11
        MyNURBSSurface12
        MyNURBSSurface21
        MyNURBSSurface22
        mapFrom11ToBaseGeometry
        mapFrom12ToBaseGeometry
        mapFrom21ToBaseGeometry
        mapFrom22ToBaseGeometry
        X
        dXdXi
        dXdEta
        detJacobian
    end
    
    methods
        %% Constructor
        function obj = RefinedNURBSSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        %% Set up
        function setUp(obj)         
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            
NumberOfXDivisions=2;
NumberOfYDivisions=2;
PolynomialDegree = 4;
 
IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
geometryFactory = ExtractedNURBSSurfaceFactory( geometryDescription );
            
            obj.MyNURBSSurface11 = geometryFactory.createArea( 1 );
            obj.MyNURBSSurface12 = geometryFactory.createArea( 2 );
            obj.MyNURBSSurface21 = geometryFactory.createArea( 3 );
            obj.MyNURBSSurface22 = geometryFactory.createArea( 4 );        
                        
            obj.X = @( coords ) [ ((7 + 3 * coords(2)) * (1 + coords(1)) * (-1 - sqrt(2) + (-1 + sqrt(2)) * coords(1)))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * coords(1)^2) ...
                                ((7 + 3 * coords(2)) * (-1 + coords(1)) * (1 + sqrt(2) + (-1 + sqrt(2)) * coords(1)))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * coords(1)^2) ...
                              0 ];
                          
            obj.dXdXi = @( coords ) [ ((7 + 3 * coords(2)) * (-1 + coords(1)) * (-2 - sqrt(2) + (-2 + sqrt(2)) * coords(1)))/(2 * (2 + sqrt(2) - (-2 + sqrt(2)) * coords(1)^2)^2) ...
                                      -(((7 + 3 * coords(2)) * (1 + coords(1)) * (2 + sqrt(2) + (-2 + sqrt(2)) * coords(1)))/( 2 * (2 + sqrt(2) - (-2 + sqrt(2)) * coords(1)^2)^2))  ...
                                    0 ];
                                
            obj.dXdEta = @( coords ) [(3 * (1 +  coords(1)) * (-1 - sqrt(2) + (-1 + sqrt(2)) * coords(1)))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * coords(1)^2) ...
                                    (3 * (-1 + coords(1)) * (1 + sqrt(2) + (-1 + sqrt(2)) * coords(1)))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * coords(1)^2) ...
                                    0 ];
                                
            obj.detJacobian = @( coords )  (3 * (7 + 3 * coords(2)) * (1 + sqrt(2) + (-1 + sqrt(2)) * coords(1)^2))/(4 * (2 + sqrt(2) - (-2 + sqrt(2)) * coords(1)^2)^2) ;
            
            obj.mapFrom11ToBaseGeometry = @( coords ) (coords-1)/2;
            obj.mapFrom12ToBaseGeometry = @( coords ) [ (coords(1)+1)/2  (coords(2)-1)/2 ];
            obj.mapFrom21ToBaseGeometry = @( coords ) [ (coords(1)-1)/2  (coords(2)+1)/2 ];
            obj.mapFrom22ToBaseGeometry = @( coords ) (coords+1)/2;
        end
        
               
        %% Test get lines
        function testLines(obj)            
           function checkLine(line, analyticalMapping)
                assertElementsAlmostEqual( line.mapLocalToGlobal([-1 0 0]), analyticalMapping(-1) );
                assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), analyticalMapping(3/10) );
                assertElementsAlmostEqual( line.mapLocalToGlobal([1 0 0]), analyticalMapping(1) );
           end
           
           lines = obj.MyNURBSSurface11.getLines();
           checkLine(lines(1), @(xi) obj.X( obj.mapFrom11ToBaseGeometry( [ xi, -1 ] ) ) )
           checkLine(lines(2), @(xi) obj.X( obj.mapFrom11ToBaseGeometry( [ 1, xi ] ) ) )
           checkLine(lines(3), @(xi) obj.X( obj.mapFrom11ToBaseGeometry( [ xi, 1 ] ) ) )
           checkLine(lines(4), @(xi) obj.X( obj.mapFrom11ToBaseGeometry( [ -1, xi ] ) ) )
           
           lines = obj.MyNURBSSurface12.getLines();
           checkLine(lines(1), @(xi) obj.X( obj.mapFrom12ToBaseGeometry( [ xi, -1 ] ) ) )
           checkLine(lines(2), @(xi) obj.X( obj.mapFrom12ToBaseGeometry( [ 1, xi ] ) ) )
           checkLine(lines(3), @(xi) obj.X( obj.mapFrom12ToBaseGeometry( [ xi, 1 ] ) ) )
           checkLine(lines(4), @(xi) obj.X( obj.mapFrom12ToBaseGeometry( [ -1, xi ] ) ) )
           
           lines = obj.MyNURBSSurface21.getLines();
           checkLine(lines(1), @(xi) obj.X( obj.mapFrom21ToBaseGeometry( [ xi, -1 ] ) ) )
           checkLine(lines(2), @(xi) obj.X( obj.mapFrom21ToBaseGeometry( [ 1, xi ] ) ) )
           checkLine(lines(3), @(xi) obj.X( obj.mapFrom21ToBaseGeometry( [ xi, 1 ] ) ) )
           checkLine(lines(4), @(xi) obj.X( obj.mapFrom21ToBaseGeometry( [ -1, xi ] ) ) )
           
           lines = obj.MyNURBSSurface22.getLines();
           checkLine(lines(1), @(xi) obj.X( obj.mapFrom22ToBaseGeometry( [ xi, -1 ] ) ) )
           checkLine(lines(2), @(xi) obj.X( obj.mapFrom22ToBaseGeometry( [ 1, xi ] ) ) )
           checkLine(lines(3), @(xi) obj.X( obj.mapFrom22ToBaseGeometry( [ xi, 1 ] ) ) )
           checkLine(lines(4), @(xi) obj.X( obj.mapFrom22ToBaseGeometry( [ -1, xi ] ) ) )
        end

        
        %% Test calculate base vector1
        function testCalcBaseVector1(obj)
            LocalCoords = [5/10, -4/10];
            
            g1 = obj.MyNURBSSurface11.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,obj.dXdXi( obj.mapFrom11ToBaseGeometry( LocalCoords ) ) /2 );
            
            g1 = obj.MyNURBSSurface12.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,obj.dXdXi(  obj.mapFrom12ToBaseGeometry( LocalCoords ) ) /2 );
            
            g1 = obj.MyNURBSSurface21.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,obj.dXdXi(  obj.mapFrom21ToBaseGeometry( LocalCoords ) ) /2 );
            
            g1 = obj.MyNURBSSurface22.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,obj.dXdXi(  obj.mapFrom22ToBaseGeometry( LocalCoords ) ) /2 );            
        end
        
        %% Test calculate base vector2
        function testCalcBaseVector2(obj)
            LocalCoords = [1/10, -9/10];
            
            g2 = obj.MyNURBSSurface11.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,obj.dXdEta( obj.mapFrom11ToBaseGeometry( LocalCoords ) ) /2);
            
            g2 = obj.MyNURBSSurface12.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,obj.dXdEta( obj.mapFrom12ToBaseGeometry( LocalCoords ) ) /2);
        
            g2 = obj.MyNURBSSurface21.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,obj.dXdEta( obj.mapFrom21ToBaseGeometry( LocalCoords ) ) /2);
        
            g2 = obj.MyNURBSSurface22.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,obj.dXdEta( obj.mapFrom22ToBaseGeometry( LocalCoords ) ) /2);
        end
        
        %% Test calculate normal vector
        function testCalcNormalVector(obj)
            LocalCoords = [0.3 0.4];
            assertElementsAlmostEqual( obj.MyNURBSSurface11.calcNormalVector(LocalCoords) ,[0 0 1]);
            assertElementsAlmostEqual( obj.MyNURBSSurface12.calcNormalVector(LocalCoords) ,[0 0 1]);
            assertElementsAlmostEqual( obj.MyNURBSSurface21.calcNormalVector(LocalCoords) ,[0 0 1]);
            assertElementsAlmostEqual( obj.MyNURBSSurface22.calcNormalVector(LocalCoords) ,[0 0 1]);
        end
        
        %% Test calculate jacobian
        function testCalcJacobian(obj)
            LocalCoords = [0.3 0.4];
            
            J = obj.MyNURBSSurface11.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [  obj.dXdXi( obj.mapFrom11ToBaseGeometry( LocalCoords ) )/2; ...
                                            obj.dXdEta( obj.mapFrom11ToBaseGeometry( LocalCoords ) )/2      ]);
                                        
            J = obj.MyNURBSSurface12.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [  obj.dXdXi( obj.mapFrom12ToBaseGeometry( LocalCoords ) )/2; ...
                                            obj.dXdEta( obj.mapFrom12ToBaseGeometry( LocalCoords ) )/2      ]);
            
            J = obj.MyNURBSSurface21.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [  obj.dXdXi( obj.mapFrom21ToBaseGeometry( LocalCoords ) )/2; ...
                                            obj.dXdEta( obj.mapFrom21ToBaseGeometry( LocalCoords ) )/2      ]);
            
            J = obj.MyNURBSSurface22.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [  obj.dXdXi( obj.mapFrom22ToBaseGeometry( LocalCoords ) )/2; ...
                                            obj.dXdEta( obj.mapFrom22ToBaseGeometry( LocalCoords ) )/2      ]);
        end
        
        %% Test calculate det jacobian
        function testCalcDetJacobian(obj)
            LocalCoords = [0.3 0.4];
            detJ = obj.MyNURBSSurface11.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,obj.detJacobian( obj.mapFrom11ToBaseGeometry( LocalCoords )  ) /4 );
            
            detJ = obj.MyNURBSSurface12.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,obj.detJacobian( obj.mapFrom12ToBaseGeometry( LocalCoords )  ) /4 );
            
            detJ = obj.MyNURBSSurface21.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,obj.detJacobian( obj.mapFrom21ToBaseGeometry( LocalCoords )  ) /4 );
            
            detJ = obj.MyNURBSSurface22.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,obj.detJacobian( obj.mapFrom22ToBaseGeometry( LocalCoords )  ) /4 );
            
        end       
     
        %% Test Map from quad coordinates (r,s) to global coordinates
       function testMapLocalToGlobalCoords(obj)

           function checkPoint( Surface, LocalCoords, MapToBaseGeometryFunction)
               assertElementsAlmostEqual( Surface.mapLocalToGlobal(LocalCoords), obj.X( MapToBaseGeometryFunction( LocalCoords ) ) );
           end
           
           function checkPointsOfSurface( Surface, MapToBaseGeometryFunction )
                checkPoint( Surface, [-1 -1], MapToBaseGeometryFunction );
                checkPoint( Surface, [ 1 -1], MapToBaseGeometryFunction );
                checkPoint( Surface, [ 1  1], MapToBaseGeometryFunction );
                checkPoint( Surface, [-1  1], MapToBaseGeometryFunction );

                checkPoint( Surface, [0.5, -0.4], MapToBaseGeometryFunction );
                checkPoint( Surface, [-0.99123, 1], MapToBaseGeometryFunction );
           end
           
            checkPointsOfSurface(obj.MyNURBSSurface11, obj.mapFrom11ToBaseGeometry)
            checkPointsOfSurface(obj.MyNURBSSurface12, obj.mapFrom12ToBaseGeometry)
            checkPointsOfSurface(obj.MyNURBSSurface21, obj.mapFrom21ToBaseGeometry)
            checkPointsOfSurface(obj.MyNURBSSurface22, obj.mapFrom22ToBaseGeometry)
           
       end

   end
end