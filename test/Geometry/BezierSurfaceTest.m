%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of quad class

classdef BezierSurfaceTest < TestCase
    
    properties
        MyBSplineSurface
    end
    
    methods
        %% Constructor
        function obj = BezierSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        %% Set up
        function setUp(obj)         
            geometryDescription = GenerateQuarterDisk(1,2.5);
            geometryDescription.removeWeights();
            
            obj.MyBSplineSurface = BezierSurface(geometryDescription.getControlPoints, geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
        end
        
               
        %% Test get lines
        function testLines(obj)
            lines = obj.MyBSplineSurface.getLines();
            
            line =  lines(1);
            assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 1 0] );
            assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [351/400, 231/400, 0] );
            assertEqual( line.mapLocalToGlobal([1 0 0]), [1 0 0] );
             
            line =  lines(2);
            assertEqual( line.mapLocalToGlobal([-1 0 0]), [1 0 0] );
            assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [79/40, 0, 0] );
            assertEqual( line.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
            
            line =  lines(3);
            assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 2.5 0] );
            assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [351/160, 231/160, 0] );
            assertEqual( line.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
            
            line =  lines(4);
            assertEqual( line.mapLocalToGlobal([-1 0 0]), [0 1 0] );
            assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), [0, 79/40, 0] );
            assertEqual( line.mapLocalToGlobal([1 0 0]), [0 2.5 0] );
        end
        
        
        %% Test calculate base vector1
        function testCalcBaseVector1(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyBSplineSurface.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,[29/80, -(87/80), 0]);
        end
%         
        %% Test calculate base vector2
        function testCalcBaseVector2(obj)
            LocalCoords = [5/10, -4/10];
            g2 = obj.MyBSplineSurface.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,[45/64, 21/64, 0]);
        end
        
        %% Test calculate normal vector
        function testCalcNormalVector(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyBSplineSurface.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
        
        %% Test calculate jacobian
        function testCalcJacobian(obj)
            LocalCoords = [-6/10, 7/10];
            J = obj.MyBSplineSurface.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [[91/50, -(91/200) 0.0];[27/100, 18/25, 0]]);
        end
        
        %% Test calculate det jacobian
        function testCalcDetJacobian(obj)
            LocalCoords = [-6/10, 7/10];
            detJ = obj.MyBSplineSurface.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,5733/4000);
        end       
       
        %% Test Map from quad coordinates (r,s) to global coordinates
       function testMapLocalToGlobalCoords(obj)
           assertEqual(obj.MyBSplineSurface.mapLocalToGlobal([-1 -1]), [0 1 0]);
           assertEqual(obj.MyBSplineSurface.mapLocalToGlobal([ 1 -1]),[1 0 0]);
           assertEqual(obj.MyBSplineSurface.mapLocalToGlobal([ 1  1]),[2.5 0 0]);
           assertEqual(obj.MyBSplineSurface.mapLocalToGlobal([-1  1]),[0 2.5 0]);
           assertElementsAlmostEqual(obj.MyBSplineSurface.mapLocalToGlobal([0 0]),[21/16, 21/16, 0]);
           
           assertElementsAlmostEqual(obj.MyBSplineSurface.mapLocalToGlobal([5/10, -4/10]),[87/64, 203/320, 0]);
       end

   end
end