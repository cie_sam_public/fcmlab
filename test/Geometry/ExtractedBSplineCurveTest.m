%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of line class

classdef ExtractedBSplineCurveTest < TestCase
    
    properties
        MyCurve1
        MyCurve2
    end
    
    methods
         %% Constructor
        function obj = ExtractedBSplineCurveTest(name)
            obj = obj@TestCase(name);
        end
        
       %% Set up
       function setUp(obj)
           ControlPoints = [
                0, 5/2, 0; ...
                125/100, 5/2, 0; ...
                5/2, 125/100, 0;...
                5/2, 0, 0];
      
            ExtractionOperator1 = [ 1         0         0;...
                                    0    1    0.5; ...
                                    0         0    0.5];
                         
            ExtractionOperator2 = [ 0.5        0         0;...
                                    0.5    1         0;...
                                    0         0    1];
            obj.MyCurve1 = ExtractedBSplineCurve( ControlPoints(1:3,:), ExtractionOperator1 );
            obj.MyCurve2 = ExtractedBSplineCurve( ControlPoints(2:4,:), ExtractionOperator2 );
       end
       
       %% Tear Down
       function tearDown(obj)
       end
                     
       %% Test Calculate Jacobian
       function testCalcJacobian(obj)
           anyLocalCoord = -0.4 ;
           assertElementsAlmostEqual(obj.MyCurve2.calcJacobian(anyLocalCoord), [7/8, -(13/8), 0]/2);
       end
       
       %% Test Calculate det Jacobian
       function testCalcDetJacobian(obj)
           anyLocalCoord = -0.4 ;
           assertElementsAlmostEqual(obj.MyCurve2.calcDetJacobian(anyLocalCoord),sqrt(109/2)/4/2);
       end
       
       %% Test Normal vector
       function testNormalVector(obj)
           anyLocalCoord = -0.4 ;
           NormalVector = obj.MyCurve2.calcNormalVector(anyLocalCoord);
           assertElementsAlmostEqual(NormalVector,[-13/sqrt(218), -7/sqrt(218) 0]);
       end
       
       %% Test Map from local coordinates to global coordinates
       function testMapLocalToGlobalCoords(obj)
                     
            assertElementsAlmostEqual(  obj.MyCurve1.mapLocalToGlobal([-1 0 0]), [0 2.5 0] );
            
            assertElementsAlmostEqual(  obj.MyCurve2.mapLocalToGlobal([-0.4 0 0]), [351/160, 231/160, 0] );
            assertElementsAlmostEqual(  obj.MyCurve2.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
       end
       
   end
end