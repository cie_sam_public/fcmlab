%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of quad class

classdef NURBSSurfaceTest < TestCase
    
    properties
        MyNURBSSurface
        X
        dXdXi
        dXdEta
        detJacobian
    end
    
    methods
        %% Constructor
        function obj = NURBSSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        %% Set up
        function setUp(obj)         
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            
            obj.MyNURBSSurface = ExtractedNURBSSurface(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            
            obj.X = @(xi,eta) [ ((7 + 3 * eta) * (1 + xi) * (-1 - sqrt(2) + (-1 + sqrt(2)) * xi))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * xi^2) ...
                                ((7 + 3 * eta) * (-1 + xi) * (1 + sqrt(2) + (-1 + sqrt(2)) * xi))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * xi^2) ...
                              0 ];
                          
            obj.dXdXi = @(xi,eta) [ ((7 + 3 * eta) * (-1 + xi) * (-2 - sqrt(2) + (-2 + sqrt(2)) * xi))/(2 * (2 + sqrt(2) - (-2 + sqrt(2)) * xi^2)^2) ...
                                      -(((7 + 3 * eta) * (1 + xi) * (2 + sqrt(2) + (-2 + sqrt(2)) * xi))/( 2 * (2 + sqrt(2) - (-2 + sqrt(2)) * xi^2)^2))  ...
                                    0 ];
                                
            obj.dXdEta = @(xi,eta) [(3 * (1 +  xi) * (-1 - sqrt(2) + (-1 + sqrt(2)) * xi))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * xi^2) ...
                                    (3 * (-1 + xi) * (1 + sqrt(2) + (-1 + sqrt(2)) * xi))/(-4 * (2 + sqrt(2)) + 4 * (-2 + sqrt(2)) * xi^2) ...
                                    0 ];
                                
            obj.detJacobian = @(xi,eta)  (3 * (7 + 3 * eta) * (1 + sqrt(2) + (-1 + sqrt(2)) * xi^2))/(4 * (2 + sqrt(2) - (-2 + sqrt(2)) * xi^2)^2) ;
        end
        
               
        %% Test get lines
        function testLines(obj)
            lines = obj.MyNURBSSurface.getLines();
            
           function checkLine(line, analyticalMapping)
                assertElementsAlmostEqual( line.mapLocalToGlobal([-1 0 0]), analyticalMapping(-1) );
                assertElementsAlmostEqual( line.mapLocalToGlobal([3/10 0 0]), analyticalMapping(3/10) );
                assertElementsAlmostEqual( line.mapLocalToGlobal([1 0 0]), analyticalMapping(1) );
           end
           
           checkLine(lines(1), @(xi) obj.X(xi, -1) )
           checkLine(lines(2), @(xi) obj.X(1, xi) )
           checkLine(lines(3), @(xi) obj.X(xi, 1) )
           checkLine(lines(4), @(xi) obj.X(-1, xi) )
        end

        
        %% Test calculate base vector1
        function testCalcBaseVector1(obj)
            LocalCoords = [5/10, -4/10];
            g1 = obj.MyNURBSSurface.calcBaseVector1(LocalCoords);
            assertElementsAlmostEqual(g1,obj.dXdXi(LocalCoords(1), LocalCoords(2)));
        end
        
        %% Test calculate base vector2
        function testCalcBaseVector2(obj)
            LocalCoords = [0 0];
            g2 = obj.MyNURBSSurface.calcBaseVector2(LocalCoords);
            assertElementsAlmostEqual(g2,obj.dXdEta(LocalCoords(1), LocalCoords(2)));
        end
        
        %% Test calculate normal vector
        function testCalcNormalVector(obj)
            LocalCoords = [0.3 0.4];
            NormalVector = obj.MyNURBSSurface.calcNormalVector(LocalCoords);
            assertElementsAlmostEqual(NormalVector,[0 0 1]);
        end
        
        %% Test calculate jacobian
        function testCalcJacobian(obj)
            LocalCoords = [0.3 0.4];
            J = obj.MyNURBSSurface.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, [obj.dXdXi(LocalCoords(1), LocalCoords(2));obj.dXdEta(LocalCoords(1), LocalCoords(2))]);
        end
        
        %% Test calculate det jacobian
        function testCalcDetJacobian(obj)
            LocalCoords = [0.3 0.4];
            detJ = obj.MyNURBSSurface.calcDetJacobian(LocalCoords);
            assertElementsAlmostEqual(detJ,obj.detJacobian(LocalCoords(1), LocalCoords(2)));
        end       
     
        %% Test Map from quad coordinates (r,s) to global coordinates
       function testMapLocalToGlobalCoords(obj)

           function checkPoint(LocalCoords)
               assertElementsAlmostEqual(obj.MyNURBSSurface.mapLocalToGlobal(LocalCoords), obj.X(LocalCoords(1), LocalCoords(2)) );
           end
           
          checkPoint([-1 -1]);
          checkPoint([ 1 -1]);
          checkPoint([ 1  1]);
          checkPoint([-1  1]);
          
          checkPoint([0.5, -0.4]);
          checkPoint([-0.99123, 1]);
       end

   end
end