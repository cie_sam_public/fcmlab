%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%Test of line class

classdef ProjectiveCurveTest < TestCase
    
    properties
        MyCurve
    end
    
    methods
         %% Constructor
        function obj = ProjectiveCurveTest(name)
            obj = obj@TestCase(name);
        end
        
       %% Set up
       function setUp(obj)
           ControlPoints = [0, 2.5, 0, 1,
                            2.5/sqrt(2), 2.5/sqrt(2), 0, 1/sqrt(2),
                            2.5, 0, 0, 1];
                        
           BSplineCurve = ExtractedBSplineCurve( ControlPoints, eye(size(ControlPoints,1)) );
           obj.MyCurve = ProjectiveCurve( BSplineCurve );
       end
       
       %% Tear Down
       function tearDown(obj)
       end
                     
       %% Test Calculate Jacobian
       function testCalcJacobian(obj)
           anyLocalCoord = 0.3;
           assertElementsAlmostEqual(obj.MyCurve.calcJacobian(anyLocalCoord), [1.045358246, -1.751309988, 0]);
       end
       
       %% Test Calculate det Jacobian
       function testCalcDetJacobian(obj)
           anyLocalCoord = 0.3;
           assertElementsAlmostEqual(obj.MyCurve.calcDetJacobian(anyLocalCoord), norm([1.045358246, -1.751309988, 0]));
       end
       
       %% Test Normal vector
       function testNormalVector(obj)
           anyLocalCoord = 0.3;
           NormalVector = obj.MyCurve.calcNormalVector(anyLocalCoord);
           assertElementsAlmostEqual(NormalVector,[-1.751309988, -1.045358246, 0]/ norm([-1.751309988, -1.045358246, 0]));
       end
       
       %% Test Map from local coordinates to global coordinates
       function testMapLocalToGlobalCoords(obj)
                     
            assertElementsAlmostEqual(  obj.MyCurve.mapLocalToGlobal([-1 0 0]), [0 2.5 0] );
            assertElementsAlmostEqual(  obj.MyCurve.mapLocalToGlobal([3/10 0 0]), [2.146661898, 1.281344097, 0] );
            assertElementsAlmostEqual(  obj.MyCurve.mapLocalToGlobal([1 0 0]), [2.5 0 0] );
       end
       
   end
end