%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticExtractedNURBSSurfaceTest < TestCase
    
    properties
        Element
        
        Shapes
        DShapesDXi
        DShapesDEta
        
        X
        Jacobian
        Perm
    end
    
    methods
        
        %% constructor
        function obj = ElasticExtractedNURBSSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Mat2D = HookePlaneStress(5,0.2,1,1); 
            PolynomialOrder = 2;
            integrator = Integrator(NoPartition(),PolynomialOrder+2);
            Domain = PseudoDomain(); % domain returns always 2
           
            DofDimension = 2;
            numberOfDofsPerDirection = PolynomialOrder+1;
            
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            
           ControlPoints = geometryDescription.getControlPoints;
            geometricFace = ExtractedNURBSSurface(ControlPoints, eye(length(ControlPoints(:,1))), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            geometricEdges = geometricFace.getLines();
            
            for iGeometricEdge = 1:length(geometricEdges)
                topologicEdges(iGeometricEdge) = Edge([], numberOfDofsPerDirection, DofDimension, geometricEdges(iGeometricEdge));
            end
            topologicFace = Face([], topologicEdges, numberOfDofsPerDirection, DofDimension, geometricFace );
                    
            obj.Element = ElasticExtractedNURBSurface([], topologicEdges,...
                topologicFace, Mat2D,integrator,PolynomialOrder,DofDimension,Domain,  ControlPoints(:,4), eye(length(ControlPoints(:,1))), 1, 1 );  
            
            obj.Perm = [1     5     2     8     9     6     4     7     3];
            
            obj.Shapes = @(coords)  [-(((-1 + coords(2))^2*(-1 + coords(1))^2)/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2)), -(((-1 + coords(2))^2*(1 + coords(1))^2)/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2)), ...
 -(((1 + coords(2))^2*(1 + coords(1))^2)/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2)), -(((1 + coords(2))^2*(-1 + coords(1))^2)/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2)), -((-1 + coords(2))^2*(-1 + coords(1)^2))/(4*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)), ...
 ((-1 + coords(2)^2)*(1 + coords(1))^2)/(-2*(2 + sqrt(2)) + 2*(-2 + sqrt(2))*coords(1)^2), -((1 + coords(2))^2*(-1 + coords(1)^2))/(4*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)), ((-1 + coords(2)^2)*(-1 + coords(1))^2)/(-2*(2 + sqrt(2)) + 2*(-2 + sqrt(2))*coords(1)^2), ...
 ((-1 + coords(2)^2)*(-1 + coords(1)^2))/(2*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2))];
            obj.DShapesDXi = @(coords) [-((-1 + coords(2))^2*(-1 + coords(1))*(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2), ((-1 + coords(2))^2*(1 + coords(1))*(2 + sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2),  ...
 ((1 + coords(2))^2*(1 + coords(1))*(2 + sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2), -((1 + coords(2))^2*(-1 + coords(1))*(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2),  ...
 -((sqrt(2)*(-1 + coords(2))^2*coords(1))/(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)^2), -(((-1 + coords(2)^2)*(1 + coords(1))*(2 + sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2),  ...
 -((sqrt(2)*(1 + coords(2))^2*coords(1))/(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)^2), ((-1 + coords(2)^2)*(-1 + coords(1))*(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2,  ...
 (2*sqrt(2)*(-1 + coords(2)^2)*coords(1))/(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)^2];

            obj.DShapesDEta = @(coords) [((-1 + coords(2))*(-1 + coords(1))^2)/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)), ((-1 + coords(2))*(1 + coords(1))^2)/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)), ((1 + coords(2))*(1 + coords(1))^2)/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)), ...
 ((1 + coords(2))*(-1 + coords(1))^2)/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)), -((-1 + coords(2))*(-1 + coords(1)^2))/(2*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)), (coords(2)*(1 + coords(1))^2)/(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)^2), ...
 -((1 + coords(2))*(-1 + coords(1)^2))/(2*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)), (coords(2)*(-1 + coords(1))^2)/(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)^2), (coords(2)*(-1 + coords(1)^2))/(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)^2)];
            
            obj.X = @(coords) [ ((7 + 3*coords(2))*(1 + coords(1))*(-1 - sqrt(2) + (-1 + sqrt(2))*coords(1)))/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2), ((7 + 3*coords(2))*(-1 + coords(1))*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)))/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2), 0];
            obj.Jacobian = @(coords) [ ((7 + 3*coords(2))*(-1 + coords(1))*(-2 - sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2), -((7 + 3*coords(2))*(1 + coords(1))*(2 + sqrt(2) + (-2 + sqrt(2))*coords(1)))/(2*(2 + sqrt(2) - (-2 + sqrt(2))*coords(1)^2)^2) 0; ...
 (3*(1 + coords(1))*(-1 - sqrt(2) + (-1 + sqrt(2))*coords(1)))/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2), (3*(-1 + coords(1))*(1 + sqrt(2) + (-1 + sqrt(2))*coords(1)))/(-4*(2 + sqrt(2)) + 4*(-2 + sqrt(2))*coords(1)^2) 0];
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            localCoord = [-6/10, 7/10 0];
            assertElementsAlmostEqual(obj.Element.calcJacobian(localCoord), obj.Jacobian(localCoord) );
        end   

        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element.mapGlobalToLocal( obj.X( localCoords ) ), localCoords );
            end
            
            checkPoint([-1 -1]);
            checkPoint([1 -1]);
            checkPoint([-1 1]);
            checkPoint([1 1]);
            
            checkPoint([0 0]);
            checkPoint([5/10, -4/10]);
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element.mapLocalToGlobal( localCoords ), obj.X( localCoords ) );
            end
            
            checkPoint([-1 -1]);
            checkPoint([1 -1]);
            checkPoint([-1 1]);
            checkPoint([1 1]);
            
            checkPoint([0 0]);
            checkPoint([5/10, -4/10]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            localCoord = [5/10, -4/10];
            
            NTest1 = obj.Element.evalShapeFunct(localCoord);
            ShapesTest = obj.Shapes(localCoord)
            assertElementsAlmostEqual(NTest1, ShapesTest(obj.Perm));
        end
        
        function testGetShapeFunctionsMatrix(obj)
            localCoord = [5/10, -4/10];
            shapesTest = obj.Shapes(localCoord);
            assertElementsAlmostEqual(obj.Element.getShapeFunctionsMatrix(localCoord), [shapesTest(obj.Perm) zeros(1,9); zeros(1,9) shapesTest(obj.Perm)]);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            localCoord = [5/10, -4/10];
             
            dN1 = obj.Element.evalDerivOfShapeFunct(localCoord);
            ShapesXiTest =  obj.DShapesDXi( localCoord );
            ShapesEtaTest = obj.DShapesDEta( localCoord );
            assertElementsAlmostEqual(dN1, [ ShapesXiTest(obj.Perm) ShapesEtaTest(obj.Perm) ] );
        end
        
        %% test evaluation of B
        function testGetB(obj)
            
            u = [(-7*(-9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2) - 7*(-128 - 107*sqrt(2) + (60 - 24*sqrt(2))/8 - (5*(-7 + 2*sqrt(2)))/4 + (3*(-9 + 7*sqrt(2)))/8)))/(435*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (-21*(-9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2) - 7*(-64 - 55*sqrt(2) + (4 - 8*sqrt(2))/8 - (5*(-7 + 2*sqrt(2)))/4 + (-5 + 3*sqrt(2))/8)))/(145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (9*(608 + 485*sqrt(2) + (12 - 24*sqrt(2))/8 + (35*(-7 + 2*sqrt(2)))/4 + (3*(-5 + 3*sqrt(2)))/8 - 9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2)))/(145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (185 + 82*sqrt(2) + (35*(-7 + 2*sqrt(2)))/4 - (9*(-5 + 2*sqrt(2)))/2 + (9*(-9 + 7*sqrt(2)))/8 + (462 + 494*sqrt(2))/2 - 9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (7*(-7*(159 + 98*sqrt(2) - 4*(-1 + sqrt(2)) - (5*(-4 + 7*sqrt(2)))/4) - 9*(-16 - 13*sqrt(2) + (-4 + 7*sqrt(2))/2)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (12*(-9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2) + 7*(104 + 80*sqrt(2) + (6 - 12*sqrt(2))/8 + (5*(-7 + 2*sqrt(2)))/4 + (3*(-5 + 3*sqrt(2)))/16)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (3*(416 + 359*sqrt(2) + 12*(-1 + sqrt(2)) - 3*(-3 + 2*sqrt(2)) - (35*(-4 + 7*sqrt(2)))/4 + (796 + 306*sqrt(2))/2 + 9*(-16 - 13*sqrt(2) + (-4 + 7*sqrt(2))/2)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (4*(-9*(13 + 8*sqrt(2) + (-7 + 2*sqrt(2))/2) + 7*(8 + 2*sqrt(2) + (90 - 36*sqrt(2))/8 + (5*(-7 + 2*sqrt(2)))/4 + (9*(-9 + 7*sqrt(2)))/16)))/...
  (435*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (4*(9*(-16 - 13*sqrt(2) + (-4 + 7*sqrt(2))/2) - 7*(-82 - 56*sqrt(2) - 6*(-1 + sqrt(2)) + (3*(-3 + 2*sqrt(2)))/2 + (5*(-4 + 7*sqrt(2)))/4)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4))];

v=[ ...
 (-7*(3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2) + 7*(-82 - 75*sqrt(2) + (5*(-7 + 2*sqrt(2)))/4 + (3*(-5 + 2*sqrt(2)))/2 + (3*(-9 + 7*sqrt(2)))/8)))/(435*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (-21*(3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2) + 7*(86 + 41*sqrt(2) + (5*(-7 + 2*sqrt(2)))/4 + (-5 + 3*sqrt(2))/8 + (-4 + 8*sqrt(2))/8)))/(145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (9*(242 + 27*sqrt(2) + (12 - 24*sqrt(2))/8 + (35*(-7 + 2*sqrt(2)))/4 - (3*(-5 + 3*sqrt(2)))/8 + 3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2)))/(145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (725 + 478*sqrt(2) + (42 - 206*sqrt(2))/2 + (35*(-7 + 2*sqrt(2)))/4 - (9*(-5 + 2*sqrt(2)))/2 - (9*(-9 + 7*sqrt(2)))/8 + 3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (7*(3*(16 + 13*sqrt(2) + (-4 + 7*sqrt(2))/2) + 7*(31 + 4*(-1 + sqrt(2)) + (5*(-4 + 7*sqrt(2)))/4)))/(145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), ...
 (12*(3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2) - 7*(-13*(2 + sqrt(2)) - (5*(-7 + 2*sqrt(2)))/4 + (3*(-1 + 2*sqrt(2)))/4 + (3*(-5 + 3*sqrt(2)))/16 + (60 + 74*sqrt(2))/2)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (-3*(-402 - 494*sqrt(2) - 12*(-1 + sqrt(2)) - 3*(-3 + 2*sqrt(2)) + (35*(-4 + 7*sqrt(2)))/4 + 3*(16 + 13*sqrt(2) + (-4 + 7*sqrt(2))/2)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (4*(3*(-13 - 8*sqrt(2) + (-7 + 2*sqrt(2))/2) - 7*(-248 - 150*sqrt(2) - (5*(-7 + 2*sqrt(2)))/4 + (9*(-5 + 2*sqrt(2)))/4 + (9*(-9 + 7*sqrt(2)))/16)))/...
  (435*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4)), (-4*(-7*(126 + 122*sqrt(2) + 6*(-1 + sqrt(2)) + (3*(-3 + 2*sqrt(2)))/2 - (5*(-4 + 7*sqrt(2)))/4) + 3*(16 + 13*sqrt(2) + (-4 + 7*sqrt(2))/2)))/...
  (145*(59 + 30*sqrt(2))*(1 + sqrt(2) + (-1 + sqrt(2))/4))];
           
                assertElementsAlmostEqual(obj.Element.getB([5/10, -4/10]), [u(obj.Perm) zeros(1,9); zeros(1,9) v(obj.Perm); v(obj.Perm) u(obj.Perm)]);
        end
        
    end
    
end

