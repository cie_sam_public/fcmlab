%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticBSplineQuadTest < TestCase
    
    properties
        Elem
        Elem2
        Integrator
        PolynomialOrder
        DofDimension
        EdgeLineCoordTest
    end
    
    methods
        
        %% constructor
        function obj = ElasticBSplineQuadTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Mat2D(1) = HookePlaneStress(5,0.2,1,1e-20);
            Mat2D(2) = HookePlaneStress(5,0.2,1,1); 
            obj.Integrator = Integrator(QuadTree(TestCircularDomainBSpline,5),4);
            obj.PolynomialOrder = 2;
            obj.DofDimension = 2;
            
            Node1 = Node([3 5 0],7);
            Node2 = Node([5 5 0],8);
            Node3 = Node([5 8 0],9);
            Node4 = Node([3 8 0],10);
            
            Edge1 = Edge([Node1 Node2],3,2);
            Edge2 = Edge([Node2 Node3],3,2);
            Edge3 = Edge([Node3 Node4],3,2);
            Edge4 = Edge([Node4 Node1],3,2);
            
            Face1 = Face([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],3,2);
            
     
            knotVector=[-1 -1 -1 1 1 1 ];

            obj.Elem = ElasticBSplineQuad([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],...
                Face1, Mat2D(2),obj.Integrator,obj.PolynomialOrder,knotVector,obj.DofDimension,PseudoDomain());  
            
            % needed for the test of the line mapping
            CenterNode = Node([4 6.5 0],2);
            obj.EdgeLineCoordTest = Edge([Node1 CenterNode],2,3);
            
            % element for domain integration in FCM
            obj.Elem2 = ElasticBSplineQuad([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],...
                Face1, Mat2D,obj.Integrator,obj.PolynomialOrder,knotVector,obj.DofDimension,TestCircularDomainBSpline());
        end
        
        function tearDown(obj)
        end

        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapGlobalToLocal([4 6.5]), [0 0 ]);
            assertVectorsAlmostEqual(obj.Elem.mapGlobalToLocal([3.5 7]), [-0.5 1/3 ]);
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapLocalToGlobal([0 0]), [4 6.5 0]);
            assertVectorsAlmostEqual(obj.Elem.mapLocalToGlobal([-0.5 1/3]), [3.5 7 0]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            NTest1 = obj.Elem.evalShapeFunct([0 0]);
            assertElementsAlmostEqual(NTest1, [0.0625 0.0625 0.0625 0.0625...
                0.125 0.125 0.125 0.125 0.25]);
            
            NTest2 = obj.Elem.evalShapeFunct([0.5 0.25]);
            assertElementsAlmostEqual(NTest2, [0.0087890625 0.079101563...
                0.21972656 0.024414063 0.052734375 0.26367188 0.14648438...
                0.029296875 0.17578125]);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            dN1 = obj.Elem.evalDerivOfShapeFunct([0 0]);
    
            
            assertElementsAlmostEqual(dN1,...
                [-0.125 0.125 0.125 -0.125 0 0.25 0 -0.25 0 -0.125 -0.125...
                0.125 0.125 -0.25 0 0.25 0 0]);
            
            dN2 = obj.Elem.evalDerivOfShapeFunct([0.5 0.25]);
            assertElementsAlmostEqual(dN2,...
                [-0.035156250 0.10546875 0.29296875 -0.097656250 -0.070312500...
                0.35156250 -0.19531250 -0.11718750 -0.23437500 -0.023437500....
                -0.21093750 0.35156250 0.039062500 -0.14062500 -0.14062500...
                0.23437500 -0.015625000 -0.093750000]);
        end
        
        %% test evaluation of B and Grad Q
        function testGetBAndGradQ(obj)
            u = [-0.035156250 0.10546875 0.29296875 -0.097656250 -0.070312500...
                0.35156250 -0.19531250 -0.11718750 -0.23437500]; 
            v = [-0.015625000 -0.14062500 0.23437500 0.026041666 -0.093750000...
                -0.093750000 0.15625000 -0.010416667 -0.062500000];
            assertElementsAlmostEqual(obj.Elem.getB([0.5 0.25]), [u zeros(1,9); zeros(1,9) v; v u]);
            assertElementsAlmostEqual(obj.Elem.getGradQ([0.5 0.25]), [u zeros(1,9); zeros(1,9) v]);

        end

    end
    
end

