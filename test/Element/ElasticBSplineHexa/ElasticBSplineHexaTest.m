%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticHexa

classdef ElasticBSplineHexaTest < TestCase
    
    properties
        Elem
        Integrator
        PolynomialDegree
        DofDimension
        EdgeLineCoordTest
        EdgeLineCoordTest2
    end
    
    methods
        
        %% constructor
        function obj = ElasticBSplineHexaTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, depth in y = 3, height in z = 9
            % E = 5, Poisson's ratio = 0.2
            Mat3D = Hooke3D(5,0.2,1,1);       
            obj.Integrator = Integrator(NoPartition,3);
            obj.PolynomialDegree = 2;
            obj.DofDimension = 3;
            Node1 = Node([3 5 0],3);
            Node2 = Node([5 5 0],3);
            Node3 = Node([5 8 0],3);
            Node4 = Node([3 8 0],3);
            Node5 = Node([3 5 9],3);
            Node6 = Node([5 5 9],3);
            Node7 = Node([5 8 9],3);
            Node8 = Node([3 8 9],3);
            Edge1 = Edge([Node1 Node2],3,3);
            Edge2 = Edge([Node2 Node3],3,3);
            Edge3 = Edge([Node4 Node3],3,3);
            Edge4 = Edge([Node1 Node4],3,3);
            Edge5 = Edge([Node1 Node5],3,3);
            Edge6 = Edge([Node2 Node6],3,3);
            Edge7 = Edge([Node3 Node7],3,3);
            Edge8 = Edge([Node4 Node8],3,3);
            Edge9 = Edge([Node5 Node6],3,3);
            Edge10 = Edge([Node6 Node7],3,3);
            Edge11 = Edge([Node8 Node7],3,3);
            Edge12 = Edge([Node5 Node8],3,3);
            Face1 = Face([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],3,3);
            Face2 = Face([Node1 Node2 Node6 Node5], [Edge1 Edge5 Edge6 Edge9],3,3);
            Face3 = Face([Node2 Node3 Node7 Node6], [Edge2 Edge6 Edge7 Edge10],3,3);
            Face4 = Face([Node3 Node4 Node8 Node7], [Edge3 Edge7 Edge8 Edge11],3,3);
            Face5 = Face([Node1 Node4 Node8 Node5], [Edge4 Edge5 Edge8 Edge12],3,3);
            Face6 = Face([Node5 Node6 Node7 Node8], [Edge9 Edge10 Edge11 Edge12],3,3);
            
            solid1 = Solid([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6],3,3);
            

            knotVector=[-1 -1 -1 1 1 1];
            
            obj.Elem = ElasticBSplineHexa([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6], solid1, Mat3D,obj.Integrator, ...
                obj.PolynomialDegree,knotVector,obj.DofDimension,PseudoDomain());  
            % needed for the test of the line mapping
            CenterNode = Node([4 6.5 4.5],2);
            obj.EdgeLineCoordTest = Edge([Node1 CenterNode],2,3);
            obj.EdgeLineCoordTest2 = Edge([Node2 Node3],2,3);
        end
        
        function tearDown(obj)
        end

        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapGlobalToLocal([4.2 5.75 8.1]), [0.2 -0.5 0.8]);
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapLocalToGlobal([0.2 -0.5 0.8]), [4.2 5.75 8.1]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            N = obj.Elem.evalShapeFunct([0.2 -0.5 0.8]);

            assertElementsAlmostEqual(N, ...
                [0.00089999998 0.0020250001 0.000225 9.9999997e-05 0.072899997....
                0.16402499 0.018224999 0.0081000002 0.0027000001 0.00135 ...
                0.00030000001 0.00060000003 0.016200000 0.036449999 0.0040500001....
                0.0018 0.21870001 0.10935 0.0243 0.048599999 0.0018 0.048599999...
                0.0243 0.0054000001 0.0108 0.14579999 0.032400001]);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            ShapeFunctionsDerivativeVectorTest = [-0.0022499999 0.0033750001...
                0.000375 -0.00025000001 -0.18224999 0.273375 0.030375...
                -0.02025 -0.001125 0.0022499999 -0.00012500001 -0.0015...
                -0.0405 0.06075 0.0067500002 -0.0044999998 -0.091124997...
                0.18224999 -0.010125 -0.1215 -0.00075000001 -0.02025 0.0405...
                -0.0022499999 -0.027000001 -0.06075 -0.0135 -0.0012000001...
                -0.0027000001 0.00089999998 0.00039999999 -0.097199999...
                -0.21870001 0.072899997 0.032400001 -0.0035999999 0.0018...
                0.0012000001 0.00079999998 -0.021600001 -0.048599999 0.0162...
                0.0071999999 -0.29159999 0.14579999 0.097199999 0.064800002...
                0.0024000001 -0.064800002 0.032400001 0.021600001 0.0144...
                0.1944 0.043200001 -0.0089999996 -0.02025 -0.0022499999...
                -0.001 0.081 0.18224999 0.02025 0.0089999996 -0.027000001...
                -0.0135 -0.003 -0.0060000001 -0.071999997 -0.162 -0.017999999...
                -0.0080000004 0.243 0.1215 0.027000001 0.054000001 -0.017999999...
                -0.21600001 -0.108 -0.024 -0.048 0.162 -0.14399999];
            DerivShapeFunction = obj.Elem.evalDerivOfShapeFunct([0.2 -0.5 0.8]);
            
            assertElementsAlmostEqual(DerivShapeFunction,ShapeFunctionsDerivativeVectorTest);
            
        end
        
        
        %% test evaluation of B
        function testGetBAndGradQ(obj)
            u = [-0.0022499999 0.0033750001 0.000375 -0.00025000001 -0.18224999...
                0.273375 0.030375 -0.02025 -0.001125 0.0022499999 -0.00012500001...
                -0.0015 -0.0405 0.06075 0.0067500002 -0.0044999998 -0.091124997...
                0.18224999 -0.010125 -0.1215 -0.00075000001 -0.02025 0.0405...
                -0.0022499999 -0.027000001 -0.06075 -0.0135];
            v = [-0.00079999998 -0.0018 0.00060000003 0.00026666667 -0.064800002...
                -0.14579999 0.048599999 0.021600001 -0.0024000001 0.0012000001...
                0.00079999998 0.00053333334 -0.0144 -0.032400001 0.0108 0.0048000002...
                -0.1944 0.097199999 0.064800002 0.043200001 0.0016 -0.043200001...
                0.021600001 0.0144 0.0096000005 0.1296 0.0288];
            w = [-0.0020000001 -0.0044999998 -0.00050000002 -0.00022222222...
                0.017999999 0.0405 0.0044999998 0.0020000001 -0.0060000001...
                -0.003 -0.00066666666 -0.0013333333 -0.016000001 -0.035999998...
                -0.0040000002 -0.0017777778 0.054000001 0.027000001...
                0.0060000001 0.012 -0.0040000002 -0.048 -0.024 -0.0053333333...
                -0.010666667 0.035999998 -0.032000002];
            z = zeros(1,27);

            assertElementsAlmostEqual(obj.Elem.getB([0.2 -0.5 0.8]),[u z z; z v z; z z w; v u z; z w v; w z u]);
            assertElementsAlmostEqual(obj.Elem.getGradQ([0.2 -0.5 0.8]),[u z z; z v z; z z w]);

        end
              

    end
    
end

