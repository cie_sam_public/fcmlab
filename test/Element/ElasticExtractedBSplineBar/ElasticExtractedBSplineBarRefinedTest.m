%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticExtractedBSplineBarRefinedTest < TestCase
    
    properties
        Element1
        Element2
        
        mapFromElement1ToBaseGeometry
        mapFromElement2ToBaseGeometry
        
        Shapes
        DShapesDXi
        
        X
        Jacobian
    end
    
    methods
        
        %% constructor
        function obj = ElasticExtractedBSplineBarRefinedTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Mat2D = HookePlaneStress(5,0.2,1,1); 
            PolynomialOrder = 2;
            integrator = Integrator(NoPartition(),PolynomialOrder+2);
            Domain = PseudoDomain(); % domain returns always 2
           
            DofDimension = 2;
            numberOfDofsPerDirection = PolynomialOrder+1;
            
            ControlPoints = [0 0 0;
                             1 0 0];
            Knots =  [-1 -1 1 1];
            geometryDescription = BSplineCurve(Knots, 1, ControlPoints );
            geometryDescription.elevateOrder(1);
            geometryDescription.insertKnots(0);
             
             ControlPoints = geometryDescription.getControlPoints;
             
            ExtractionOperator1 = [1 0 0; 0 1 0.5; 0 0 0.5];
            geometricCurve1 = ExtractedBSplineCurve(ControlPoints(1:3,:), ExtractionOperator1 );
            ExtractionOperator2 = [0.5 0 0; 0.5 1 0; 0 0 1];
            geometricCurve2 = ExtractedBSplineCurve(ControlPoints(2:4,:), ExtractionOperator2 );
            
            topologicEdge1 = Edge([], numberOfDofsPerDirection, DofDimension, geometricCurve1 );
            topologicEdge2 = Edge([], numberOfDofsPerDirection, DofDimension, geometricCurve2 );
            
            obj.Element1 = ElasticExtractedBSplineBar([], topologicEdge1,Mat2D,integrator,geometryDescription.getPolynomialDegreeXi,DofDimension,Domain,  ExtractionOperator1);  
            obj.Element2 = ElasticExtractedBSplineBar([], topologicEdge2,Mat2D,integrator,geometryDescription.getPolynomialDegreeXi,DofDimension,Domain,  ExtractionOperator2);  
            
             function shapes = shapes(coords)
                xi = coords(1);             
                if xi > -1 &&  xi < 0 
                    shapes =  [xi^2, (1 + xi)^2/2,-((1 + xi)*(-1 + 3*xi))/2];
                else
                    shapes =  [(-1 + xi)^2/2, xi^2, 1/2 + xi - (3*xi^2)/2];
                end
             end
             
            function shapes = dShapesDXi(coords)
                xi = coords(1);             
                if xi > -1 &&  xi < 0 
                    shapes =  [2*xi, 1 + xi, (1 - 3*xi)/2 - (3*(1 + xi))/2];
                else
                     shapes =  [-1 + xi, 2*xi,  1 - 3*xi]; 
                end
             end
            
            obj.Shapes = @(coords)  shapes(coords);
            obj.DShapesDXi = @(coords) dShapesDXi(coords);
                   
            obj.X = @(coords) [(1 + coords(1))/2, 0, 0];
            obj.Jacobian = @(coords) [1/2, 0, 0];
            
            obj.mapFromElement1ToBaseGeometry = @( coords ) (coords-1)/2;
            obj.mapFromElement2ToBaseGeometry = @( coords ) (coords+1)/2;
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            localCoord = [-6/10, 7/10 0];
            assertElementsAlmostEqual(obj.Element1.calcJacobian(localCoord), obj.Jacobian(obj.mapFromElement1ToBaseGeometry(localCoord))/2);
            assertElementsAlmostEqual(obj.Element2.calcJacobian(localCoord), obj.Jacobian(obj.mapFromElement2ToBaseGeometry(localCoord))/2);
        end   

        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element1.mapLocalToGlobal( localCoords ), obj.X( obj.mapFromElement1ToBaseGeometry(localCoords) ) );
                assertElementsAlmostEqual(obj.Element2.mapLocalToGlobal( localCoords ), obj.X( obj.mapFromElement2ToBaseGeometry(localCoords) ) );
            end
            
            checkPoint([-1]);
            checkPoint([1 ]);
            
            checkPoint([0 ]);
            checkPoint([5/10]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            localCoord = [5/10];
            
            assertElementsAlmostEqual(obj.Element1.evalShapeFunct(localCoord), obj.Shapes(obj.mapFromElement1ToBaseGeometry(localCoord)));
            assertElementsAlmostEqual(obj.Element2.evalShapeFunct(localCoord), obj.Shapes(obj.mapFromElement2ToBaseGeometry(localCoord)));
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            localCoord = [5/10];
                         
            assertElementsAlmostEqual(obj.Element1.evalDerivOfShapeFunct(localCoord), obj.DShapesDXi(obj.mapFromElement1ToBaseGeometry(localCoord)) / 2)
            assertElementsAlmostEqual(obj.Element2.evalDerivOfShapeFunct(localCoord), obj.DShapesDXi(obj.mapFromElement2ToBaseGeometry(localCoord)) / 2)
        end

        
    end
    
end

