%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticExtractedBSplineBarTest < TestCase
    
    properties
        Element
        
        Shapes
        DShapesDXi
        DShapesDEta
        
        X
        Jacobian
    end
    
    methods
        
        %% constructor
        function obj = ElasticExtractedBSplineBarTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Mat2D = HookePlaneStress(5,0.2,1,1); 
            PolynomialOrder = 1;
            integrator = Integrator(NoPartition(),PolynomialOrder+2);
            Domain = PseudoDomain(); % domain returns always 2
           
            DofDimension = 2;
            numberOfDofsPerDirection = PolynomialOrder+1;
            
            ControlPoints = [0 0 0;
                             1 0 0];
            Knots =  [-1 -1 1 1];
            geometryDescription = BSplineCurve(Knots, 1, ControlPoints );
            
            geometricCurve = ExtractedBSplineCurve(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints) );           
            topologicEdge = Edge([], numberOfDofsPerDirection, DofDimension, geometricCurve );
            
            obj.Element = ElasticExtractedBSplineBar([], topologicEdge,...
                 Mat2D,integrator,geometryDescription.getPolynomialDegreeXi,DofDimension,Domain,  eye(geometryDescription.numberOfControlPoints));  
            
                   obj.Shapes = @(coords)  [(1 - coords(1))/2, (1 + coords(1))/2];
            obj.DShapesDXi = @(coords) [-1/2, 1/2];
                 
            obj.X = @(coords) [(1 + coords(1))/2, 0, 0];
            obj.Jacobian = @(coords) [1/2, 0, 0];
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            localCoord = [-6/10, 7/10 0];
            assertElementsAlmostEqual(obj.Element.calcJacobian(localCoord), obj.Jacobian(localCoord));
        end   

        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element.mapLocalToGlobal( localCoords ), obj.X( localCoords ) );
            end
            
            checkPoint([-1]);
            checkPoint([1 ]);
            
            checkPoint([0 ]);
            checkPoint([5/10]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            localCoord = [5/10];
            
            NTest1 = obj.Element.evalShapeFunct(localCoord);
            assertElementsAlmostEqual(NTest1, obj.Shapes(localCoord));
        end
        
        function testGetShapeFunctionsMatrix(obj)
            localCoord = [5/10];
            assertElementsAlmostEqual(obj.Element.getShapeFunctionsMatrix(localCoord), [obj.Shapes(localCoord) 0 0; 0 0 obj.Shapes(localCoord)]);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            localCoord = [5/10];
             
            dN1 = obj.Element.evalDerivOfShapeFunct(localCoord);
            assertElementsAlmostEqual(dN1,  obj.DShapesDXi( localCoord )  );
        end
        
        %% test evaluation of B
        function testGetB(obj)
            assertElementsAlmostEqual(obj.Element.getB([5/10]), [-1 1]);
        end
        
    end
    
end

