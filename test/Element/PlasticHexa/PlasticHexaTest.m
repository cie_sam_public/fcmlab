%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% testing of class PlasticHexa

classdef PlasticHexaTest < TestCase
    
    properties
        Elem
        Integrator
        PolynomialDegree
        DofDimension
        EdgeLineCoordTest
        EdgeLineCoordTest2
        
        PlasticElem
        PD1
        Integrator1
        GPCoord
    end
    
    methods
        
        %% constructor
        function obj = PlasticHexaTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, depth in y = 3, height in z = 9
            % E = 5, Poisson's ratio = 0.2
            Mat3D = Hooke3D(5,0.2,1,1);
            obj.Integrator = Integrator(NoPartition,3);
            obj.Integrator1 = Integrator(NoPartition,2);
            obj.PolynomialDegree = 2;
            obj.PD1= 1;
            obj.DofDimension = 3;
            Node1 = Node([3 5 0],3);
            Node2 = Node([5 5 0],3);
            Node3 = Node([5 8 0],3);
            Node4 = Node([3 8 0],3);
            Node5 = Node([3 5 9],3);
            Node6 = Node([5 5 9],3);
            Node7 = Node([5 8 9],3);
            Node8 = Node([3 8 9],3);
            Edge1 = Edge([Node1 Node2],3,3);
            Edge2 = Edge([Node2 Node3],3,3);
            Edge3 = Edge([Node4 Node3],3,3);
            Edge4 = Edge([Node1 Node4],3,3);
            Edge5 = Edge([Node1 Node5],3,3);
            Edge6 = Edge([Node2 Node6],3,3);
            Edge7 = Edge([Node3 Node7],3,3);
            Edge8 = Edge([Node4 Node8],3,3);
            Edge9 = Edge([Node5 Node6],3,3);
            Edge10 = Edge([Node6 Node7],3,3);
            Edge11 = Edge([Node8 Node7],3,3);
            Edge12 = Edge([Node5 Node8],3,3);
            Face1 = Face([Node1 Node2 Node3 Node4], [Edge1 Edge2 Edge3 Edge4],3,3);
            Face2 = Face([Node1 Node2 Node6 Node5], [Edge1 Edge5 Edge6 Edge9],3,3);
            Face3 = Face([Node2 Node3 Node7 Node6], [Edge2 Edge6 Edge7 Edge10],3,3);
            Face4 = Face([Node3 Node4 Node8 Node7], [Edge3 Edge7 Edge8 Edge11],3,3);
            Face5 = Face([Node1 Node4 Node8 Node5], [Edge4 Edge5 Edge8 Edge12],3,3);
            Face6 = Face([Node5 Node6 Node7 Node8], [Edge9 Edge10 Edge11 Edge12],3,3);
            
            solid1 = Solid([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6],3,3);
            
            obj.Elem = ElasticHexa([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6], solid1, Mat3D,obj.Integrator, ...
                obj.PolynomialDegree,obj.DofDimension,PseudoDomain());
            % needed for the test of the line mapping
            CenterNode = Node([4 6.5 4.5],2);
            obj.EdgeLineCoordTest = Edge([Node1 CenterNode],2,3);
            obj.EdgeLineCoordTest2 = Edge([Node2 Node3],2,3);
            
            % to test plasticity
            PlasticMat = Plastic3DMisesIsotropicHardening(1,20,1,100,1,1);
            obj.PlasticElem = PlasticHexa([Node1 Node2 Node3 Node4 Node5 Node6 Node7 Node8], ...
                [Edge1 Edge2 Edge3 Edge4 Edge5 Edge6 Edge7 Edge8 Edge9 Edge10 Edge11 Edge12],...
                [Face1 Face2 Face3 Face4 Face5 Face6], solid1, PlasticMat,obj.Integrator1, ...
                obj.PD1,obj.DofDimension,PseudoDomain());
            
            %8 GPs for the PD = 1 hexa
            GP1 = [-5.773502691896258e-01 -5.773502691896258e-01 -5.773502691896258e-01];
            GP2 = [5.773502691896258e-01 -5.773502691896258e-01 -5.773502691896258e-01];
            
            GP3 = [-5.773502691896258e-01 5.773502691896258e-01 -5.773502691896258e-01];
            GP4 = [5.773502691896258e-01 5.773502691896258e-01 -5.773502691896258e-01];
            
            
            GP5 = [-5.773502691896258e-01 -5.773502691896258e-01 5.773502691896258e-01];
            GP6 = [5.773502691896258e-01 -5.773502691896258e-01 5.773502691896258e-01];
            
            GP7 = [-5.773502691896258e-01 5.773502691896258e-01  5.773502691896258e-01];
            GP8 = [5.773502691896258e-01 5.773502691896258e-01  5.773502691896258e-01];
            
            obj.PlasticElem.AddContainer(GP1);
            obj.PlasticElem.AddContainer(GP2);
            obj.PlasticElem.AddContainer(GP3);
            obj.PlasticElem.AddContainer(GP4);
            obj.PlasticElem.AddContainer(GP5);
            obj.PlasticElem.AddContainer(GP6);
            obj.PlasticElem.AddContainer(GP7);
            obj.PlasticElem.AddContainer(GP8);
            
            % to initialize internal variables
            initContainer = containers.Map('KeyType','int32','ValueType','any');
            initContainer(1) = zeros(3);   % Elastic Strain
            initContainer(2) = 0;   % Plastic Strain
            initContainer(3) = 200*eye(3); % Stress
           
            obj.PlasticElem.setTempContAtGP(GP1, initContainer);
            obj.PlasticElem.setTempContAtGP(GP2, initContainer);
            obj.PlasticElem.setTempContAtGP(GP3, initContainer);
            obj.PlasticElem.setTempContAtGP(GP4, initContainer);
            obj.PlasticElem.setTempContAtGP(GP5, initContainer);
            obj.PlasticElem.setTempContAtGP(GP6, initContainer);
            obj.PlasticElem.setTempContAtGP(GP7, initContainer);
            obj.PlasticElem.setTempContAtGP(GP8, initContainer);
            
            obj.PlasticElem.initializeDOFVector([1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24]')
            
            obj.GPCoord = GP1;
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            assertEqual(obj.Elem.calcJacobian([0 0 0]), [1 0 0; 0 1.5 0; 0 0 4.5]);
        end
        
        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapGlobalToLocal([4.2 5.75 8.1]), [0.2 -0.5 0.8]);
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            assertVectorsAlmostEqual(obj.Elem.mapLocalToGlobal([0.2 -0.5 0.8]), [4.2 5.75 8.1]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            N = obj.Elem.evalShapeFunct([0.2 -0.5 0.8]);
            assertElementsAlmostEqual([N N N], ...
                [0.03 0.045 0.015 0.01 0.27 0.405 0.135 0.09 -0.0441 -0.0276 -0.0147 -0.0184 -0.0661 ...
                -0.0992 -0.0331 -0.022 -0.3968 -0.248 -0.1323 -0.1653 0.027 0.0972 0.0607 0.0324 ...
                0.0405 0.243 -0.0595 ...
                0.03 0.045 0.015 0.01 0.27 0.405 0.135 0.09 -0.0441 -0.0276 -0.0147 -0.0184 -0.0661 ...
                -0.0992 -0.0331 -0.022 -0.3968 -0.248 -0.1323 -0.1653 0.027 0.0972 0.0607 0.0324 ...
                0.0405 0.243 -0.0595 ...
                0.03 0.045 0.015 0.01 0.27 0.405 0.135 0.09 -0.0441 -0.0276 -0.0147 -0.0184 -0.0661 ...
                -0.0992 -0.0331 -0.022 -0.3968 -0.248 -0.1323 -0.1653 0.027 0.0972 0.0607 0.0324 ...
                0.0405 0.243 -0.0595], 'absolute', 1e-4);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            ShapeFunctionsDerivativeVectorTest = [-0.0375 0.0375 0.0125 -0.0125 -0.3375 0.3375 0.1125 -0.1125 0.0184 -0.023 0.0061...
                0.023 0.0827 -0.0827 -0.0276 0.0276 0.1653 -0.2067 0.0551 0.2067 -0.0113 -0.0405 ...
                0.0506 -0.0135 -0.0506 -0.1013 0.0248, ...
                -0.02 -0.03 0.03 0.02 -0.18 -0.27 0.27 0.18 0.0294 -0.0367 -0.0294 -0.0245 0.0441 ...
                0.0661 -0.0661 -0.0441 0.2645 -0.3307 -0.2645 -0.2205 0.036 -0.0648 0.081 0.0648 0.054 ...
                0.324 -0.0794,...
                -0.15 -0.225 -0.075 -0.05 0.15 0.225 0.075 0.05 0.22045 0.13778 0.07348 0.09186 0.29394 ...
                0.44091 0.14697 0.09798 -0.22045 -0.13778 -0.07348 -0.09186 -0.135 -0.432 -0.27 -0.144 -0.18 0.135 ...
                0.26454];
            DerivShapeFunction = obj.Elem.evalDerivOfShapeFunct([0.2 -0.5 0.8]);
            
            for i=1:3*27
                assertElementsAlmostEqual(DerivShapeFunction(1,i),ShapeFunctionsDerivativeVectorTest(1,i),'absolute', 1e-3);
            end
        end
        
        %% test evaluation of B
        function testGetBAndGradQ(obj)
            u = [-0.0375 0.0375 0.0125 -0.0125 -0.3375 0.3375 0.1125 -0.1125 0.0184 -0.023 0.0061...
                0.023 0.0827 -0.0827 -0.0276 0.0276 0.1653 -0.2067 0.0551 0.2067 -0.0113 -0.0405 ...
                0.0506 -0.0135 -0.0506 -0.1013 0.0248];
            v = [-0.02 -0.03 0.03 0.02 -0.18 -0.27 0.27 0.18 0.0294 -0.0367 -0.0294 -0.0245 0.0441 ...
                0.0661 -0.0661 -0.0441 0.2645 -0.3307 -0.2645 -0.2205 0.036 -0.0648 0.081 0.0648 0.054 ...
                0.324 -0.0794]/1.5;
            w = [-0.15 -0.225 -0.075 -0.05 0.15 0.225 0.075 0.05 0.22045 0.13778 0.07348 0.09186 0.29394 ...
                0.44091 0.14697 0.09798 -0.22045 -0.13778 -0.07348 -0.09186 -0.135 -0.432 -0.27 -0.144 -0.18 0.135 ...
                0.26454]/4.5;
            z = zeros(1,27);
            assertElementsAlmostEqual(obj.Elem.getB([0.2 -0.5 0.8]),[u z z; z v z; z z w; v u z; z w v; w z u],'absolute', 1e-3);
            assertElementsAlmostEqual(obj.Elem.getGradQ([0.2 -0.5 0.8]),[u z z; z v z; z z w],'absolute', 1e-3);
        end
        
        
        %% test calculation of element stiffness matrix
        function testLinearElementStiffnessMatrix(obj)
            K = obj.Elem.calcStiffnessMatrix();
            
            EntryVector = [K(1,5) K(3,30) K(16,80) K(40,50) K(50,80) K(81,81)];
            ResultVector = [1525/324 125/48 125*sqrt(6)/864 1115*sqrt(6)/2592 0 383/108];
            
            assertVectorsAlmostEqual(EntryVector,ResultVector);
            % check for symmetry of K
            assertElementsAlmostEqual(K-K',zeros(3*(obj.PolynomialDegree+1)^3,3*(obj.PolynomialDegree+1)^3));
            % check for first six eigenvalues being zero -> rigid body modes
            Eigenvalues = sort(eig(K));
            for i=1:6
                assertElementsAlmostEqual(Eigenvalues(i),0);
            end
        end
        
        function testCalcElementBodyLoadVector(obj)
            TestVector = zeros(81,1);
            TestVector(55:81) = 9.81*27/4;
            TestVector(55:62) = TestVector(55:62)*1;
            TestVector(63:74) = TestVector(63:74)*(-sqrt(6)/3);
            TestVector(75:80) = TestVector(75:80)*2/3;
            TestVector(81) = TestVector(81)*(-6^1.5/27);
            VolLoad = {@(x,y,z) [0; 0; 9.81]};
            assertVectorsAlmostEqual(obj.Elem.calcBodyLoadVector(VolLoad),TestVector);
        end
        
        function testElementMassMatrix(obj)
            M = obj.Elem.calcMassMatrix();
            EntryVector = [ M(1,1) M(1,2) M(1,3) M(2,9) M(7,16) M(81,81)];
            ResultVector = [2.0000 1.0000 0.5000 -1.2247 -0.6124 0.4320];
            assertVectorsAlmostEqual(EntryVector,ResultVector,'absolute',1e-4);
            assertElementsAlmostEqual(M-M',zeros(3*(obj.PolynomialDegree+1)^3,3*(obj.PolynomialDegree+1)^3));
        end
        
        %% TEST PLASTICITY
        
        function testGetIndicesOfTempCont(obj)
            [ID_E_El, ID_E_Pl, ID_S] = obj.PlasticElem.getIndicesOfTempCont();
            
            assertEqual(ID_E_El, 1);
            assertEqual(ID_E_Pl, 2);
            assertEqual(ID_S, 3);
        end
        
        function testGetStress(obj)
            %Coord should be one of the GPs
            S = obj.PlasticElem.getStress(obj.GPCoord);
            stressVector = [ 200;   200;   200;     0;     0;     0];
            assertVectorsAlmostEqual(S, stressVector,'absolute',1e-4);
        end
        
        function testCalcIntegrandFint(obj)
            integrandFint = obj.PlasticElem.calcIntegrandFint(obj.GPCoord);            
            ResultVector = [-62.2008 62.2008 16.6667 -16.6667 -16.6667 16.6667 4.4658 -4.4658 -41.4672 -11.1111 11.1111 41.4672...
              -11.1111 -2.9772 2.9772 11.1111 -13.8224 -3.7037 -0.9924 -3.7037 13.8224 3.7037 0.9924 3.7037]';
            assertVectorsAlmostEqual(integrandFint,ResultVector,'absolute',1e-3);
        end
        
        function testCalcInternalForceVector(obj)
            Fe_int = obj.PlasticElem.calcInternalForceVector();
            ResultVector = 1.0e+03*[-1.3500 1.3500 1.3500 -1.3500 -1.3500 1.3500 1.3500 -1.3500 -0.9000 -0.9000 0.9000 0.9000...
               -0.9000 -0.9000 0.9000 0.9000 -0.3000 -0.3000 -0.3000 -0.3000 0.3000 0.3000 0.3000 0.3000]';
            assertVectorsAlmostEqual(Fe_int,ResultVector,'absolute',1e-4);
        end
        
        %% test calculation of nonlinear element stiffness matrix
        function testNonlinearElementStiffnessMatrix(obj)
            
            Knl = obj.PlasticElem.calcStiffnessMatrix();
            
            stiffnessmatrix = [0.0524   -0.0260   -0.0249    0.0024    0.0236   -0.0143   -0.0131   -0.0001   -0.0005   -0.0362    0.0005    0.0362   -0.0002	   -0.0181    0.0002    0.0181   -0.0002   -0.0121   -0.0060   -0.0001    0.0121    0.0002    0.0001    0.0060								;
           -0.0260    0.0524    0.0024   -0.0249   -0.0143    0.0236   -0.0001   -0.0131    0.0362    0.0005   -0.0362   -0.0005    0.0181	    0.0002   -0.0181   -0.0002    0.0121    0.0002    0.0001    0.0060   -0.0002   -0.0121   -0.0060   -0.0001								;
           -0.0249    0.0024    0.0524   -0.0260   -0.0131   -0.0001    0.0236   -0.0143    0.0005    0.0362   -0.0005   -0.0362    0.0002	    0.0181   -0.0002   -0.0181    0.0060    0.0001    0.0002    0.0121   -0.0001   -0.0060   -0.0121   -0.0002								;
            0.0024   -0.0249   -0.0260    0.0524   -0.0001   -0.0131   -0.0143    0.0236   -0.0362   -0.0005    0.0362    0.0005   -0.0181     -0.0002    0.0181    0.0002   -0.0001   -0.0060   -0.0121   -0.0002    0.0060    0.0001    0.0002    0.0121								;
            0.0236   -0.0143   -0.0131   -0.0001    0.0524   -0.0260   -0.0249    0.0024   -0.0002   -0.0181    0.0002    0.0181   -0.0005	   -0.0362    0.0005    0.0362   -0.0121   -0.0002   -0.0001   -0.0060    0.0002    0.0121    0.0060    0.0001								;
           -0.0143    0.0236   -0.0001   -0.0131   -0.0260    0.0524    0.0024   -0.0249    0.0181    0.0002   -0.0181   -0.0002    0.0362	    0.0005   -0.0362   -0.0005    0.0002    0.0121    0.0060    0.0001   -0.0121   -0.0002   -0.0001   -0.0060								;
           -0.0131   -0.0001    0.0236   -0.0143   -0.0249    0.0024    0.0524   -0.0260    0.0002    0.0181   -0.0002   -0.0181    0.0005      0.0362   -0.0005   -0.0362    0.0001    0.0060    0.0121    0.0002   -0.0060   -0.0001   -0.0002   -0.0121								;
           -0.0001   -0.0131   -0.0143    0.0236    0.0024   -0.0249   -0.0260    0.0524   -0.0181   -0.0002    0.0181    0.0002   -0.0362	   -0.0005    0.0362    0.0005   -0.0060   -0.0001   -0.0002   -0.0121    0.0001    0.0060    0.0121    0.0002								;
           -0.0005    0.0362    0.0005   -0.0362   -0.0002    0.0181    0.0002   -0.0181    0.0529   -0.0271   -0.0251    0.0033    0.0238	   -0.0149   -0.0132    0.0003   -0.0001   -0.0001   -0.0040   -0.0080    0.0080    0.0040    0.0001    0.0001								;
           -0.0362    0.0005    0.0362   -0.0005   -0.0181    0.0002    0.0181   -0.0002   -0.0271    0.0529    0.0033   -0.0251   -0.0149		0.0238    0.0003   -0.0132   -0.0001   -0.0001   -0.0080   -0.0040    0.0040    0.0080    0.0001    0.0001								;
            0.0005   -0.0362   -0.0005    0.0362    0.0002   -0.0181   -0.0002    0.0181   -0.0251    0.0033    0.0529   -0.0271   -0.0132		0.0003    0.0238   -0.0149    0.0040    0.0080    0.0001    0.0001   -0.0001   -0.0001   -0.0080   -0.0040								;
            0.0362   -0.0005   -0.0362    0.0005    0.0181   -0.0002   -0.0181    0.0002    0.0033   -0.0251   -0.0271    0.0529    0.0003      -0.0132   -0.0149    0.0238    0.0080    0.0040    0.0001    0.0001   -0.0001   -0.0001   -0.0040   -0.0080								;
           -0.0002    0.0181    0.0002   -0.0181   -0.0005    0.0362    0.0005   -0.0362    0.0238   -0.0149   -0.0132    0.0003    0.0529		-0.0271   -0.0251    0.0033   -0.0080   -0.0040   -0.0001   -0.0001    0.0001    0.0001    0.0040    0.0080								;
           -0.0181    0.0002    0.0181   -0.0002   -0.0362    0.0005    0.0362   -0.0005   -0.0149    0.0238    0.0003   -0.0132   -0.0271		0.0529    0.0033   -0.0251   -0.0040   -0.0080   -0.0001   -0.0001    0.0001    0.0001    0.0080    0.0040								;
            0.0002   -0.0181   -0.0002    0.0181    0.0005   -0.0362   -0.0005    0.0362   -0.0132    0.0003    0.0238   -0.0149   -0.0251		0.0033    0.0529   -0.0271    0.0001    0.0001    0.0080    0.0040   -0.0040   -0.0080   -0.0001   -0.0001								;
            0.0181   -0.0002   -0.0181    0.0002    0.0362   -0.0005   -0.0362    0.0005    0.0003   -0.0132   -0.0149    0.0238    0.0033		-0.0251   -0.0271    0.0529    0.0001    0.0001    0.0040    0.0080   -0.0080   -0.0040   -0.0001   -0.0001								;
           -0.0002    0.0121    0.0060   -0.0001   -0.0121    0.0002    0.0001   -0.0060   -0.0001   -0.0001    0.0040    0.0080   -0.0080		-0.0040    0.0001    0.0001    0.0533   -0.0269   -0.0254    0.0028    0.0241   -0.0147   -0.0133    0.0001								;
           -0.0121    0.0002    0.0001   -0.0060   -0.0002    0.0121    0.0060   -0.0001   -0.0001   -0.0001    0.0080    0.0040   -0.0040		-0.0080    0.0001    0.0001   -0.0269    0.0533    0.0028   -0.0254   -0.0147    0.0241    0.0001   -0.0133								;
           -0.0060    0.0001    0.0002   -0.0121   -0.0001    0.0060    0.0121   -0.0002   -0.0040   -0.0080    0.0001    0.0001   -0.0001		-0.0001    0.0080    0.0040   -0.0254    0.0028    0.0533   -0.0269   -0.0133    0.0001    0.0241   -0.0147								;
           -0.0001    0.0060    0.0121   -0.0002   -0.0060    0.0001    0.0002   -0.0121   -0.0080   -0.0040    0.0001    0.0001   -0.0001		-0.0001    0.0040    0.0080    0.0028   -0.0254   -0.0269    0.0533    0.0001   -0.0133   -0.0147    0.0241								;
            0.0121   -0.0002   -0.0001    0.0060    0.0002   -0.0121   -0.0060    0.0001    0.0080    0.0040   -0.0001   -0.0001    0.0001		0.0001   -0.0040   -0.0080    0.0241   -0.0147   -0.0133    0.0001    0.0533   -0.0269   -0.0254    0.0028								;
            0.0002   -0.0121   -0.0060    0.0001    0.0121   -0.0002   -0.0001    0.0060    0.0040    0.0080   -0.0001   -0.0001    0.0001		0.0001   -0.0080   -0.0040   -0.0147    0.0241    0.0001   -0.0133   -0.0269    0.0533    0.0028   -0.0254								;
            0.0001   -0.0060   -0.0121    0.0002    0.0060   -0.0001   -0.0002    0.0121    0.0001    0.0001   -0.0080   -0.0040    0.0040		0.0080   -0.0001   -0.0001   -0.0133    0.0001    0.0241   -0.0147   -0.0254    0.0028    0.0533   -0.0269								;
            0.0060   -0.0001   -0.0002    0.0121    0.0001   -0.0060   -0.0121    0.0002    0.0001    0.0001   -0.0040   -0.0080    0.0080		0.0040   -0.0001   -0.0001    0.0001   -0.0133   -0.0147    0.0241    0.0028   -0.0254   -0.0269    0.0533								];

            assertElementsAlmostEqual(Knl, stiffnessmatrix,'absolute',1e-4);
        end
    end
end