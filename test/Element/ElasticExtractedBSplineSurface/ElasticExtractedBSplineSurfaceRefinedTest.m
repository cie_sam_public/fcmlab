%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticExtractedBSplineSurfaceRefinedTest < TestCase
    
    properties
        Element11
        Element12
        Element21
        Element22
        
        Shapes
DShapes
        
        X
        Jacobian
        mapFrom11ToBaseGeometry
        mapFrom12ToBaseGeometry
        mapFrom21ToBaseGeometry
        mapFrom22ToBaseGeometry
        
        mapFromBaseGeometryTo12
        Perm
    end
    
    methods
        
        %% constructor
        function obj = ElasticExtractedBSplineSurfaceRefinedTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Material = HookePlaneStress(5,0.2,1,1); 
            PolynomialOrder = 2;
            integrator = Integrator(NoPartition(),PolynomialOrder+2);
            Domain = PseudoDomain(); % domain returns always 2
           
            DofDimension = 2;
            numberOfDofsPerDirection = PolynomialOrder+1;
            
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            geometryDescription.removeWeights();
           
NumberOfXDivisions=2;
NumberOfYDivisions=2;
PolynomialDegree = 2;
 
IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );

            obj.Perm = [1     5     2     8     9     6     4     7     3];
            
elementFactory = ElementFactoryElasticExtractedBSplineSurface( Material,PolynomialOrder+2, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta ); 
            function element = createElement(index)
                geometricFace = geometryFactory.createArea( index );
               geometricEdges = geometricFace.getLines();
                for iGeometricEdge = 1:length(geometricEdges)
                    topologicEdges(iGeometricEdge) = Edge([], numberOfDofsPerDirection, DofDimension, geometricEdges(iGeometricEdge));
                end
                topologicFace = Face([], topologicEdges, numberOfDofsPerDirection, DofDimension, geometricFace );
                
               element = elementFactory.createElement([],topologicEdges,topologicFace,[],geometryDescription.getPolynomialDegreeXi,DofDimension,index );
            end
            
            obj.Element11 = createElement(1);
            obj.Element12 = createElement(2);
            obj.Element21 = createElement(3);
            obj.Element22 = createElement(4);
            
            obj.Element11.setLocationMatrix(1:18);
            obj.Element12.setLocationMatrix(1:18);
            obj.Element21.setLocationMatrix(1:18);
            obj.Element22.setLocationMatrix(1:18);
            
            function shapes = shapes(coords)
                xi = coords(1);
                eta = coords(2);
                
                 
                if xi > -1 &&  xi < 0  && eta > -1 &&  eta < 0
                 shapes =  [eta^2*xi^2, (eta^2*(1 + xi)^2)/2, ((1 + eta)^2*(1 + xi)^2)/4, ((1 + eta)^2*xi^2)/2,-(eta^2*(1 + xi)*(-1 + 3*xi))/2,-((1 + eta)*(-1 + 3*eta)*(1 + xi)^2)/4,-((1 + eta)^2*(1 + xi)*(-1 + 3*xi))/4,-((1 + eta)*(-1 + 3*eta)*xi^2)/2,((1 + eta)*(-1 + 3*eta)*(1 + xi)*(-1 + 3*xi))/4];

                elseif xi > 0 && xi < 1 && eta > -1 && eta < 0
                shapes =  [(eta^2*(-1 + xi)^2)/2, eta^2*xi^2, ((1 + eta)^2*xi^2)/2, ((1 + eta)^2*(-1 + xi)^2)/4,(eta^2*(1 + (2 - 3*xi)*xi))/2,-((1 + eta)*(-1 + 3*eta)*xi^2)/2,-((1 + eta)^2*(-1 + xi)*(1 + 3*xi))/4,-((1 + eta)*(-1 + 3*eta)*(-1 + xi)^2)/4,((1 + eta)*(-1 + 3*eta)*(-1 + xi)*(1 + 3*xi))/4];

                elseif xi > -1 &&  xi < 0  && eta > 0 &&  eta < 1
                shapes =  [((-1 + eta)^2*xi^2)/2, ((-1 + eta)^2*(1 + xi)^2)/4, (eta^2*(1 + xi)^2)/2, eta^2*xi^2,-((-1 + eta)^2*(1 + xi)*(-1 + 3*xi))/4,-((-1 + eta)*(1 + 3*eta)*(1 + xi)^2)/4,-(eta^2*(1 + xi)*(-1 + 3*xi))/2,((1 + (2 - 3*eta)*eta)*xi^2)/2,((-1 + eta)*(1 + 3*eta)*(1 + xi)*(-1 + 3*xi))/4];

                elseif xi > 0 &&  xi < 1  && eta > 0 &&  eta < 1
                shapes =  [((-1 + eta)^2*(-1 + xi)^2)/4, ((-1 + eta)^2*xi^2)/2, eta^2*xi^2, (eta^2*(-1 + xi)^2)/2,-((-1 + eta)^2*(-1 + xi)*(1 + 3*xi))/4,((1 + (2 - 3*eta)*eta)*xi^2)/2,(eta^2*(1 + (2 - 3*xi)*xi))/2,-((-1 + eta)*(1 + 3*eta)*(-1 + xi)^2)/4,((-1 + eta)*(1 + 3*eta)*(-1 + xi)*(1 + 3*xi))/4];
                end
               
                shapes=shapes(obj.Perm);
                
            end
            
            function shapes = jacobian(coords)
                xi = coords(1);
                eta = coords(2);
                
                 
                if xi > -1 && xi < 0 && eta > -1 && eta < 0
                shapes = [2*eta^2*xi, eta^2*(1 + xi), ((1 + eta)^2*(1 + xi))/2, (1 + eta)^2*xi,-(eta^2*(1 + 3*xi)),-((1 + eta)*(-1 + 3*eta)*(1 + xi))/2,-((1 + eta)^2*(1 + 3*xi))/2,-((1 + eta)*(-1 + 3*eta)*xi), ((1 + eta)*(-1 + 3*eta)*(1 + 3*xi))/2 ...
                2*eta*xi^2, eta*(1 + xi)^2, ((1 + eta)*(1 + xi)^2)/2, (1 + eta)*xi^2,-(eta*(1 + xi)*(-1 + 3*xi)),-((1 + 3*eta)*(1 + xi)^2)/2,-((1 + eta)*(1 + xi)*(-1 + 3*xi))/2,-((1 + 3*eta)*xi^2), ((1 + 3*eta)*(1 + xi)*(-1 + 3*xi))/2];

                elseif xi > 0 && xi < 1 && eta > -1 && eta < 0
                shapes = [eta^2*(-1 + xi), 2*eta^2*xi, (1 + eta)^2*xi, ((1 + eta)^2*(-1 + xi))/2,eta^2*(1 - 3*xi),-((1 + eta)*(-1 + 3*eta)*xi),-((1 + eta)^2*(-1 + 3*xi))/2,-((1 + eta)*(-1 + 3*eta)*(-1 + xi))/2, ((1 + eta)*(-1 + 3*eta)*(-1 + 3*xi))/2 ...
                eta*(-1 + xi)^2, 2*eta*xi^2, (1 + eta)*xi^2, ((1 + eta)*(-1 + xi)^2)/2,eta*(1 + (2 - 3*xi)*xi),-((1 + 3*eta)*xi^2),-((1 + eta)*(-1 + xi)*(1 + 3*xi))/2,-((1 + 3*eta)*(-1 + xi)^2)/2, ((1 + 3*eta)*(-1 + xi)*(1 + 3*xi))/2];

                elseif xi > -1 && xi < 0 && eta > 0 && eta < 1
                shapes = [(-1 + eta)^2*xi, ((-1 + eta)^2*(1 + xi))/2, eta^2*(1 + xi), 2*eta^2*xi,-((-1 + eta)^2*(1 + 3*xi))/2,-((-1 + eta)*(1 + 3*eta)*(1 + xi))/2,-(eta^2*(1 + 3*xi)),(1 + (2 - 3*eta)*eta)*xi, ((-1 + eta)*(1 + 3*eta)*(1 + 3*xi))/2 ...
                    (-1 + eta)*xi^2, ((-1 + eta)*(1 + xi)^2)/2, eta*(1 + xi)^2, 2*eta*xi^2,-((-1 + eta)*(1 + xi)*(-1 + 3*xi))/2,-((-1 + 3*eta)*(1 + xi)^2)/2,-(eta*(1 + xi)*(-1 + 3*xi)),(1 - 3*eta)*xi^2, ((-1 + 3*eta)*(1 + xi)*(-1 + 3*xi))/2];

                elseif xi > 0 && xi < 1 && eta > 0 && eta < 1
                shapes = [((-1 + eta)^2*(-1 + xi))/2, (-1 + eta)^2*xi, 2*eta^2*xi, eta^2*(-1 + xi),-((-1 + eta)^2*(-1 + 3*xi))/2,(1 + (2 - 3*eta)*eta)*xi,eta^2*(1 - 3*xi),-((-1 + eta)*(1 + 3*eta)*(-1 + xi))/2, ((-1 + eta)*(1 + 3*eta)*(-1 + 3*xi))/2 ...
                    ((-1 + eta)*(-1 + xi)^2)/2, (-1 + eta)*xi^2, 2*eta*xi^2, eta*(-1 + xi)^2,-((-1 + eta)*(-1 + xi)*(1 + 3*xi))/2,(1 - 3*eta)*xi^2,eta*(1 + (2 - 3*xi)*xi),-((-1 + 3*eta)*(-1 + xi)^2)/2, ((-1 + 3*eta)*(-1 + xi)*(1 + 3*xi))/2];
                end
                 
                shapes=shapes([obj.Perm 9+obj.Perm]);
                
            end
            
            %obj.Shapes = @(coords)  [1/16 *(-1 + coords(2))^2 *(-1 + coords(1))^2, 1/16* (-1 + coords(2))^2 *(1 + coords(1))^2,1/16 *(1 + coords(2))^2 *(1 + coords(1))^2,1/16 *(1 + coords(2))^2 *(-1 + coords(1))^2, -(1/8)* (-1 + coords(2))^2 *(-1 + coords(1)) *(1 + coords(1)), -(1/8) * (-1 + coords(2)) * (1 + coords(2)) * (1 + coords(1))^2, -(1/8) * (1 + coords(2))^2 *(-1 + coords(1)) *(1 + coords(1)), -(1/8)* (-1 + coords(2))* (1 + coords(2)) *(-1 + coords(1))^2,1/4 *(-1 + coords(2)) *(1 + coords(2)) *(-1 + coords(1)^2) ];
            obj.Shapes = @shapes;
            obj.DShapes = @jacobian;
            
            obj.X = @(coords) [-(1/16)* (7 + 3* coords(2)) *(-3 + coords(1)) *(1 + coords(1)), -(1/16) *(7 + 3* coords(2)) *(-1 + coords(1)) *(3 + coords(1)), 0 ];
            obj.Jacobian = @(coords) [-(1/8) *(7 + 3 *coords(2)) *(-1 + coords(1)), -(1/8)* (7 + 3 *coords(2))* (1 + coords(1)),0; ...
                -(3/16) *(-3 + coords(1)) * (1 + coords(1)), -(3/16) *(-1 + coords(1))* (3 + coords(1)),0];
            
            obj.mapFrom11ToBaseGeometry = @( coords ) (coords-1)/2;
            obj.mapFrom12ToBaseGeometry = @( coords ) [ (coords(1)+1)/2  (coords(2)-1)/2 ];
            obj.mapFrom21ToBaseGeometry = @( coords ) [ (coords(1)-1)/2  (coords(2)+1)/2 ];
            obj.mapFrom22ToBaseGeometry = @( coords ) (coords+1)/2;
            
            %obj.mapFrom11ToBaseGeometry = @( coords ) (coords-1)/2;
            %obj.mapFromBaseGeometryTo12 = @( coords ) [ 2*coords(1)+1  2*coords(2)-1 ];
            %obj.mapFrom21ToBaseGeometry = @( coords ) [ (coords(1)-1)/2  (coords(2)+1)/2 ];
            %obj.mapFrom22ToBaseGeometry = @( coords ) (coords+1)/2;
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            
            LocalCoords = [0.3 0.4];
            
            J = obj.Element11.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, obj.Jacobian( obj.mapFrom11ToBaseGeometry( LocalCoords ) )/2 );
            
            J = obj.Element12.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J,obj.Jacobian( obj.mapFrom12ToBaseGeometry( LocalCoords ) )/2 );
            
            J = obj.Element21.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, obj.Jacobian( obj.mapFrom21ToBaseGeometry( LocalCoords ) )/2 );
            
            J = obj.Element22.calcJacobian(LocalCoords);
            assertElementsAlmostEqual(J, obj.Jacobian( obj.mapFrom22ToBaseGeometry( LocalCoords ) )/2 );
        end

        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
          function checkPoint( Element, LocalCoords, MapToBaseGeometryFunction)
               assertElementsAlmostEqual( Element.mapGlobalToLocal( obj.X( MapToBaseGeometryFunction( LocalCoords ) ) ), LocalCoords);
           end
           
           function checkPointsOfElement( Element, MapToBaseGeometryFunction )
                checkPoint( Element, [-1 -1], MapToBaseGeometryFunction );
                checkPoint( Element, [ 1 -1], MapToBaseGeometryFunction );
                checkPoint( Element, [ 1  1], MapToBaseGeometryFunction );
                checkPoint( Element, [-1  1], MapToBaseGeometryFunction );

                checkPoint( Element, [0.5, -0.4], MapToBaseGeometryFunction );
                checkPoint( Element, [-0.99123, 1], MapToBaseGeometryFunction );
           end
           
            checkPointsOfElement(obj.Element11, obj.mapFrom11ToBaseGeometry)
            checkPointsOfElement(obj.Element12, obj.mapFrom12ToBaseGeometry)
            checkPointsOfElement(obj.Element21, obj.mapFrom21ToBaseGeometry)
            checkPointsOfElement(obj.Element22, obj.mapFrom22ToBaseGeometry)
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
          function checkPoint( Element, LocalCoords, MapToBaseGeometryFunction)
               assertElementsAlmostEqual( Element.mapLocalToGlobal(LocalCoords), obj.X( MapToBaseGeometryFunction( LocalCoords ) ) );
           end
           
           function checkPointsOfElement( Element, MapToBaseGeometryFunction )
                checkPoint( Element, [-1 -1], MapToBaseGeometryFunction );
                checkPoint( Element, [ 1 -1], MapToBaseGeometryFunction );
                checkPoint( Element, [ 1  1], MapToBaseGeometryFunction );
                checkPoint( Element, [-1  1], MapToBaseGeometryFunction );

                checkPoint( Element, [0.5, -0.4], MapToBaseGeometryFunction );
                checkPoint( Element, [-0.99123, 1], MapToBaseGeometryFunction );
           end
           
            checkPointsOfElement(obj.Element11, obj.mapFrom11ToBaseGeometry)
            checkPointsOfElement(obj.Element12, obj.mapFrom12ToBaseGeometry)
            checkPointsOfElement(obj.Element21, obj.mapFrom21ToBaseGeometry)
            checkPointsOfElement(obj.Element22, obj.mapFrom22ToBaseGeometry)
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
           function checkShapes( Element, LocalCoords, MapToBaseGeometryFunction)
               assertElementsAlmostEqual( Element.evalShapeFunct(LocalCoords), obj.Shapes( MapToBaseGeometryFunction( LocalCoords ) ) );
           end
            
           LocalCoords = [5/10, -4/10];
           
            checkShapes(obj.Element11, LocalCoords, obj.mapFrom11ToBaseGeometry)
            checkShapes(obj.Element12, LocalCoords, obj.mapFrom12ToBaseGeometry)
            checkShapes(obj.Element21, LocalCoords, obj.mapFrom21ToBaseGeometry)
            checkShapes(obj.Element22, LocalCoords, obj.mapFrom22ToBaseGeometry) 
        end
        
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            function checkShapes( Element, LocalCoords, MapToBaseGeometryFunction)
               assertElementsAlmostEqual( Element.evalDerivOfShapeFunct(LocalCoords), obj.DShapes( MapToBaseGeometryFunction( LocalCoords ) )/2 );
           end
            
           LocalCoords = [5/10, -4/10];
           
            checkShapes(obj.Element11, LocalCoords, obj.mapFrom11ToBaseGeometry)
            checkShapes(obj.Element12, LocalCoords, obj.mapFrom12ToBaseGeometry)
            checkShapes(obj.Element21, LocalCoords, obj.mapFrom21ToBaseGeometry)
            checkShapes(obj.Element22, LocalCoords, obj.mapFrom22ToBaseGeometry)
        end
                
    end
    
end

