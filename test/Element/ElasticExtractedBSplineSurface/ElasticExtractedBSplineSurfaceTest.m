%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
% testing of class ElasticQuad

classdef ElasticExtractedBSplineSurfaceTest < TestCase
    
    properties
        Element
        
        Shapes
        DShapesDXi
        DShapesDEta
        
        X
        Jacobian
        Perm
    end
    
    methods
        
        %% constructor
        function obj = ElasticExtractedBSplineSurfaceTest(name)
            obj = obj@TestCase(name);
        end
        
        function setUp(obj)
            % test with an element of polynomial degree of 2
            % width in x = 2, height in y = 3
            % E = 5, Poisson's ratio = 0.2
            Mat2D = HookePlaneStress(5,0.2,1,1); 
            PolynomialOrder = 2;
            integrator = Integrator(NoPartition(),PolynomialOrder+2);
            Domain = PseudoDomain(); % domain returns always 2
           
            DofDimension = 2;
            numberOfDofsPerDirection = PolynomialOrder+1;
            
            geometryDescription = GenerateQuarterDisk(1, 2.5);
            geometryDescription.removeWeights();
            
            geometricFace = ExtractedBSplineSurface(geometryDescription.getControlPoints, eye(geometryDescription.numberOfControlPoints), geometryDescription.numberOfControlPointsInXi,geometryDescription.numberOfControlPointsInEta );
            geometricEdges = geometricFace.getLines();
            
            for iGeometricEdge = 1:length(geometricEdges)
                topologicEdges(iGeometricEdge) = Edge([], numberOfDofsPerDirection, DofDimension, geometricEdges(iGeometricEdge));
            end
            topologicFace = Face([], topologicEdges, numberOfDofsPerDirection, DofDimension, geometricFace );
            
            obj.Element = ElasticExtractedBSplineSurface([], topologicEdges,...
                topologicFace, Mat2D,integrator,PolynomialOrder,DofDimension,Domain,  eye(geometryDescription.numberOfControlPoints));  
            
            obj.Element.setLocationMatrix(1:18);
            
            obj.Perm = [1     5     2     8     9     6     4     7     3];
            
            
            obj.Shapes = @(coords)  [1/16 *(-1 + coords(2))^2 *(-1 + coords(1))^2, 1/16* (-1 + coords(2))^2 *(1 + coords(1))^2,1/16 *(1 + coords(2))^2 *(1 + coords(1))^2,1/16 *(1 + coords(2))^2 *(-1 + coords(1))^2, -(1/8)* (-1 + coords(2))^2 *(-1 + coords(1)) *(1 + coords(1)), -(1/8) * (-1 + coords(2)) * (1 + coords(2)) * (1 + coords(1))^2, -(1/8) * (1 + coords(2))^2 *(-1 + coords(1)) *(1 + coords(1)), -(1/8)* (-1 + coords(2))* (1 + coords(2)) *(-1 + coords(1))^2,1/4 *(-1 + coords(2)) *(1 + coords(2)) *(-1 + coords(1)^2) ];
            obj.DShapesDXi = @(coords) [1/8* (-1 + coords(2))^2* (-1 + coords(1)), 1/8 *(-1 + coords(2))^2 *(1 + coords(1)), 1/8 *(1 + coords(2))^2* (1 + coords(1)), 1/8 *(1 + coords(2))^2* (-1 + coords(1)), -(1/ 4)* (-1 + coords(2))^2* coords(1),-(1/4) *(-1 + coords(2)^2) *(1 + coords(1)), -(1/4) *(1 + coords(2))^2 *coords(1), -(1/4) *(-1 + coords(2)^2)* (-1 + coords(1)), 1/2 *(-1 + coords(2)^2) * coords(1)];
            obj.DShapesDEta = @(coords) [ 1/8* (-1 + coords(2)) *(-1 + coords(1))^2, 1/8 *(-1 + coords(2))* (1 + coords(1))^2, 1/8 *(1 +    coords(2)) *(1 + coords(1))^2, 1/8* (1 + coords(2))* (-1 + coords(1))^2, -(1/4) *(-1 + coords(2)) *(-1 + coords(1)^2),-(1/4)* coords(2) *(1 + coords(1))^2, -(1/4)* (1 + coords(2))* (-1 + coords(1)^2),-(1/4) *coords(2) *(-1 + coords(1))^2,1/2 *coords(2) *(-1 + coords(1)^2) ];
            
            obj.X = @(coords) [-(1/16)* (7 + 3* coords(2)) *(-3 + coords(1)) *(1 + coords(1)), -(1/16) *(7 + 3* coords(2)) *(-1 + coords(1)) *(3 + coords(1)), 0 ];
            obj.Jacobian = @(coords) [-(1/8) *(7 + 3 *coords(2)) *(-1 + coords(1)), -(1/8)* (7 + 3 *coords(2))* (1 + coords(1)),0; ...
                -(3/16) *(-3 + coords(1)) * (1 + coords(1)), -(3/16) *(-1 + coords(1))* (3 + coords(1)),0];
        end
        
        function tearDown(obj)
        end
        
        %% test calcuation of Jacobian
        function testJacobian(obj)
            localCoord = [-6/10, 7/10 0];
            assertElementsAlmostEqual(obj.Element.calcJacobian(localCoord), obj.Jacobian(localCoord));
        end   

        %% test mapping from global to local coordinate system
        function testMapGlobalToLocalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element.mapGlobalToLocal( obj.X( localCoords ) ), localCoords );
            end
            
            checkPoint([-1 -1]);
            checkPoint([1 -1]);
            checkPoint([-1 1]);
            checkPoint([1 1]);
            
            checkPoint([0 0]);
            checkPoint([5/10, -4/10]);
        end
        
        %% test mapping from local to global coordinate system
        function testMapLocalToGlobalCoords(obj)
            
            function checkPoint(localCoords)
                assertElementsAlmostEqual(obj.Element.mapLocalToGlobal( localCoords ), obj.X( localCoords ) );
            end
            
            checkPoint([-1 -1]);
            checkPoint([1 -1]);
            checkPoint([-1 1]);
            checkPoint([1 1]);
            
            checkPoint([0 0]);
            checkPoint([5/10, -4/10]);
        end
        
        %% test evaluation of shape functions
        function testGetShapeFunctionsVector(obj)
            localCoord = [5/10, -4/10];
            
            NTest1 = obj.Element.evalShapeFunct(localCoord);
            test= obj.Shapes(localCoord);
            assertElementsAlmostEqual(NTest1, test(obj.Perm));
        end
        
        function testGetShapeFunctionsMatrix(obj)
            localCoord = [5/10, -4/10];
            test= obj.Shapes(localCoord); 
            assertElementsAlmostEqual(obj.Element.getShapeFunctionsMatrix(localCoord), [test(obj.Perm) 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0 0 test(obj.Perm)]);
        end
        
        %% test evaluation of shape function derivatives
        function testGetShapeFunctionsDerivativeVector(obj)
            localCoord = [5/10, -4/10];
             
            dN1 = obj.Element.evalDerivOfShapeFunct(localCoord);
            testXi = obj.DShapesDXi( localCoord );
            testEta = obj.DShapesDEta( localCoord );
            assertElementsAlmostEqual(dN1, [ testXi(obj.Perm) testEta(obj.Perm)  ] );
        end
        
        %% test evaluation of B
        function testGetB(obj)
            u = [-(749/7540), -(525/1508), 27/116, 111/7540, -(1561/3770), 297/754, 459/3770, -(31/3770), 201/1885]; 
            v = [1799/22620, -(3423/7540), 9/580, 193/7540, 329/3770, -(597/3770), 309/3770, 1061/11310, 431/1885];
            assertElementsAlmostEqual(obj.Element.getB([5/10, -4/10]), [u(obj.Perm) zeros(1,9); zeros(1,9) v(obj.Perm); v(obj.Perm) u(obj.Perm)]);
        end
        
    end
    
end

