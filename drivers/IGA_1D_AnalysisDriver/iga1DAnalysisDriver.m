%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 

%% Preliminarities
clear all;
% Set path for Logger
%Logger.setPathForLogger('pFEM1DAnalysisDriver.m','log1DpFEM.out',1);

%% Input parameters
% Numerical parameters
NumberOfXDivisions = 3;
PolynomialDegree = 3;
NumberGP = PolynomialDegree+1; % number of Gauss points
DofDimension = 1;

% Mechanical parameters
E = 1;      % Young's modulus
A = 1;      % cross-sectional area
L = 1;      % length
Density = 1;

% Loads
F = 1;      % point load at the end of the bar
DistributedLoad = @(x,y,z)(- sin(8*x)); % distributed load

%% Instanciation of the problem
Mat1D = Hooke1D(A,E,Density,1);
Mat1D = [Mat1D Mat1D]; % to make it work with FCM

% Creation of the FEM system

ControlPoints = [0 0 0;
                 1 0 0];
curve = BSplineCurve( [-1 -1 1 1], 1, ControlPoints );
knots = linspace(-1, 1, NumberOfXDivisions+1);
curve.elevateOrder( PolynomialDegree-1 );
curve.insertKnots( knots(2:length(knots)-1) );


MyElementFactory = ElementFactoryElasticExtractedBSplineBar(Mat1D,NumberGP, curve.getKnotsXi(), curve.getPolynomialDegreeXi);
MyGeometryFactory = ExtractedBSplineCurveFactory( curve );
MyMeshFactory = MeshFactoryIGA1D( curve, DofDimension, MyElementFactory, MyGeometryFactory);

MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Adding line load

LoadCase = LoadCase();
LoadCase.addBodyLoad(DistributedLoad);

MyAnalysis.addLoadCases(LoadCase);

% Adding Dirichlet boundary conditions
MyDirichletAlgorithm = StrongPenaltyAlgorithm(10E4);

leftNodeFilter = @(ControlPoints)( isClose(ControlPoints(:,1), 0) );
PointSupport = StrongControlPointsDirichletBoundaryCondition(0, [1],MyDirichletAlgorithm, leftNodeFilter);

MyAnalysis.addDirichletBoundaryCondition(PointSupport);

% Resolution of the linear equation system
MyAnalysis.solve;



%% Postprocessing
loadCaseToVisualize = 1;
indexOfPhysicalDomain = 2;

gridSize = 0.05;

postProcessingFactory = VisualPostProcessingFactory1D( gridSize );
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @SolutionVector, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( MyAnalysis.getMesh() );