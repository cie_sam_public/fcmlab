%% 2D p-FEM - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
Logger.setPathForLogger('pFEM2DAnalysisDriver.m','log2DpFEM.out',1);

%% Input parameters
% Numerical parameters
MeshOrigin = [0.0 0.0 0.0];
NumberOfXDivisions = 40;
NumberOfYDivisions = 5;
PolynomialDegree = 2;
NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points

% Mechanical parameters
E = 1000;
PoissonsRatio = 0.0;
Lx = 10.0;
Ly = 1.0;
Density = 1.0;

Force = .1;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
DofDimension = 2;

MyElementFactory = ElementFactoryElasticQuad(Material,NumberOfGaussPoints);

MyMeshFactory = MeshFactory2DUniform(NumberOfXDivisions,...
    NumberOfYDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    DofDimension,MeshOrigin,Lx,Ly,MyElementFactory);

MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
Load = WeakEdgeNeumannBoundaryCondition([Lx,0.0,0.0],[Lx,Ly,0.0],GaussLegendre(NumberOfGaussPoints),@(x,y,z)[0.0;-Force]);
LoadCase = LoadCase();
LoadCase.addNeumannBoundaryCondition(Load);

MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC
SupportEdge = StrongEdgeDirichletBoundaryCondition([0.0 0.0 0.0],[0.0 Ly 0.0],@(x,y,z)(0.0),[1 1],StrongDirectConstrainingAlgorithm);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 1 0.25] ;
warpScalingFactor = 1;

postProcessingFactory = VisualPostProcessingFactory2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor);
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );

% w = F*l^3/(3*E*I)
% I = b*h^3/12

I = 1*Ly^3/12;

w = Force*Lx^3/(3*E*I)
