%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% 3D FCM - Driver file - Cube with Sperical Hole
% Reference: D.Schillinger, PhD Thesis, P 41, Figure 4.1b)

%% Preliminarities
clear all;
clc;
% Set path for Logger
Logger.setPathForLogger('FCM3DPlasticityCubeWithSphericalHoleDriver.m','FCM3DPlasticityCubeWithSphericalHoleDriver.out',0);

Logger.ConsoleLevel('debug');

%% Input parameters
% Numerical parameters
meshOrigin = [0.0 0.0 0.0];

numberOfXDivisions = 2;
numberOfYDivisions = 2;
numberOfZDivisions = 2;

PolynomialDegree = 1;

NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points
NoIncrements = 1;

refinementDepth = 2;
alpha = 1E-10;

% Mechanical parameters
E = 200000;
poissonsRatio = 0.3;
density = 1;
yieldStress = 100;
plasticModulus = 100;

zLoad = -10;

% Geometry parameters
edgeLength = 10;

holeRadius = 3.5;
                               
% Mesh Parameters
Lx = edgeLength/2;
Ly = edgeLength/2;
Lz = edgeLength;
          
% Domain
cube = CubeWithSphericalHole( meshOrigin, edgeLength, holeRadius);

%% Instanciation of the problem
mat3DFCM(1) = Plastic3DMisesIsotropicHardening(E,poissonsRatio,plasticModulus,yieldStress,density,alpha);
mat3DFCM(2) = Plastic3DMisesIsotropicHardening(E,poissonsRatio,plasticModulus,yieldStress,density,1);

% Creation of the FEM system
dofDimension = 3;

myElementFactory = ElementFactoryPlasticHexaFCM(mat3DFCM,cube,...
    NumberOfGaussPoints,refinementDepth);

myMeshFactory = MeshFactory3DUniform(numberOfXDivisions,...
    numberOfYDivisions,numberOfZDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    dofDimension,meshOrigin,Lx,Ly,Lz,myElementFactory);

myAnalysis = NonLinearAnalysis(myMeshFactory);

%% Symmetry Boundary Conditions
% XZ
startPoint = meshOrigin;
endPoint = meshOrigin + [ edgeLength/2 0.0 edgeLength];
fixedDirection = [0 1 0];
boundaryConditionXZ = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);

% YZ
startPoint = meshOrigin;
endPoint = startPoint + [ 0.0 edgeLength/2 edgeLength ];
fixedDirection = [1 0 0];
boundaryConditionYZ = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);

% Clamped bottom
startPoint = meshOrigin;
endPoint = meshOrigin + [ edgeLength/2 edgeLength/2 0.0 ];
fixedDirection = [0 0 1];
boundaryConditionXY = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);

myAnalysis.addDirichletBoundaryCondition(boundaryConditionXZ );
myAnalysis.addDirichletBoundaryCondition(boundaryConditionYZ );
myAnalysis.addDirichletBoundaryCondition(boundaryConditionXY );

%% Loads
% prescribed displacement
startPoint = [0.0 0.0 edgeLength];
endPoint = startPoint + [edgeLength/2 edgeLength/2 0.0];
fixedDirection = [0 0 1];
prescribedDisplacement = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(-0.3),fixedDirection ,StrongDirectConstrainingAlgorithm);
% myAnalysis.addDirichletBoundaryCondition(prescribedDisplacement );

Load = WeakFaceNeumannBoundaryCondition(startPoint,endPoint,GaussLegendre(NumberOfGaussPoints),@(x,y,z) [0 ; 0 ; zLoad]);
LoadCase1 = LoadCase();
LoadCase1.addNeumannBoundaryCondition(Load);
myAnalysis.addLoadCases(LoadCase1);
%%
[ ~, strainEnergy ] = myAnalysis.solve(NoIncrements);

%% Post Processing
indexOfPhysicalDomain = 2;
 
FeMesh = myAnalysis.getMesh();
 
% create the point processors 
loadCaseToVisualize = 1; 
 
% Surface Plots
% gridSize = [ 0.1 0.1 0.1];
gridSize = [0.25 0.25 0.25];
postProcessingFactory = VisualPostProcessingFactory3DFCM( FeMesh, cube, indexOfPhysicalDomain, gridSize );
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );

postProcessor.registerPointProcessor( @VonMisesStressHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );
postProcessor.registerPointProcessor( @StrainElastHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );
postProcessor.registerPointProcessor( @StrainPlastHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );