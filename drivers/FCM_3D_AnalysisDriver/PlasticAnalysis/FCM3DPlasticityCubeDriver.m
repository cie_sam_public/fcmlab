%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% 3D FCM - Driver file - Cube with Sperical Hole
% Reference: D.Schillinger, PhD Thesis, P 41, Figure 4.1b)

%% Preliminarities
clear all;
clc;
% Set path for Logger
Logger.setPathForLogger('FCM3DPlasticityCubeDriver.m','FCM3DPlasticityCubeDriver.out',0);

Logger.ConsoleLevel('debug');

%% Input parameters
% Numerical parameters
MeshOrigin = [0.0 0.0 0.0];

numberOfXDivisions = 1;
numberOfYDivisions = 1;
numberOfZDivisions = 1;

PolynomialDegree = 1;

NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points
NoIncrements = 4;

refinementDepth = 2;
alpha = 1E-10;

% Mechanical parameters
E = 1000;
poissonsRatio = 0.3;
density = 1;
yieldStress = 100;
plasticModulus = 100;

load = 200;

% Geometry parameters
edgeLength = 1;
                               
% Mesh Parameters
Lx = edgeLength;
Ly = edgeLength;
Lz = edgeLength;
          
lengths = [Lx, Ly, Lz];

% Domain
cube = EmbeddedCuboid( MeshOrigin, lengths);

%% Instanciation of the problem
mat3DFCM(1) = Plastic3DMisesIsotropicHardening(E,poissonsRatio,plasticModulus,yieldStress,density,alpha);
mat3DFCM(2) = Plastic3DMisesIsotropicHardening(E,poissonsRatio,plasticModulus,yieldStress,density,1);

% Creation of the FEM system
dofDimension = 3;

myElementFactory = ElementFactoryPlasticHexaFCM(mat3DFCM,cube,...
    NumberOfGaussPoints,refinementDepth);

myMeshFactory = MeshFactory3DUniform(numberOfXDivisions,...
    numberOfYDivisions,numberOfZDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    dofDimension,MeshOrigin,Lx,Ly,Lz,myElementFactory);

MyAnalysis = NonLinearAnalysis(myMeshFactory);

%% Boundary Conditions

% Dirichlet Boundary Conditions
PenaltyValue = 10E14;
% StrDirAlg = StrongPenaltyAlgorithm(PenaltyValue); % strong dirichlet algorithm
StrDirAlg = StrongDirectConstrainingAlgorithm;

% Fixed Support
SupportF1 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[Lx Ly 0],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF2 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[Lx 0 Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF3 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[0 Ly Lz],@(x,y,z)(0),[1 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF4 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[Lx 0 0], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF5 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[0 Ly 0], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF6 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[0 0 Lz], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));

MyAnalysis.addDirichletBoundaryCondition(SupportF1);
MyAnalysis.addDirichletBoundaryCondition(SupportF2);
MyAnalysis.addDirichletBoundaryCondition(SupportF3);
MyAnalysis.addDirichletBoundaryCondition(SupportF4);
MyAnalysis.addDirichletBoundaryCondition(SupportF5);
MyAnalysis.addDirichletBoundaryCondition(SupportF6);

FaceLoad = WeakFaceNeumannBoundaryCondition([Lx,0,0],[Lx,Ly,Lz],GaussLegendre(NumberOfGaussPoints),@(x,y,z)[load;0;0]);
FaceLoad2 = WeakFaceNeumannBoundaryCondition([Lx,0,0],[Lx,Ly,Lz],GaussLegendre(NumberOfGaussPoints),@(x,y,z)[2*load;0;0]);
LoadCase1 = LoadCase();
LoadCase2 = LoadCase();
LoadCase1.addNeumannBoundaryCondition(FaceLoad);
LoadCase2.addNeumannBoundaryCondition(FaceLoad2);
MyAnalysis.addLoadCases(LoadCase1);
% MyAnalysis.addLoadCases(LoadCase2);

%%
[ solutionVectors , strainEnergy ] = MyAnalysis.solve(NoIncrements);

%% Post Processing
indexOfPhysicalDomain = 2;
 
FeMesh = MyAnalysis.getMesh();
 
% create the point processors 
loadCaseToVisualize = 1; 
 
% Surface Plots
% gridSize = [ 0.1 0.1 0.1];
gridSize = [0.25 0.25 0.25];
postProcessingFactory = VisualPostProcessingFactory3DFCM( FeMesh, cube, indexOfPhysicalDomain, gridSize );
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
% keyboard
postProcessor.registerPointProcessor( @VonMisesStressHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );
% postProcessor.registerPointProcessor( @StrainElastHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );
% postProcessor.registerPointProcessor( @StrainPlastHistory, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );