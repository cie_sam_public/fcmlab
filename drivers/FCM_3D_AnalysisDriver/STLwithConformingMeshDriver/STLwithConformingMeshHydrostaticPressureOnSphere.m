%% Preliminarities
clear;
close all;
clc;

%% Domain Setup


Origin = [0 0 0];
Radius = 1;

MyEmbeddedSphere= EmbeddedSphere(Origin,Radius);



%% Material Setup

YoungsModulus = 1;
PoissonRatio = 0.3;
Density = 1;
alpha = 0;

MyMaterial(1) = Hooke3D(YoungsModulus, PoissonRatio, Density, alpha);
MyMaterial(2) = Hooke3D(YoungsModulus, PoissonRatio, Density, 1);



%% ElementFactory Setup

PolynomialDegree = 5;
NumberOfGaussPoints = PolynomialDegree+1;
RefinementSteps = 1;

MyElementFactory = ElementFactoryElasticHexaFCM(MyMaterial,MyEmbeddedSphere,PolynomialDegree, RefinementSteps);


%% MeshFactory Setup
MeshOrigin = [0 0 0];
LX = 1.5;
LY = 1.5;
LZ = 1.5;
NumberOfXDivisions = 2; 
NumberOfYDivisions = 2;
NumberOfZDivisions = 2;
myNumberingScheme = TopologicalSorting;
DofDimension = 3;


MyMeshFactory = MeshFactory3DUniform(NumberOfXDivisions,NumberOfYDivisions,...
                           NumberOfZDivisions,PolynomialDegree,myNumberingScheme,...
                           DofDimension,MeshOrigin,LX,LY,LZ,MyElementFactory);


%% Analysis

MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);




%% Load Case
 
MyLoadCase =  LoadCase();

STLFile = 'sphere.stl';

tolerance = 1E-7;
IntegrationScheme = GaussLegendre(NumberOfGaussPoints);

%MyBoundaryFactory = STLBoundaryFactory(STLFile);
MyBoundaryFactory = STLwithConformingMeshBoundaryFactory(STLFile,tolerance);
MyBoundaryFactory.extractPlanesFromMeshData(MeshOrigin, NumberOfXDivisions, NumberOfYDivisions, NumberOfZDivisions, LX, LY, LZ );
MyBoundaryFactory.processBoundary();
MyBoundaryFactory.plotBoundary('new');
Boundary = MyBoundaryFactory.getBoundary();

Force = @(x,y,z) -[x;y;z]/norm([x y z])*100;

NeumannBC = WeakNeumannBoundaryCondition(Force,IntegrationScheme, Boundary);

MyLoadCase.addNeumannBoundaryCondition(NeumannBC); 

MyAnalysis.addLoadCases(MyLoadCase);

%% BoundaryConditions



startPoint = Origin;
endPoint = Origin + [ LX LY 0.0 ];
fixedDirection = [0 0 1];
 
boundaryConditionXY = StrongFaceDirichletBoundaryCondition(startPoint,endPoint ,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);
 
startPoint = Origin + [LX 0 0];
endPoint = Origin + [ LX LY LZ ];
fixedDirection = [1 0 0 ];

boundaryConditionYZ = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);

startPoint = Origin;
endPoint = Origin + [ LX 0.0 LZ ];
fixedDirection = [0 1 0];

boundaryConditionXZ = StrongFaceDirichletBoundaryCondition(startPoint,endPoint,@(x,y,z)(0),fixedDirection ,StrongDirectConstrainingAlgorithm);

MyAnalysis.addDirichletBoundaryCondition(boundaryConditionXY);
MyAnalysis.addDirichletBoundaryCondition(boundaryConditionYZ);
MyAnalysis.addDirichletBoundaryCondition(boundaryConditionXZ);



%% Analysis

MyAnalysis.solve();



%% Postprocessing

FeMesh = MyAnalysis.getMesh();

% Visual Post Processing

% create the point processors
indexOfPhysicalDomain = 2;
loadCaseToVisualize = 1;


vonMisesStress = VonMisesStress(1,indexOfPhysicalDomain);

displacementNorm = DisplacementNorm( ...
    loadCaseToVisualize );


gridSize = [0.05 0.05 0.05];
postProscessingFactory = VisualPostProcessingFactory3DFCM(FeMesh, MyEmbeddedSphere, indexOfPhysicalDomain, gridSize); 

postProcessor = postProscessingFactory.creatVisualPostProcessor();
postProcessor.registerPointProcessor(displacementNorm);
postProcessor.registerPointProcessor(vonMisesStress);

postProcessor.visualizeResults(FeMesh);






