%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% VoxelCube
clear all;
% Set path for Logger
Logger.setPathForLogger('CuboidTopOpt.m','log.out',1);
Logger.ConsoleLevel('debug');

%% Input parameters
% Numerical parameters
MeshOrigin = [0 0 0]; % offset values from the voxel file
NumberOfXDivisions = 10;
NumberOfYDivisions = 10;
NumberOfZDivisions = 10;
PolynomialDegree = 5;
NumberOfGaussPoints = PolynomialDegree+1;
PenaltyValue = 1E6;
RefinementSteps = 3;
% TopOptParameters
AlphaMin = 10^-10;
SigmaObj = 25;
Roh = 3;
SicherheitsFaktor = 1.5;
NumberOfIterationsteps = 10;
Tolleranzwert = 0.001;
% Mesh Parameters in m
Lx = 1.5;
Ly = 1.5;
Lz = 1.5;

% MaterialParameter
E = 206.9 * 1E6;
Poisson = 0.3;
Density = 1;
VoxelDataFile = 'Cuboid_CTData.txt';
MyVoxelDomain = VoxelDomain(VoxelDataFile);
%% Instanciation of the problem
Mat3DFCM(1) = VoxelMaterialHooke3D(E,Poisson,Density,1,MyVoxelDomain);
Mat3DFCM(2) = VoxelMaterialHooke3D(E, Poisson,Density,1,MyVoxelDomain);

% Creation of the FEM system

DofDimension = 3;

MyElementFactory = ElementFactoryElasticHexaFCM(Mat3DFCM,MyVoxelDomain,...
    NumberOfGaussPoints,RefinementSteps);

MyMeshFactory = MeshFactory3DUniform(NumberOfXDivisions,...
    NumberOfYDivisions,NumberOfZDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    DofDimension,MeshOrigin,Lx,Ly,Lz,MyElementFactory);
Analysis = TopOptAnalysis(MyMeshFactory, MyVoxelDomain, AlphaMin, SigmaObj, Roh,SicherheitsFaktor, NumberOfIterationsteps);

%% LoadCases 
%Load = Gravity3D(Density);
LoadBoundary = LoadSurfaceFactory.getBoundary();
pressure = -5; % [N/mm^2] ( should be about 1000N = 100Kg in the integral)
Traction = @(x,y,z) [0;0; pressure];
Load = WeakNeumannBoundaryCondition(Traction,GaussLegendre(NumberOfGaussPoints),LoadBoundary);

LoadCase = LoadCase();
LoadCase.addNeumannBoundaryCondition(Load);
Analysis.addLoadCases(LoadCase);

Support = StrongFaceDirichletBoundaryCondition(MeshOrigin,MeshOrigin+[Lx Ly 0],@(x,y,z)(0),[1 1 1],StrongPenaltyAlgorithm(PenaltyValue));

Analysis.addDirichletBoundaryCondition(Support);
 
Analysis.solve; 

%% Post Processing
indexOfPhysicalDomain = 2;
 
FeMesh = Analysis.getMesh();
 
% create the point processors 
loadCaseToVisualize = 1; 
displacementNorm = DisplacementNorm( loadCaseToVisualize ); 
vonMises = VonMisesStress( loadCaseToVisualize, indexOfPhysicalDomain );
 
% Surface Plots
gridSize = [1 1 1];
postProcessingFactory = VisualPostProcessingFactory3DFCM( FeMesh, MyVoxelDomain,  indexOfPhysicalDomain, gridSize);
%postProcessingFactory = VisualPostProcessingFactoryForVTK( FeMesh,  gridSize );
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

% bis hier hin wurden nur Informationen auf Variablen geladen

%postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );

% set color scaling for stresses
caxis([0 10]);
view(54,27);
xlabel('mm');
ylabel('');
zlabel('');
set(gcf, 'Position', [100 100 351 400])


