%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% 3D p-FEM - Driver file 

%% Preliminarities
clear all,clc;
% Set path for Logger
Logger.setPathForLogger('pFEM3DAnalysisDriver.m','log3DpFEM.out',1);

%% Input parameters
% Numerical parameters
MeshOrigin = [0 0 0];
NumberOfXDivisions = 1;
NumberOfYDivisions = 1;
NumberOfZDivisions = 1;
PolynomialDegree = 1;
NumberOfGaussPoints = PolynomialDegree + 1; % number of Gauss points
NoIncrements = 4;

% Mechanical parameters
E = 1000.0;
PoissonsRatio = 0;
Lx = 1;
Ly = 1;
Lz = 1;
Density = 1.0;
load = 101;
yieldStress = 100;
plasticModulus = 100;

%% Instanciation of the problem
Material = Plastic3DMisesIsotropicHardening(E,PoissonsRatio,plasticModulus,yieldStress,Density,1);

% Creation of the FEM system
DofDimension = 3;

% Plastic Analysis Setup
MyElementFactory = ElementFactoryPlasticHexa(Material,NumberOfGaussPoints);

MyMeshFactory = MeshFactory3DUniform(NumberOfXDivisions,...
    NumberOfYDivisions,NumberOfZDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    DofDimension,MeshOrigin,Lx,Ly,Lz,MyElementFactory);

MyAnalysis = NonLinearAnalysis(MyMeshFactory);

%% Boundary Conditions

% Dirichlet Boundary Conditions
PenaltyValue = 10E14;
% StrDirAlg = StrongPenaltyAlgorithm(PenaltyValue); % strong dirichlet algorithm
StrDirAlg = StrongDirectConstrainingAlgorithm;

% Fixed Support
SupportF1 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[Lx Ly 0],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF2 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[Lx 0 Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF3 = StrongFaceDirichletBoundaryCondition(MeshOrigin, MeshOrigin+[0 Ly Lz],@(x,y,z)(0),[1 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF4 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[Lx 0 0], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF5 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[0 Ly 0], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));
SupportF6 = StrongFaceDirichletBoundaryCondition(MeshOrigin+[0 0 Lz], MeshOrigin+[Lx Ly Lz],@(x,y,z)(0),[0 1 1],StrongPenaltyAlgorithm(PenaltyValue));

MyAnalysis.addDirichletBoundaryCondition(SupportF1);
MyAnalysis.addDirichletBoundaryCondition(SupportF2);
MyAnalysis.addDirichletBoundaryCondition(SupportF3);
MyAnalysis.addDirichletBoundaryCondition(SupportF4);
MyAnalysis.addDirichletBoundaryCondition(SupportF5);
MyAnalysis.addDirichletBoundaryCondition(SupportF6);

FaceLoad = WeakFaceNeumannBoundaryCondition([Lx,0,0],[Lx,Ly,Lz],GaussLegendre(NumberOfGaussPoints),@(x,y,z)[load;0;0]);
FaceLoad2 = WeakFaceNeumannBoundaryCondition([Lx,0,0],[Lx,Ly,Lz],GaussLegendre(NumberOfGaussPoints),@(x,y,z)[2*load;0;0]);
LoadCase1 = LoadCase();
LoadCase2 = LoadCase();
LoadCase1.addNeumannBoundaryCondition(FaceLoad);
LoadCase2.addNeumannBoundaryCondition(FaceLoad2);
MyAnalysis.addLoadCases(LoadCase1);
% MyAnalysis.addLoadCases(LoadCase2);

%% Solution 
[solution, strainEnergy] = MyAnalysis.solve(NoIncrements);

%% Post Processing
GPPostProcessor(MyAnalysis.getMesh(),1,2);
