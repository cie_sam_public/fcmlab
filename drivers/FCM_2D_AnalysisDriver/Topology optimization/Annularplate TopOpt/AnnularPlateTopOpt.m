%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% 2D FCM - Driver file

%% Preliminarities
clear all;
% Set path for Logger
Logger.setPathForLogger('AnnularPlateTopOpt.m','log2DFCM.out',0);

Logger.ConsoleLevel('debug');

%% Input parameters
% Numerical parameters
MeshOrigin = [0.0 0.0 0.0];
MeshLengthX = 1.500;
MeshLengthY = 1.000;
NumberOfXDivisions = 42;
NumberOfYDivisions = 33;
PolynomialDegree = 3;
NumberOfGaussPoints = PolynomialDegree+1;
Lx = MeshLengthX;
Ly = MeshLengthY;

PenaltyValue = 1E20;
RefinementSteps = 3; %integration depth

% TopOptParameters
AlphaMin = 1E-10;
SigmaObj = 25;
Roh = 3;
SicherheitsFaktor = 1.2;
NumberOfIterationsteps = 2;
Tolleranzwert= 0.1;
% Mechanical parameters
E = 206.9E3;
Poisson = 0.29;
Density = 1.0;
ScalingFactor = 1E-10;

% Material Parameters
VoxelDataFile = 'Plate_CT_Data.txt';
MyVoxelDomain = VoxelDomain(VoxelDataFile);


%% Instanciation of the problem
 

Mat2DFCM(1) = VoxelMaterial2DHookePlaneStress(E,Poisson,Density,...
    ScalingFactor,MyVoxelDomain);
Mat2DFCM(2) = VoxelMaterial2DHookePlaneStress(E,Poisson,Density,...
    1,MyVoxelDomain);

% Creation of the FEM system

DofDimension = 2;

MyElementFactory = ElementFactoryElasticQuadFCM(Mat2DFCM,MyVoxelDomain,...
    NumberOfGaussPoints,RefinementSteps);

MyMeshFactory = MeshFactory2DUniform(NumberOfXDivisions,...
    NumberOfYDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
    DofDimension,MeshOrigin,Lx,Ly,MyElementFactory);

MyAnalysis = TopOptAnalysis(MyMeshFactory, MyVoxelDomain, AlphaMin,...
    SigmaObj, Roh, SicherheitsFaktor, NumberOfIterationsteps,...
    Tolleranzwert);

% LoadCases

Load = WeakEdgeNeumannBoundaryCondition([MeshLengthX,...
    (16*(MeshLengthY/NumberOfYDivisions)),0.0],...
    [MeshLengthX,(17*(MeshLengthY/NumberOfYDivisions)),0.0],...
    GaussLegendre(NumberOfGaussPoints),@(x,y,z)[0.0;-(175)]);

LoadCase = LoadCase();
LoadCase.addNeumannBoundaryCondition(Load);
MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

% Restrict movement in x,y-direction along the edge
SupportEdge = StrongEdgeDirichletBoundaryCondition([0.0 0.0 0.0],...
    [0.0 MeshLengthY 0.0],@(x,y,z)(0.0),[1 1],...
    StrongPenaltyAlgorithm(PenaltyValue));

MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve; 
%% Post Processing
indexOfPhysicalDomain = 2;
 
FeMesh = MyAnalysis.getMesh();
 
% create the point processors 
loadCaseToVisualize = 1; 
%displacementNorm = DisplacementNorm( loadCaseToVisualize ); 
vonMises = VonMisesStress( loadCaseToVisualize, indexOfPhysicalDomain );
 
% Surface Plots
gridSize = 0.02;
postProcessingFactory = VisualPostProcessingFactory2DFCM( FeMesh,...
    MyVoxelDomain,  indexOfPhysicalDomain, gridSize);

postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @VonMisesStress, ...
    { loadCaseToVisualize, indexOfPhysicalDomain } );
postProcessor.visualizeResults( FeMesh );


