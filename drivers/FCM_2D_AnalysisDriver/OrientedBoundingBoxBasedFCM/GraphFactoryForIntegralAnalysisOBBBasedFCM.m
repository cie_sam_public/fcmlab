%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Calculation of integration error in Oriented Bounding Boxes.

 %% 2D FCM - Driver file

%% Preliminarities
clear all;
close all;
% Set path for Logger
% Logger.setPathForLogger('EmbeddedRectanglePlaneStress.m','log2DFCM.out',0);

% Logger.ConsoleLevel('debug');

%% Construction of OBBs
% Embedded Domain Setup
Center = [ -0.0 -0.0 0.0 ];
Lengths = [ 1.2 0.4 ];

% Plot Parameter
xPlot = linspace(0,(90),11);
Colors = ['b','r','g','m','y','c','b'];

for m = 1:11
steps = m-1;
Rec = EmbeddedRotatedRectangle( Center, Lengths, (pi*steps)/20 );

% Geometry Setup
realGeometry = Rectangle([...
Vertex([-1.0,-1.0,0.0]),...
Vertex([ 1.0,-1.0,0.0]),...
Vertex([ 1.0, 1.0,0.0]),...
Vertex([-1.0, 1.0,0.0])],[]);

% Partition Space Tree
quadTree = SpaceQuadTree(Rec, realGeometry);
quadTree.partition(4,8)
quadTree.createOrientedBoundingBoxes(0)
GlobalOBBs = quadTree.getAllOBBs();
OBBs(m) = GlobalOBBs(1);
end

for SpaceTreeDepth = 1:2
for m = 1:11
steps = m-1;
Rec = EmbeddedRotatedRectangle( Center, Lengths, (pi*steps)/20 );

%% Input parameters
% Numerical parameters complete domain
number = 1;
MeshOrigin = [(-OBBs(m).lengths(1)/2), (-OBBs(m).lengths(2)/2), 0.0];
MeshLengthX = OBBs(m).lengths(1);
MeshLengthY = OBBs(m).lengths(2);
transformMatrix = [[OBBs(m).vectors(1,1), OBBs(m).vectors(1,2), 0];...
    [OBBs(m).vectors(2,1),OBBs(m).vectors(2,2), 0];[0,0,1]];
centerOfCoordinates = (1)*[OBBs(m).center(1) OBBs(m).center(2) 0.0];

NumberOfXDivisions = 2;
NumberOfYDivisions = 2;
PolynomialDegree = 1;
NumberOfGaussPoints = PolynomialDegree+1;


% Mechanical parameters
YoungsModulus = 1.0;
PoissonsRatio = 0.3;
Density = 1.0;

% FCM Parameter
Alpha = 0;


%% Instanciation of the problem

% Creation of Materials
Materials(1) = HookePlaneStress( ...
    YoungsModulus, PoissonsRatio, ...
    Density, Alpha );
Materials(2) = HookePlaneStress( ...
    YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Creation of the FEM system
DofDimension = 2;

ElementFactory = ElementFactoryElasticOrientedQuadFCM(Materials,Rec,...
    NumberOfGaussPoints,SpaceTreeDepth);

MeshFactory = MeshFactory2DOrientedUniform(NumberOfXDivisions,...
    NumberOfYDivisions,PolynomialDegree,TopologicalSorting,...
    DofDimension,MeshOrigin,MeshLengthX,MeshLengthY,ElementFactory, transformMatrix, centerOfCoordinates);


%% Integration Post Processing

postProcessor = IntegrationPostProcessor( );

postProcessor.registerPointProcessor( IntegrationMass(1.0));

FeMesh = MeshOriented(MeshFactory);

numberOfDofs = FeMesh.getNumberOfDofs();
            
soultionVector = ones(numberOfDofs,1);

FeMesh.scatterSolution( soultionVector );
integral(m) = postProcessor.integrate( FeMesh );

calculationError(SpaceTreeDepth,m) = (((1.2*0.4)-integral(m))/(1.2*0.4))*100;
end

plot(xPlot,calculationError(SpaceTreeDepth,:),'color', Colors(SpaceTreeDepth))
hold on;
end
hold off;
xlabel('rotation in degree');
ylabel('error in %');

