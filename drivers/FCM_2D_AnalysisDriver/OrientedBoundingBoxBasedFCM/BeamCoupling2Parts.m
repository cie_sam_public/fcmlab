%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Beam with predefined displacement. Separated in two parts. Coupled over
% intersection surface or intersection line.

%% Preliminarities
clear all;
close all;

%% Corner of complete geometry
Center = [ -0.00 -0.0 0.0 ];
GlobalLengths = [ 1.0 0.2 ];
% Origin = [-Lengths(1)/2 -Lengths(2)/2 0.0];

Theta =pi/3;
RotationMatrix = [[cos(Theta) -sin(Theta) 0];[sin(Theta) cos(Theta) 0];[0 0 1]];
% v2------v3
% |       |
% v1------v4
v1 = (RotationMatrix*[-GlobalLengths(1)/2 -GlobalLengths(2)/2 0.0]')'+Center;
v2 = (RotationMatrix*[-GlobalLengths(1)/2 GlobalLengths(2)/2 0.0]')'+Center;
v3 = (RotationMatrix*[GlobalLengths(1)/2 GlobalLengths(2)/2 0.0]')'+Center;
v4 = (RotationMatrix*[GlobalLengths(1)/2 -GlobalLengths(2)/2 0.0]')'+Center;

v5 = (RotationMatrix*[0.0 0.1 0.0]')'+Center;
v6 = (RotationMatrix*[0.0 -0.1 0.0]')'+Center;
%% First Mesh

%% Embedded Domain Setup
% Center = [ -0.1414213562 -0.1414213562 0.0 ];
Lengths = [ 0.60 0.20 ];
% Origin = [-Lengths(1)/2 0.0 0.0];
Origin = [-0.2 0.0 0.0];
% Theta = pi/4;
% RotationMatrix = [[cos(Theta) -sin(Theta) 0];[sin(Theta) cos(Theta) 0];[0 0 1]];
Center = (RotationMatrix*Origin')';
% Create the domain
Rectangle = EmbeddedRotatedRectangle( Center, Lengths, Theta);

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 3;
NumberOfGaussPoints = PolynomialDegree+3;
SpaceTreeDepth = 1;

ElementFactory = ElementFactoryElasticOrientedQuadFCM( Materials, Rectangle, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-Lengths(1)/2 -Lengths(2)/2 0.0];
MeshLengthX = Lengths(1);
MeshLengthY = Lengths(2);
CenterOfMesh = Center;%[0.0 0.0 0.0];
NumberOfXDivisions = 2;
NumberOfYDivisions = 1;
DofDimension = 2;

MeshFactories{1} = MeshFactory2DOrientedUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory,...
    RotationMatrix, CenterOfMesh );

%% Second Mesh
%% Embedded Domain Setup
% Center = [ 0.1414213562 0.1414213562 0.0 ];
Lengths = [ 0.6 0.20 ];
Origin = [0.2 0.0 0.0];

% Theta = pi/4;
% RotationMatrix = [[cos(Theta) -sin(Theta) 0];[sin(Theta) cos(Theta) 0];[0 0 1]];
Center = (RotationMatrix*abs(Origin)')';
% Create the domain
Rectangle2 = EmbeddedRotatedRectangle( Center, Lengths, Theta);

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 7;
NumberOfGaussPoints = PolynomialDegree+1;
SpaceTreeDepth = 1;

ElementFactory = ElementFactoryElasticOrientedQuadFCM( Materials, Rectangle2, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-Lengths(1)/2 -Lengths(2)/2 0.0];
MeshLengthX = Lengths(1);
MeshLengthY = Lengths(2);
CenterOfMesh = Center;%[0.0 0.0 0.0];
NumberOfXDivisions = 2;
NumberOfYDivisions = 1;
DofDimension = 2;

MeshFactories{2} = MeshFactory2DOrientedUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory,...
    RotationMatrix, CenterOfMesh );

%% Analysis Setup

% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactories);

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary

% Interface Geometry for Rotated System with Boundary line
Origin = v5;
Lengths = v6;
NumberOfBoundarySegments = 5;
InterfaceBoundaryFactory = StraightLineBoundaryFactory( Origin, Lengths, NumberOfBoundarySegments);
InterfaceGeometry = InterfaceBoundaryFactory.getBoundary();

% Interface Rectangle only for Axis-Aligned Systems
% Origin = [-0.1 -0.1 0.0];
% Lengths = [0.1*sqrt(2) 0.1*sqrt(2)];
% Lengths= [0.2 0.2];
% NumberOfBoundarySegments = 5;
% InterfaceBoundaryFactory = RectangleBoundaryFactory( Origin, Lengths, NumberOfBoundarySegments);
% InterfaceGeometry = InterfaceBoundaryFactory.getBoundary();


% Create Coupling condition
Beta = 1e8;
FirstMeshId = 1;
SecondMeshId = 2; 
InterfaceCoupling = PenaltyInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
% OR: InterfaceCoupling = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);

% Register conditions at analysis
Analysis.addInterfaceCoupling( InterfaceCoupling, FirstMeshId, SecondMeshId);

%% Create new load case 
 LoadCase = LoadCase();
 Analysis.addLoadCases(LoadCase);

%% Apply weak Dirichlet boundary conditions (Left)

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint = v1;
EndPoint = v2;
NumberOfBoundarySegments = 3;
BoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
Boundary = BoundaryFactory.getBoundary();

% Selet constraining strategy
Beta = 1e8;
ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );

% Give analytical description of boundary values
ux = @(x,y,z) 0;
uy = @(x,y,z) 0;

% Create boundary condition in X
BoundaryConditionXMeshId = 1; 
ConstrainedDirections = [1 0];
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );                                

% Create boundary condition in Y
BoundaryConditionYMeshId = 1; 
ConstrainedDirections = [0 1];
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );  

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY, BoundaryConditionYMeshId );

%% Apply weak Dirichlet boundary conditions (Right)

% Select integration scheme
IntegrationScheme2 = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint2 = v3;
EndPoint2 = v4;
NumberOfBoundarySegments2 = 3;
BoundaryFactory2 = StraightLineBoundaryFactory( StartPoint2, EndPoint2, NumberOfBoundarySegments2);
Boundary2 = BoundaryFactory2.getBoundary();

% Selet constraining strastegy
Beta2 = 1e8;
ConstrainingAlgorithm2 = WeakNitscheDirichlet2DAlgorithm( Beta2 );

% Give analytical description of boundary values
% u = [0.0 0 0];
u = RotationMatrix*[0.10 0.0 0]';
ux2=@(x,y,z)u(1);
uy2=@(x,y,z)u(2);

% Create boundary condition in X
BoundaryConditionXMeshId2 = 2; 
ConstrainedDirections2 = [1 0];
BoundaryConditionX2 = WeakDirichletBoundaryCondition( ux2, ConstrainedDirections2, ...
    IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );                                

% Create boundary condition in Y
BoundaryConditionYMeshId2 = 2; 
ConstrainedDirections2 = [0 1];
BoundaryConditionY2 = WeakDirichletBoundaryCondition( uy2, ConstrainedDirections2, ...
    IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );                             
% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX2, BoundaryConditionXMeshId2 );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY2, BoundaryConditionYMeshId2 );

%% Run the analysis

Analysis.solve();

%% Post processing

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
DisplacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
StrainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
L2Error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                   Rectangle, indexOfPhysicalDomain );


%% Perform Visual Post Processing (FirstMesh)

PostProcessingGridSize = 0.05;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;

FeMesh = Analysis.getMesh();

% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh{1}, Rectangle, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( DisplacementNorm );
% PostProcessor.registerPointProcessor( VonMises );
% PostProcessor.registerPointProcessor( L2Error );

% Visualize results

PostProcessor.visualizeResults( FeMesh{1} );

%% Perform Visual Post Processing (SecondMesh)

% Decide on post processor type
PostProcessingFactory2 = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh{2}, Rectangle2, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor2 = PostProcessingFactory2.creatVisualPostProcessor( );

% Register result point processors
PostProcessor2.registerPointProcessor( DisplacementNorm );
% PostProcessor2.registerPointProcessor( VonMises );
% PostProcessor2.registerPointProcessor( L2Error );

% Visualize results

PostProcessor2.visualizeResults( FeMesh{2} );
