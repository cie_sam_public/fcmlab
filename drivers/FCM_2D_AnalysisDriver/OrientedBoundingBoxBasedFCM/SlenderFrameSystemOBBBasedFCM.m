%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%


%% 2D FCM - Driver file
%skript to do calculation with a set of embedded domains
tic;
%% Preliminarities
clear all;
close all;

%% set up of Embedded Domain
Rec1 = EmbeddedRotatedRectangle([0 2.25 0], [5 0.5], 0);
OBBs(1) = OrientedBoundingBox(getQuad([0 2.25 0], [5 0.5], 0));

Rec2 = EmbeddedRotatedRectangle([0 0.25 0], [10 0.5], 0);
OBBs(2) = OrientedBoundingBox(getQuad([0 0.25 0], [10 0.5], 0));

Rec3 = EmbeddedRotatedRectangle([3.593826238 1.304782798 0],[3.201562119 0.5], 2.465601123);
OBBs(3) = OrientedBoundingBox(getQuad([3.593826238 1.304782798 0],[3.201562119 0.5], 2.465601123));

Rec4 = EmbeddedRotatedRectangle([-3.593826238 1.304782798 0],[3.201562119 0.5], -2.465601123);
OBBs(4) = OrientedBoundingBox(getQuad([-3.593826238 1.304782798 0],[3.201562119 0.5], -2.465601123));

Rec5 = EmbeddedRotatedRectangle([1.093826238 1.304782798 0],[3.201562119 0.5], -2.465601123);
OBBs(5) = OrientedBoundingBox(getQuad([1.093826238 1.304782798 0],[3.201562119 0.5], -2.465601123));

Rec6 = EmbeddedRotatedRectangle([-1.093826238 1.304782798 0],[3.201562119 0.5], 2.465601123);
OBBs(6) = OrientedBoundingBox(getQuad([-1.093826238 1.304782798 0],[3.201562119 0.5], 2.465601123));

Hole(1) = Plate([-2.0 2.25 0],0.15);
Hole(2) = Plate([-1.0 2.25 0],0.15);
Hole(3) = Plate([0.0 2.25 0],0.15);
Hole(4) = Plate([1.0 2.25 0],0.15);
Hole(5) = Plate([2.0 2.25 0],0.15);

Hole(6) = Plate([-4.5 0.25 0],0.15);
Hole(7) = Plate([-3.5 0.25 0],0.15);
Hole(8) = Plate([-2.5 0.25 0],0.15);
Hole(9) = Plate([-1.5 0.25 0],0.15);
Hole(10) = Plate([-0.5 0.25 0],0.15);
Hole(11) = Plate([0.5 0.25 0],0.15);
Hole(12) = Plate([1.5 0.25 0],0.15);
Hole(13) = Plate([2.5 0.25 0],0.15);
Hole(14) = Plate([3.5 0.25 0],0.15);
Hole(15) = Plate([4.5 0.25 0],0.15);
%outside right
Hole(16) = Plate([3.593826238-0.625 1.304782798+0.5 0],0.15);
Hole(17) = Plate([3.593826238 1.304782798 0],0.15);
Hole(18) = Plate([3.593826238+0.625 1.304782798-0.5 0],0.15);
%outside left
Hole(19) = Plate([-3.593826238-0.625 1.304782798-0.5 0],0.15);
Hole(20) = Plate([-3.593826238 1.304782798 0],0.15);
Hole(21) = Plate([-3.593826238+0.625 1.304782798+0.5 0],0.15);
%middle right
Hole(22) = Plate([1.093826238+0.625 1.304782798+0.5 0],0.15);
Hole(23) = Plate([1.093826238 1.304782798 0],0.15);
Hole(24) = Plate([1.093826238-0.625 1.304782798-0.5 0],0.15);
%middle left
Hole(25) = Plate([-1.093826238-0.625 1.304782798+0.5 0],0.15);
Hole(26) = Plate([-1.093826238 1.304782798 0],0.15);
Hole(27) = Plate([-1.093826238+0.625 1.304782798-0.5 0],0.15);

% defining Embedded Domain
wholeEmbeddedDomain = AggregatedDomain([Rec1 Rec2 Rec3 Rec4 Rec5 Rec6],Hole);


%% Calculate Intersections
Intersections= [];
for n = 1:(length(OBBs)-1)
    for i = n+1:length(OBBs)
        if (OBBs(n).hasCollisionWith(OBBs(i)))
            Intersections = [Intersections; [n,i]];
        end
    end
end
% Calculate intersection surfaces
for n=1:length(Intersections(:,1))
    OBB2 = OBBs(Intersections(n,2));

    EmbeddedOBB = EmbeddedRotatedRectangle(OBB2.quadIndexGeometry);
    
    IntersectionTree = SpaceQuadTree(EmbeddedOBB, OBBs(Intersections(n,1)).quadIndexGeometry);
    IntersectionTree.partition(2,15)
    IntersectionTree.createOrientedBoundingBoxes(0)
    
%     VisualizeOBB(IntersectionTree)
    
    LocalOBB = IntersectionTree.getAllOBBs();
    
    IntersectionQuads(n) = LocalOBB(1).quadIndexGeometry;
end

%% Meshes
for n = 1:length(OBBs)
% Geometry Domain Setup
LocalOBB = OBBs(n);
Center = [LocalOBB.center; 0.0]';
Lengths = LocalOBB.lengths;
Origin = [-Lengths(1)/2 -Lengths(2)/2 0.0];

RotationMatrix = [[LocalOBB.vectors(1,1),LocalOBB.vectors(1,2), 0];...
    [LocalOBB.vectors(2,1),LocalOBB.vectors(2,2), 0];...
    [0, 0, 1]];


% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 2;
NumberOfGaussPoints = PolynomialDegree+1;
SpaceTreeDepth = 0;

ElementFactory = ElementFactoryElasticOrientedQuadFCM( Materials, wholeEmbeddedDomain, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-Lengths(1)/2 -Lengths(2)/2 0.0];
MeshLengthX = Lengths(1);
MeshLengthY = Lengths(2);
CenterOfMesh = Center;%[0.0 0.0 0.0];
NumberOfXDivisions = 20;
NumberOfYDivisions = 2;
DofDimension = 2;

MeshFactories{n} = MeshFactory2DOrientedUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory,...
    RotationMatrix, CenterOfMesh );
end

%% Analysis Setup

% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactories);

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

%  InterfaceGeometries
NumberOfBoundarySegments = 3;

for n = 1:length(Intersections(:,1))

     InterfaceGeometry = Line.empty(1,0);
    A = IntersectionQuads(n).getVerticesCoordinates;
    for m=1:4
        InterfaceBoundaryFactory = StraightLineBoundaryFactory( A(m,:), A(mod(m,4)+1,:), NumberOfBoundarySegments);
        InterfaceGeometry = [InterfaceGeometry InterfaceBoundaryFactory.getBoundary()];
    end

% Create Coupling condition
Beta = 1e8;
FirstMeshId = Intersections(n,1);
SecondMeshId = Intersections(n,2); 
InterfaceCoupling = PenaltyInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
% OR: InterfaceCoupling = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);

% Register conditions at analysis
Analysis.addInterfaceCoupling( InterfaceCoupling, FirstMeshId, SecondMeshId);
end

%% First Boundary Set Up(left and right)

%% Apply weak Dirichlet boundary conditions

% set up body forces
b=[0 0.0 0];
bx=@(x,y,z)b(1);
by=@(x,y,z)b(2);

% Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];

% Create new load case 
LoadCase = LoadCase();
LoadCase.addBodyLoad( BodyForce );

% Register load case at analysis
Analysis.addLoadCases( LoadCase );


% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
NumberOfBoundarySegments1 = 5;
factory = StraightLineBoundaryFactory( [-5 0 0], [-4.5 0 0], NumberOfBoundarySegments1 );
factory22 = StraightLineBoundaryFactory( [4.5 0 0], [5 0 0], NumberOfBoundarySegments1 );
Boundary = [factory.getBoundary factory22.getBoundary];

% Selet constraining strategy
Beta = 1e8;
ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );

% Give analytical description of boundary values
u = [0 0.0 0];
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);

% Create boundary condition in X
BoundaryConditionXMeshId = 2; 
ConstrainedDirections = [1 1];
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );                                

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );

%% Second Boundary Set Up (top)
%% Apply weak Neumann boundary conditions
% Select integration scheme
IntegrationScheme2 = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint2 = [-2.5 2.5 0];
EndPoint2 = [2.5 2.5 0];
NumberOfBoundarySegments2 = 20;
factory2 = StraightLineBoundaryFactory( StartPoint2, EndPoint2, NumberOfBoundarySegments2 );
Boundary2 = [factory2.getBoundary];


% Give analytical description of boundary values
u2 = [0 0.0 0];
ux2 =@(x,y,z)u2(1);
uy2 =@(x,y,z)u2(2);


% create load vector
LoadFunction = @(x,y,z)([0 ; -0.01]);


% % Create boundary condition in X
BoundaryConditionMeshId2 = 1; 
BoundaryCondition2 = WeakNeumannBoundaryCondition( LoadFunction, ...
    IntegrationScheme2, Boundary2 );                                 
% Register Analysis Conditions
LoadCase.addNeumannBoundaryCondition( BoundaryCondition2, BoundaryConditionMeshId2 );

Analysis.addLoadCases( LoadCase );

%% Run the analysis 
Analysis.solve()

%% Post processing

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
DisplacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
StrainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
L2Error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                   wholeEmbeddedDomain, indexOfPhysicalDomain);


FeMesh = Analysis.getMesh();


%% Perform Visual Post Processing (Mesh-n)
for n = 1:length(FeMesh)

PostProcessingGridSize = 1;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;



% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh{n}, wholeEmbeddedDomain, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( DisplacementNorm );
% PostProcessor.registerPointProcessor( VonMises );
% PostProcessor.registerPointProcessor( L2Error );


% Visualize results
PostProcessor.visualizeResults( FeMesh{n} );
xlabel('')
ylabel('')
ax(n) = gca;
end

%% Visualize every results in one figure
figure;
axall = gca;

for n = 1:length(FeMesh)
copyobj(allchild(ax(n)),axall); 
end

xlabel('x')
ylabel('y')