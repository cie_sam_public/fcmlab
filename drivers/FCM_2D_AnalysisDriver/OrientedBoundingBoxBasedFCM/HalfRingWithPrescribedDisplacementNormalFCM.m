%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Half ring with Prescribed Displacement


close all;
clear all;

%% Embedded Domain Setup
Center = [ 0.00 0.0 0.0 ];
OuterRadius = 1.0;
InnerRadius = 0.8;
Rectangle = AnnularPlate(Center,OuterRadius,InnerRadius);

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 5;
NumberOfGaussPoints = PolynomialDegree+2;
SpaceTreeDepth = 4;

ElementFactory = ElementFactoryElasticQuadFCM( Materials, Rectangle, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-1 0.0 0.0];
MeshLengthX = 2;
MeshLengthY = 1;
NumberOfXDivisions = 2;
NumberOfYDivisions = 1;
DofDimension = 2;

MeshFactory = MeshFactory2DUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory);

%% Analysis Setup 
% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactory );

%% Apply Body Load
% % First Boundary Condition
% Analytical description of body force
% b = RotationMatrix*[0.0 -0.10 0.0]';
% b = [0.5 0 0]
b=[0 0.0 0];
bx=@(x,y,z)b(1);
by=@(x,y,z)b(2);
% u = RotationMatrix*[0.0 0.0 0.0]'
u = [0 0.0 0];
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);
  % Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];

% Create new load case 
LoadCase = LoadCase();
LoadCase.addBodyLoad( BodyForce );

% Register load case at analysis
Analysis.addLoadCases( LoadCase );

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
NumberOfBoundarySegments = 3;
factory = StraightLineBoundaryFactory( [-0.999 0 0], [-0.9 0 0], NumberOfBoundarySegments );
Boundary = [factory.getBoundary];

% Selet constraining strategy
Beta = 100000000;
ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );

% Create boundary condition in X
% DirectionX = RotationMatrix*[1 0 0]'
DirectionX = [1 0];
ConstrainedDirections = DirectionX;
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% % Create boundary condition in Y
% % DirectionY = RotationMatrix*[0 1 0]'
DirectionY = [0 1];
ConstrainedDirections = DirectionY;
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY );

% % Second Boundary Condition
u = [0.2 0.0 0];
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);

factory2 = StraightLineBoundaryFactory( [0.8 0 0], [0.999 0 0], NumberOfBoundarySegments );
Boundary = [factory2.getBoundary];
DirectionX = [1 0];
ConstrainedDirections = DirectionX;
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );
DirectionY = [0 1];
ConstrainedDirections = DirectionY;
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );
% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY );

%% Run the analysis
Analysis.solve();

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
displacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
strainenergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
l2error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                   Rectangle, indexOfPhysicalDomain );

%% Perform Visual Post Processing
PostProcessingGridSize = 0.05;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;

FeMesh = Analysis.getMesh();

% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh, Rectangle, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( displacementNorm );
PostProcessor.registerPointProcessor( VonMises );
PostProcessor.registerPointProcessor( l2error );

% Visualize results
PostProcessor.visualizeResults( FeMesh );
