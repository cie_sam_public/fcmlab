%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Half ring with prescribed displacement.

%% Preliminarities
clear all;
close all;

%% Construction of OBBs
% Embedded Domain Setup
Rec = AnnularPlate(0, 0.999, 0.8);

% Geometry Setup
realGeometry = Rectangle([...
Vertex([-1.0,-0.0,0.0]),...
Vertex([ 1.0,-0.0,0.0]),...
Vertex([ 1.0, 2.0,0.0]),...
Vertex([-1.0, 2.0,0.0])],[]);

% Partition Space Tree
quadTree = SpaceQuadTree(Rec, realGeometry);
quadTree.partition(4,7)
quadTree.createOrientedBoundingBoxes(1)
OBBs = quadTree.getAllOBBs();

%% Calculate Intersections
Intersections= [];
for n = 1:(length(OBBs)-1)
    for i = n+1:length(OBBs)
        if (OBBs(n).hasCollisionWith(OBBs(i)))
            Intersections = [Intersections; [n,i]];
        end
    end
end
%     IntersectionQuads = Quad.empty(1,1);
for n=1:length(Intersections(:,1))
    OBB2 = OBBs(Intersections(n,2));

    EmbeddedOBB = EmbeddedRotatedRectangle(OBB2.quadIndexGeometry);

    IntersectionTree = SpaceQuadTree(EmbeddedOBB, OBBs(Intersections(n,1)).quadIndexGeometry);
    IntersectionTree.partition(4,3)
    IntersectionTree.createOrientedBoundingBoxes(0)
    
    IntersectionQuads(n) = IntersectionTree.getAllOBBs().quadIndexGeometry;
end

%% Mesh
for n = 1:length(OBBs)
%% Embedded Domain Setup
LocalOBB = OBBs(n);
Center = [LocalOBB.center; 0.0]';
Lengths = LocalOBB.lengths;

LocalOBB.vectors

RotationMatrix = [[LocalOBB.vectors(1,1),LocalOBB.vectors(1,2), 0];...
    [LocalOBB.vectors(2,1),LocalOBB.vectors(2,2), 0];...
    [0, 0, 1]];
% Create the domain
Rectangle = Rec;

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 5;
NumberOfGaussPoints = PolynomialDegree+2;
SpaceTreeDepth = 4;

ElementFactory = ElementFactoryElasticOrientedQuadFCM( Materials, Rectangle, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-Lengths(1)/2 -Lengths(2)/2 0.0];
MeshLengthX = Lengths(1);
MeshLengthY = Lengths(2);
CenterOfMesh = Center;%[0.0 0.0 0.0];
NumberOfXDivisions = 1;
NumberOfYDivisions = 2;
DofDimension = 2;

MeshFactories{n} = MeshFactory2DOrientedUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory,...
    RotationMatrix, CenterOfMesh );
end

%% Analysis Setup

% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactories);

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary

NumberOfBoundarySegments = 3;
for n = 1:length(Intersections(:,1))
    InterfaceGeometry = Line.empty(4,0);
    A = IntersectionQuads(n).getVerticesCoordinates;
    for m=1:4
        InterfaceBoundaryFactory = StraightLineBoundaryFactory( A(m,:), A(mod(m,4)+1,:), NumberOfBoundarySegments);
        InterfaceGeometry = [InterfaceGeometry InterfaceBoundaryFactory.getBoundary()];
    end
%  InterfaceGeometry

% Create Coupling condition
Beta = 1e8;
FirstMeshId = Intersections(n,1);
SecondMeshId = Intersections(n,2); 
InterfaceCoupling = PenaltyInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);
% OR: InterfaceCoupling = NitscheInterfaceCoupling(IntegrationScheme, InterfaceGeometry, Beta);

% Register conditions at analysis
Analysis.addInterfaceCoupling( InterfaceCoupling, FirstMeshId, SecondMeshId);
end

% b = RotationMatrix*[0.0 -0.10 0.0]';
% b = [0.5 0 0]
b=[0 -0.0 0];
bx=@(x,y,z)b(1);
by=@(x,y,z)b(2);
% % u = RotationMatrix*[0.0 0.0 0.0]'
% u = [0 0 0];
% ux=@(x,y,z)u(1);
% uy=@(x,y,z)u(2);

% Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];
%% Create new load case 
 LoadCase = LoadCase();
 
 LoadCase.addBodyLoad(BodyForce,1)
 LoadCase.addBodyLoad(BodyForce,2)
 Analysis.addLoadCases(LoadCase);

%% Apply weak Dirichlet boundary conditions (Left)

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint = [-0.999   0         0];
EndPoint = [ -0.8   0         0];
NumberOfBoundarySegments = 3;
BoundaryFactory = StraightLineBoundaryFactory( StartPoint, EndPoint, NumberOfBoundarySegments);
Boundary = BoundaryFactory.getBoundary();

% Selet constraining strategy
Beta = 1e8;
ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );

% Give analytical description of boundary values
ux = @(x,y,z) 0;
uy = @(x,y,z) 0;

% Create boundary condition in X
BoundaryConditionXMeshId = 1; 
ConstrainedDirections = [1 1];
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );                                


% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX, BoundaryConditionXMeshId );

%% Apply boundary conditions (Right)

% Select integration scheme
IntegrationScheme2 = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint2 = [0.8 0.0 0.0];
EndPoint2 = [0.999 0.0 0.0];
NumberOfBoundarySegments2 = 3;
BoundaryFactory2 = StraightLineBoundaryFactory( StartPoint2, EndPoint2, NumberOfBoundarySegments2);
Boundary2 = BoundaryFactory2.getBoundary();

% Selet constraining strastegy
Beta2 = 1e8;
ConstrainingAlgorithm2 = WeakNitscheDirichlet2DAlgorithm( Beta2 );

% Give analytical description of boundary values
ux2 = @(x,y,z) 0.2;
uy2 = @(x,y,z) 0;

% Create boundary condition in X
BoundaryConditionXMeshId2 = 2; 
ConstrainedDirections2 = [1 0];
BoundaryConditionX2 = WeakDirichletBoundaryCondition( ux2, ConstrainedDirections2, ...
    IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );                                

% Create boundary condition in Y
BoundaryConditionYMeshId2 = 2; 
ConstrainedDirections2 = [0 1];
BoundaryConditionY2 = WeakDirichletBoundaryCondition( uy2, ConstrainedDirections2, ...
    IntegrationScheme2, Boundary2, ConstrainingAlgorithm2 );                             
% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX2, BoundaryConditionXMeshId2 );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY2, BoundaryConditionYMeshId2 );

%% Run the analysis

Analysis.solve()

%% Post processing

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
DisplacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
StrainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
L2Error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                   Rec, indexOfPhysicalDomain );

% Axis = [[-1.2 0.2 0 1.2];[-0.2 1.2 0 1.2]];

FeMesh = Analysis.getMesh();
for n = 1:length(FeMesh)
%% Perform Visual Post Processing (n th Mesh)

PostProcessingGridSize = 0.01;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;



% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh{n}, Rectangle, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( DisplacementNorm );
% PostProcessor.registerPointProcessor( VonMises );
% PostProcessor.registerPointProcessor( L2Error );

% Visualize results
PostProcessor.visualizeResults( FeMesh{n} );
% axis(Axis(n,:))
end
