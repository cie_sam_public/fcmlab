%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

%% 2D FCM - Driver file
%skript to do calculation with a set of embedded domains
tic
%% Preliminarities
clear all;
close all;
% Set path for Logger
% Logger.setPathForLogger('EmbeddedRectanglePlaneStress.m','log2DFCM.out',0);

% Logger.ConsoleLevel('debug');

%% set up of embedded domains
Rec1 = EmbeddedRotatedRectangle([0 2.25 0], [5 0.5], 0);

Rec2 = EmbeddedRotatedRectangle([0 0.25 0], [10 0.5], 0);

Rec3 = EmbeddedRotatedRectangle([3.593826238 1.304782798 0],[3.201562119 0.5], 2.465601123);

Rec4 = EmbeddedRotatedRectangle([-3.593826238 1.304782798 0],[3.201562119 0.5], -2.465601123);

Rec5 = EmbeddedRotatedRectangle([1.093826238 1.304782798 0],[3.201562119 0.5], -2.465601123);

Rec6 = EmbeddedRotatedRectangle([-1.093826238 1.304782798 0],[3.201562119 0.5], 2.465601123);

Hole(1) = Plate([-2.0 2.25 0],0.15);
Hole(2) = Plate([-1.0 2.25 0],0.15);
Hole(3) = Plate([0.0 2.25 0],0.15);
Hole(4) = Plate([1.0 2.25 0],0.15);
Hole(5) = Plate([2.0 2.25 0],0.15);

Hole(6) = Plate([-4.5 0.25 0],0.15);
Hole(7) = Plate([-3.5 0.25 0],0.15);
Hole(8) = Plate([-2.5 0.25 0],0.15);
Hole(9) = Plate([-1.5 0.25 0],0.15);
Hole(10) = Plate([-0.5 0.25 0],0.15);
Hole(11) = Plate([0.5 0.25 0],0.15);
Hole(12) = Plate([1.5 0.25 0],0.15);
Hole(13) = Plate([2.5 0.25 0],0.15);
Hole(14) = Plate([3.5 0.25 0],0.15);
Hole(15) = Plate([4.5 0.25 0],0.15);
%au�en rechts
Hole(16) = Plate([3.593826238-0.625 1.304782798+0.5 0],0.15);
Hole(17) = Plate([3.593826238 1.304782798 0],0.15);
Hole(18) = Plate([3.593826238+0.625 1.304782798-0.5 0],0.15);
%au�en links
Hole(19) = Plate([-3.593826238-0.625 1.304782798-0.5 0],0.15);
Hole(20) = Plate([-3.593826238 1.304782798 0],0.15);
Hole(21) = Plate([-3.593826238+0.625 1.304782798+0.5 0],0.15);
%mitte rechts
Hole(22) = Plate([1.093826238+0.625 1.304782798+0.5 0],0.15);
Hole(23) = Plate([1.093826238 1.304782798 0],0.15);
Hole(24) = Plate([1.093826238-0.625 1.304782798-0.5 0],0.15);
%mitte links
Hole(25) = Plate([-1.093826238-0.625 1.304782798+0.5 0],0.15);
Hole(26) = Plate([-1.093826238 1.304782798 0],0.15);
Hole(27) = Plate([-1.093826238+0.625 1.304782798-0.5 0],0.15);


wholeEmbeddedDomain = AggregatedDomain([Rec1 Rec2 Rec3 Rec4 Rec5 Rec6],Hole);

Theta =0;
RotationMatrix = [[cos(Theta) -sin(Theta) 0];[sin(Theta) cos(Theta) 0];[0 0 1]];


%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 3;
NumberOfGaussPoints = PolynomialDegree+2;
SpaceTreeDepth = 2;

ElementFactory = ElementFactoryElasticQuadFCM( Materials, wholeEmbeddedDomain, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-5 0.0 0.0];
MeshLengthX = 10;
MeshLengthY = 2.5;
CenterOfMesh = [0.0 0.0 0.0];
NumberOfXDivisions = 20;
NumberOfYDivisions = 5;
DofDimension = 2;

MeshFactory = MeshFactory2DUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory);

%% Analysis Setup 
% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactory );


%% First Boundary Set Up

%% Apply weak Dirichlet boundary conditions

% Analytical description of body force
b=[0 0.0 0];
bx=@(x,y,z)b(1);
by=@(x,y,z)b(2);

% u = RotationMatrix*[0.0 0.0 0.0]'
u = [0 0.0 0];
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);
% Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];

% Create new load case 
LoadCase = LoadCase();
LoadCase.addBodyLoad( BodyForce );

% Register load case at analysis
Analysis.addLoadCases( LoadCase );

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
NumberOfBoundarySegments = 3;
factory = StraightLineBoundaryFactory( [-5 0 0], [-4.5 0 0], NumberOfBoundarySegments );
factory2 = StraightLineBoundaryFactory( [5 0 0], [4.5 0 0], NumberOfBoundarySegments );
Boundary = [factory.getBoundary factory2.getBoundary];

% Selet constraining strategy
Beta = 100000000;
ConstrainingAlgorithm = WeakNitscheDirichlet2DAlgorithm( Beta );

% Create boundary condition in X
DirectionX = [1 0];
ConstrainedDirections = DirectionX;
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% % Create boundary condition in Y
DirectionY = [0 1];
ConstrainedDirections = DirectionY;
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY );


%% Second Boundary Set Up
%% Apply weak Neumann boundary conditions (top)
% Select integration scheme
IntegrationScheme2 = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
StartPoint2 = [-2.5 2.5 0];
EndPoint2 = [2.5 2.5 0];
NumberOfBoundarySegments2 = 20;
factory2 = StraightLineBoundaryFactory( StartPoint2, EndPoint2, NumberOfBoundarySegments2 );
Boundary2 = [factory2.getBoundary];


% Give analytical description of boundary values
u2 = [0 0.0 0];
ux2 =@(x,y,z)u2(1);
uy2 =@(x,y,z)u2(2);

% create load function
LoadFunction = @(x,y,z)([0 ; -0.01]);


% Create boundary condition
BoundaryConditionMeshId2 = 1; 
BoundaryCondition2 = WeakNeumannBoundaryCondition( LoadFunction, ...
    IntegrationScheme2, Boundary2 );  
                              
% Register Analysis Conditions
LoadCase.addNeumannBoundaryCondition( BoundaryCondition2, BoundaryConditionMeshId2 );

% Register conditions at analysis
Analysis.addLoadCases( LoadCase );

%% Run the analysis
Analysis.solve();

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
displacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
strainenergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
l2error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                   wholeEmbeddedDomain, indexOfPhysicalDomain );

%% Perform Visual Post Processing
PostProcessingGridSize = 0.5;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;

FeMesh = Analysis.getMesh();

% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh, wholeEmbeddedDomain, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( displacementNorm );
% PostProcessor.registerPointProcessor( VonMises );
% PostProcessor.registerPointProcessor( l2error );

% Visualize results
PostProcessor.visualizeResults( FeMesh );
