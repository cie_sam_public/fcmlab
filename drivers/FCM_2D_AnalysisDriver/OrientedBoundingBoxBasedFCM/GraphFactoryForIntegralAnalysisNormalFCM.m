%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Calculation of integration error.

%% 2D FCM - Driver file

%% Preliminarities
clear all;
close all;

%% Input parameters
% Numerical parameters
MeshOrigin = [-1.0 -1.0 0.0];
MeshLengthX = 2;
MeshLengthY = 2;
NumberOfXDivisions = 2;
NumberOfYDivisions = 2;
PolynomialDegree = 1;
NumberOfGaussPoints = PolynomialDegree+1;

% Geometry Setup
realGeometry = Rectangle([...
Vertex([-1.0,-1.0,0.0]),...
Vertex([ 1.0,-1.0,0.0]),...
Vertex([ 1.0, 1.0,0.0]),...
Vertex([-1.0, 1.0,0.0])],[]);


% Mechanical parameters
YoungsModulus = 1.0;
PoissonsRatio = 0.3;
Density = 1.0;
% FCM Parameter
Alpha = 0;

% Boundary Mesh
numberOfSegments = 4;

% Plot Parameter
xPlot = linspace(0,(90),11);

%% Instanciation of the problem

% Creation of Materials
Materials(1) = HookePlaneStress( ...
    YoungsModulus, PoissonsRatio, ...
    Density, Alpha );
Materials(2) = HookePlaneStress( ...
    YoungsModulus, PoissonsRatio, ...
    Density, 1 );

Center = [ 0.0 0.0 0.0 ];
Lengths = [ 1.20 0.40 ];

area = Lengths(1)*Lengths(2);
integral = zeros(1,11);
calculationError = zeros(1,11);
Colors = ['b','r','g','m','y','c','b'];
for d = 1:6
    % FCM Parameter
    SpaceTreeDepth = d;
    for n = 1:11
        % Creation of Domain Geoemtry
        
        Theta = ((n-1)/20)*3.14;
        Rectangle = EmbeddedRotatedRectangle( Center, Lengths, Theta );
        
        BoundaryFactory = RectangleBoundaryFactory( Center, Lengths,  numberOfSegments );
        
        % Creation of the FEM system
        DofDimension = 2;
        
        ElementFactory = ElementFactoryElasticQuadFCM(Materials,Rectangle,...
            NumberOfGaussPoints,SpaceTreeDepth);
        
        MeshFactory = MeshFactory2DUniform(NumberOfXDivisions,...
            NumberOfYDivisions,PolynomialDegree,TopologicalSorting,...
            DofDimension,MeshOrigin,MeshLengthX,MeshLengthY,ElementFactory);
        
        %%
        indexOfPhysicalDomain = 2;
             
        %% Integration Post Processing
        
        postProcessor = IntegrationPostProcessor( );
        
        
        postProcessor.registerPointProcessor( IntegrationMass(1.0));
        
        FeMesh = Mesh(MeshFactory);
        
        numberOfDofs = FeMesh.getNumberOfDofs();
        
        soultionVector = ones(numberOfDofs,1);
        
        FeMesh.scatterSolution( soultionVector );
        integral(n) = postProcessor.integrate( FeMesh );
        calculationError(SpaceTreeDepth,n) = ((area-integral(n))/area)*100;
        
    end
    plot(xPlot,calculationError(SpaceTreeDepth,:),'color', Colors(SpaceTreeDepth))
%     disp(calculationError(SpaceTreeDepth,:))
    hold on;
end

xlabel('rotation in degree');
ylabel('number of points');
hold off;