%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

% Beam with predefined displacement. Can be rotated arbitrarily.

%% Preliminarities
clear all;
close all;


%% Embedded Domain Setup
Center = [ -0.00 -0.0 0.0 ];
Lengths = [ 1.0 0.2 ];
Origin = [-Lengths(1)/2 -Lengths(2)/2 0.0];

Theta =pi/3;
RotationMatrix = [[cos(Theta) -sin(Theta) 0];[sin(Theta) cos(Theta) 0];[0 0 1]];
% v2------v3
% |       |
% v1------v4
v1 = (RotationMatrix*[-Lengths(1)/2 -Lengths(2)/2 0.0]')'+Center;
v2 = (RotationMatrix*[-Lengths(1)/2 Lengths(2)/2 0.0]')'+Center;
v3 = (RotationMatrix*[Lengths(1)/2 Lengths(2)/2 0.0]')'+Center;
v4 = (RotationMatrix*[Lengths(1)/2 -Lengths(2)/2 0.0]')'+Center;
% Create the domain
Rectangle = EmbeddedRotatedRectangle( Center, Lengths, Theta);

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.0;
Density = 1.0;
Alpha = 1e-8;

% Material for the void (background) domain
Materials(1) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = HookePlaneStress( YoungsModulus, PoissonsRatio, ...
    Density, 1 );

% Step 1: decide on element type
PolynomialDegree = 5;
NumberOfGaussPoints = PolynomialDegree+3;
SpaceTreeDepth = 1;

ElementFactory = ElementFactoryElasticOrientedQuadFCM( Materials, Rectangle, ...
    NumberOfGaussPoints, SpaceTreeDepth );

% Step 2: create the mesh
MeshOrigin = [-Lengths(1)/2 -Lengths(2)/2 0.0];
MeshLengthX = Lengths(1);
MeshLengthY = Lengths(2);
CenterOfMesh = Center;%[0.0 0.0 0.0];
NumberOfXDivisions = 2;
NumberOfYDivisions = 1;
DofDimension = 2;

MeshFactory = MeshFactory2DOrientedUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY, ElementFactory,...
    RotationMatrix, CenterOfMesh );

%% Analysis Setup 
% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactory );

%% Apply Body Load
% % First Boundary Condition
% Analytical description of body force
b = RotationMatrix*[0.0 0.0 0.0]';
% b = [0.5 0 0]
% b=[0 0.5 0]
bx=@(x,y,z)b(1);
by=@(x,y,z)b(2);
% u = RotationMatrix*[0.0 0.0 0.0]'
u = [0.0 0 0];
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);
  % Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];

% Create new load case 
LoadCase = LoadCase();
LoadCase.addBodyLoad( BodyForce );

% Register load case at analysis
Analysis.addLoadCases( LoadCase );

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );

% Get geometrical description of boundary
NumberOfBoundarySegments = 3;
factory = StraightLineBoundaryFactory( v1,v2, NumberOfBoundarySegments );
Boundary = [factory.getBoundary];

% Selet constraining strategy
Beta = 100000000;
ConstrainingAlgorithm = WeakPenaltyAlgorithm( Beta );

% Give analytical description of boundary values
DirectionX = [1 1];
ConstrainedDirections = DirectionX;
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );


% % Second Boundary Condition
u = RotationMatrix*[0.20 0.0 0]';
ux=@(x,y,z)u(1);
uy=@(x,y,z)u(2);

factory2 = StraightLineBoundaryFactory( v4, v3, NumberOfBoundarySegments );
Boundary2 = [factory2.getBoundary];

DirectionX = [1 0];
ConstrainedDirections = DirectionX;
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary2, ConstrainingAlgorithm );
DirectionY = [0 1];
ConstrainedDirections = DirectionY;
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary2, ConstrainingAlgorithm );

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY );

%% Run the analysis
Analysis.solve();

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;

% Displacement
displacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
strainenergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ) ];
l2error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian, ...
                    Rectangle, indexOfPhysicalDomain );

%% Perform Visual Post Processing
PostProcessingGridSize = 0.05;
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;

FeMesh = Analysis.getMesh();

% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory2DFCMWarped( ...
    FeMesh, Rectangle, IndexOfPhysicalDomain, PostProcessingGridSize, ...
    SolutionNumbersToWarp, WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( displacementNorm );
PostProcessor.registerPointProcessor( VonMises );
PostProcessor.registerPointProcessor( l2error );

% Visualize results
PostProcessor.visualizeResults( FeMesh );
