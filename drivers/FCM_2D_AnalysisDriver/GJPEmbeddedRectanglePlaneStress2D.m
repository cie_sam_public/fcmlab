%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%This is a example driver for the Gradient Jump Penalisation. 


%% 2D FCM - Driver file

%% Preliminarities
clear
%close all;
% Set path for Logger
% Logger.setPathForLogger('EmbeddedRectanglePlaneStress.m','log2DFCM.out',0);

Logger.ConsoleLevel('debug');


myBasket = variableContainer();

%% Input parameters
% Numerical parameters
meshOrigin = [0.0 0.0 0.0];
meshLengthX = 1.7;
meshLengthY = 1.7;
numberOfXDivisions = 2;
numberOfYDivisions = 2;
polynomialDegree = 4;
numberOfGaussPoints = polynomialDegree+1;

% Mechanical parameters
youngsModulus = 1.0;
poissonsRatio = 0.3;
density = 1.0;



% Creation of Domain Geoemtry
center = [ 0.0 0.0 0.0 ];
lengths = [ 1 1 ];
myRectangle = EmbeddedRectangle( center, lengths );
protrusionX = mod(lengths(1),(meshLengthX/numberOfXDivisions));
protrusionY = mod(lengths(2),(meshLengthY/numberOfYDivisions));

% FCM Parameter
spaceTreeDepth = 1;
alpha = 0;
beta = 0;


% Body Force
bx  = @(x,y,z) ...
    - ( ( 1 - poissonsRatio ) * ( 35 * power( x, 4 ) * power( y, 6 ) + 6 * power( x, 2 ) * y ) * youngsModulus ) ...
    / ( 2 * ( 1 - poissonsRatio * poissonsRatio ) ) - ( 35 * poissonsRatio * power( x, 4 ) * power( y, 6 ) * youngsModulus ) / ( 1 - poissonsRatio * poissonsRatio ) ...
    - ( 2 * power( y, 3 ) * youngsModulus ) / ( 1 - poissonsRatio * poissonsRatio );
by = @(x,y,z) ...
    - ( ( 1 - poissonsRatio ) * ( 20 * power( x, 3 ) * power( y, 7 ) + 6 * x * power( y, 2 ) ) * youngsModulus ) ...
    / ( 2 * ( 1 - poissonsRatio * poissonsRatio ) ) - ( 42 * power( x, 5 ) * power( y, 5 ) * youngsModulus ) / ( 1 - poissonsRatio * poissonsRatio ) ...
    - ( 6 * poissonsRatio * x * power( y, 2 ) * youngsModulus ) / ( 1 - poissonsRatio * poissonsRatio ) ;


% bx  =
%    - ( ( 1 - v ) * ( 35 * x^4 *  y^6 + 6 * x^2 * y ) * E ) / ( 2 * ( 1 - v ^2 ) ) - ( 35 * v * x^4 * y^6 * E ) / ( 1 - v^2 ) - ( 2 * y^3 * E ) / ( 1 - v^2 );
% by =
%  - ( ( 1 - v ) * ( 20 * x^3 * y^7 + 6 * x * y^2 ) * E ) / ( 2 * ( 1 - v * v ) ) - ( 42 * x^5 * y^5 * E ) / ( 1 - v ^2 ) - ( 6 * v * x * y^2 * E ) / ( 1 - v^2 )

%% Instanciation of the problem

% Creation of Materials
materials(1) = HookePlaneStress( ...
    youngsModulus, poissonsRatio, ...
    density, alpha );
materials(2) = HookePlaneStress( ...
    youngsModulus, poissonsRatio, ...
    density, 1 );

%Boundary Conditions
u = @(x,y,z) power( x, 2 ) .* power( y, 3 );
v = @(x,y,z) power( x, 5 ) .* power( y, 7 );

%driDeve U
eXX = @(x,y,z) 2 * x * power(y,3);
eYY = @(x,y,z) power(x,5) * 7 * power(y,6);
eXY = @(x,y,z) ((power(x,2) * 3 * power(y,2)) + (5 * power(x,4) * power(y,7)));
strain = @(x,y,z) [eXX(x,y,z);eYY(x,y,z);eXY(x,y,z)];
stresses = @(x,y,z) materials(2).getMaterialMatrix * strain(x,y,z);
tractionX = @(x,y,z) [1 0 0; 0 0 1] * stresses(x,y,z);
tractionY = @(x,y,z) [0 0 1; 0 1 0] * stresses(x,y,z);



%Boundary
lineBoundaryFactoryRight = StraightLineBoundaryFactory([1 0 0],[1 1 0],1);
tempBoundary = lineBoundaryFactoryRight.getBoundary();
conformingBoundaryFactoryRight = ConformingMeshWrappedBoundaryFactory2D(tempBoundary,meshOrigin,...
    meshLengthX,meshLengthY,numberOfXDivisions,numberOfYDivisions);
boundaryRight = conformingBoundaryFactoryRight.getBoundary();

lineBoundaryFactoryTop  = StraightLineBoundaryFactory([1 1 0],[0 1 0],1);
tempBoundary = lineBoundaryFactoryTop.getBoundary();
conformingBoundaryFactoryTop = ConformingMeshWrappedBoundaryFactory2D(tempBoundary,meshOrigin,...
    meshLengthX,meshLengthY,numberOfXDivisions,numberOfYDivisions);
boundaryTop = conformingBoundaryFactoryTop.getBoundary();


% Creation of the FEM system
DofDimension = 2;


%My Partitioner
%quadPartitioner = QuadTree(myRectangle,spaceTreeDepth);
myNewPartitioner = exactRectanglePartitioner(myRectangle,protrusionX,protrusionY);

elementFactory = ElementFactoryElasticQuadFCMArbitraryPart(materials,myRectangle,myNewPartitioner,...
    numberOfGaussPoints);

meshFactory = MeshFactory2DUniform(numberOfXDivisions,...
    numberOfYDivisions,polynomialDegree,TopologicalSorting,...
    DofDimension,meshOrigin,meshLengthX,meshLengthY,elementFactory);
meshFactory.resetElementCounter();

% Create Analysis
analysis = QuasiStaticAnalysis( meshFactory );
analysis.setCondition(true);

% Loads
bodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z)];
loadCase = LoadCase();
loadCase.addBodyLoad( bodyForce );
analysis.addLoadCases( loadCase );

% Strong EdgeDirichletBC Left, Bottom
% WeakNeumannBC Top, Bottom

constrainingAlgorithm = ...
    StrongDirectConstrainingAlgorithm();
boundaryIntegrator = ...
    GaussLegendre( numberOfGaussPoints );

% Create innter boundary condition in X

fixedDirections = [1 1];
lineStart = meshOrigin;
lineEnd = [meshLengthX 0 0 ];
conditionX = ... 
    StrongEdgeDirichletBoundaryCondition( lineStart,lineEnd,...
    @(x,y,z) 0,fixedDirections,constrainingAlgorithm);

ConditionNeumannX = WeakNeumannBoundaryCondition(tractionX,boundaryIntegrator,boundaryRight);


% Create innter boundary condition in Y 

fixedDirections = [1 1];
lineStart = meshOrigin;
lineEnd = [0 meshLengthY 0 ];
conditionY = ...
    StrongEdgeDirichletBoundaryCondition(lineStart,lineEnd,...
    @(x,y,z) 0,fixedDirections,constrainingAlgorithm);

ConditionNeumannY = WeakNeumannBoundaryCondition(tractionY,boundaryIntegrator,boundaryTop);

%  Register condition at analysis
analysis.addDirichletBoundaryCondition( ...
    conditionX );
analysis.addDirichletBoundaryCondition( ...
    conditionY )

% %  Register condition at analysis
loadCase.addNeumannBoundaryCondition(ConditionNeumannX);
loadCase.addNeumannBoundaryCondition(ConditionNeumannY);


%Gradient Jump Penalisation  ==================================================

integrationScheme = GaussLegendre(numberOfGaussPoints);
SpaceDim = 2; 
domainIndex = 2;
resolution = [10 10];
ratio = 1;
penaltyValue = 10000;

GJP = GradientJumpPenalisation(integrationScheme, SpaceDim, domainIndex, resolution, ratio, penaltyValue);
analysis.addGradientJumpPenalisation(GJP);


% Run analysis
analysis.solve();

%% Post processing

analyticalSolutionCartesian = @(globalCoords) [u(globalCoords(1),globalCoords(2),globalCoords(3)); v(globalCoords(1),globalCoords(2),globalCoords(3))];
errorComponent = 1;

%% Visual Post Processing

% GradientJumpPenalisation
feMesh = analysis.getMesh();

GJPVisualizer = GradientJumpPenalisationVisualizer(feMesh, GJP);
GJPVisualizer.visualizeMySetup('Mesh','Seedpoints');

% create the point processors
indexOfPhysicalDomain = 2;
loadCaseToVisualize = 1;

displacementNorm = DisplacementNorm( ...
    loadCaseToVisualize );
vonMises = VonMisesStress( ...
    loadCaseToVisualize, ...
    indexOfPhysicalDomain );
strainEnergy = StrainEnergy( ...
    loadCaseToVisualize, ...
    indexOfPhysicalDomain );
solutionError = SolutionError( ...
    loadCaseToVisualize, ...
    analyticalSolutionCartesian, ...
    errorComponent );
% l2Error = L2Error( ...
%     loadCaseToVisualize, ...
%     analyticalSolutionCartesian, Rectangle, indexOfPhysicalDomain );

% Surface Plots

gridSize = 0.05;
postProcessingFactory = ...
    VisualPostProcessingFactory2DFCMWarped( ...
    feMesh,	myRectangle, ...
    indexOfPhysicalDomain, gridSize, 1, 0 );
% postProcessingFactory = ...
%     VisualPostProcessingFactory2DFCM( ...
%     FeMesh,	Rectangle, ...
%     indexOfPhysicalDomain, gridSize, ...
%     {BoundaryFactory} );
postProcessor = ...
    postProcessingFactory.creatVisualPostProcessor( );

% postProcessor.registerPointProcessor( ...
%     displacementNorm );
postProcessor.registerPointProcessor( ...
    vonMises );
postProcessor.registerPointProcessor( ...
    solutionError );

postProcessor.visualizeResults( feMesh );


%% Integration Post Processing

postProcessor = IntegrationPostProcessor( );

postProcessor.registerPointProcessor( strainEnergy );
% postProcessor.registerPointProcessor( l2Error );

integrals = postProcessor.integrate( feMesh );

strainEnergy = ( ( 117403 * poissonsRatio - 1378393 ) * youngsModulus ) / ( 390 * ( 6930 * poissonsRatio * poissonsRatio - 6930 ) );

error = 1 - integrals/strainEnergy;

fprintf('Relative Error: %e %%\n', 100*error);

