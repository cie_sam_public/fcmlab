%% 2D IGA - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
Logger.setPathForLogger('Iga2DPlateWithAHole.m','Iga2DPlateWithAHole.out',1);

%% Input parameters
% Numerical parameters


 PolynomialDegreeXi = 1;
 KnotsXi =    buildOpenEquidistantKnotVector(1, 1, PolynomialDegreeXi, 1);

 PolynomialDegreeEta = 1;
 KnotsEta =    buildOpenEquidistantKnotVector(1, 0, PolynomialDegreeEta, 1);
 
 
ControlPoints = [ ...  
  [-1 1 0]; ...
  [-1 -1 0]; ...
  [1 -1 0]; ...
  [0 1 0]; ...
  [0 0 0]; ...
  [1 0 0]; ...
];

geometryDescription = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints );

%% refinement
NumberOfXDivisions=6;
NumberOfYDivisions=6;
 PolynomialDegree = 3;


IncreasePolynomialDegreeXi = PolynomialDegree-PolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-PolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 0, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertXi2 = linspace(0, 1, NumberOfXDivisions+1);
knotsToInsertXi = [ knotsToInsertXi knotsToInsertXi2(2:length(knotsToInsertXi2)-1) ];

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);



 geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
 geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
 

% ControlPoints = reshape(rCP, size(rCP,1) * size(rCP,2) ,3);

NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points

% Mechanical parameters
E = 1000;
PoissonsRatio = 0.0;
Density = 1.0;

Force = 100.;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
 DofDimension = 2;
% MyElementFactory = ElementFactoryElasticQuad(Material,NumberOfGaussPoints);
% MyMeshFactory = MeshFactory2DUniform(NumberOfXDivisions,...
%     NumberOfYDivisions,PolynomialDegree,PolynomialDegreeSorting(),...
%     DofDimension,MeshOrigin,Lx,Ly,MyElementFactory);

    %KnotsXi =    buildOpenEquidistantKnotVector(1, NumberOfXDivisions-1, PolynomialDegree, 1);
    %PolynomialDegreeXi = PolynomialDegree;
    %KnotsEta =    buildOpenEquidistantKnotVector(1, NumberOfYDivisions-1, PolynomialDegree, 1);
    %PolynomialDegreeEta = PolynomialDegree;
    
    MyElementFactory = ElementFactoryElasticExtractedBSplineSurface(Material,NumberOfGaussPoints, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta); 
    
%     MyMeshFactory = MeshFactory2DFromKnotVector(KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta ,...
%                         [],DofDimension,MyElementFactory);

%KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints, DofDimension, ElementFactory

geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );
MyMeshFactory = MeshFactoryIGA2D(geometryDescription, DofDimension, MyElementFactory, geometryFactory);
                     
% MyMeshFactory.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
% MyMeshFactory.insertKnots( knotsToInsertXi,  knotsToInsertEta );
                     


MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
LoadCase = LoadCase();

leftControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,1), -1) );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)[-Force;0],leftControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

bottomControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), -1) );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)[0;-Force],bottomControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

internalEdgesFilter = @(ControlPoints)( ( isClose(ControlPoints(:,1), 0) | isClose(ControlPoints(:,2), 0) ) & (ControlPoints(:,1)>=0 & ControlPoints(:,2)>=0) );
SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,[1 1],StrongDirectConstrainingAlgorithm, internalEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 1 0.25] ;
warpScalingFactor = 0;

postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor);
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );

% w = F*l^3/(3*E*I)
% I = b*h^3/12

%I = 1*Ly^3/12;

%w = Force*Lx^3/(3*E*I);
