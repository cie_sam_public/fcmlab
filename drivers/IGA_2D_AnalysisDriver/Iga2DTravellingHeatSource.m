%% 2D IGA - Driver file

%% Preliminarities
clear all;
clc;

% Set path for Logger
% Logger.setPathForLogger('Iga2DTravellingHeatSource.m','Iga2DTravellingHeatSource.out',1);

%% Input parameters

% Material parameters
rho = 7500.0;                                               % density [kg/m^3]
c = 600.0;                                                  % specific heat [J/(kg°C)]
T0 = 20.0;                                                  % Initial temperature [°C]
entalphyJump = 261e+03;                                     % entalphy [J/Kg]
Tmelt = 1450.0;                                             % melting temperature [°C]

sizeScale = 0.001;                                          % [mm]
tBegin = 0.0;
tEnd = 1.2;                                                 
timeState = TimeState(tBegin);

xEnd = 8.0*sizeScale;                                               
xStart = 3.0*sizeScale;
yEnd = 5.0*sizeScale;                                               
yStart = 5.0*sizeScale;

% Numerical parameters
dimensionality = 1;                                         % DOF dimension of the problem
scalingFactor = 1.0;
numberOfTimeSteps = 10;
plateLength = 10.0*sizeScale;
origin = [0.0, 0.0];

knotsXi = [-1,-1,-1,1,1,1];
polynomialDegreeXi = 2;

knotsEta = [-1,-1,-1,1,1,1];
polynomialDegreeEta = 2;

controlPoints = [ ...
    [origin(1) origin(2) 0 1]; ...
    [origin(1) plateLength/2 0 1]; ...
    [origin(1) plateLength 0 1]; ...
    [plateLength/2 origin(2) 0 1]; ...
    [plateLength/2 plateLength/2 0 1]; ...
    [plateLength/2 plateLength 0 1]; ...
    [plateLength origin(2) 0 1]; ...
    [plateLength plateLength/2 0 1]; ...
    [plateLength plateLength 0 1]; ...
    ];


plateGeometry = BSplineSurface( knotsXi, polynomialDegreeXi, knotsEta, polynomialDegreeEta, controlPoints );
plateGeometry.removeWeights();

% plotControlPoints(controlPoints);

%% Neumann BBox parameters
widthBBox = 0.02 * sizeScale;
lengthBBox = 0.02 * sizeScale;
pathX = linspace(xStart, xEnd, numberOfTimeSteps + 1);
pathY = linspace(yStart, yEnd, numberOfTimeSteps + 1);
pathZ = linspace(0.0, 0.0, numberOfTimeSteps + 1);
path = [pathX', pathY', pathZ'];

%% Define initial mesh
NumberOfXDivisions=5;
NumberOfYDivisions=5;
PolynomialDegree = 2;

IncreasePolynomialDegreeXi = PolynomialDegree-plateGeometry.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-plateGeometry.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

plateGeometry.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
plateGeometry.insertKnots( knotsToInsertXi,  knotsToInsertEta );

Levels = ones(200,200);
multiLevelGeometries{1} = plateGeometry;

% %Levels = ones(3,2);
% % Levels(5:36,5:36) = 2;
% % Levels(9:32,9:32) = 3;
% % Levels(14:29,14:29) = 4;
% 
% %Levels(13:20,21:24) = 3;
% %Levels = ones(4,4);
% numberOfLevels = max(max(Levels));
% 
% multiLevelGeometries{1} = plateGeometry;
% for iLevel = 2:numberOfLevels
%     multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
% end
% 
multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, Levels, Levels);
ControlPoints = multiLevelGeometry.getControlPoints;

numberOfGaussPoints = PolynomialDegree+1; % number of Gauss points

%% Instanciation of the problem
material = PhaseChangeThermal(rho, c, ...
                 entalphyJump, Tmelt, timeState,...
                 dimensionality, scalingFactor);

% Creation of the FEM system
DofDimension = dimensionality;           %Thermal Problem

[MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTreeLocal( multiLevelGeometry );

geometryFactory = ExtractedTHBSurfaceFactory( multiLevelGeometry, MultiLevelBezierExtractionOperator );
elementFactory = ElementFactoryPhaseChangeExtractedTHBSurface(material,numberOfGaussPoints, multiLevelGeometry, MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator );
initialMeshFactory = MeshFactoryAdaptiveIGA2D(multiLevelGeometry, DofDimension, elementFactory, geometryFactory, origin, plateLength, plateLength);

%% Refinement startegy
refinementDepth = 2;

hRefinementStrategy = RefineTowardPath(initialMeshFactory, path, timeState, refinementDepth);
refinementStrategy = Refinement(hRefinementStrategy);

%% Post Processor
indexOfPhysicalDomain = 1;

% Surface Plots
gridSizes = [ 0.01 0.01 ] ;
warpScalingFactor = 0;

postProcessingFactory = VisualPostProcessingFactoryIGA2DForVTK( initialMeshFactory.createMesh(), gridSizes, timeState, 'IgATransient' );
% % postProcessingFactory = VisualPostProcessingFactoryIGA2DTransientWarped( initialMeshFactory.createMesh(), gridSizes, timeState, warpScalingFactor);


%% Analysis
MyAnalysis = BackwardEulerAnalysis(initialMeshFactory, material, refinementStrategy,...
    refinementDepth, plateGeometry, postProcessingFactory, numberOfTimeSteps,...
    tEnd, T0, timeState, indexOfPhysicalDomain, DofDimension, numberOfGaussPoints,...
    origin, plateLength, plateLength);

% Loads
% model the laser beam with a double elliptical model from 
% N. T. Nguyen, A. Ohta, K. Matsuoka, N. Suzuki, and Y. Maeda, 
%“Analytical solutions for transient temperature of semi-infinite body 
% subjected to 3-D moving heat sources,” WELDING JOURNAL-NEW YORK-, 
% vol. 78, pp. 265–s, 1999. 

LoadCase = LoadCase();
Q = 508300;                                         % Source Power
a = 0.010 * sizeScale;                              % laser beam trasversal dimension
c = 0.015 * sizeScale;                              % laser beam longitudinal dimension
pathLength = sqrt((xEnd-xStart)^2 + (yEnd-yStart)^2);
v = pathLength / (tEnd-tBegin);

loadFunctor = @(x,y,z,t) (3*Q/(pi*a*c))*exp(-3*y^2/(a^2)-3*(x)^2/(c^2));

% loadFunctor = @(x,y,z,t) Q;

outerControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), 1)  );
%outerControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), 0) );
% Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)([0; 0.5*(1+y)*y] * Force ),outerControlPointsFilter);
Load = MovingSurfaceNeumannBoundaryCondition(GaussLegendre(numberOfGaussPoints+1), path,...
    loadFunctor, widthBBox, lengthBBox, timeState);
LoadCase.addNeumannBoundaryCondition(Load);

MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

leftEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,1), -1) );
SupportBottomEdge = StrongControlPointsDirichletBoundaryCondition(0.0,1,StrongDirectConstrainingAlgorithm, leftEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportBottomEdge);

rightEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,1), 1) );
SupportBottomEdge = StrongControlPointsDirichletBoundaryCondition(0.0,1,StrongDirectConstrainingAlgorithm, rightEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportBottomEdge);

topEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,2), 1) );
SupportBottomEdge = StrongControlPointsDirichletBoundaryCondition(0.0,1,StrongDirectConstrainingAlgorithm, topEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportBottomEdge);

bottomEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,2), -1) );
SupportBottomEdge = StrongControlPointsDirichletBoundaryCondition(0.0,1,StrongDirectConstrainingAlgorithm, bottomEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportBottomEdge);


MyAnalysis.solve;
