%% 2D IGA - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
%Logger.setPathForLogger('Iga2DPlateWithAHole.m','Iga2DPlateWithAHole.out',1);

%% Input parameters
% Numerical parameters


InnerRadius = 1;
OuterRadius = 2.5;
geometryDescription = GenerateQuarterDisk(InnerRadius, OuterRadius);

geometryDescription.removeWeights();

%% refinement
NumberOfXDivisions=6;
NumberOfYDivisions=6;
PolynomialDegree = 3;
 
IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );

%ControlPoints = geometryDescription.getControlPoints;

%Levels = ones(3,2);
 Levels = ones(24,24);
Levels(5:24,5:24) = 2;
Levels(13:16,13:20) = 3;
Levels(13:20,21:24) = 3;

%Levels(13:20,21:24) = 3;
%Levels = ones(4,4);
numberOfLevels = max(max(Levels));

multiLevelGeometries{1} = geometryDescription;
for iLevel = 2:numberOfLevels
    multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
end

multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, 1, Levels);
ControlPoints = multiLevelGeometry.getControlPoints;

    [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTreeLocal( multiLevelGeometry, true(1) );
               
 NumberOfGaussPoints = PolynomialDegree+2; % number of Gauss points

% Mechanical parameters
E = 1000;
PoissonsRatio = 0.0;
Density = 1.0;

Force = 10;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
 DofDimension = 2;
 
geometryFactory = ExtractedTHBSurfaceFactory( multiLevelGeometry, MultiLevelBezierExtractionOperator, BezierExtractionOperator );
MyElementFactory = ElementFactoryElasticExtractedTHBSurface(Material,NumberOfGaussPoints, multiLevelGeometry ); 
MyMeshFactory = MeshFactoryAdaptiveIGA2D(multiLevelGeometry, DofDimension, MyElementFactory, geometryFactory);

MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
 LoadCase = LoadCase();

outerControlPointsFilter=@(ControlPoints)(  ControlPoints(:,1).^2 + ControlPoints(:,2).^2 >= OuterRadius^2 -1e-9 );
%outerControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), 0) );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)([-x/OuterRadius; -y/OuterRadius]*Force ),outerControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

% 
 MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

leftEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,1), 0) );
SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,[1 1],StrongDirectConstrainingAlgorithm, leftEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 0.25 0.25] ;
warpScalingFactor = 1;

postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor);
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );
