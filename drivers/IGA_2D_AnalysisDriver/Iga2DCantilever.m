%% 2D IGA - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
%Logger.setPathForLogger('Iga2DPlateWithAHole.m','Iga2DPlateWithAHole.out',1);

%% Input parameters
% Numerical parameters


 PolynomialDegreeXi = 1;
 KnotsXi =    buildOpenEquidistantKnotVector(1, 0, PolynomialDegreeXi, 1);

 PolynomialDegreeEta = 1;
 KnotsEta =    buildOpenEquidistantKnotVector(1, 0, PolynomialDegreeEta, 1);

ControlPoints = [ ...
  [0 0 0]; ...  
  [10 0 0]; ...
  [0 1 0]; ...
  [10 1 0]; ...
];

geometryDescription = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints );

%% refinement
NumberOfXDivisions=4;
NumberOfYDivisions=2;
PolynomialDegree = 5;


IncreasePolynomialDegreeXi = PolynomialDegree-PolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-PolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta ); 


NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points

% Mechanical parameters
E = 1000;
PoissonsRatio = 0.0;
Density = 1.0;

Force = .1;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
 DofDimension = 2;

geometryFactory = ExtractedBSplineSurfaceFactory( geometryDescription );
MyElementFactory = ElementFactoryElasticExtractedBSplineSurface(Material,NumberOfGaussPoints, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta); 
MyMeshFactory = MeshFactoryIGA2D(geometryDescription, DofDimension, MyElementFactory, geometryFactory);
                     
MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
LoadBezierControlPointsFilter = @(ControlPoints)(   isClose( ControlPoints(:,1), 10 )  );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)[0.0;-Force], LoadBezierControlPointsFilter);

LoadCase = LoadCase();
LoadCase.addNeumannBoundaryCondition(Load);

MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC
DisplacementControlPointsFilter = @(ControlPoints)( isClose( ControlPoints(:,1), 0 ) );
SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,[1 1],StrongDirectConstrainingAlgorithm, DisplacementControlPointsFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 1 0.25] ;
warpScalingFactor = 1;

postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor, { LoadBezierControlPointsFilter }, { DisplacementControlPointsFilter} );
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );
