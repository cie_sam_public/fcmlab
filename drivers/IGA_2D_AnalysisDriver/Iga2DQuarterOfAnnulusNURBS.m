%% 2D IGA - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
%Logger.setPathForLogger('Iga2DPlateWithAHole.m','Iga2DPlateWithAHole.out',1);

%% Input parameters
% Numerical parameters


InnerRadius = 1;
OuterRadius = 2.5;
geometryDescription = GenerateQuarterDisk(InnerRadius, OuterRadius);

%% refinement
NumberOfXDivisions=1;
NumberOfYDivisions=1;
PolynomialDegree = 12;
 
IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;

knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);

knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);

geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );

ControlPoints = geometryDescription.getControlPoints;
geometryFactory = ExtractedNURBSSurfaceFactory( geometryDescription );

 NumberOfGaussPoints = PolynomialDegree+2; % number of Gauss points

% Mechanical parameters
E = 1000;
PoissonsRatio = 0.0;
Density = 1.0;

Force = 1;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
 DofDimension = 2;
    
MyElementFactory = ElementFactoryElasticExtractedNURBSurface(Material,NumberOfGaussPoints, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta, ControlPoints(:,4)); 
MyMeshFactory = MeshFactoryIGA2D(geometryDescription, DofDimension, MyElementFactory, geometryFactory);

MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
 LoadCase = LoadCase();

outerControlPointsFilter=@(ControlPoints)( ControlPoints(:,1).^2 + ControlPoints(:,2).^2 >= OuterRadius^2 -1e-9 );
%outerControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), 0) );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)([-x/OuterRadius; -y/OuterRadius]*Force ),outerControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

% 
 MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

leftEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,1), 0) );
SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,[1 1],StrongDirectConstrainingAlgorithm, leftEdgesFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 0.1 0.1] ;
warpScalingFactor = 1;

postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor);
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );
