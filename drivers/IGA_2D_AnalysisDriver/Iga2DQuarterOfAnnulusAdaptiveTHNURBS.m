%% 2D IGA - Driver file

%% Preliminarities
clear all;
clc;

%% refinement
NumberOfXDivisions=2;
NumberOfYDivisions=2;
PolynomialDegree = 2;

maxCurrentLevel = 1;
Levels = ones(2*NumberOfXDivisions, NumberOfYDivisions);



ndofs=0;
energy=0;

for iRefinement = 1:10
    clearvars -except iRefinement ndofs energy NumberOfXDivisions NumberOfYDivisions PolynomialDegree  geometryDescription NumberOfGaussPoints OuterRadius InnerRadius maxCurrentLevel Levels errorH1Norm errorEnergyNorm
    
    %% Input parameters
    % Numerical parameters
    InnerRadius = 1;
    OuterRadius = 8;
    geometryDescription = GenerateQuarterDisk(InnerRadius, OuterRadius);
    
    
    
    IncreasePolynomialDegreeXi = PolynomialDegree-geometryDescription.getPolynomialDegreeXi;
    IncreasePolynomialDegreeEta  = PolynomialDegree-geometryDescription.getPolynomialDegreeEta;
    
    knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
    knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);
    
    knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
    knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);
    
    geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
    geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
    
    
    NumberOfGaussPoints = PolynomialDegree+2; % number of Gauss points
    
    numberOfLevels = maxCurrentLevel;
    
    multiLevelGeometries{1} = geometryDescription;
    for iLevel = 2:numberOfLevels
        multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
    end
    
    multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, 1, Levels);
    ControlPoints = multiLevelGeometry.getControlPoints;
    
    [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTreeLocal( multiLevelGeometry, true(1) );
    %[MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTree( multiLevelGeometry );
    
    
    % Mechanical parameters
    E = 1e5;
    PoissonsRatio = 0.3;
    Density = 1.0;
    
    UniaxialTensileStress = 10;
    
    analyticalStressR = @(r, theta) UniaxialTensileStress/2 * (1- InnerRadius^2/r^2 + (1-4*InnerRadius^2/r^2+3*InnerRadius^4/r^4) *cos(2*theta));
    analyticalStressTheta = @(r, theta) UniaxialTensileStress/2 * (1+ InnerRadius^2/r^2 - (1+3*InnerRadius^4/r^4) *cos(2*theta));
    analyticalStressRTheta = @(r, theta) UniaxialTensileStress/2 * (-1-2* InnerRadius^2/r^2 +3*InnerRadius^4/r^4 )*sin(2*theta);
    analyticalStressPolar = @(r, theta) [analyticalStressR(r,theta)      analyticalStressRTheta(r,theta); ...
        analyticalStressRTheta(r,theta) analyticalStressTheta(r,theta); ];
    
    %for plane stress
    nu = E/(2*(1+PoissonsRatio));
    k = (3-PoissonsRatio) / (1+PoissonsRatio);
    
    analyticalDisplacementCartesianX =  @(r, theta)  UniaxialTensileStress * InnerRadius / 8 / nu * ( r/InnerRadius*(k+1)*cos(theta) + 2*InnerRadius/r * ((1+k)*cos(theta)+cos(3*theta))-2*InnerRadius^3/r^3 * cos(3*theta) );
    analyticalDisplacementCartesianY =  @(r, theta)  UniaxialTensileStress * InnerRadius / 8 / nu * ( r/InnerRadius*(k-3)*sin(theta) + 2*InnerRadius/r * ((1-k)*sin(theta)+sin(3*theta))-2*InnerRadius^3/r^3 * sin(3*theta) );
    analyticalDisplacementCartesian =  @(r, theta)  [ analyticalDisplacementCartesianX(r, theta); analyticalDisplacementCartesianY(r, theta) ];
    analyticalDisplacementCartesianXY =  @(x,y)  analyticalDisplacementCartesian(sqrt(x^2+y^2), atan2(y,x));
    analyticalDisplacementCartesianXYCoord =  @(coord)  analyticalDisplacementCartesianXY(coord(1), coord(2));
    
    analyticalDisplacementPolarR  = @(r, theta) UniaxialTensileStress * r *  cos(2*theta)/4/ nu * (1 + (k + 1)*InnerRadius^2/r^2 - InnerRadius^4/r^4) +   UniaxialTensileStress * r /8/nu *((k - 1) + 2*InnerRadius^2/r^2);
    analyticalDisplacementPolarTheta = @(r, theta) - UniaxialTensileStress * r *   sin(2*theta)/4/   nu * (1 + (k - 1)*InnerRadius^2/r^2 + InnerRadius^4/r^4) ;
    analyticalDisplacementPolar = @(r, theta) [ analyticalDisplacementPolarR(r, theta); analyticalDisplacementPolarTheta(r, theta) ];
    analyticalDisplacementPolarToCartesian = @(x, y) polarToCartesian2DVector(x,y,analyticalDisplacementPolar);
    
    analyticalDisplacementCartesianXdX =  @(x,y) (UniaxialTensileStress*((1 + k)*(x^2 + y^2)*(-2*InnerRadius^2*(x^2 - y^2) + (x^2 + y^2)^2) - ...
        2*InnerRadius^2*x*sqrt(x^2 + y^2)*(-3*InnerRadius^2 + x^2 + y^2)*cos(3*atan2(y,x)) + ...
        6*InnerRadius^2*y*sqrt(x^2 + y^2)*(-InnerRadius^2 + x^2 + y^2)*sin(3*atan2(y,x))))/(8.*nu*(x^2 + y^2)^3);
    
    analyticalDisplacementCartesianXdY =  @(x,y) -(InnerRadius^2*UniaxialTensileStress*(y*sqrt(x^2 + y^2)*(-3*InnerRadius^2 + x^2 + y^2)*cos(3*atan2(y,x)) + ...
        x*(2*(1 + k)*y*(x^2 + y^2) + 3*sqrt(x^2 + y^2)*(-InnerRadius^2 + x^2 + y^2)*sin(3*atan2(y,x)))))/(4.*nu*(x^2 + y^2)^3);
    
    analyticalDisplacementCartesianYdX =  @(x,y)(InnerRadius^2*UniaxialTensileStress*(-3*y*(-InnerRadius^2 + x^2 + y^2)*cos(3*atan2(y,x)) - ...
        x*(-2*(-1 + k)*y*sqrt(x^2 + y^2) + (-3*InnerRadius^2 + x^2 + y^2)*sin(3*atan2(y,x)))))/(4.*nu*(x^2 + y^2)^2.5);
    
    analyticalDisplacementCartesianYdY =  @(x,y)(UniaxialTensileStress*(sqrt(x^2 + y^2)*(-2*InnerRadius^2*(-1 + k)*(x^2 - y^2) + (-3 + k)*(x^2 + y^2)^2) - ...
        6*InnerRadius^2*x*(InnerRadius^2 - x^2 - y^2)*cos(3*atan2(y,x)) + 2*InnerRadius^2*y*(3*InnerRadius^2 - x^2 - y^2)*sin(3*atan2(y,x))))/ ...
        (8.*nu*(x^2 + y^2)^2.5);
    
    analyticalDisplacementGradient =  @(x,y) [ analyticalDisplacementCartesianXdX(x,y); analyticalDisplacementCartesianXdY(x,y);  analyticalDisplacementCartesianYdX(x,y); analyticalDisplacementCartesianYdY(x,y)  ];
    analyticalDisplacementGradientCoords =  @(coord) analyticalDisplacementGradient(coord(1), coord(2));
    
    analyticalStrainVoight =  @(x,y) [ analyticalDisplacementCartesianXdX(x,y); analyticalDisplacementCartesianYdY(x,y); analyticalDisplacementCartesianXdY(x,y)+analyticalDisplacementCartesianYdX(x,y) ];
    analyticalStrainVoightCoords =  @(coords)analyticalStrainVoight(coords(1), coords(2));
    
    %analyticalStressCartesian = @(x,y) analyticalStressPolar(  sqrt(x^2+y^2), atan2(y,x) );
    analyticalStressCartesian = @(x,y) polarToCartesian2DMatrix(  x,y, analyticalStressPolar);
    analyticalStressCartesianCoord = @(coord) analyticalStressCartesian(coord(1), coord(2));
    
    
    
    
    %% Instanciation of the problem
    Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);
    %Material = HookePlaneStrain (E,PoissonsRatio,Density,1.0);
    
    % Creation of the FEM system
    DofDimension = 2;
    
    geometryFactory = ExtractedTHNURBSSurfaceFactory( multiLevelGeometry, MultiLevelBezierExtractionOperator );
    MyElementFactory = ElementFactoryElasticExtractedTHNURBSSurface(Material,NumberOfGaussPoints, multiLevelGeometry, MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator );
    MyMeshFactory = MeshFactoryAdaptiveIGA2D(multiLevelGeometry, DofDimension, MyElementFactory, geometryFactory);
    
    MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);
    
    %     % Loads
    LoadCase = LoadCase();
    %
    outerControlPointsFilter=@(ControlPoints)( ControlPoints(:,1).^2 + ControlPoints(:,2).^2 >= OuterRadius^2 -1e-9 );
    Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)(analyticalStressCartesian(x,y) * [x; y]/OuterRadius ),outerControlPointsFilter);
    LoadCase.addNeumannBoundaryCondition(Load);
    %
    MyAnalysis.addLoadCases(LoadCase);
    %
    % Dirichlet BC
    leftEdgesFilter = @(ControlPoints)( isClose(ControlPoints(:,1), 0) );
    SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,[1 0],StrongDirectConstrainingAlgorithm, leftEdgesFilter);
    MyAnalysis.addDirichletBoundaryCondition(SupportEdge);
    
    leftEdgesFilter2 = @(ControlPoints)( isClose(ControlPoints(:,2), 0) );
    SupportEdge2 = StrongControlPointsDirichletBoundaryCondition(0,[0 1],StrongDirectConstrainingAlgorithm, leftEdgesFilter2);
    MyAnalysis.addDirichletBoundaryCondition(SupportEdge2);
    
    
    [solutionVector, TotalStrainEnergy]  = MyAnalysis.solve();
    
    clc;
    
    
    
    
    elements = MyAnalysis.getMesh.getElements;
    residuals = zeros(size(elements));
    integrator = Integrator(NoPartition(),NumberOfGaussPoints);
    parfor iElement = 1:length(elements)
        element = elements(iElement);
        
        residualFunction = @(localCoords) 0.5 * ( analyticalStrainVoightCoords(element.mapLocalToGlobal(localCoords)) - element.getB(localCoords) * element.getDofVector(1) )' * element.getMaterialAtPoint(localCoords).getMaterialMatrix(localCoords)* ( analyticalStrainVoightCoords(element.mapLocalToGlobal(localCoords)) - element.getB(localCoords) * element.getDofVector(1) );
        
        %         displacementResidual = @(localCoords) analyticalDisplacementCartesianXYCoord(element.mapLocalToGlobal(localCoords)) - element.getShapeFunctionsMatrix(localCoords) * element.getDofVector(1);
        %         gradientResidual = @(localCoords) analyticalDisplacementGradientCoords(element.mapLocalToGlobal(localCoords)) - element.getGradientOperator(localCoords) * element.getDofVector(1);
        %         residualFunction = @(localCoords) displacementResidual(localCoords)' * displacementResidual(localCoords) + gradientResidual(localCoords)' * gradientResidual(localCoords) ;
        %residualFunction = @(localCoords) ( analyticalDisplacementGradientCoords(element.mapLocalToGlobal(localCoords)) - element.getGradientOperator(localCoords) * element.getDofVector(1) )' *  ( analyticalDisplacementGradientCoords(element.mapLocalToGlobal(localCoords)) - element.getGradientOperator(localCoords) * element.getDofVector(1) );
        
        residual = integrator.integrate(residualFunction, element.getGeometricSupport);
        residuals(iElement) = residual;
    end
    
    
    
    
    analyticalH1Norm = 0.00001052673696863816;  % for UniaxialTensileStress = 10
    analyticalEnergy = 0.025501574400630703375;% for UniaxialTensileStress = 10
    
    %     analyticalH1Norm = 1.052673696863816e-7;   % for UniaxialTensileStress = 1
    %     analyticalEnergy = 0.00025501574400630703375;% for UniaxialTensileStress = 1
    %
    %     if sqrt(abs(analyticalEnergy-TotalStrainEnergy-sum(residuals)))>1e-7
    %         sum(residuals)
    %         TotalStrainEnergy
    %         abs(1-sum(residuals)/TotalStrainEnergy)
    %     end
    
    iRefinement
    ndofs(iRefinement) = length(solutionVector)
    energy(iRefinement) = TotalStrainEnergy
    errorH1Norm(iRefinement) = sqrt(sum(residuals)/analyticalH1Norm)
    %errorEnergyNorm =  sqrt( 1-energy/analyticalEnergy )
    errorEnergyNorm(iRefinement) =  sqrt( sum(residuals)/analyticalEnergy )
    
    if length(ndofs)>3
        logndofs=log(ndofs);
        
        logerrH1=log(errorH1Norm);
        polyfH1 = polyfit(logndofs(end-2:end),logerrH1(end-2:end),1)
        %         figure;
        %         loglog(ndofs, errorH1Norm, 'rs-', ndofs,exp(polyfH1(2))*ndofs.^polyfH1(1),'k--');
        %         title(['H1 Error']);
        
        logerrEnergy=log(errorEnergyNorm);
        polyfEnergy = polyfit(logndofs(end-2:end),logerrEnergy(end-2:end),1)
        %         figure;
        %         loglog(ndofs, errorEnergyNorm, 'rs-', ndofs,exp(polyfEnergy(2))*ndofs.^polyfEnergy(1),'k--');
        %         title(['Energy Error']);
    end
    
    
         maxResidual = max(residuals);
         threshold =0.4 * maxResidual;
         elementToRefineFlag = residuals >= threshold;
    
%     [sr I] = sort(residuals);
%     %numElemesToRefine = fix(length(I) * 0.15); %* 0.2);
%     numElemesToRefine = fix(length(I) * 0.6);
%     ix = I(end-numElemesToRefine:end);
%     elementToRefineFlag = logical(zeros(size(residuals)));
%     elementToRefineFlag(ix)=1;
    
    
    %     thresehold_value=0.4;
    %         dummy=sort(residuals);
    %     threshold=dummy(ceil(thresehold_value*length(dummy)));
    %     elementToRefineFlag2=residuals>=threshold;
    %
    %     if elementToRefineFlag ~= elementToRefineFlag2
    %         elementToRefineFlag
    %     end
    
    maxElementToRefineLevel=1;
    for element = elements(elementToRefineFlag)
        maxElementToRefineLevel= max(element.Level ,maxElementToRefineLevel   );
    end
    
    newMaxLevel = max( maxElementToRefineLevel+1, maxCurrentLevel );
    fact=2^(newMaxLevel-1);
    
    %Levels = 1* ones(2*NumberOfXDivisions*fact,NumberOfYDivisions*fact);
    Levels = ones(2*NumberOfXDivisions*fact,NumberOfYDivisions*fact, 'int8');
    
    for iElement = 1:length(elements)
        element = elements(iElement);
        iElementX = element.Index(1);
        iElementY = element.Index(2);
        
        factLevel=2^(newMaxLevel-element.Level);
        
        levelsIxX = (iElementX-1)*factLevel+1 : (iElementX)*factLevel;
        levelsIxY = (iElementY-1)*factLevel+1 : (iElementY)*factLevel;
        
        if elementToRefineFlag(iElement)
            Levels( levelsIxX, levelsIxY ) =  element.Level +1;
        else
            Levels( levelsIxX, levelsIxY ) =  element.Level;
        end
        
        
    end
    
    maxCurrentLevel = newMaxLevel;
%end

analyticalH1Norm = 0.00001052673696863816;  % for UniaxialTensileStress = 10
analyticalEnergy = 0.025501574400630703375;% for UniaxialTensileStress = 10

% analyticalH1Norm = 1.052673696863816e-7;   % for UniaxialTensileStress = 1
% analyticalEnergy = 0.00025501574400630703375;% for UniaxialTensileStress = 1

iRefinement
ndofs(iRefinement) = length(solutionVector)
energy(iRefinement) = TotalStrainEnergy
errorH1Norm(iRefinement) = sqrt(sum(residuals)/analyticalH1Norm)
errorEnergyNorm =  sqrt( 1-energy/analyticalEnergy )

if length(ndofs)>3
    logndofs=log(ndofs);
    
    logerrH1=log(errorH1Norm);
    polyfH1 = polyfit(logndofs(end-2:end),logerrH1(end-2:end),1)
    figure;
    loglog(ndofs, errorH1Norm, 'bo-', ndofs,exp(polyfH1(2))*ndofs.^polyfH1(1),'k--');
    title(['H1 Error']);
    
    logerrEnergy=log(errorEnergyNorm);
    polyfEnergy = polyfit(logndofs(end-2:end),logerrEnergy(end-2:end),1)
    figure;
    loglog(ndofs, errorEnergyNorm, 'bo-', ndofs,exp(polyfEnergy(2))*ndofs.^polyfEnergy(1),'k--');
    title(['Energy Error']);
end


    % if length(ndofs)>3
    %     analyticalEnergy = 0.025501574400630703375;
    %     error =  sqrt( 1-energy/analyticalEnergy )
    %     %error = sqrt( analyticalEnergy-energy );
    %     logndofs=log(ndofs);
    %     logerr=log(error);
    %     polyf = polyfit(logndofs(end-3:end),logerr(end-3:end),1)
    %     figure;
    %     loglog(ndofs, error, ndofs,exp(polyf(2))*ndofs.^polyf(1));
    % end
    
    %% Post Processing
    indexOfPhysicalDomain = 1;
    
    FeMesh = MyAnalysis.getMesh();
    
    % coord = [-1,-1];
    % %element = FeMesh.getElement(1);
    % element = FeMesh.findElementByPoint([0 1 0])
    % B = element.getB(coord);
    % C = element.getMaterialAtPoint(coord).getMaterialMatrix(coord);
    % dofs = element.getDofVector(1);
    % strains = B*dofs;
    % StressVector = C*strains;
    % sigmaXX = StressVector(1)
    % ast = analyticalStressCartesian(0,InnerRadius);
    % ast(1,1)
    
    %element = FeMesh.findElementByPoint([1 1 0]);
    %element.evalShapeFunct([0,0]);
    
    %
    % % create the point processors
    loadCaseToVisualize = [ 1 ];
    
    % Surface Plots
    gridSizes = [ 0.2 0.2] ;
    warpScalingFactor = 1;
    
    postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor, {},{});
    postProcessor = postProcessingFactory.creatVisualPostProcessor( );
    
    
    %postProcessor.registerPointProcessor( @H1NormError, { loadCaseToVisualize, analyticalDisplacementCartesianXYCoord,  analyticalDisplacementGradientCoords, indexOfPhysicalDomain, 1/analyticalH1Norm } );
    %postProcessor.registerPointProcessor( @EnergyNormError, { loadCaseToVisualize, analyticalStrainVoightCoords, indexOfPhysicalDomain, 1/analyticalEnergy } );
    postProcessor.registerPointProcessor( @StressComponent, { loadCaseToVisualize, indexOfPhysicalDomain, 1 } );
    
    postProcessor.visualizeResults( FeMesh );
    set(gca,'LooseInset',get(gca,'TightInset')); 
ndofs
end

