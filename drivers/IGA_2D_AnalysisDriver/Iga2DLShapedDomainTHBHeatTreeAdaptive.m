%% 2D IGA - Driver file

clear all;
ndofs=0
energy=0

%% refinement
NumberOfXDivisions=2;
NumberOfYDivisions=2;
PolynomialDegree = 2;

maxCurrentLevel = 1;
Levels = ones(2*NumberOfXDivisions, NumberOfYDivisions);

for iRefinement = 1:10
    clearvars -except iRefinement ndofs energy NumberOfXDivisions NumberOfYDivisions PolynomialDegree elementLevel maxCurrentLevel Levels errorEnergyNorm

    PolynomialDegreeXi = 1;
    KnotsXi =    buildOpenEquidistantKnotVector(1, 1, PolynomialDegreeXi, 1);
    
    PolynomialDegreeEta = 1;
    KnotsEta =    buildOpenEquidistantKnotVector(1, 0, PolynomialDegreeEta, 1);
    
    ControlPoints = [ ...
        [0  -1 0 1 ]; ...
        [0    0 0 1]; ...
        [1    0 0 1 ]; ...
        [-1 -1 0 1]; ...
        [-1 1 0 1]; ...
        [1 1 0 1]; ...
        ];
    
    geometryDescription = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints );
    
    IncreasePolynomialDegreeXi = PolynomialDegree-PolynomialDegreeXi;
    IncreasePolynomialDegreeEta  = PolynomialDegree-PolynomialDegreeEta;
    
    knotsToInsertXi = linspace(-1, 0, NumberOfXDivisions+1);
    knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);
    
    knotsToInsertXi2 = linspace(0, 1, NumberOfXDivisions+1);
    knotsToInsertXi = [ knotsToInsertXi knotsToInsertXi2(2:length(knotsToInsertXi2)-1) ];
    
    knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
    knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);
    
    geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
    geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
    
    geometryDescription.removeWeights;
    
    
    analyticalSolutionPolar = @(r,phi) r^(2/3)* sin( 2/3*phi );
    
    analyticalSolution = @(x,y) analyticalSolutionPolar( sqrt(x^2+y^2), mod(2*pi+atan2(y,x),2*pi)  );
    analyticalSolutionGradient = @(x,y) 2/3*sqrt(x^2+y^2)^(-4/3)* [x*sin(2/3*mod(2*pi+atan2(y,x),2*pi)) - y*cos(2/3*mod(2*pi+atan2(y,x),2*pi)); y*sin(2/3*mod(2*pi+atan2(y,x),2*pi)) + x*cos(2/3*mod(2*pi+atan2(y,x),2*pi))];
    analyticalSolutionCoords = @(coords) analyticalSolution( coords(1), coords(2)  );
    analyticalSolutionGradientCoords = @(coords) analyticalSolutionGradient( coords(1), coords(2)  );
    
    
    numberOfLevels = maxCurrentLevel;
    
    multiLevelGeometries{1} = geometryDescription;
    for iLevel = 2:numberOfLevels
        multiLevelGeometries{iLevel} =  multiLevelGeometries{iLevel-1}.refineBisect();
    end
    
    multiLevelGeometry = StackOfBSplineSurfaces(multiLevelGeometries, 1, Levels);
    ControlPoints = multiLevelGeometry.getControlPoints;
    
    [MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTreeLocal( multiLevelGeometry, true(1));
    %[MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator] = multiLevelBezierExtractionOperator2DTree( multiLevelGeometry );
    
    NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points
    
    %% Instanciation of the problem
    Material = Heat2D(2,1);
    
    % Creation of the FEM system
    DofDimension = 1;
    
    geometryFactory = ExtractedTHBSurfaceFactory( multiLevelGeometry, MultiLevelBezierExtractionOperator, BezierExtractionOperator );
    MyElementFactory = ElementFactoryHeatConductionExtractedTHBSurface(Material,NumberOfGaussPoints, multiLevelGeometry, MultiLevelBezierExtractionOperator, MultiLevelOperator, BezierExtractionOperator, AmMultiLevelBezierExtractionOperator );
    MyMeshFactory = MeshFactoryAdaptiveIGA2D(multiLevelGeometry, DofDimension, MyElementFactory, geometryFactory);
    
    MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);
    
    % Loads
    LoadCase = LoadCase();
    
    leftControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,1), -1) );
    Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)analyticalSolutionGradient(x,y)' * [-1;0],leftControlPointsFilter);
    LoadCase.addNeumannBoundaryCondition(Load);
    
    bottomControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), -1) );
    Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)analyticalSolutionGradient(x,y)' * [0;-1],bottomControlPointsFilter);
    LoadCase.addNeumannBoundaryCondition(Load);
    
    rightControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,1), 1) );
    Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)analyticalSolutionGradient(x,y)' * [1;0],rightControlPointsFilter);
    LoadCase.addNeumannBoundaryCondition(Load);
    
    topControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), 1) );
    Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)analyticalSolutionGradient(x,y)' * [0;1],topControlPointsFilter);
    LoadCase.addNeumannBoundaryCondition(Load);
    
    MyAnalysis.addLoadCases(LoadCase);
    
    % Dirichlet BC
    
    internalEdgesFilter = @(ControlPoints)( ( isClose(ControlPoints(:,1), 0) | isClose(ControlPoints(:,2), 0) ) & (ControlPoints(:,1)>=0 & ControlPoints(:,2)<=0) );
    SupportEdge = StrongControlPointsDirichletBoundaryCondition(0,1,StrongDirectConstrainingAlgorithm, internalEdgesFilter);
    MyAnalysis.addDirichletBoundaryCondition(SupportEdge);
    
    [solutionVector, TotalStrainEnergy]  = MyAnalysis.solve();
    
    ndofs(iRefinement) = length(solutionVector);
    energy(iRefinement) = TotalStrainEnergy;
    
    
    elements = MyAnalysis.getMesh.getElements;
    residuals = zeros(size(elements));
    integrator = Integrator(NoPartition(),NumberOfGaussPoints+2);
    parfor iElement = 1:length(elements)
        element = elements(iElement);
        residualFunction = @(localCoords) ( analyticalSolutionGradientCoords(element.mapLocalToGlobal(localCoords)) - element.getB(localCoords) * element.getDofVector(1) )' * ( analyticalSolutionGradientCoords(element.mapLocalToGlobal(localCoords)) - element.getB(localCoords) * element.getDofVector(1) );
        residual = integrator.integrate(residualFunction, element.getFaces.getgeometricSupport);
        
        residuals(iElement) = residual;
    end
    
    
     %analyticalH1Norm = 0.00001052673696863816;  % for UniaxialTensileStress = 10
    analyticalEnergy = 0.918113330937581317214154740159;
    
     clc;
    iRefinement
    ndofs(iRefinement) = length(solutionVector)
    energy(iRefinement) = TotalStrainEnergy
    %errorH1Norm(iRefinement) = sqrt(sum(residuals)/analyticalH1Norm)
    %errorEnergyNorm =  sqrt( 1-energy/analyticalEnergy )
    errorEnergyNorm(iRefinement) =  sqrt( sum(residuals)/analyticalEnergy )
 
    if length(ndofs)>3
        logndofs=log(ndofs);
           
        %logerrH1=log(errorH1Norm);
        %polyfH1 = polyfit(logndofs(end-3:end),logerrH1(end-3:end),1)
        %figure;
        %loglog(ndofs, errorH1Norm, 'rs-', ndofs,exp(polyfH1(2))*ndofs.^polyfH1(1),'k--');
        %title(['H1 Error']);
        
        logerrEnergy=log(errorEnergyNorm);
        polyfEnergy = polyfit(logndofs(end-1:end),logerrEnergy(end-1:end),1)
        %figure;
        %loglog(ndofs, errorEnergyNorm, 'rs-', ndofs,exp(polyfEnergy(2))*ndofs.^polyfEnergy(1),'k--');
        %title(['Energy Error']);
    end
    
    
    
     maxResidual = max(residuals);
     threshold =0.25 * maxResidual;
     elementToRefineFlag = residuals >= threshold;

%     [sr I] = sort(residuals);
%     numElemesToRefine = fix(length(I) * 0.2); %0.15);
%      ix = I(end-numElemesToRefine:end);
%      elementToRefineFlag = logical(zeros(size(residuals)));
%      elementToRefineFlag(ix)=1;
    
    maxElementToRefineLevel=1;
    for element = elements(elementToRefineFlag)
        maxElementToRefineLevel= max(element.Level ,maxElementToRefineLevel   );
    end
    
    newMaxLevel = max( maxElementToRefineLevel+1, maxCurrentLevel );
    fact=2^(newMaxLevel-1);
    
    %Levels = 1* ones(2*NumberOfXDivisions*fact,NumberOfYDivisions*fact);
    Levels = ones(2*NumberOfXDivisions*fact,NumberOfYDivisions*fact, 'int8');
    
    for iElement = 1:length(elements)
        element = elements(iElement);
        iElementX = element.Index(1);
        iElementY = element.Index(2);
        
        factLevel=2^(newMaxLevel-element.Level);
        
        levelsIxX = (iElementX-1)*factLevel+1 : (iElementX)*factLevel;
        levelsIxY = (iElementY-1)*factLevel+1 : (iElementY)*factLevel;
        
        if elementToRefineFlag(iElement)
            Levels( levelsIxX, levelsIxY ) =  element.Level +1;
        else
            Levels( levelsIxX, levelsIxY ) =  element.Level;
        end
        
        
    end
    
    maxCurrentLevel = newMaxLevel;
    
%end


    if length(ndofs)>3
        logndofs=log(ndofs);
           
        %logerrH1=log(errorH1Norm);
        %polyfH1 = polyfit(logndofs(end-3:end),logerrH1(end-3:end),1)
        %figure;
        %loglog(ndofs, errorH1Norm, 'rs-', ndofs,exp(polyfH1(2))*ndofs.^polyfH1(1),'k--');
        %title(['H1 Error']);
        
        logerrEnergy=log(errorEnergyNorm);
        polyfEnergy = polyfit(logndofs(end-1:end),logerrEnergy(end-1:end),1)
        figure;
        loglog(ndofs, errorEnergyNorm, 'bo-', ndofs,exp(polyfEnergy(2))*ndofs.^polyfEnergy(1),'k--');
        title(['Energy Error']);
    end
    
%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 0.5 0.25] ;
warpScalingFactor = 0;

postProcessingFactory = VisualPostProcessingFactoryIGA2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor, {}, {});
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @SolutionVector, { loadCaseToVisualize } );
 %postProcessor.registerPointProcessor( @SolutionGradientNorm, { loadCaseToVisualize, indexOfPhysicalDomain } );
% postProcessor.registerPointProcessor( @SolutionError, { loadCaseToVisualize, analyticalSolutionCoords, indexOfPhysicalDomain} );
% postProcessor.registerPointProcessor( @H1SeminormError, { loadCaseToVisualize, analyticalSolutionGradientCoords, indexOfPhysicalDomain} );

postProcessor.visualizeResults( FeMesh );
%set(gca,'position',[0 0 1 1],'units','normalized')
set(gca,'LooseInset',get(gca,'TightInset')); 
ndofs
end