%% 2D IGA - Driver file 

%% Preliminarities
clear all;
clc;
% Set path for Logger
%Logger.setPathForLogger('Iga2DPlateWithAHole.m','Iga2DPlateWithAHole.out',1);

%% Input parameters
% Numerical parameters


 PolynomialDegreeXi = 2;
 KnotsXi =    buildOpenEquidistantKnotVector(1, 1, PolynomialDegreeXi, 1);

 PolynomialDegreeEta = 2;
 KnotsEta =    buildOpenEquidistantKnotVector(1, 0, PolynomialDegreeEta, 1);

ControlPoints = [ ...
  [-1 0 0 1]; ...  
  [-1 sqrt(2)-1 0 1]; ... 
   [1-sqrt(2) 1 0 1]; ...  
   [0 1 0 1]; ...  
   
  [-2.5 0 0 1]; ...
  [-2.5 0.75 0 1]; ...
   [-0.75 2.5 0 1]; ...
   [0 2.5 0 1]; ...
   
  [-4 0 0 1]; ...
  [-4 4 0 1]; ...
  [-4 4 0 1]; ...
  [0 4 0 1]; ...
];

geometryDescription = BSplineSurface( KnotsXi, PolynomialDegreeXi, KnotsEta, PolynomialDegreeEta, ControlPoints );

%% refinement
%  NumberOfXDivisions=1;
%  NumberOfYDivisions=1;
  PolynomialDegree = 3;
% 
% 
% IncreasePolynomialDegreeXi = PolynomialDegree-PolynomialDegreeXi;
% IncreasePolynomialDegreeEta  = PolynomialDegree-PolynomialDegreeEta;
% 
% knotsToInsertXi = linspace(-1, 1, NumberOfXDivisions+1);
% knotsToInsertXi = knotsToInsertXi(2:length(knotsToInsertXi)-1);
% 
% knotsToInsertEta = linspace(-1, 1, NumberOfYDivisions+1);
% knotsToInsertEta = knotsToInsertEta(2:length(knotsToInsertEta)-1);
% 
% geometryDescription.elevateOrder( IncreasePolynomialDegreeXi,  IncreasePolynomialDegreeEta );
% geometryDescription.insertKnots( knotsToInsertXi,  knotsToInsertEta );
 

% ControlPoints = reshape(rCP, size(rCP,1) * size(rCP,2) ,3);

 geometryFactory = ExtractedNURBSSurfaceFactory( geometryDescription );
NumberOfGaussPoints = PolynomialDegree+1; % number of Gauss points

% Mechanical parameters
E = 1e5;
PoissonsRatio = 0.3;
Density = 1.0;

Force = 10.;

%% Instanciation of the problem
Material = HookePlaneStress(E,PoissonsRatio,Density,1.0);

% Creation of the FEM system
 DofDimension = 2;

 MyElementFactory = ElementFactoryElasticExtractedBSplineSurface(Material,NumberOfGaussPoints, geometryDescription.getKnotsXi, geometryDescription.getPolynomialDegreeXi, geometryDescription.getKnotsEta, geometryDescription.getPolynomialDegreeEta); 
    
MyMeshFactory = MeshFactoryIGA2D(geometryDescription, DofDimension, MyElementFactory, geometryFactory);
MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);

% Loads
LoadCase = LoadCase();

leftControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,1), min(ControlPoints(:,1) )) );
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)[-Force;0],leftControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

topControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), max(ControlPoints(:,2)) ));
Load = WeakEdgesControlPointsNeumannBoundaryCondition(GaussLegendre(NumberOfGaussPoints),@(x,y,z)[0;-Force],topControlPointsFilter);
LoadCase.addNeumannBoundaryCondition(Load);

MyAnalysis.addLoadCases(LoadCase);

% Dirichlet BC

bottomControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,2), min(ControlPoints(:,2)) ));
SupportEdge = StrongControlPointsDirichletBoundaryCondition(@(x,y,z)(0.0), [0 1], StrongDirectConstrainingAlgorithm, bottomControlPointsFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

rightControlPointsFilter=@(ControlPoints)( isClose(ControlPoints(:,1), max(ControlPoints(:,1)) ));
SupportEdge = StrongControlPointsDirichletBoundaryCondition(@(x,y,z)(0.0), [1 0], StrongDirectConstrainingAlgorithm, bottomControlPointsFilter);
MyAnalysis.addDirichletBoundaryCondition(SupportEdge);

MyAnalysis.solve;

%% Post Processing
indexOfPhysicalDomain = 1;

FeMesh = MyAnalysis.getMesh();

% create the point processors 
loadCaseToVisualize = [ 1 ];

% Surface Plots
gridSizes = [ 1 0.25] ;
warpScalingFactor = 1;

postProcessingFactory = VisualPostProcessingFactory2DWarped( FeMesh, gridSizes, loadCaseToVisualize, warpScalingFactor);
postProcessor = postProcessingFactory.creatVisualPostProcessor( );

postProcessor.registerPointProcessor( @DisplacementNorm, { loadCaseToVisualize } );
postProcessor.registerPointProcessor( @VonMisesStress, { loadCaseToVisualize, indexOfPhysicalDomain } );

postProcessor.visualizeResults( FeMesh );

% w = F*l^3/(3*E*I)
% I = b*h^3/12

%I = 1*Ly^3/12;

%w = Force*Lx^3/(3*E*I);
