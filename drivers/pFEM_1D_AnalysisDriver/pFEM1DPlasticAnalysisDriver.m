%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%

%% 1D plasticity - Driver file 

%% Preliminarities
clear all; clc;
% Set path for Logger
Logger.setPathForLogger('pFEM1DPlasticAnalysisDriver.m','log1Dplasticity.out',1);

%% Input parameters
% Numerical parameters
MeshOrigin = 0;
NumberOfXDivisions = 1;
PolynomialDegree = 1;
NumberGP = PolynomialDegree + 1; % number of Gauss points
NoIncrements = 4;   % Number of load increments

% Mechanical parameters
E = 1;              % Young's modulus
E_t = .2;           % Elastoplastic Tangent Modulus
YieldStress = .4;   % Yield Stress
A = 1;              % cross-sectional area
L = 1;              % length
Density = 1;

% Loads
% Fyield = YieldStress * A = 0.4
Forces = [0,.4,.6,-1,0,1,2];

%% Instanciation of the problem
Mat1D = Plastic1DIsotropicHardening(A,E,E_t,YieldStress,Density,1);

MyElementFactory = ElementFactoryPlasticBar(Mat1D,NumberGP);

MyMeshFactory = MeshFactory1DUniform(NumberOfXDivisions,...
                PolynomialDegree,PolynomialDegreeSorting(),1,...
                MeshOrigin,L,MyElementFactory);
                      
MyAnalysis = NonLinearAnalysis(MyMeshFactory);

% Adding  point loads
for i = 1: length(Forces)
    PointLoads(i) = NodalNeumannBoundaryCondition([L 0 0],Forces(i));
    LoadCases(i) = LoadCase();
    LoadCases(i).addNeumannBoundaryCondition(PointLoads(i)); 
end
MyAnalysis.addLoadCases(LoadCases);

% Adding Dirichlet boundary conditions
MyDirichletAlgorithm = StrongDirectConstrainingAlgorithm;
PointSupport = StrongNodeDirichletBoundaryCondition([0 0 0],0,1,MyDirichletAlgorithm);

MyAnalysis.addDirichletBoundaryCondition(PointSupport);

% Solution of the linear equation system
MyAnalysis.solve(NoIncrements);

%% Postprocessing
GPPostProcessor(MyAnalysis.getMesh(),1,2);