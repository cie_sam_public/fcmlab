%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Computation in Engineering, Technische Universitaet Muenchen          %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article                   %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, A. Duester, S. Kollmannsberger, J. Parvizian,        %
% M. Ruess, D. Schillinger, E. Rank:                                    %
% "FCMLab: A Finite Cell Research Toolbox for MATLAB."                  %
% Submitted to Advances in Engineering Software, 2013					%
%                                                                       %
%=======================================================================%
 
%% Driver file to observe the convergence between p-Fem, h-Fem and hp-Fem
clear all;

% Set path for Logger
Logger.setPathForLogger('hpFEM1DSingularConvergence.m','hp1DSingularConvergence.out',1);
    
% Numerical parameter
MeshOrigin = 0;

% Geometry
Area = 1;     % cross sectional area
Length = 1;

% Material
E = 1;        % young modulus
Density = 1;
Mat1D = Hooke1D(Area, E, Density, 1);
Mat1D = [Mat1D Mat1D]; % to make it work with the pseudo domain

% Loads
% For the problem 2.2.2 given in the High Order FEM script:
lambda = 0.65;
DistributedLoad = @(x,y,z) lambda * ( lambda -1 ) * x .^ (lambda - 2);

% Exact energy
ExactEnergy = 0.5 * lambda^2 * ( Length - (2 * Length ^ lambda)/lambda  ...
               + (Length ^ (2 * lambda - 1))/ (2 * lambda - 1) );

loadCaseToVisualize = 1;
indexOfPhysicalDomain = 2;

%% h-FEM
PolynomialDegree = 1;
RefinementLevels = 5;
InitialNumberOfElements = 1;
NumberGP = PolynomialDegree + 1; % number of Gauss points

TotalStrainEnergy = zeros(RefinementLevels,1);
NumberOfDofsHMethod = zeros(RefinementLevels,1);

for i = 1:RefinementLevels
    clear MyElementFactory MyMeshFactory MyAnalysis 
    
    NumberOfElements = InitialNumberOfElements * i^2;  

    % Analysis
    MyElementFactory = ElementFactoryElasticBar(Mat1D,NumberGP);
    MyMeshFactory = MeshFactory1DUniform(NumberOfElements,...
        PolynomialDegree,TopologicalSorting(),1,...
        MeshOrigin,Length,MyElementFactory);
    
    MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);
    
    % Loads
    MyLoadCase = LoadCase();
    MyLoadCase.addBodyLoad(DistributedLoad);
    MyAnalysis.addLoadCases(MyLoadCase);
    
    
    % Boundary Conditions  
    MyDirichletAlgorithm = StrongPenaltyAlgorithm(10E4);
    PointSupport = StrongNodeDirichletBoundaryCondition([0 0 0],0,1,MyDirichletAlgorithm);
    MyAnalysis.addDirichletBoundaryCondition(PointSupport);
    
    % Solution
    [solutionVector, TotalStrainEnergy(i)] = MyAnalysis.solve();
    NumberOfDofsHMethod(i) = length (solutionVector);
end

ErrorOfHmethod = abs(TotalStrainEnergy - ExactEnergy) / ExactEnergy;
clear TotalStrainEnergy

%% p-FEM
NumberOfElements = 1;
pMin = 1;
pMax = 11;

TotalStrainEnergy = zeros(pMax - pMin +1, 1);
NumberOfDofsPMethod = zeros(pMax - pMin +1, 1);

for p = pMin:pMax % Increasing p refinement
    %clear MyAnalysisFactory MyAnalysis MyPostProcessorStrainEnergy
    clear MyElementFactory MyMeshFactory MyAnalysis 
    
    NumberGP = p + 1;  % number of Gauss points
    
    % Analysis
    MyElementFactory = ElementFactoryElasticBar(Mat1D,NumberGP);
    MyMeshFactory = MeshFactory1DUniform(NumberOfElements,...
        p,TopologicalSorting(), 1, MeshOrigin,Length,MyElementFactory);
    
    MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);
    
    % Loads
    MyLoadCase = LoadCase();
    MyLoadCase.addBodyLoad(DistributedLoad);
    MyAnalysis.addLoadCases(MyLoadCase);
    
    % Boundary Conditions  
    MyDirichletAlgorithm = StrongPenaltyAlgorithm(10E4);
    PointSupport = StrongNodeDirichletBoundaryCondition([0 0 0],0,1,MyDirichletAlgorithm);
    MyAnalysis.addDirichletBoundaryCondition(PointSupport);
    
    % Solution
    [solutionVector, TotalStrainEnergy(p)] = MyAnalysis.solve();
    NumberOfDofsPMethod(p) = length(solutionVector);
end

ErrorOfPmethod = abs(TotalStrainEnergy - ExactEnergy) / ExactEnergy;
clear TotalStrainEnergy

%% hp graded refinement
NumberOfElementsSet = 1:10;
gradingFactor = 0.15;

TotalStrainEnergy = zeros(length(NumberOfElementsSet), 1);
NumberOfDofsHPMethod = zeros(length(NumberOfElementsSet), 1);

for  i = 1:length(NumberOfElementsSet)
    clear MyElementFactory MyMeshFactory MyAnalysis 
    
    NumberOfElements = NumberOfElementsSet(i);
    
    % Equation 2.26, High Order FEM script:
    maxPolynomialDegree = max(1, round( (2 * lambda - 1) * (NumberOfElements - 1) ) );
    
    NumberGP = maxPolynomialDegree + 1;
    
    % Analysis
    MyElementFactory = ElementFactoryElasticBar(Mat1D,NumberGP);
    MyMeshFactory = MeshFactory1DGraded(i, gradingFactor,...
                maxPolynomialDegree,TopologicalSorting(),1,MeshOrigin,...
                Length,MyElementFactory);
    
    MyAnalysis = QuasiStaticAnalysis(MyMeshFactory);
    
    % Loads
    MyLoadCase = LoadCase();
    MyLoadCase.addBodyLoad(DistributedLoad);
    MyAnalysis.addLoadCases(MyLoadCase);
    
    
    % Boundary Conditions  
    MyDirichletAlgorithm = StrongPenaltyAlgorithm(10E4);
    PointSupport = StrongNodeDirichletBoundaryCondition([0 0 0],0,1,MyDirichletAlgorithm);
    MyAnalysis.addDirichletBoundaryCondition(PointSupport);
    
    % Solution
    [solutionVector, TotalStrainEnergy(i)] = MyAnalysis.solve();
    NumberOfDofsHPMethod(i) = length (solutionVector);    
end

ErrorOfHPmethod = abs(TotalStrainEnergy - ExactEnergy) / ExactEnergy;
clear TotalStrainEnergy

%% Plot of convergence
loglog(NumberOfDofsHMethod, sqrt (ErrorOfHmethod)* 100,...
       NumberOfDofsPMethod, sqrt (ErrorOfPmethod)* 100,...
       NumberOfDofsHPMethod,sqrt(ErrorOfHPmethod)* 100);
xlabel('Degrees Of Freedom');
ylabel('Relative Error In Strain Energy');
legend('h-version','p-version','hp graded');
title('Convergence Rate');
%axis([1 12 5 100]);