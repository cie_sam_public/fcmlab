%=======================================================================%
%                     ______________  _____          __                 %
%                    / ____/ ____/  |/  / /   ____ _/ /_                %
%                   / /_  / /   / /|_/ / /   / __ `/ __ \               %
%                  / __/ / /___/ /  / / /___/ /_/ / /_/ /               %
%                 /_/    \____/_/  /_/_____/\__,_/_.___/                %
%                                                                       %
%                                                                       %
% Copyright (c) 2012, 2013                                              %
% Chair for Computation in Engineering, Technical University Muenchen   %
%                                                                       %
% This file is part of the MATLAB toolbox FCMLab. This library is free  %
% software; you can redistribute it and/or modify it under the terms of %
% the GNU General Public License as published by the Free Software      %
% Foundation; either version 3, or (at your option) any later version.  %
%                                                                       %
% This library is distributed in the hope that it will be useful,       %
% but WITHOUT ANY WARRANTY; without even the implied warranty of        %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          %
% GNU General Public License for more details.                          %
%                                                                       %
% You should have received a copy of the GNU General Public License     %
% along with this program; see the files COPYING respectively.          %
% If not, see <http://www.gnu.org/licenses/>.                           %
%                                                                       %
% In case of a scientific publication of results obtained using FCMLab, %
% we ask the authors to cite the introductory article "xxxxx" published %
% in yyyyy:                                                             %
%                                                                       %
% N. Zander, T. Bog, M. Elhaddad, R. Espinoza, H. Hu, A. F. Joly,       %
% C. Wu, P. Zerbe, S. Kollmannsberger, D. Schillinger, M. Ruess,        %
% A. Duester, E. Rank. FCMLab: A Finite Cell Research Toolbox for	    %
% MATLAB, AGREATJOURNAL, xxxx, 2013                                     %
%                                                                       %
%=======================================================================%

% Uses p-Version of FCM 
% and Lagrange Multiplier Method with Point Collocation (delta-Dirac function
% on integration points)for the boundary discretization

% ATTENTION: the Point Collocation Method DOES NOT yield accurate results in every case !!

%% Preliminarities 
clear all;
close all;
clc;


%% Domain extension parameter

h=0.0;


%% Embedded Domain Setup
Origin = [ 0.0 0.0 0.0 ];
Lengths = [ 1.0 1.0 1.0 ];

% Create the domain
Cube = EmbeddedCuboid( Origin, Lengths );

%% Material Setup
YoungsModulus = 1.0;
PoissonsRatio = 0.3;
Density = 1.0;
Alpha = 1e-10;

% Material for the void (background) domain
Materials(1) = Hooke3D( YoungsModulus, PoissonsRatio, ...
    Density, Alpha );

% Material for the actual domain
Materials(2) = Hooke3D( YoungsModulus, PoissonsRatio, ...
    Density, 1.0 );

%% Mesh Setup

% Step 1: decide on element type
PolynomialDegree = 4;
NumberOfGaussPoints =PolynomialDegree+1;
SpaceTreeDepth =0;

ElementFactory = ElementFactoryElasticHexaFCM( Materials, Cube, ...
    NumberOfGaussPoints, SpaceTreeDepth);

% Step 2: create the mesh
MeshOrigin = [-h -h -h];
MeshLengthX =1.0+2*h;
MeshLengthY =1.0+2*h;
MeshLengthZ =1.0+2*h;

NumberOfXDivisions = 1;
NumberOfYDivisions = 1;
NumberOfZDivisions = 1;

DofDimension = 3;

MeshFactory = MeshFactory3DUniform( NumberOfXDivisions, ...
    NumberOfYDivisions, NumberOfZDivisions,PolynomialDegree, TopologicalSorting, ...
    DofDimension, MeshOrigin, MeshLengthX, MeshLengthY,MeshLengthZ, ElementFactory );
%% Analysis Setup

% Decide on anlysis type
Analysis = QuasiStaticAnalysis( MeshFactory );

%% Apply Body Load

% Analytical description of body force
 bx  = @(x,y,z) ...
             - 1.0 * ( YoungsModulus / ( ( 1.0 + PoissonsRatio ) * ( 1.0 - 2.0 * PoissonsRatio ) ) ) *...
          (...
          ( 2.0 * ( 1.0 - PoissonsRatio ) * y^3 * z^4 + 12.0 * PoissonsRatio * x^2 * y^3 * z^2 +...
          12.0 * PoissonsRatio * x^3 * y^2 * z^2 ) +...
          ( 1.0 - 2.0 * PoissonsRatio ) * ( 3.0 * x^2 * y * z^4 + 6.0 * x^2 * y^3 * z^2) + ...
          ( 1.0 - 2.0 * PoissonsRatio ) * ( 6.0 * x^2 * y^3 * z^2 + 6.0 * x^3 * y^2 * z^2 )...
          );

      
        by = @(x,y,z) ...
            - 1.0 * ( YoungsModulus / ( ( 1.0 + PoissonsRatio ) * ( 1.0 - 2.0 * PoissonsRatio ) ) ) * ...
            (...
            ( 1.0 - 2.0 * PoissonsRatio ) * ( 3.0 * x * y^2 * z^4 + 3.0 * x * y^4 * z^2 )...
            +( 6.0 * PoissonsRatio * x * y^2 * z^4 + 12.0 * ( 1.0 - PoissonsRatio ) *...
            x^3 * y^2 * z^2 + 6.0 * PoissonsRatio * x^4 * y * z^2 ) +...
            ( 1.0 - 2.0 * PoissonsRatio ) * ( x^3 * y^4 + 3.0 * x^4 * y * z^2 )...
            );
       
        
        bz = @(x,y,z) ...
             - 1.0 * ( YoungsModulus / ( ( 1.0 + PoissonsRatio ) * ( 1.0 - 2.0 * PoissonsRatio ) ) ) *...
             (...
             ( 1.0 - 2.0 * PoissonsRatio ) *( 4.0 * x *  y^3 *  z^3 +6.0 * x^2 * y^2 * z^3 )+...
             ( 1.0 - 2.0 * PoissonsRatio ) *( 4.0 * x^3 * y^3 * z +x^4 * z^3)+...
             ( 8.0 * PoissonsRatio * x * y^3 * z^3 +8.0 * PoissonsRatio *...
              x^3 * y^3 * z +6.0 * ( 1.0 - PoissonsRatio ) * x^4 * y^2 * z )...
             );
       
% Transform to vector function
BodyForce = @(x,y,z) [ bx(x,y,z); by(x,y,z); bz(x,y,z)];
% Create new load case 
loadCase = LoadCase();
loadCase.addBodyLoad( BodyForce );
% Register load case at analysis
Analysis.addLoadCases( loadCase );
%% Apply weak Dirichlet boundary conditions

% Select integration scheme
IntegrationScheme = GaussLegendre( NumberOfGaussPoints );
NumberOfBoundarySegments=2;

% Get geometrical description of boundary
origin=[ 0.0 0.0  0.0 ];
vectorXi= [ 1.0 0  0.0 ];
vectorEta= [0.0 1.0 0.0 ];
vectorZeta= [0.0 0.0 1.0 ];

BoundaryFactory = ParallelepipedBoundaryFactory(origin, vectorXi,vectorEta, ...
    vectorZeta,NumberOfBoundarySegments,NumberOfBoundarySegments,NumberOfBoundarySegments );
Boundary = BoundaryFactory.getBoundary();

%% Selet constraining strategy

ConstrainingAlgorithm=LagrangePointCollocationAlgorithm(); % this algorithm does not yield accurate results 


%% Give analytical description of boundary values
ux = @(x,y,z) power( x, 2 ) * power( y, 3 ) * power( z, 4 );
uy = @(x,y,z) power( x, 3 ) * power( y, 4 ) * power( z, 2 );
uz = @(x,y,z) power( x, 4 ) * power( y, 2 ) * power( z, 3 );

% Create boundary condition in X
ConstrainedDirections = [1 0 0];
BoundaryConditionX = WeakDirichletBoundaryCondition( ux, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );
% Create boundary condition in Y
ConstrainedDirections = [0 1 0];
BoundaryConditionY = WeakDirichletBoundaryCondition( uy, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );
% Create boundary condition in Z
ConstrainedDirections = [0 0 1];
BoundaryConditionZ = WeakDirichletBoundaryCondition( uz, ConstrainedDirections, ...
    IntegrationScheme, Boundary, ConstrainingAlgorithm );

% Register conditions at analysis
Analysis.addDirichletBoundaryCondition( BoundaryConditionX );
Analysis.addDirichletBoundaryCondition( BoundaryConditionY );
Analysis.addDirichletBoundaryCondition( BoundaryConditionZ );

%% Run the analysis

Analysis.solve();


%% Post processing

% Create result point processors
IndexOfPhysicalDomain = 2;
LoadCaseToVisualize = 1;
% Displacement
DisplacementNorm = DisplacementNorm( LoadCaseToVisualize );

% Stress
VonMises = VonMisesStress( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Energy
StrainEnergy = StrainEnergy( LoadCaseToVisualize, IndexOfPhysicalDomain );

% Error
AnalyticalSolutionCartesian = @(globalCoords) ...
    [ ux( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uy( globalCoords(1), globalCoords(2), globalCoords(3) ); ...
      uz( globalCoords(1), globalCoords(2), globalCoords(3) )];
  
L2Error = L2Error( LoadCaseToVisualize, AnalyticalSolutionCartesian,Cube, IndexOfPhysicalDomain);
%% Perform Visual Post Processing
PostProcessingGridSize = [0.05 0.05 0.05];
SolutionNumbersToWarp = 1;
WarpScalingFactor = 1;

FeMesh = Analysis.getMesh();

% Decide on post processor type
PostProcessingFactory = VisualPostProcessingFactory3DFCMWarpedMODIFIED( ...
    FeMesh, Cube, IndexOfPhysicalDomain, PostProcessingGridSize,...
    SolutionNumbersToWarp,WarpScalingFactor );

PostProcessor = PostProcessingFactory.creatVisualPostProcessor( );

% Register result point processors
PostProcessor.registerPointProcessor( DisplacementNorm );
PostProcessor.registerPointProcessor( VonMises );
PostProcessor.registerPointProcessor( L2Error );

% Visualize results
PostProcessor.visualizeResults( FeMesh );

%% Perform Integration Post Processing

% Decide on post processor type
PostProcessor = IntegrationPostProcessor();

% Register result point processors
PostProcessor.registerPointProcessor( StrainEnergy );

% Integrate the results over the domain
NumercialStrainEnergy = PostProcessor.integrate( FeMesh );

% Compare to analytical solution
AnalyticalStrainEnergy = ...
  ( 4183.0 / 66150.0 ) * YoungsModulus * ( 1.0 - PoissonsRatio )...
            / ( ( 1.0 + PoissonsRatio ) * ( 1.0 - 2.0 * PoissonsRatio ) )...
        + ( 32197.0 / 264600.0 ) * YoungsModulus / ( 1.0 + PoissonsRatio )...
        + ( 313.0 / 2940.0 ) * YoungsModulus * PoissonsRatio /...
        ( ( 1.0 + PoissonsRatio ) * ( 1.0 - 2.0 * PoissonsRatio ) );

    Error =  abs( 1 - NumercialStrainEnergy / AnalyticalStrainEnergy );

Logger.Log(['Relative Error: ', num2str( Error, '%e\n' ) ],'release');

