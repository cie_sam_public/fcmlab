# FCMLab: A Finite Cell Research Toolbox for MATLAB

Scientific Publication
06/21/2013

Main scientific article explaining the conceptional ideas of the Finite Cell Method and the implementation of the FCMLab Toolbox.

Zander, N.; Bog, T; Elhaddad, M.; Espinoza, R.; Hu, H.; Joly, A.F.; Wu, C.; Zerbe, P.; Düster, A.; Kollmannsberger, S.; Parvizian, J.; Ruess, M.; Schillinger, D.; Rank, E. <br> 
FCMLab: A Finite Cell Research Toolbox for MATLAB <br> 
Advances in Engineering Software 74, pp. 49-63, 2014 <br> 
DOI: 10.1016/j.advengsoft.2014.04.004 <br> 
https://www.sciencedirect.com/science/article/abs/pii/S0965997814000684 <br> 

For further informations visit our <a href="https://gitlab.lrz.de/cie_sam_public/fcmlab/-/wikis/home" class="document">Wiki</a>
